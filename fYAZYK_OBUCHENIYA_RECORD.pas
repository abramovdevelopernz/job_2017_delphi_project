unit fYAZYK_OBUCHENIYA_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfYAZYK_OBUCHENIYA_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_yazyk_obucheniya: TLabel;

    Edit_yazyk_obucheniya: TEdit;

    Check_yazyk_obucheniya: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fYAZYK_OBUCHENIYA_RECORDf: TfYAZYK_OBUCHENIYA_RECORDf;

implementation

uses  
    fYAZYK_OBUCHENIYA,
    fMW;

{$R *.dfm}

procedure TfYAZYK_OBUCHENIYA_RECORDf.CheckInput;
begin

if trim(Edit_yazyk_obucheniya.Text)='' then begin
  Edit_yazyk_obucheniya.SetFocus;
  abort;
end;

end;
procedure TfYAZYK_OBUCHENIYA_RECORDf.CLALL;
begin


  Check_yazyk_obucheniya.Checked:=false;
  Check_yazyk_obucheniya.Visible:=false;

  Edit_yazyk_obucheniya.Clear;

  Edit_yazyk_obucheniya.Setfocus;

end;

procedure TfYAZYK_OBUCHENIYA_RECORDf.FormActivated;
begin

  CLALL;

if pActionYAZYK_OBUCHENIYA='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionYAZYK_OBUCHENIYA='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from yazyk_obucheniya where id='+pID);
  ADQ.Open;
  ADQ.First;

  Edit_yazyk_obucheniya.Text:=ADQ.fieldbyname('yazyk_obucheniya').AsString;

end;

if pActionYAZYK_OBUCHENIYA='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_yazyk_obucheniya.Visible:=true;

end;

end;

procedure TfYAZYK_OBUCHENIYA_RECORDf.BtnOK;
begin

if pActionYAZYK_OBUCHENIYA='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'yazyk_obucheniya '+
  '('+
  'yazyk_obucheniya,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+trim(Edit_yazyk_obucheniya.Text)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionYAZYK_OBUCHENIYA='UPDATE' then begin

 CheckInput;

 pS:='UPDATE '+
  'yazyk_obucheniya set ' +
  'yazyk_obucheniya='''+trim(Edit_yazyk_obucheniya.Text)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fYAZYK_OBUCHENIYAf.ListView1.Selected.SubItems[0]:=trim(Edit_yazyk_obucheniya.Text);

end;

if pActionYAZYK_OBUCHENIYA='SEARCH' then begin

if (Check_yazyk_obucheniya.Checked=false) then abort;

   pSearch:='';

   if Check_yazyk_obucheniya.Checked=true then pSearch:=pSearch+' and yazyk_obucheniya.yazyk_obucheniya like '''+trim(Edit_yazyk_obucheniya.Text)+''' ';

end;

Close;

end;

procedure TfYAZYK_OBUCHENIYA_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfYAZYK_OBUCHENIYA_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfYAZYK_OBUCHENIYA_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfYAZYK_OBUCHENIYA_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.