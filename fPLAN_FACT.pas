unit fPLAN_FACT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls;

type
  TfPLAN_FACTf = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    Label3: TLabel;
    Combo_prepodavatelmz_id: TComboBox;
    Button_prepodavatelmz_id: TButton;
    ListView1: TListView;
    Label1: TLabel;
    Combo_vid_nagruzki_id: TComboBox;
    procedure Button_prepodavatelmz_idClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);


    procedure GetPREPODAVATELMZ(pCombo:TComboBox;pSID:integer);
    procedure LoadPREPODAVATELMZ;
    procedure LOAD_VID_NAGRUZKI;

    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Button2Click(Sender: TObject);




  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fPLAN_FACTf: TfPLAN_FACTf;

implementation

uses fPREPODAVATELMZ,fMW;

{$R *.dfm}

procedure TfPLAN_FACTf.LOAD_VID_NAGRUZKI;
begin

Combo_vid_nagruzki_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from vid_nagruzki where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_vid_nagruzki_id.AddItem(ADQ.FieldByName('vid_nagruzki').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_vid_nagruzki_id.ItemIndex:=0;

end;

procedure TfPLAN_FACTf.GetPREPODAVATELMZ(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfPLAN_FACTf.LoadPREPODAVATELMZ;
begin

Combo_prepodavatelmz_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from prepodavatelmz where deleted=0 order by fio');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_prepodavatelmz_id.AddItem(ADQ.FieldByName('fio').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_prepodavatelmz_id.ItemIndex:=0;

end;

procedure TfPLAN_FACTf.Button_prepodavatelmz_idClick(Sender: TObject);
begin
  fPREPODAVATELMZf.Caption:='����� �������������';
  if Combo_prepodavatelmz_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fPREPODAVATELMZf.ShowModal;

  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='RASPISANIE';

  if pNoAction=false then begin
    LoadPREPODAVATELMZ;
    if pSelectedID<>'' then GetPREPODAVATELMZ(Combo_prepodavatelmz_id,strtoint(pSelectedID));
  end;
end;

procedure TfPLAN_FACTf.Button1Click(Sender: TObject);
begin
Close
end;

procedure TfPLAN_FACTf.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=vk_escape then close;
end;

procedure TfPLAN_FACTf.FormActivate(Sender: TObject);
begin
LoadPREPODAVATELMZ;
LOAD_VID_NAGRUZKI;
ListView1.Items.Clear;
fMWf.SetLCW(ListView1);
end;

procedure TfPLAN_FACTf.FormResize(Sender: TObject);
begin
if ListView1.Columns.Count>0 then fMWf.SetLCW(ListView1);
end;

procedure TfPLAN_FACTf.Button2Click(Sender: TObject);
var
  pCount_par_teoriyaX:string;
  pCount_par_praktikaX:string;

begin

ListView1.Clear;
Screen.Cursor := crHourglass;
Application.ProcessMessages;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add
(
'Select distinct '+
'nagruzka_na_prepodavatelya.distsiplina_id,distsiplina.distsiplina_na_russkom '+
'from nagruzka_na_prepodavatelya,distsiplina '+
'where '+
'nagruzka_na_prepodavatelya.vid_nagruzki_id='+Inttostr(Integer(Combo_vid_nagruzki_id.Items.Objects[Combo_vid_nagruzki_id.ItemIndex]))+' and '+
'nagruzka_na_prepodavatelya.distsiplina_id=distsiplina.id and '+
'nagruzka_na_prepodavatelya.god_id='+pCurrent_God_id+' and '+
'nagruzka_na_prepodavatelya.polugodie_id='+pCurrent_Polugodie_id+' and '+
'prepodavatelmz_id='+Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))
);
ADQ.Open;

if ADQ.RecordCount = 0 then
begin
ListView1.Clear;
Screen.Cursor := crDefault;
exit;
end;

ListView1.Visible := false;
ListView1.Items.Clear;

ADQ.Last;
ADQ.First;

while not ADQ.Eof do begin
   L := ListView1.Items.Add;
   L.Caption := trim(ADQ.fieldbyname('distsiplina_id').AsString);
   L.SubItems.Append(ADQ.fieldbyname('distsiplina_na_russkom').AsString);

   pS:=
   'Select '+
   'sum(praktika_chasov_itogo) as sum_praktika_chasov_itogo, '+
   'sum(teoriya_chasov_itogo) as sum_teoriya_chasov_itogo '+
   'from nagruzka_na_prepodavatelya where '+
   'distsiplina_id='+ADQ.fieldbyname('distsiplina_id').AsString+' and '+
   'prepodavatelmz_id='+Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+' and '+
   'vid_nagruzki_id='+Inttostr(Integer(Combo_vid_nagruzki_id.Items.Objects[Combo_vid_nagruzki_id.ItemIndex]))+' and '+
   'god_id='+pCurrent_God_id+' and '+
   'polugodie_id='+pCurrent_Polugodie_id;

   ADQ1.Close;
   ADQ1.SQL.Clear;
   ADQ1.SQL.Add(pS);
   ADQ1.Open;

   pSum_praktika_chasov_itogo:=ADQ1.fieldbyname('sum_praktika_chasov_itogo').AsString;
   pSum_teoriya_chasov_itogo:=ADQ1.fieldbyname('sum_teoriya_chasov_itogo').AsString;


   //������
   pS:=
   'Select count(id) as count_par_teoriya '+
   'from raspisanie where '+
   'tip_zanyatiya_id=2 and '+
   'distsiplina_id='+ADQ.fieldbyname('distsiplina_id').AsString+' and '+
   'prepodavatelmz_id='+Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+' and '+
   'vid_nagruzki_id='+Inttostr(Integer(Combo_vid_nagruzki_id.Items.Objects[Combo_vid_nagruzki_id.ItemIndex]))+' and '+
   'god_id='+pCurrent_God_id+' and '+
   'polugodie_id='+pCurrent_Polugodie_id;

   ADQ1.Close;
   ADQ1.SQL.Clear;
   ADQ1.SQL.Add(pS);
   ADQ1.Open;

   pCount_par_teoriya:=inttostr(ADQ1.fieldbyname('count_par_teoriya').AsInteger*2);
   pCount_par_teoriyaX:=ADQ1.fieldbyname('count_par_teoriya').asstring;


   //��������
   pS:=
   'Select count(id) as count_par_praktika '+
   'from raspisanie where '+
   'tip_zanyatiya_id=3 and '+
   'distsiplina_id='+ADQ.fieldbyname('distsiplina_id').AsString+' and '+
   'prepodavatelmz_id='+Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+' and '+
   'vid_nagruzki_id='+Inttostr(Integer(Combo_vid_nagruzki_id.Items.Objects[Combo_vid_nagruzki_id.ItemIndex]))+' and '+
   'god_id='+pCurrent_God_id+' and '+
   'polugodie_id='+pCurrent_Polugodie_id;

   ADQ1.Close;
   ADQ1.SQL.Clear;
   ADQ1.SQL.Add(pS);
   ADQ1.Open;

   pCount_par_praktika:=inttostr(ADQ1.fieldbyname('count_par_praktika').AsInteger*2);
   pCount_par_praktikaX:=ADQ1.fieldbyname('count_par_praktika').asstring;

   L.SubItems.Append(pSum_teoriya_chasov_itogo+' / '  +pCount_par_teoriya  + ' ('+pCount_par_teoriyaX  +') ���(-�)');
   L.SubItems.Append(pSum_praktika_chasov_itogo+' / ' +pCount_par_praktika + ' ('+pCount_par_praktikaX +') ���(-�)');

ADQ.Next;
end;

  ListView1.Visible := true;
  if ListView1.Items.Count>0 then ListView1.Items[ListView1.Items.Count-1].Selected:=true;
  Screen.Cursor := crDefault;

end;

end.
