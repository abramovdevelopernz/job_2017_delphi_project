object fVOZMOZHNOSTMZ_RABOTYf: TfVOZMOZHNOSTMZ_RABOTYf
  Left = 463
  Top = 261
  Width = 660
  Height = 500
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 454
    Width = 652
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 405
    Width = 652
    Height = 49
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 168
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 248
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      TabOrder = 2
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 328
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100
      TabOrder = 3
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 488
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1055#1086#1080#1089#1082
      TabOrder = 4
      OnClick = Button5Click
    end
    object Button6: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1055#1077#1095#1072#1090#1100
      TabOrder = 5
      OnClick = Button6Click
    end
    object Button7: TButton
      Left = 568
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1042#1089#1077' '#1079#1072#1087#1080#1089#1080
      TabOrder = 6
      OnClick = Button7Click
    end
    object Button8: TButton
      Left = 408
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      TabOrder = 7
      OnClick = Button8Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 652
    Height = 405
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 2
    object Splitter1: TSplitter
      Left = 341
      Top = 1
      Height = 403
      Align = alRight
    end
    object ListView1: TListView
      Left = 1
      Top = 1
      Width = 340
      Height = 403
      Align = alClient
      Columns = <>
      Ctl3D = False
      FlatScrollBars = True
      GridLines = True
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      PopupMenu = PopupMenu1
      TabOrder = 0
      ViewStyle = vsReport
      OnClick = ListView1Click
      OnDblClick = ListView1DblClick
      OnKeyUp = ListView1KeyUp
    end
    object Panel3: TPanel
      Left = 344
      Top = 1
      Width = 307
      Height = 403
      Align = alRight
      BevelOuter = bvLowered
      Color = clWhite
      TabOrder = 1
      object Image1: TImage
        Left = 1
        Top = 1
        Width = 305
        Height = 401
        Cursor = crHandPoint
        Hint = #1044#1083#1103' '#1074#1099#1074#1086#1076#1072' '#1084#1077#1085#1102' '#1082#1083#1080#1082#1085#1080#1090#1077' '#1087#1088#1072#1074#1086#1081' '#1082#1085#1086#1087#1082#1086#1081' '#1084#1099#1096#1082#1080
        Align = alClient
        Center = True
        ParentShowHint = False
        PopupMenu = PopupMenu2
        Proportional = True
        ShowHint = True
        OnClick = Image1Click
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 8
    Top = 57
    object N_PHOTO: TMenuItem
      Caption = #1048#1079#1086#1073#1088#1072#1078#1077#1085#1080#1077
      OnClick = N_PHOTOClick
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = '*.jpg|*.jpg'
    Left = 8
    Top = 89
  end
  object PopupMenu2: TPopupMenu
    Left = 40
    Top = 57
    object N_LOAD_IMAGE: TMenuItem
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1079#1086#1073#1088#1072#1078#1077#1085#1080#1077
      OnClick = N_LOAD_IMAGEClick
    end
    object N_DELETE_IMAGE: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079#1086#1073#1088#1072#1078#1077#1085#1080#1077
      OnClick = N_DELETE_IMAGEClick
    end
  end
end
