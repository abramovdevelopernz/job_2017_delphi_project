object fKABINETY_DISTSIPLIN_RECORDf: TfKABINETY_DISTSIPLIN_RECORDf
  Left = 465
  Top = 161
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  ClientHeight = 95
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 47
    Width = 472
    Height = 48
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = 'OK'
      TabOrder = 0
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 472
    Height = 47
    Align = alClient
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label_distsiplina_id: TLabel
      Left = 72
      Top = 16
      Width = 63
      Height = 13
      Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1072
    end
    object Check_distsiplina_id: TCheckBox
      Left = 448
      Top = 16
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 1
    end
    object Combo_distsiplina_id: TComboBox
      Left = 144
      Top = 16
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 0
    end
    object Button_distsiplina_id: TButton
      Left = 416
      Top = 16
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 2
      OnClick = Button_distsiplina_idClick
    end
  end
end
