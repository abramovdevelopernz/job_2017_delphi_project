unit fBUSY_KABINET;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls;

type
  TfBUSY_KABINETf = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    ListView4: TListView;
    DateTimePicker1: TDateTimePicker;
    ProgressBar1: TProgressBar;
    Button3: TButton;
    procedure Button2Click(Sender: TObject);
    procedure LV4;
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fBUSY_KABINETf: TfBUSY_KABINETf;

implementation
uses fMW, fFORM_RASPISANIE;
{$R *.dfm}

procedure TfBUSY_KABINETf.LV4;
var
pXc:widestring;
i,z:integer;
begin

  ListView4.Clear;
//  fLENTAf.ListView1.Clear;
  Screen.Cursor := crHourglass;
  Application.ProcessMessages;

ADQ1.Close;
ADQ1.SQL.Clear;
ADQ1.SQL.Add('Select * from kabinet where deleted=0 and id<>1');
ADQ1.Open;

  if ADQ1.RecordCount = 0 then
  begin
    ListView4.Clear;
//    fLENTAf.ListView1.Clear;
    Screen.Cursor := crDefault;
    exit;
  end;


//  fLENTAf.ListView1.Visible := false;
//  fLENTAf.ListView1.Items.Clear;
  ProgressBar1.Max:=ADQ1.RecordCount;
  ProgressBar1.Visible:=true;
  z:=0;
  ListView4.Visible := false;
  ListView4.Items.Clear;

  ADQ1.Last;
  ADQ1.First;

  while not ADQ1.Eof do
  begin
    L := ListView4.Items.Add;
    L.Caption := trim(ADQ1.fieldbyname('id').AsString);
    L.SubItems.Append(ADQ1.fieldbyname('nomer_kabineta').AsString);


    for i:=1 to 7 do begin

    pS:='Select gruppa.nazvanie_gruppy,podgruppa.podgruppa from gruppa,raspisanie,podgruppa where '+
    'raspisanie.gruppa_id=gruppa.id and '+
    'raspisanie.podgruppa_id=podgruppa.id and '+



    'raspisanie.deleted=0 and raspisanie.para_id='+inttostr(i)+' and '+
    'raspisanie.kabinet_id='+ADQ1.fieldbyname('id').AsString+' and ';
    pXc:='data=#'+FormatDateTime('MM/DD/YYYY',DateTimePicker1.Date)+'#';
    pXc  := StringReplace(pXc, '.', '/',[rfReplaceAll, rfIgnoreCase]);

//    Clipboard.AsText:=pS+pXc;
//    ShowMessage(pS+pXc);

    ADQ2.Close;
    ADQ2.SQL.Clear;
    ADQ2.SQL.Add(pS+pXc);
    ADQ2.Open;

    if ADQ2.RecordCount>0 then L.SubItems.Append(ADQ2.fieldbyname('nazvanie_gruppy').AsString+' ['+ADQ2.fieldbyname('podgruppa').AsString+']') else L.SubItems.Append('-');

    end;


    inc(z);
    ProgressBar1.Position:=z;    
    ADQ1.Next;
  end;

  ProgressBar1.Visible:=false;
  ListView4.Visible := true;
//  fLENTAf.ListView1.Visible := true;

  if ListView4.Items.Count>0 then begin
    ListView4.Items[0].Selected:=true;
//    fLENTAf.ListView1.Items[0].Selected:=true;
  end;

  Screen.Cursor := crDefault;

end;


procedure TfBUSY_KABINETf.Button2Click(Sender: TObject);
begin
LV4;
end;

procedure TfBUSY_KABINETf.FormResize(Sender: TObject);
begin
if ListView4.Columns.Count>0 then fMWf.SetLCW(ListView4);
end;

procedure TfBUSY_KABINETf.FormActivate(Sender: TObject);
begin
  ListView4.Clear;
  Self.WindowState:=wsMaximized;
  DateTimePicker1.Date:=strtodate(fFORM_RASPISANIEf.ListView1.Selected.Caption);
  Self.Caption:='������� �������� �� '+datetostr(DateTimePicker1.Date);
  ProgressBar1.Visible:=false;
  fMWf.SetLCW(ListView4);
end;

procedure TfBUSY_KABINETf.DateTimePicker1Change(Sender: TObject);
begin
  Self.Caption:='������� �������� �� '+datetostr(DateTimePicker1.Date);
end;

procedure TfBUSY_KABINETf.Button1Click(Sender: TObject);
begin
Close;
end;

procedure TfBUSY_KABINETf.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=vk_escape then Close;
end;

procedure TfBUSY_KABINETf.Button3Click(Sender: TObject);
begin
  fMWf.LVToExcel(ListView4);
end;

end.
