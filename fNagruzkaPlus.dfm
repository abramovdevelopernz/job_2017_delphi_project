object fNagruzkaPlusf: TfNagruzkaPlusf
  Left = 361
  Top = 207
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = #1053#1072#1075#1088#1091#1079#1082#1072' '#1087#1086' '#1089#1086#1074#1084#1077#1097#1077#1085#1080#1102
  ClientHeight = 188
  ClientWidth = 389
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 389
    Height = 147
    Align = alClient
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 79
      Height = 13
      Caption = #1055#1088#1077#1087#1086#1076#1072#1074#1072#1090#1077#1083#1100
    end
    object Label2: TLabel
      Left = 40
      Top = 40
      Width = 63
      Height = 13
      Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1072
    end
    object Label3: TLabel
      Left = 68
      Top = 64
      Width = 35
      Height = 13
      Caption = #1043#1088#1091#1087#1087#1072
    end
    object Label4: TLabel
      Left = 49
      Top = 88
      Width = 54
      Height = 13
      Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
    end
    object Label5: TLabel
      Left = 40
      Top = 112
      Width = 63
      Height = 13
      Caption = #1058#1080#1087' '#1079#1072#1085#1103#1090#1080#1103
    end
    object Combo_prepodavatelmz_id: TComboBox
      Left = 112
      Top = 16
      Width = 265
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 0
    end
    object Combo_distsiplina_id: TComboBox
      Left = 112
      Top = 40
      Width = 265
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 1
    end
    object Combo_podgruppa_id: TComboBox
      Left = 111
      Top = 88
      Width = 266
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 2
    end
    object Combo_gruppa_id: TComboBox
      Left = 111
      Top = 64
      Width = 266
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 3
    end
    object Combo_tip_zanyatiya_id: TComboBox
      Left = 112
      Top = 112
      Width = 265
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 4
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 147
    Width = 389
    Height = 41
    Align = alBottom
    BevelOuter = bvLowered
    Color = clHighlightText
    TabOrder = 1
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Caption = #1055#1088#1080#1073#1072#1074#1080#1090#1100
      TabOrder = 1
      OnClick = Button2Click
    end
  end
end
