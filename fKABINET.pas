unit fKABINET;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, ComCtrls, StdCtrls, Menus, Clipbrd, Idglobal, JPEG;

type
  TfKABINETf = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    ListView1: TListView;
    PopupMenu1: TPopupMenu;
    Button8: TButton;
    Panel3: TPanel;
    Image1: TImage;
    Splitter1: TSplitter;
    N_PHOTO: TMenuItem;
    OpenDialog1: TOpenDialog;
    PopupMenu2: TPopupMenu;
    N_LOAD_IMAGE: TMenuItem;
    N_DELETE_IMAGE: TMenuItem;
    N_KABINETY_DISTSIPLIN: TMenuItem;

    procedure CreateCol;                 //�������� ������� ��� ������
    procedure LV1(pSearchString:string); //�������� ������ ��������
    procedure SearchRecord;              //����� �������
    procedure UpdateRecord;              //���������� �������
    procedure InsertRecord;              //���������� �������
    procedure DeleteRecord;              //�������� �������

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure ListView1Click(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure ListView1KeyUp(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure Image1Click(Sender: TObject);
    procedure N_LOAD_IMAGEClick(Sender: TObject);
    procedure N_PHOTOClick(Sender: TObject);
    procedure N_DELETE_IMAGEClick(Sender: TObject);
    procedure N_KABINETY_DISTSIPLINClick(Sender: TObject);
    procedure FormResize(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fKABINETf: TfKABINETf;

implementation

uses
fMW,
fKABINETY_DISTSIPLIN,
fSHOWRECORD,fKABINET_RECORD;
{$R *.dfm}

//���������� �������
procedure TfKABINETf.InsertRecord;
begin
  pNoAction:=false;
  pActionKABINET:='INSERT';
  fKABINET_RECORDf.showmodal;
  if pNoAction=false then LV1('');
end;

//���������� �������
procedure TfKABINETf.UpdateRecord;
begin
  if ListView1.ItemIndex=-1 then abort;
  pNoAction:=false;
  pID:=ListView1.Selected.Caption;
  pActionKABINET:='UPDATE';
  fKABINET_RECORDf.showmodal;
end;

//����� �������
procedure TfKABINETf.SearchRecord;
begin
  pNoAction:=false;
  pActionKABINET:='SEARCH';
  fKABINET_RECORDf.showmodal;

  if pNoAction=false then begin
    LV1(pSearch);
    Button7.Visible:=true;
  end;

end;

//�������� �������
procedure TfKABINETf.DeleteRecord;
begin

if ListView1.ItemIndex=-1 then abort;

if MessageBox(Self.Handle, '������������� �������� ������?', '�������� ������', MB_OKCANCEL) =  IDOK then begin

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Update kabinet set deleted=1,deleteuser='''+pMyUser+''',deletedate='''+datetostr(now)+' '+timetostr(now)+''' where id='+ListView1.Selected.Caption);
  ADQ.ExecSQL;

LV1('');

end;

end;

//�������� ������ ��������
procedure TfKABINETf.LV1(pSearchString:string);
begin

  ListView1.Clear;
  Screen.Cursor := crHourglass;
  Application.ProcessMessages;

pS:='Select '+
' kabinet.nomer_kabineta,'+ 
' kabinet.kolichestvo_mest,'+ 
' kabinet.id '+
' from '+
' kabinet '+
' where '+
' kabinet.deleted=0';

  if trim(pSearchString)<>'' then pS:=pS+pSearchString;

  if trim(pLinkKABINET)<>'' then begin 
   pS:=pS+pLinkKABINET;
   Button5.Visible:=false;
  end;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS+' order by kabinet.id');
  ADQ.Open;

  if ADQ.RecordCount = 0 then
  begin
    ListView1.Clear;
    StatusBar1.panels[0].text:='����� ������� : 0';
    Screen.Cursor := crDefault;
    exit;
  end;

  ListView1.Visible := false;
  ListView1.Items.Clear;

  ADQ.First;
  while not ADQ.Eof do
  begin
    L := ListView1.Items.Add;
    L.Caption := trim(ADQ.fieldbyname('id').AsString);
    L.SubItems.Append(ADQ.fieldbyname('nomer_kabineta').AsString); 
    L.SubItems.Append(ADQ.fieldbyname('kolichestvo_mest').AsString); 
    ADQ.Next;
  end;

  ListView1.Visible := true;

  if ListView1.Items.Count>0 then begin
    ListView1.Items[ListView1.Items.Count-1].Selected:=true;
    Button3.Enabled:=true;
    Button4.Enabled:=true;
    Button8.Enabled:=true;
  end else begin
    Button3.Enabled:=false;
    Button4.Enabled:=false;
    Button8.Enabled:=false;
  end;

  StatusBar1.panels[0].text:='����� ������� : '+inttostr(ADQ.recordcount);

  if Self.Caption='����� �������' then begin
    L := ListView1.FindCaption(1,pSelectedSID,True,True,False);
      if L <> nil then
        begin
          ListView1.Selected := L;
          L.MakeVisible(True);
          ListView1.SetFocus;
        end;
  end;

  Screen.Cursor := crDefault;

end;

//�������� ������� ��� ������ 
procedure TfKABINETf.CreateCol;
var
  i:integer;
begin

  pTableName:='KABINET';

  for i:=0 to ListView1.Columns.Count-1 do begin
    ListView1.Columns.Delete(0);
  end;

  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='����� ��������';
  ListView1.Column[1].Width:=100;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���������� ����';
  ListView1.Column[2].Width:=100;

end;

procedure TfKABINETf.ListView1DblClick(Sender: TObject);
begin

  if Self.Caption='����� �������' then begin
    pNoAction:=false;
    pSelectedID:=ListView1.Selected.Caption;
    Close;
  end;

  if Self.Caption<>'����� �������' then  Button3Click(Self);

end;

procedure TfKABINETf.N_KABINETY_DISTSIPLINClick(Sender: TObject);
begin

  if ListView1.ItemIndex>-1 then begin
   pSearch:='';
   pPhotoVrem:=pPhoto;
   pTableNameVrem:=pTableName;
   if ListView1.Items.count=0 then abort;
   pKABINET_ID:=ListView1.Selected.Caption;
   pLinkKABINETY_DISTSIPLIN:=' and KABINET_id='+ListView1.Selected.Caption;
   fKABINETY_DISTSIPLINf.Caption:='�������� ���������';
   fKABINETY_DISTSIPLINf.ShowModal;
   pPhoto:=pPhotoVrem;
   pTableName:=pTableNameVrem;
   pLink:='';
  end;

end;

procedure TfKABINETf.FormActivate(Sender: TObject);
begin
  if pLink<>'' then begin
    Button5.Visible:=false;
    Button7.Visible:=false;
  end else begin
    Button5.Visible:=true;
    Button7.Visible:=true;
  end;

  Button7.Visible:=false;

  pPhoto:=false;

  Splitter1.Visible:=false;
  if pPhoto=true then begin
    Panel3.Visible:=false;
    N_PHOTO.Visible:=true;
    N_PHOTO.Checked:=false;

  end else begin
    Panel3.Visible:=false;
    N_PHOTO.Visible:=false;
  end;

  CreateCol;

  LV1('');
  fMWf.SetLCW(ListView1);
  Self.WindowState:=wsMaximized; 
end;

procedure TfKABINETf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close;
end;

procedure TfKABINETf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_insert then Button2Click(Self);
  if key=vk_f2 then Button3Click(Self);
  if key=vk_delete then Button4Click(Self);
  if key=vk_f3 then Button5Click(Self);
  if key=vk_escape then close;
end;

procedure TfKABINETf.Button2Click(Sender: TObject);
begin
  InsertRecord;
end;

procedure TfKABINETf.Button3Click(Sender: TObject);
begin
  UpdateRecord;
end;

procedure TfKABINETf.Button4Click(Sender: TObject);
begin
  DeleteRecord;
end;

procedure TfKABINETf.Button5Click(Sender: TObject);
begin
  SearchRecord;
end;

procedure TfKABINETf.Button6Click(Sender: TObject);
begin
  fMWf.LVToExcel(ListView1);
end;

procedure TfKABINETf.ListView1Click(Sender: TObject);
begin
if (ListView1.ItemIndex<0) then begin
  Button3.Enabled:=false;
  Button4.Enabled:=false;
  Button8.Enabled:=false;
  Panel3.Visible:=false;
end else begin
  Button3.Enabled:=true;
  Button4.Enabled:=true;
  Button8.Enabled:=true;

  if N_PHOTO.Checked=true then begin
    Panel3.Visible:=true;
    Splitter1.Visible:=true;
    Splitter1.Align:=alNone;
    Panel3.Align:=alNone;
    ListView1.Align:=alNone;
    Panel3.Align:=alRight;
    Splitter1.Align:=alRight;
    ListView1.Align:=alClient;

    if FileExists(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg')= true then begin
      Image1.Picture.LoadFromFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg');
    end else begin
      image1.Picture.Bitmap:=nil;
    end;

  end else begin
    Panel3.Visible:=false;
    Splitter1.Visible:=false;
  end;

end;
end;

procedure TfKABINETf.Button7Click(Sender: TObject);
begin
  Button7.Visible:=false;
  LV1('');
end;

procedure TfKABINETf.Button8Click(Sender: TObject);
begin
if ListView1.ItemIndex>-1 then begin
  fMWf.ShowRecord(ListView1);
  fSHOWRECORDf.ShowModal;
end;
end;

procedure TfKABINETf.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  pPhoto:=false;
end;

procedure TfKABINETf.N_PHOTOClick(Sender: TObject);
begin
if N_PHOTO.Checked=true then begin
  Splitter1.Visible:=false;
  N_PHOTO.Checked:=false;
  Panel3.Visible:=false;
end else begin
  N_PHOTO.Checked:=true;
  Panel3.Visible:=true;
  Splitter1.Visible:=true;
  Splitter1.Align:=alNone;
  Panel3.Align:=alNone;
  ListView1.Align:=alNone;
  Panel3.Align:=alRight;
  Splitter1.Align:=alRight;
  ListView1.Align:=alClient;
end;
end;

procedure TfKABINETf.N_LOAD_IMAGEClick(Sender: TObject);
begin
  if ListView1.Items.Count=0 then abort;
  if MessageBox(Self.Handle, '������� �����������?', '����� �����������', MB_OKCANCEL) =  IDCANCEL then abort;
  OpenDialog1.Execute;
  if trim(OpenDialog1.FileName)='' then abort;
  Application.ProcessMessages;
  Screen.Cursor := crHourglass;
  DeleteFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg');
  CopyFileTo(trim(OpenDialog1.FileName),ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg');
  Image1.Picture.LoadFromFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg');
  Screen.Cursor := crDefault;
end;

procedure TfKABINETf.N_DELETE_IMAGEClick(Sender: TObject);
begin
 if ListView1.Items.Count=0 then abort;
 if MessageBox(Self.Handle, '������� �����������?', '�������� �����', MB_OKCANCEL) =  IDCANCEL then abort;
   if FileExists(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg')= true then begin
     DeleteFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg');
     Image1.Picture.Bitmap:=nil;
   end;
end;

procedure TfKABINETf.ListView1KeyUp(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  ListView1Click(Self);
end;

procedure TfKABINETf.Image1Click(Sender: TObject);
begin
  Button8Click(Self);
end;

procedure TfKABINETf.FormResize(Sender: TObject);
begin
if ListView1.Columns.Count>0 then fMWf.SetLCW(ListView1);
end;

end.