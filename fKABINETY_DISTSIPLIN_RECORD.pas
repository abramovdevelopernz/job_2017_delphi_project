unit fKABINETY_DISTSIPLIN_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfKABINETY_DISTSIPLIN_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_distsiplina_id: TLabel;

    Combo_distsiplina_id: TComboBox;
    Button_distsiplina_id: TButton;

    Check_distsiplina_id: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;
    procedure LoadDISTSIPLINA;
    procedure GetDISTSIPLINA(pCombo:TComboBox;pSID:integer);
    procedure Button_distsiplina_idClick(Sender: TObject);


    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fKABINETY_DISTSIPLIN_RECORDf: TfKABINETY_DISTSIPLIN_RECORDf;

implementation

uses  
    fKABINETY_DISTSIPLIN,
    fDISTSIPLINA,
    fMW;

{$R *.dfm}

procedure TfKABINETY_DISTSIPLIN_RECORDf.GetDISTSIPLINA(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfKABINETY_DISTSIPLIN_RECORDf.LoadDISTSIPLINA;
begin

Combo_distsiplina_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from distsiplina where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_distsiplina_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_distsiplina_id.ItemIndex:=0;

end;

procedure TfKABINETY_DISTSIPLIN_RECORDf.Button_distsiplina_idClick(Sender: TObject);
begin

  fDISTSIPLINAf.Caption:='����� ����������';
  if Combo_distsiplina_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_distsiplina_id.Items.Objects[Combo_distsiplina_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fDISTSIPLINAf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='KABINETY_DISTSIPLIN';

  if pNoAction=false then begin
    LoadDISTSIPLINA;
    if pSelectedID<>'' then GetDISTSIPLINA(Combo_distsiplina_id,strtoint(pSelectedID));
  end;

end;

procedure TfKABINETY_DISTSIPLIN_RECORDf.CheckInput;
begin

if trim(Combo_distsiplina_id.Text)='' then begin
  Combo_distsiplina_id.SetFocus;
  abort;
end;

end;
procedure TfKABINETY_DISTSIPLIN_RECORDf.CLALL;
begin

  LoadDISTSIPLINA;

  Check_distsiplina_id.Checked:=false;
  Check_distsiplina_id.Visible:=false;


  Combo_distsiplina_id.Setfocus;

end;

procedure TfKABINETY_DISTSIPLIN_RECORDf.FormActivated;
begin

  CLALL;

if pActionKABINETY_DISTSIPLIN='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionKABINETY_DISTSIPLIN='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from kabinety_distsiplin where id='+pID);
  ADQ.Open;
  ADQ.First;

  GetDISTSIPLINA(Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);

end;

if pActionKABINETY_DISTSIPLIN='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_distsiplina_id.Visible:=true;

end;

end;

procedure TfKABINETY_DISTSIPLIN_RECORDf.BtnOK;
begin

if pActionKABINETY_DISTSIPLIN='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'kabinety_distsiplin '+
  '('+
  'kabinet_id,'+
  'distsiplina_id,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+pkabinet_ID+''', '+
  ' '''+Inttostr(Integer(Combo_distsiplina_id.Items.Objects[Combo_distsiplina_id.ItemIndex]))+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionKABINETY_DISTSIPLIN='UPDATE' then begin


 pS:='UPDATE '+
  'kabinety_distsiplin set ' +
  'distsiplina_id='''+Inttostr(Integer(Combo_distsiplina_id.Items.Objects[Combo_distsiplina_id.ItemIndex]))+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fKABINETY_DISTSIPLINf.ListView1.Selected.SubItems[0]:=trim(Combo_distsiplina_id.Text);

end;

if pActionKABINETY_DISTSIPLIN='SEARCH' then begin

if (Check_distsiplina_id.Checked=false) then abort;

   pSearch:='';

   if Check_distsiplina_id.Checked=true then pSearch:=pSearch+' and kabinety_distsiplin.distsiplina_id = '+Inttostr(Integer(Combo_distsiplina_id.Items.Objects[Combo_distsiplina_id.ItemIndex]))+' ';

end;

Close;

end;

procedure TfKABINETY_DISTSIPLIN_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfKABINETY_DISTSIPLIN_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfKABINETY_DISTSIPLIN_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfKABINETY_DISTSIPLIN_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.