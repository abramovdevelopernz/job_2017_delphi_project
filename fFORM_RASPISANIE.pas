unit fFORM_RASPISANIE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, Buttons,Clipbrd, Menus,comobj;

type
  TfFORM_RASPISANIEf = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Panel2: TPanel;
    ListView1: TListView;
    Panel5: TPanel;
    Panel3: TPanel;
    Panel6: TPanel;
    Combo_distsiplina11_id: TComboBox;
    Combo_distsiplina12_id: TComboBox;
    Combo_distsiplina13_id: TComboBox;
    Combo_prepodavatelmz11_id: TComboBox;
    Combo_prepodavatelmz12_id: TComboBox;
    Combo_prepodavatelmz13_id: TComboBox;
    Combo_tip_zanyatiya11_id: TComboBox;
    Combo_tip_zanyatiya12_id: TComboBox;
    Combo_tip_zanyatiya13_id: TComboBox;
    Combo_kabinet11_id: TComboBox;
    Combo_kabinet12_id: TComboBox;
    Combo_kabinet13_id: TComboBox;
    Panel7: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Combo_podgruppa11_id: TComboBox;
    Combo_podgruppa12_id: TComboBox;
    Combo_podgruppa13_id: TComboBox;
    Panel8: TPanel;
    Panel9: TPanel;
    Combo_distsiplina21_id: TComboBox;
    Combo_distsiplina22_id: TComboBox;
    Combo_distsiplina23_id: TComboBox;
    Combo_prepodavatelmz21_id: TComboBox;
    Combo_prepodavatelmz22_id: TComboBox;
    Combo_prepodavatelmz23_id: TComboBox;
    Combo_tip_zanyatiya21_id: TComboBox;
    Combo_tip_zanyatiya22_id: TComboBox;
    Combo_tip_zanyatiya23_id: TComboBox;
    Combo_kabinet21_id: TComboBox;
    Combo_kabinet22_id: TComboBox;
    Combo_kabinet23_id: TComboBox;
    Combo_podgruppa21_id: TComboBox;
    Combo_podgruppa22_id: TComboBox;
    Combo_podgruppa23_id: TComboBox;
    Panel10: TPanel;
    Panel11: TPanel;
    Combo_distsiplina71_id: TComboBox;
    Combo_distsiplina72_id: TComboBox;
    Combo_distsiplina73_id: TComboBox;
    Combo_prepodavatelmz71_id: TComboBox;
    Combo_prepodavatelmz72_id: TComboBox;
    Combo_prepodavatelmz73_id: TComboBox;
    Combo_tip_zanyatiya71_id: TComboBox;
    Combo_tip_zanyatiya72_id: TComboBox;
    Combo_tip_zanyatiya73_id: TComboBox;
    Combo_kabinet71_id: TComboBox;
    Combo_kabinet72_id: TComboBox;
    Combo_kabinet73_id: TComboBox;
    Combo_podgruppa71_id: TComboBox;
    Combo_podgruppa72_id: TComboBox;
    Combo_podgruppa73_id: TComboBox;
    Panel12: TPanel;
    Panel13: TPanel;
    Combo_distsiplina31_id: TComboBox;
    Combo_distsiplina32_id: TComboBox;
    Combo_distsiplina33_id: TComboBox;
    Combo_prepodavatelmz31_id: TComboBox;
    Combo_prepodavatelmz32_id: TComboBox;
    Combo_prepodavatelmz33_id: TComboBox;
    Combo_tip_zanyatiya31_id: TComboBox;
    Combo_tip_zanyatiya32_id: TComboBox;
    Combo_tip_zanyatiya33_id: TComboBox;
    Combo_kabinet31_id: TComboBox;
    Combo_kabinet32_id: TComboBox;
    Combo_kabinet33_id: TComboBox;
    Combo_podgruppa31_id: TComboBox;
    Combo_podgruppa32_id: TComboBox;
    Combo_podgruppa33_id: TComboBox;
    Panel14: TPanel;
    Panel15: TPanel;
    Combo_distsiplina41_id: TComboBox;
    Combo_distsiplina42_id: TComboBox;
    Combo_distsiplina43_id: TComboBox;
    Combo_prepodavatelmz41_id: TComboBox;
    Combo_prepodavatelmz42_id: TComboBox;
    Combo_prepodavatelmz43_id: TComboBox;
    Combo_tip_zanyatiya41_id: TComboBox;
    Combo_tip_zanyatiya42_id: TComboBox;
    Combo_tip_zanyatiya43_id: TComboBox;
    Combo_kabinet41_id: TComboBox;
    Combo_kabinet42_id: TComboBox;
    Combo_kabinet43_id: TComboBox;
    Combo_podgruppa41_id: TComboBox;
    Combo_podgruppa42_id: TComboBox;
    Combo_podgruppa43_id: TComboBox;
    Panel16: TPanel;
    Panel17: TPanel;
    Combo_distsiplina51_id: TComboBox;
    Combo_distsiplina52_id: TComboBox;
    Combo_distsiplina53_id: TComboBox;
    Combo_prepodavatelmz51_id: TComboBox;
    Combo_prepodavatelmz52_id: TComboBox;
    Combo_prepodavatelmz53_id: TComboBox;
    Combo_tip_zanyatiya51_id: TComboBox;
    Combo_tip_zanyatiya52_id: TComboBox;
    Combo_tip_zanyatiya53_id: TComboBox;
    Combo_kabinet51_id: TComboBox;
    Combo_kabinet52_id: TComboBox;
    Combo_kabinet53_id: TComboBox;
    Combo_podgruppa51_id: TComboBox;
    Combo_podgruppa52_id: TComboBox;
    Combo_podgruppa53_id: TComboBox;
    Panel18: TPanel;
    Panel19: TPanel;
    Combo_distsiplina61_id: TComboBox;
    Combo_distsiplina62_id: TComboBox;
    Combo_distsiplina63_id: TComboBox;
    Combo_prepodavatelmz61_id: TComboBox;
    Combo_prepodavatelmz62_id: TComboBox;
    Combo_prepodavatelmz63_id: TComboBox;
    Combo_tip_zanyatiya61_id: TComboBox;
    Combo_tip_zanyatiya62_id: TComboBox;
    Combo_tip_zanyatiya63_id: TComboBox;
    Combo_kabinet61_id: TComboBox;
    Combo_kabinet62_id: TComboBox;
    Combo_kabinet63_id: TComboBox;
    Combo_podgruppa61_id: TComboBox;
    Combo_podgruppa62_id: TComboBox;
    Combo_podgruppa63_id: TComboBox;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    SpeedButton10: TSpeedButton;
    SpeedButton11: TSpeedButton;
    SpeedButton12: TSpeedButton;
    SpeedButton13: TSpeedButton;
    SpeedButton14: TSpeedButton;
    SpeedButton15: TSpeedButton;
    SpeedButton16: TSpeedButton;
    SpeedButton17: TSpeedButton;
    SpeedButton18: TSpeedButton;
    SpeedButton19: TSpeedButton;
    SpeedButton20: TSpeedButton;
    SpeedButton21: TSpeedButton;
    SpeedButton22: TSpeedButton;
    SpeedButton23: TSpeedButton;
    SpeedButton24: TSpeedButton;
    SpeedButton25: TSpeedButton;
    SpeedButton26: TSpeedButton;
    SpeedButton27: TSpeedButton;
    SpeedButton28: TSpeedButton;
    SpeedButton29: TSpeedButton;
    SpeedButton30: TSpeedButton;
    SpeedButton31: TSpeedButton;
    SpeedButton32: TSpeedButton;
    SpeedButton33: TSpeedButton;
    SpeedButton34: TSpeedButton;
    SpeedButton35: TSpeedButton;
    SpeedButton36: TSpeedButton;
    SpeedButton37: TSpeedButton;
    SpeedButton38: TSpeedButton;
    SpeedButton39: TSpeedButton;
    SpeedButton40: TSpeedButton;
    SpeedButton41: TSpeedButton;
    SpeedButton42: TSpeedButton;
    SpeedButton43: TSpeedButton;
    SpeedButton44: TSpeedButton;
    SpeedButton45: TSpeedButton;
    SpeedButton46: TSpeedButton;
    SpeedButton47: TSpeedButton;
    SpeedButton48: TSpeedButton;
    SpeedButton49: TSpeedButton;
    SpeedButton50: TSpeedButton;
    SpeedButton51: TSpeedButton;
    SpeedButton52: TSpeedButton;
    SpeedButton53: TSpeedButton;
    SpeedButton54: TSpeedButton;
    SpeedButton55: TSpeedButton;
    SpeedButton56: TSpeedButton;
    SpeedButton57: TSpeedButton;
    SpeedButton58: TSpeedButton;
    SpeedButton59: TSpeedButton;
    SpeedButton60: TSpeedButton;
    SpeedButton61: TSpeedButton;
    SpeedButton62: TSpeedButton;
    SpeedButton63: TSpeedButton;
    Panel20: TPanel;
    Combo_gruppa: TComboBox;
    GroupBox1: TGroupBox;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    GroupBox2: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    SpeedButton64: TSpeedButton;
    SpeedButton65: TSpeedButton;
    SpeedButton66: TSpeedButton;
    SpeedButton67: TSpeedButton;
    SpeedButton68: TSpeedButton;
    SpeedButton69: TSpeedButton;
    SpeedButton70: TSpeedButton;
    SpeedButton71: TSpeedButton;
    SpeedButton72: TSpeedButton;
    SpeedButton73: TSpeedButton;
    SpeedButton74: TSpeedButton;
    SpeedButton75: TSpeedButton;
    SpeedButton76: TSpeedButton;
    SpeedButton77: TSpeedButton;
    SpeedButton78: TSpeedButton;
    SpeedButton79: TSpeedButton;
    SpeedButton80: TSpeedButton;
    SpeedButton81: TSpeedButton;
    SpeedButton82: TSpeedButton;
    SpeedButton83: TSpeedButton;
    SpeedButton84: TSpeedButton;
    Image4: TImage;
    Label14: TLabel;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N11: TMenuItem;
    Splitter2: TSplitter;
    SpeedButton85: TSpeedButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ListView2: TListView;
    TabSheet2: TTabSheet;
    PopupMenu2: TPopupMenu;
    N2: TMenuItem;
    ListView3: TListView;
    N4: TMenuItem;
    N21: TMenuItem;
    N5: TMenuItem;
    Button1: TButton;
    Button2: TButton;
    procedure FormActivate(Sender: TObject);
    procedure DISTINCT_DATA;
    procedure LoadDISTSIPLINA;
    procedure LOAD_KABINET;
    procedure LoadTIP_ZANYATIYA;
    procedure GetComboRecord(pCombo:TComboBox;pSID:integer);
    procedure ListView1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure ListView1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Combo_distsiplina11_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz11_idChange(Sender: TObject);

    procedure UpdateRecord;
    procedure Progul;

    procedure Check_1;     //�������� �� ���������������� ������������� ����� ��������  - ����������� �� �������������
    procedure Check_21;    //�������� �� ����� ���������� ����� ���������� ����� ����������� �� ����� 50 ��������� ������������� �����
    procedure Check_24;    //� ��������� ����� ���������� �� ����� 2-� �����
    procedure Check_14;    //���� ������������� � ���� � �� �� ����� �� ����� ����� ���������� � ������ �������
    procedure Check_15_16; //������� �������� �� �������� ������������� ���������� �� ����� 4 ���
    procedure Check_17;    //������� �������� �� ������ �� ����� 4 ���
    procedure Check_18;    //����������� �� ����� 2-� ��� � ������ � �� ����� 1 ���� � ����
    procedure Check_19;    //�� ����� ���������� ������ �� ����� 1 ���� � ����
    procedure Check_20;    //�� ����� ���������� �������� �� ����� 2 ��� � ����
    procedure Check_25;    //���������� �� ����� ��������� ������������� ������� �� 5-� ����
    procedure Check_23;    //������� ������������� �� ����� �������� � ���� 3 ��������� ������������ ������� � ����� ������ �������� 2
    procedure Check_22;    //��� ���������� ������������ ������� � ���� ���� ������ ���� ������������� ��� ��������� ����� ������

    procedure LoadCombo1;
    procedure LoadCombo2;
    procedure GetCurrentElement;
    procedure ClearCombo;
    procedure ClearRecord;
    procedure LoadGRUPPA;
    procedure Raschet;
    procedure Check_Kabinet_Zanyat;
    procedure DobavNagrNaPrepod;
    procedure GetOneString;
    procedure GetOneString1;

    procedure LV2;

    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure SpeedButton16Click(Sender: TObject);
    procedure SpeedButton19Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure SpeedButton14Click(Sender: TObject);
    procedure SpeedButton17Click(Sender: TObject);
    procedure SpeedButton20Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton12Click(Sender: TObject);
    procedure SpeedButton15Click(Sender: TObject);
    procedure SpeedButton18Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SpeedButton21Click(Sender: TObject);
    procedure Combo_distsiplina12_idChange(Sender: TObject);
    procedure Combo_distsiplina13_idChange(Sender: TObject);
    procedure Combo_distsiplina21_idChange(Sender: TObject);
    procedure Combo_distsiplina22_idChange(Sender: TObject);
    procedure Combo_distsiplina23_idChange(Sender: TObject);
    procedure Combo_distsiplina31_idChange(Sender: TObject);
    procedure Combo_distsiplina32_idChange(Sender: TObject);
    procedure Combo_distsiplina33_idChange(Sender: TObject);
    procedure Combo_distsiplina41_idChange(Sender: TObject);
    procedure Combo_distsiplina42_idChange(Sender: TObject);
    procedure Combo_distsiplina43_idChange(Sender: TObject);
    procedure Combo_distsiplina51_idChange(Sender: TObject);
    procedure Combo_distsiplina52_idChange(Sender: TObject);
    procedure Combo_distsiplina53_idChange(Sender: TObject);
    procedure Combo_distsiplina61_idChange(Sender: TObject);
    procedure Combo_distsiplina62_idChange(Sender: TObject);
    procedure Combo_distsiplina63_idChange(Sender: TObject);
    procedure Combo_distsiplina71_idChange(Sender: TObject);
    procedure Combo_distsiplina72_idChange(Sender: TObject);
    procedure Combo_distsiplina73_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz12_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz13_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz21_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz22_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz23_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz31_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz32_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz33_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz41_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz42_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz43_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz51_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz52_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz53_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz61_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz62_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz63_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz71_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz72_idChange(Sender: TObject);
    procedure Combo_prepodavatelmz73_idChange(Sender: TObject);
    procedure SpeedButton22Click(Sender: TObject);
    procedure SpeedButton23Click(Sender: TObject);
    procedure SpeedButton24Click(Sender: TObject);
    procedure SpeedButton25Click(Sender: TObject);
    procedure SpeedButton26Click(Sender: TObject);
    procedure SpeedButton27Click(Sender: TObject);
    procedure SpeedButton28Click(Sender: TObject);
    procedure SpeedButton29Click(Sender: TObject);
    procedure SpeedButton30Click(Sender: TObject);
    procedure SpeedButton31Click(Sender: TObject);
    procedure SpeedButton32Click(Sender: TObject);
    procedure SpeedButton33Click(Sender: TObject);
    procedure SpeedButton34Click(Sender: TObject);
    procedure SpeedButton35Click(Sender: TObject);
    procedure SpeedButton36Click(Sender: TObject);
    procedure SpeedButton37Click(Sender: TObject);
    procedure SpeedButton38Click(Sender: TObject);
    procedure SpeedButton39Click(Sender: TObject);
    procedure SpeedButton40Click(Sender: TObject);
    procedure SpeedButton41Click(Sender: TObject);
    procedure SpeedButton42Click(Sender: TObject);
    procedure SpeedButton43Click(Sender: TObject);
    procedure SpeedButton44Click(Sender: TObject);
    procedure SpeedButton45Click(Sender: TObject);
    procedure SpeedButton46Click(Sender: TObject);
    procedure SpeedButton47Click(Sender: TObject);
    procedure SpeedButton48Click(Sender: TObject);
    procedure SpeedButton49Click(Sender: TObject);
    procedure SpeedButton50Click(Sender: TObject);
    procedure SpeedButton51Click(Sender: TObject);
    procedure SpeedButton52Click(Sender: TObject);
    procedure SpeedButton53Click(Sender: TObject);
    procedure SpeedButton54Click(Sender: TObject);
    procedure SpeedButton55Click(Sender: TObject);
    procedure SpeedButton56Click(Sender: TObject);
    procedure SpeedButton57Click(Sender: TObject);
    procedure SpeedButton58Click(Sender: TObject);
    procedure SpeedButton59Click(Sender: TObject);
    procedure SpeedButton60Click(Sender: TObject);
    procedure SpeedButton61Click(Sender: TObject);
    procedure SpeedButton62Click(Sender: TObject);
    procedure SpeedButton63Click(Sender: TObject);
    procedure Combo_gruppaChange(Sender: TObject);
    procedure Combo_tip_zanyatiya11_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya12_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya13_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya21_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya22_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya23_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya31_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya32_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya33_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya41_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya42_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya43_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya51_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya52_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya53_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya61_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya62_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya63_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya71_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya72_idChange(Sender: TObject);
    procedure Combo_tip_zanyatiya73_idChange(Sender: TObject);
    procedure SpeedButton64Click(Sender: TObject);
    procedure SpeedButton65Click(Sender: TObject);
    procedure SpeedButton66Click(Sender: TObject);
    procedure SpeedButton67Click(Sender: TObject);
    procedure SpeedButton68Click(Sender: TObject);
    procedure SpeedButton69Click(Sender: TObject);
    procedure SpeedButton70Click(Sender: TObject);
    procedure SpeedButton71Click(Sender: TObject);
    procedure SpeedButton72Click(Sender: TObject);
    procedure SpeedButton73Click(Sender: TObject);
    procedure SpeedButton74Click(Sender: TObject);
    procedure SpeedButton75Click(Sender: TObject);
    procedure SpeedButton76Click(Sender: TObject);
    procedure SpeedButton77Click(Sender: TObject);
    procedure SpeedButton78Click(Sender: TObject);
    procedure SpeedButton79Click(Sender: TObject);
    procedure SpeedButton80Click(Sender: TObject);
    procedure SpeedButton81Click(Sender: TObject);
    procedure SpeedButton82Click(Sender: TObject);
    procedure SpeedButton83Click(Sender: TObject);
    procedure SpeedButton84Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure N1Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure SpeedButton85Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Combo_prepodavatelmz11_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz12_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz13_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz21_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz22_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz23_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz31_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz32_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz33_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz41_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz42_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz43_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz51_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz52_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz53_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz61_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz62_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz63_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz71_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz72_idClick(Sender: TObject);
    procedure Combo_prepodavatelmz73_idClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fFORM_RASPISANIEf: TfFORM_RASPISANIEf;
  pPara_id,pStroka:string;
  pCurrent_Combo_distsiplina_id,pCurrent_Combo_prepodavatelmz_id,pCurrent_Combo_podgruppa_id,pCurrent_Combo_tip_zanyatiya_id,pCurrent_Combo_kabinet_id:TComboBox;
  pD,pP,pK,pX:widestring;
  pPodgruppa:string;

implementation

uses fCREATE_DATE,fMW, DB, fTIP_DISTSIPLINY, fKABINET, fPREPODAVATELMZ, fPRICHINA_MSG, fNagruzkaPlus, fSVOBOD_CHASY, fLENTA,
  fBUSY_KABINET, fLENTA_GRUPP;

{$R *.dfm}


procedure TfFORM_RASPISANIEf.LV2;
begin

  ListView2.Clear;
  Screen.Cursor := crHourglass;
  Application.ProcessMessages;

  pS:='Select * from svobod_chasy where dayz_of_week='+ListView1.Selected.SubItems[1]+' and prepodavatelmz_id='+pCurrent_Combo_prepodavatelmz_id_x+' order by id';

//  ShowMessage(pS);

  ADQ1.Close;
  ADQ1.SQL.Clear;
  ADQ1.SQL.Add(pS);
  ADQ1.Open;

  if ADQ1.RecordCount = 0 then
  begin
    ListView2.Clear;
    Screen.Cursor := crDefault;
    exit;
  end;

  ListView2.Visible := false;
  ListView2.Items.Clear;

  ADQ1.First;
  while not ADQ1.Eof do
  begin
    L := ListView2.Items.Add;
    L.Caption := trim(ADQ1.fieldbyname('para_id').AsString);
    if  ADQ1.fieldbyname('svoboden').AsString='1' then  L.SubItems.Append('��������');
    if  ADQ1.fieldbyname('svoboden').AsString='0' then  L.SubItems.Append('�����');
    ADQ1.Next;
  end;

  ListView2.Visible := true;

  if ListView2.Items.Count>0 then begin
    ListView2.Items[0].Selected:=true;
  end;

  Screen.Cursor := crDefault;

end;

procedure TfFORM_RASPISANIEf.GetOneString;
begin

if ADQ.FieldByName('podgruppa_id').AsString='1' then pPodgruppa:='';
if ADQ.FieldByName('podgruppa_id').AsString='2' then pPodgruppa:='I';
if ADQ.FieldByName('podgruppa_id').AsString='3' then pPodgruppa:='II';
if ADQ.FieldByName('podgruppa_id').AsString='4' then pPodgruppa:='III';

if ADQ.FieldByName('yazyk_obucheniya_id').AsString='1' then pD:=pD+pPodgruppa+' '+ADQ.FieldByName('distsiplina_na_russkom').AsString+'; ';
if ADQ.FieldByName('yazyk_obucheniya_id').AsString='2' then pD:=pD+pPodgruppa+' '+ADQ.FieldByName('distsiplina_na_kazakhskom').AsString+'; ';

pP:=pP+' '+ADQ.FieldByName('fio').AsString+'; ';

if ADQ.FieldByName('kabinet_id').AsInteger>34 then  pK:=pK+' ����;' else pK:=pK+' '+ADQ.FieldByName('nomer_kabineta').AsString+'; ';

end;

procedure TfFORM_RASPISANIEf.GetOneString1;
begin

if ADQ.FieldByName('podgruppa_id').AsString='1' then pPodgruppa:='';
if ADQ.FieldByName('podgruppa_id').AsString='2' then pPodgruppa:='I';
if ADQ.FieldByName('podgruppa_id').AsString='3' then pPodgruppa:='II';
if ADQ.FieldByName('podgruppa_id').AsString='4' then pPodgruppa:='III';

//if ADQ.FieldByName('yazyk_obucheniya_id').AsString='1' then pD:=pD+pPodgruppa+' '+ADQ.FieldByName('distsiplina_na_russkom').AsString+'; ';
//if ADQ.FieldByName('yazyk_obucheniya_id').AsString='2' then pD:=pD+pPodgruppa+' '+ADQ.FieldByName('distsiplina_na_kazakhskom').AsString+'; ';

if ADQ.FieldByName('yazyk_obucheniya_id').AsString='1' then pD:=pD+' '+ADQ.FieldByName('distsiplina_na_russkom').AsString+'; ';
if ADQ.FieldByName('yazyk_obucheniya_id').AsString='2' then pD:=pD+' '+ADQ.FieldByName('distsiplina_na_kazakhskom').AsString+'; ';

pX:=pX+pPodgruppa+',';

//pX:=Copy(pX,1,length(pX)-1);


pP:=pP+' '+ADQ.FieldByName('fio').AsString+'; ';

if ADQ.FieldByName('kabinet_id').AsInteger>34 then  pK:=pK+' ����;' else pK:=pK+' '+ADQ.FieldByName('nomer_kabineta').AsString+'; ';

end;

procedure TfFORM_RASPISANIEf.Check_22;    //��� ���������� ������������ ������� � ���� ���� ������ ���� ������������� ��� ��������� ����� ������
var
pKol:integer;
begin


if Panel5.Visible=false then exit;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select distinct raspisanie.distsiplina_id,distsiplina.tip_distsipliny_id '+
'from '+
'raspisanie,distsiplina where '+
'raspisanie.deleted=0 and  '+
'raspisanie.gruppa_id='+Inttostr(Integer(Combo_gruppa.Items.Objects[Combo_gruppa.ItemIndex]))+' and '+
'distsiplina.tip_distsipliny_id<>1 and  '+
'raspisanie.tip_zanyatiya_id=3 and '+
'raspisanie.distsiplina_id=distsiplina.id and '+
'raspisanie.god_id='+pCurrent_God_id+' and '+
'raspisanie.polugodie_id='+pCurrent_Polugodie_id+' and '+
'raspisanie.data=#'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(ListView1.Selected.Caption)), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# order by tip_distsipliny_id');
ADQ.Open;

//Clipboard.AsText:=ADQ.SQL.Text;

ADQ.First;

while not ADQ.Eof do begin

if ADQ.fieldbyname('tip_distsipliny_id').AsString='2' then pKol:=2;
if ADQ.fieldbyname('tip_distsipliny_id').AsString='3' then pKol:=2;
if ADQ.fieldbyname('tip_distsipliny_id').AsString='4' then pKol:=2;
if ADQ.fieldbyname('tip_distsipliny_id').AsString='5' then pKol:=3;

ADQ1.Close;
ADQ1.SQL.Clear;
ADQ1.SQL.Add('Select distinct podgruppa_id from raspisanie where '+
'distsiplina_id='+ADQ.fieldbyname('distsiplina_id').AsString+ ' and '+
'gruppa_id='+Inttostr(Integer(Combo_gruppa.Items.Objects[Combo_gruppa.ItemIndex]))+' and '+
'god_id='+pCurrent_God_id+' and '+
'polugodie_id='+pCurrent_Polugodie_id+' and '+
'data=#'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(ListView1.Selected.Caption)), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# ');
ADQ1.Open;

if ADQ1.RecordCount<pKol then begin
  ShowMessage('��� ���������� ������������ ������� � ���� ���� ������ ���� ������������� ��� ��������� ����� ������');
  abort;
end;

ADQ.Next;
end;

end;

procedure TfFORM_RASPISANIEf.DobavNagrNaPrepod;
var
pZ1,pZ2:string;
begin

  fNagruzkaPlusf.ShowModal;
  abort;

  GetCurrentElement;

 if Inttostr(Integer(pCurrent_Combo_tip_zanyatiya_id.Items.Objects[pCurrent_Combo_tip_zanyatiya_id.ItemIndex]))='2' then begin
  pZ1:='1';
  pZ2:='0';
 end;

 if Inttostr(Integer(pCurrent_Combo_tip_zanyatiya_id.Items.Objects[pCurrent_Combo_tip_zanyatiya_id.ItemIndex]))='3' then begin
  pZ1:='0';
  pZ2:='1';
 end;

  pS:='INSERT into '+
  'nagruzka_na_prepodavatelya '+
  '('+
  'prepodavatelmz_id,'+
  'god_id,'+
  'polugodie_id,'+
  'distsiplina_id,'+
  'gruppa_id,'+
  'podgruppa_id,'+
  'vid_nagruzki_id,'+
  'teoriya_chasov_itogo,'+
  'praktika_chasov_itogo,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+

  ' '''+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]))+''', '+
  ' '''+pCurrent_God_id+''', '+
  ' '''+pCurrent_Polugodie_id+''', '+
  ' '''+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex]))+''', '+
  ' '''+pGRUPPA_ID+''', '+
  ' '''+Inttostr(Integer(pCurrent_Combo_podgruppa_id.Items.Objects[pCurrent_Combo_podgruppa_id.ItemIndex]))+''', '+
  ' ''2'', '+ //����������
  ' '''+pZ1+''', '+
  ' '''+pZ2+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

procedure TfFORM_RASPISANIEf.Check_Kabinet_Zanyat;
begin

//ShowMessage(Inttostr(Integer(pCurrent_Combo_kabinet_id.Items.Objects[pCurrent_Combo_kabinet_id.ItemIndex])));


if Inttostr(Integer(pCurrent_Combo_kabinet_id.Items.Objects[pCurrent_Combo_kabinet_id.ItemIndex]))='33' then exit;


pS:='Select '+
'raspisanie.id, '+
'gruppa.nazvanie_gruppy '+
'from raspisanie,gruppa '+
'where '+
'raspisanie.gruppa_id=gruppa.id and '+
'raspisanie.para_id='+pPara_id+' and '+
'raspisanie.kabinet_id='+Inttostr(Integer(pCurrent_Combo_kabinet_id.Items.Objects[pCurrent_Combo_kabinet_id.ItemIndex]))+' and '+
'raspisanie.god_id='+pCurrent_God_id+' and '+
'raspisanie.polugodie_id='+pCurrent_Polugodie_id+' and '+
'raspisanie.data=#'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(ListView1.Selected.Caption)), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#';

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.Open;

if ADQ.RecordCount>0 then begin
  ShowMessage('�� ������ ����� � ������ '+ADQ.fieldbyname('nazvanie_gruppy').AsString+' ��� ������� ���� � ������ ��������');
  abort;
end;


end;

procedure TfFORM_RASPISANIEf.Raschet;
var
p1,p2:integer;
begin

GetCurrentElement;

    pS:=
   'Select '+
   'sum(praktika_chasov_itogo) as sum_praktika_chasov_itogo, '+
   'sum(teoriya_chasov_itogo) as sum_teoriya_chasov_itogo '+
   'from nagruzka_na_prepodavatelya where '+
   'deleted=0 and  '+
   'gruppa_id='+Inttostr(Integer(Combo_gruppa.Items.Objects[Combo_gruppa.ItemIndex]))+' and '+
   'distsiplina_id='+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex]))+' and '+
   'prepodavatelmz_id='+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]))+' and '+
   'podgruppa_id='+Inttostr(Integer(pCurrent_Combo_podgruppa_id.Items.Objects[pCurrent_Combo_podgruppa_id.ItemIndex]))+' and '+
   'god_id='+pCurrent_God_id+' and '+
   'polugodie_id='+pCurrent_Polugodie_id;

   ADQ1.Close;
   ADQ1.SQL.Clear;
   ADQ1.SQL.Add(pS);
   ADQ1.Open;

   if Inttostr(Integer(pCurrent_Combo_tip_zanyatiya_id.Items.Objects[pCurrent_Combo_tip_zanyatiya_id.ItemIndex]))='1' then begin
     label10.Caption:='����� ��������: #';
   end;

   if Inttostr(Integer(pCurrent_Combo_tip_zanyatiya_id.Items.Objects[pCurrent_Combo_tip_zanyatiya_id.ItemIndex]))='2' then begin
     label10.Caption:='����� ��������: '+ ADQ1.fieldbyname('sum_teoriya_chasov_itogo').AsString;
     p1:=ADQ1.fieldbyname('sum_teoriya_chasov_itogo').AsInteger;
   end;

   if Inttostr(Integer(pCurrent_Combo_tip_zanyatiya_id.Items.Objects[pCurrent_Combo_tip_zanyatiya_id.ItemIndex]))='3' then begin
     label10.Caption:='����� ��������: '+ ADQ1.fieldbyname('sum_praktika_chasov_itogo').AsString;
     p1:=ADQ1.fieldbyname('sum_praktika_chasov_itogo').AsInteger;
   end;



   if Inttostr(Integer(pCurrent_Combo_tip_zanyatiya_id.Items.Objects[pCurrent_Combo_tip_zanyatiya_id.ItemIndex]))='1' then begin
     label11.Caption:='����� �� �����: #';
   end;

   if Inttostr(Integer(pCurrent_Combo_tip_zanyatiya_id.Items.Objects[pCurrent_Combo_tip_zanyatiya_id.ItemIndex]))='2' then begin

   pS:=
   'Select count(id) as count_par_teoriya '+
   'from raspisanie where '+
   'deleted=0 and  '+
   'tip_zanyatiya_id=2 and '+
   'gruppa_id='+Inttostr(Integer(Combo_gruppa.Items.Objects[Combo_gruppa.ItemIndex]))+' and '+
   'distsiplina_id='+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex]))+' and '+
   'prepodavatelmz_id='+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]))+' and '+
   'podgruppa_id='+Inttostr(Integer(pCurrent_Combo_podgruppa_id.Items.Objects[pCurrent_Combo_podgruppa_id.ItemIndex]))+' and '+
   'god_id='+pCurrent_God_id+' and '+
   'polugodie_id='+pCurrent_Polugodie_id;

   ADQ1.Close;
   ADQ1.SQL.Clear;
   ADQ1.SQL.Add(pS);
   ADQ1.Open;

   pCount_par_teoriya:=inttostr(ADQ1.fieldbyname('count_par_teoriya').AsInteger*2);

   label11.Caption:='����� �� �����: '+pCount_par_teoriya;
   p2:=strtoint(pCount_par_teoriya);

  end;


   if Inttostr(Integer(pCurrent_Combo_tip_zanyatiya_id.Items.Objects[pCurrent_Combo_tip_zanyatiya_id.ItemIndex]))='3' then begin

   pS:=
   'Select count(id) as count_par_teoriya '+
   'from raspisanie where '+
   'deleted=0 and  '+
   'tip_zanyatiya_id=3 and '+
   'gruppa_id='+Inttostr(Integer(Combo_gruppa.Items.Objects[Combo_gruppa.ItemIndex]))+' and '+
   'distsiplina_id='+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex]))+' and '+
   'prepodavatelmz_id='+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]))+' and '+
   'podgruppa_id='+Inttostr(Integer(pCurrent_Combo_podgruppa_id.Items.Objects[pCurrent_Combo_podgruppa_id.ItemIndex]))+' and '+
   'god_id='+pCurrent_God_id+' and '+
   'polugodie_id='+pCurrent_Polugodie_id;

   ADQ1.Close;
   ADQ1.SQL.Clear;
   ADQ1.SQL.Add(pS);
   ADQ1.Open;

   pCount_par_praktika:=inttostr(ADQ1.fieldbyname('count_par_teoriya').AsInteger*4);

   label11.Caption:='����� �� �����: '+pCount_par_praktika;
   p2:=strtoint(pCount_par_praktika);

  end;

  if Inttostr(Integer(pCurrent_Combo_tip_zanyatiya_id.Items.Objects[pCurrent_Combo_tip_zanyatiya_id.ItemIndex]))='1' then label13.Caption:='#'
  else label13.Caption:= inttostr(p1-p2);



end;

procedure TfFORM_RASPISANIEf.LoadGRUPPA;
begin

Combo_gruppa.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from gruppa where deleted=0 order by nazvanie_gruppy');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  if ADQ.FieldByName('yazyk_obucheniya_id').AsString='1' then Combo_gruppa.AddItem(ADQ.FieldByName('nazvanie_gruppy').AsString+'-���', Pointer(ADQ.FieldByName('id').AsInteger));
  if ADQ.FieldByName('yazyk_obucheniya_id').AsString='2' then Combo_gruppa.AddItem(ADQ.FieldByName('nazvanie_gruppy').AsString+'-���', Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then begin
  Combo_gruppa.ItemIndex:=0;
  pGRUPPA_ID:=Inttostr(Integer(Combo_gruppa.Items.Objects[Combo_gruppa.ItemIndex]));
  Combo_gruppa.DropDownCount:=ADQ.RecordCount;
end;

end;


procedure TfFORM_RASPISANIEf.LOAD_KABINET;
begin

ADQ.Close;
ADQ.SQL.Clear;

ADQ.SQL.Add(
'Select '+
'kabinet.id,'+
'kabinet.nomer_kabineta '+
'from '+
'kabinet '+
'where '+
'kabinet.deleted=0'
);

ADQ.Open;

ADQ.Last;
ADQ.First;

while not ADQ.Eof do begin
//  pCurrent_Combo_kabinet_id.AddItem(ADQ1.FieldByName('nomer_kabineta').AsString, Pointer(ADQ1.FieldByName('id').AsInteger));
  Combo_kabinet11_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_kabinet12_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_kabinet13_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_kabinet21_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_kabinet22_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_kabinet23_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_kabinet31_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_kabinet32_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_kabinet33_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_kabinet41_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_kabinet42_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_kabinet43_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));


  Combo_kabinet51_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_kabinet52_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_kabinet53_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_kabinet61_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_kabinet62_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_kabinet63_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_kabinet71_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_kabinet72_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_kabinet73_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));



  ADQ.Next;
end;

if ADQ.RecordCount>0 then begin

Combo_kabinet11_id.ItemIndex:=0;
Combo_kabinet12_id.ItemIndex:=0;
Combo_kabinet13_id.ItemIndex:=0;

Combo_kabinet21_id.ItemIndex:=0;
Combo_kabinet22_id.ItemIndex:=0;
Combo_kabinet23_id.ItemIndex:=0;

Combo_kabinet31_id.ItemIndex:=0;
Combo_kabinet32_id.ItemIndex:=0;
Combo_kabinet33_id.ItemIndex:=0;

Combo_kabinet41_id.ItemIndex:=0;
Combo_kabinet42_id.ItemIndex:=0;
Combo_kabinet43_id.ItemIndex:=0;

Combo_kabinet51_id.ItemIndex:=0;
Combo_kabinet52_id.ItemIndex:=0;
Combo_kabinet53_id.ItemIndex:=0;

Combo_kabinet61_id.ItemIndex:=0;
Combo_kabinet62_id.ItemIndex:=0;
Combo_kabinet63_id.ItemIndex:=0;

Combo_kabinet71_id.ItemIndex:=0;
Combo_kabinet72_id.ItemIndex:=0;
Combo_kabinet73_id.ItemIndex:=0;







Combo_kabinet11_id.DropDownCount:=ADQ.RecordCount-30;
Combo_kabinet12_id.DropDownCount:=ADQ.RecordCount-30;
Combo_kabinet13_id.DropDownCount:=ADQ.RecordCount-30;

Combo_kabinet21_id.DropDownCount:=ADQ.RecordCount-30;
Combo_kabinet22_id.DropDownCount:=ADQ.RecordCount-30;
Combo_kabinet23_id.DropDownCount:=ADQ.RecordCount-30;

Combo_kabinet31_id.DropDownCount:=ADQ.RecordCount-30;
Combo_kabinet32_id.DropDownCount:=ADQ.RecordCount-30;
Combo_kabinet33_id.DropDownCount:=ADQ.RecordCount-30;

Combo_kabinet41_id.DropDownCount:=ADQ.RecordCount-30;
Combo_kabinet42_id.DropDownCount:=ADQ.RecordCount-30;
Combo_kabinet43_id.DropDownCount:=ADQ.RecordCount-30;

Combo_kabinet51_id.DropDownCount:=ADQ.RecordCount-30;
Combo_kabinet52_id.DropDownCount:=ADQ.RecordCount-30;
Combo_kabinet53_id.DropDownCount:=ADQ.RecordCount-30;

Combo_kabinet61_id.DropDownCount:=ADQ.RecordCount-30;
Combo_kabinet62_id.DropDownCount:=ADQ.RecordCount-30;
Combo_kabinet63_id.DropDownCount:=ADQ.RecordCount-30;

Combo_kabinet71_id.DropDownCount:=ADQ.RecordCount;
Combo_kabinet72_id.DropDownCount:=ADQ.RecordCount;
Combo_kabinet73_id.DropDownCount:=ADQ.RecordCount;

 
end;

end;

procedure TfFORM_RASPISANIEf.ClearCombo;
var
i:integer;
begin
  for i := 0 to ComponentCount - 1 do
    if (Components[i] is TComboBox) then
      begin
        TComboBox(Components[i]).Clear;
      end;
end;

procedure TfFORM_RASPISANIEf.Check_21;    //�������� �� ����� ���������� ����� ���������� ����� ����������� �� ����� 50 ��������� ������������� �����
var
pSum_teoriya_chasov_itogo1,pCount_par_teoriya1:real;
begin

  if Inttostr(Integer(pCurrent_Combo_tip_zanyatiya_id.Items.Objects[pCurrent_Combo_tip_zanyatiya_id.ItemIndex]))='3' then begin

   pS:=
   'Select '+
   'sum(teoriya_chasov_itogo) as sum_teoriya_chasov_itogo '+
   'from nagruzka_na_prepodavatelya where '+
   'deleted=0 and  '+
   'distsiplina_id='+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex]))+' and '+
   'gruppa_id='+pGRUPPA_ID+' and '+
   'god_id='+pCurrent_God_id+' and '+
   'polugodie_id='+pCurrent_Polugodie_id;
   
//   'podgruppa_id='+Inttostr(Integer(pCurrent_Combo_podgruppa_id.Items.Objects[pCurrent_Combo_podgruppa_id.ItemIndex]))+' and '+
//   'prepodavatelmz_id='+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]))+' and '+

//   ShowMessage(pS);
//   Clipboard.Astext:=pS;

   ADQ1.Close;
   ADQ1.SQL.Clear;
   ADQ1.SQL.Add(pS);
   ADQ1.Open;

   pSum_teoriya_chasov_itogo1:=ADQ1.fieldbyname('sum_teoriya_chasov_itogo').AsFloat;

   //������
   pS:=
   'Select count(id) as count_par_teoriya '+
   'from raspisanie where '+
   'deleted=0 and  '+
   'tip_zanyatiya_id=2 and '+
   'distsiplina_id='+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex]))+' and '+
   'gruppa_id='+pGRUPPA_ID+' and '+
   'god_id='+pCurrent_God_id+' and '+
   'polugodie_id='+pCurrent_Polugodie_id;

//   'podgruppa_id='+Inttostr(Integer(pCurrent_Combo_podgruppa_id.Items.Objects[pCurrent_Combo_podgruppa_id.ItemIndex]))+' and '+
//   'prepodavatelmz_id='+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]))+' and '+


   ADQ1.Close;
   ADQ1.SQL.Clear;
   ADQ1.SQL.Add(pS);
   ADQ1.Open;

   pCount_par_teoriya1:=ADQ1.fieldbyname('count_par_teoriya').asfloat*2;

   if pCount_par_teoriya1<(pSum_teoriya_chasov_itogo1/2)
   then begin
    ShowMessage('�������� �� ������� ���������� ����� ���������� ����� ����������� �� ����� 50 ��������� '+
    '������������� �����'+#13#10+'�� ������� ������ �������� '+floattostr(pCount_par_teoriya1)+' �� '+floattostr(pSum_teoriya_chasov_itogo1));
    abort;
   end;

   end;

end;

procedure TfFORM_RASPISANIEf.Progul;
begin

GetCurrentElement;

fPRICHINA_MSGf.ShowModal;

if pOtmena=true then abort;

// ���� ������� ������� �������
  pS:='INSERT into '+
  'reestr_progulov '+
  '('+
  'god_id,'+
  'polugodie_id,'+
  'data_progula,'+
  'para_id,'+
  'nomer_stroki,'+
  'prepodavatelmz_id,'+
  'distsiplina_id,'+
  'gruppa_id,'+
  'podgruppa_id,'+
  'tip_zanyatiya_id,'+
  'kabinet_id,'+
  'prichina_progula,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+pCurrent_God_id+''', '+
  ' '''+pCurrent_Polugodie_id+''', '+
  ' '''+ListView1.Selected.Caption+''', '+
  ' '''+pPara_id+''', '+
  ' '''+pStroka+''', '+
  ' '''+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex]))+''', '+
 ' '''+pGRUPPA_ID+''', '+
  ' '''+Inttostr(Integer(pCurrent_Combo_podgruppa_id.Items.Objects[pCurrent_Combo_podgruppa_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(pCurrent_Combo_tip_zanyatiya_id.Items.Objects[pCurrent_Combo_tip_zanyatiya_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(pCurrent_Combo_kabinet_id.Items.Objects[pCurrent_Combo_kabinet_id.ItemIndex]))+''', '+
  ' '''+trim(pPRICHINA_MSG)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;


end;

procedure TfFORM_RASPISANIEf.ClearRecord;
begin

pS:='UPDATE '+
  'raspisanie set ' +

  'distsiplina_id=''1'','+
  'prepodavatelmz_id=''1'','+
  'kabinet_id=''1'','+
  'tip_zanyatiya_id=''1'','+
  'podgruppa_id=''1'','+
  'fizkultura=''0'','+
  'sportzal=''0'','+
  'vid_nagruzki_id=''1'','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where '+

  'para_id='+pPara_id+' and '+
  'stroka='+pStroka+' and '+
  'gruppa_id='+pGRUPPA_ID+' and '+
  'data=#'+FormatDateTime('MM.DD.YYYY',strtodate(ListView1.Selected.Caption))+'#';
  pS  := StringReplace(pS, '.', '/',[rfReplaceAll, rfIgnoreCase]);

//  ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,1);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,1);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,1);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,1);
GetComboRecord(pCurrent_Combo_kabinet_id,1);

end;



procedure TfFORM_RASPISANIEf.Check_1;    //�������� �� ���������������� ������������� ����� ��������
begin

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from prepodavatelmz where deleted=0 and id='+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex])));
ADQ.Open;

if (ADQ.FieldByName('shtatnyy').AsString='True') and
   (strtoint(pPara_id)<ADQ.FieldByName('para_id').AsInteger) then begin
   ShowMessage('������ ������������� ������������ �������� ������� ���� � ���� '+ADQ.FieldByName('para_id').AsString);
//   ClearRecord;
   abort;
   end;

end;

procedure TfFORM_RASPISANIEf.Check_23;    //������� ������������� �� ����� �������� � ���� 3 ��������� ������������ ������� � ����� ������ �������� 2
var
pKol:integer;
begin

exit;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from prepodavatelmz where id='+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex])));
ADQ.Open;

if ADQ.FieldByName('shtatnyy').AsString='True' then pKol:=2;
if ADQ.FieldByName('shtatnyy').AsString='False' then pKol:=1;


pS:='Select distinct podgruppa_id from raspisanie where '+
'deleted=0 and  '+
'tip_zanyatiya_id=3 and '+
'distsiplina_id='+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex]))+' and '+
'gruppa_id='+pGRUPPA_ID+' and '+
'god_id='+pCurrent_God_id+' and '+
'polugodie_id='+pCurrent_Polugodie_id+' and '+
'data=#'+FormatDateTime('MM.DD.YYYY',strtodate(ListView1.Selected.Caption))+'#';
pS  := StringReplace(pS, '.', '/',[rfReplaceAll, rfIgnoreCase]);

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.Open;

if ADQ.RecordCount=pKol then begin
  if pKol=1 then ShowMessage('������� ������������� �������� ���������� � �� ����� ��������� ����� ������ ������������� ������� � ����� ������');
  if pKol=2 then ShowMessage('������� ������������� �������� ������� � �� ����� ��������� ����� ���� ������������ ������� � ����� ������');
//  ClearRecord;
  abort;
end;

end;

procedure TfFORM_RASPISANIEf.Check_25;    //���������� �� ����� ��������� ������������� ������� �� 5-� ����
begin

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from prepodavatelmz where id='+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex])));
ADQ.Open;

if (ADQ.FieldByName('shtatnyy').AsString='False') and
   (Inttostr(Integer(pCurrent_Combo_tip_zanyatiya_id.Items.Objects[pCurrent_Combo_tip_zanyatiya_id.ItemIndex]))='2') and
   (strtoint(pPara_id)<5) then begin

   ShowMessage('���������� �� ����� ��������� ������������� ������� �� 5-� ����');
//   ClearRecord;
   abort;
   end;

end;

procedure TfFORM_RASPISANIEf.Check_24;    //� ��������� ����� ���������� �� ����� 2-� �����
var
pListGrup,pXX:string;
begin

//ShowMessage('�����');
{
pS:='Select distinct gruppa_id from raspisanie where '+
'deleted=0 and  '+
'fizkultura=1 and '+
'sportzal=1 and '+
'para_id='+pPara_id+' and '+
'god_id='+pCurrent_God_id+' and '+
'polugodie_id='+pCurrent_Polugodie_id+' and '+
'data=#'+FormatDateTime('MM.DD.YYYY',strtodate(ListView1.Selected.Caption))+'#';
pS  := StringReplace(pS, '.', '/',[rfReplaceAll, rfIgnoreCase]);
}




pXX:=FormatDateTime('MM.DD.YYYY',strtodate(ListView1.Selected.Caption));
pXX  := StringReplace(pXX, '.', '/',[rfReplaceAll, rfIgnoreCase]);

pS:='Select gruppa.nazvanie_gruppy from  raspisanie, gruppa where  raspisanie.gruppa_id=gruppa.id and '+
'raspisanie.deleted=0 and  '+
'raspisanie.fizkultura=1 and '+
'raspisanie.sportzal=1 and '+
'raspisanie.para_id='+pPara_id+' and '+
'raspisanie.god_id='+pCurrent_God_id+' and '+
'raspisanie.polugodie_id='+pCurrent_Polugodie_id+' and '+
'raspisanie.data=#'+pXX+'#';

//Clipboard.AsText:=pS;
//ShowMessage(pS);


ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.Open;

pListGrup:='';


//ShowMessage(inttostr(ADQ.RecordCount));

if ADQ.RecordCount>1 then begin

ADQ.First;

while not ADQ.Eof do begin
  pListGrup:=pListGrup+ADQ.fieldbyname('nazvanie_gruppy').AsString+',';
  ADQ.Next;
end;

pListGrup:=copy(pListGrup,1,length(pListGrup)-1);

  ShowMessage('� ��������� ����� ���������� �� ����� 2-� �����'+#13#10+'�� ������� ������ ��������� ������ ���������� � ������ ����� '+pListGrup);
//  ClearRecord;
  abort;
end;

end;



//�� ����� ���������� �������� �� ����� 2 ��� � ����
procedure TfFORM_RASPISANIEf.Check_20;
begin

pS:='Select distinct para_id from raspisanie where '+
'deleted=0 and  '+
'gruppa_id='+pGRUPPA_ID+' and '+
'podgruppa_id='+Inttostr(Integer(pCurrent_Combo_podgruppa_id.Items.Objects[pCurrent_Combo_podgruppa_id.ItemIndex]))+' and '+
'distsiplina_id='+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex]))+' and '+
'tip_zanyatiya_id=3 and '+
'god_id='+pCurrent_God_id+' and '+
'polugodie_id='+pCurrent_Polugodie_id+' and '+
'data=#'+FormatDateTime('MM.DD.YYYY',strtodate(ListView1.Selected.Caption))+'#';
pS  := StringReplace(pS, '.', '/',[rfReplaceAll, rfIgnoreCase]);

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.Open;

if ADQ.RecordCount=2 then begin
  ShowMessage('�� ����� ���������� �������� �� ����� 2 ��� � ����');
//  ClearRecord;
  abort;
end;

end;

//�� ����� ���������� ������ �� ����� 1 ���� � ����
procedure TfFORM_RASPISANIEf.Check_19;
begin

if Inttostr(Integer(pCurrent_Combo_tip_zanyatiya_id.Items.Objects[pCurrent_Combo_tip_zanyatiya_id.ItemIndex]))='2' then begin

pS:=
'Select distinct para_id from raspisanie where '+
'deleted=0 and  '+
'gruppa_id='+pGRUPPA_ID+' and '+
'distsiplina_id='+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex]))+' and '+
'tip_zanyatiya_id=2 and '+
'god_id='+pCurrent_God_id+' and '+
'polugodie_id='+pCurrent_Polugodie_id+' and '+
'data=#'+FormatDateTime('MM.DD.YYYY',strtodate(ListView1.Selected.Caption))+'#';
pS  := StringReplace(pS, '.', '/',[rfReplaceAll, rfIgnoreCase]);

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.Open;

if ADQ.RecordCount=1 then begin
  ShowMessage('�� ����� ���������� ������ �� ����� 1 ���� � ����');
//  ClearRecord;
  abort;
end;

end;

end;

//����������� �� ����� 2-� ��� � ������ � �� ����� 1 ���� � ����
procedure TfFORM_RASPISANIEf.Check_18;
begin


ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from distsiplina where id='+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex])));
ADQ.Open;

if ADQ.FieldByName('fizkultura').AsInteger=0 then exit;



pS:='Select distinct para_id from raspisanie where '+
'deleted=0 and  '+
'gruppa_id='+pGRUPPA_ID+' and '+
'fizkultura=1 and '+
'god_id='+pCurrent_God_id+' and '+
'polugodie_id='+pCurrent_Polugodie_id+' and '+
'data=#'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(ListView1.Selected.Caption)), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#';

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.Open;

if ADQ.RecordCount=1 then begin
  ShowMessage('����������� ����� ���� �� ����� 1 ���� � ����');
//  ClearRecord;
  abort;
end;

pS:='Select distinct data from raspisanie where '+
'deleted=0 and  '+
'gruppa_id='+pGRUPPA_ID+' and '+
'fizkultura=1 and '+
'god_id='+pCurrent_God_id+' and '+
'polugodie_id='+pCurrent_Polugodie_id+' and '+
'nomer_nedeli='+inttostr(fCREATE_DATEf.WeekNum(strtodate(ListView1.Selected.Caption)));

//pS  := StringReplace(pS, '.', '/',[rfReplaceAll, rfIgnoreCase]);

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.Open;

if ADQ.RecordCount=2 then begin
  ShowMessage('����������� ����� ���� �� ����� 2 ��� � ������');
//  ClearRecord;
  abort;
end;

end;

//������� �������� �� ������ �� ����� 4 ���
procedure TfFORM_RASPISANIEf.Check_17;
begin

pS:='Select distinct para_id from raspisanie where '+
'distsiplina_id<>1 and deleted=0 and  '+
'gruppa_id='+pGRUPPA_ID+' and '+
'god_id='+pCurrent_God_id+' and '+
'polugodie_id='+pCurrent_Polugodie_id+' and '+
'data=#'+FormatDateTime('MM.DD.YYYY',strtodate(ListView1.Selected.Caption))+'#';

pS  := StringReplace(pS, '.', '/',[rfReplaceAll, rfIgnoreCase]);

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.Open;

if ADQ.RecordCount=7 then begin
  ShowMessage('������� �������� �� ������ �� ����� 4 ���');
//  ClearRecord;
  abort;
end;

end;

//������� �������� �� �������� ������������� ���������� �� ����� 4 ���
procedure TfFORM_RASPISANIEf.Check_15_16;
var
pShtat:string;
begin

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(
'Select id,shtatnyy from prepodavatelmz where '+
'id='+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]))
);
ADQ.Open;

pShtat:=ADQ.fieldbyname('shtatnyy').AsString;

pS:=
'Select distinct para_id from raspisanie where '+
'deleted=0 and  '+
'prepodavatelmz_id='+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]))+' and '+
'god_id='+pCurrent_God_id+' and '+
'polugodie_id='+pCurrent_Polugodie_id+' and '+
'data=#'+FormatDateTime('MM.DD.YYYY',strtodate(ListView1.Selected.Caption))+'#';

pS  := StringReplace(pS, '.', '/',[rfReplaceAll, rfIgnoreCase]);

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.Open;


if pShtat='False' then begin
if ADQ.RecordCount=3 then begin
  ShowMessage('������� �������� �� ����������� ������������� �� ����� ���� ����� 3-� ���');
//  ClearRecord;
  abort;
end;
end;

if pShtat='True' then begin
if ADQ.RecordCount=4 then begin
  ShowMessage('������� �������� �� �������� ������������� �� ����� ���� ����� 4-� ���');
//  ClearRecord;
  abort;
end;
end;

end;

//���� ������������� � ���� � �� �� ����� �� ����� ����� ���������� � ������ �������
procedure TfFORM_RASPISANIEf.Check_14;
begin


pS:='Select id from raspisanie where '+
'deleted=0 and  '+
'para_id='+pPara_id+' and '+
'prepodavatelmz_id='+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]))+' and '+
'god_id='+pCurrent_God_id+' and '+
'polugodie_id='+pCurrent_Polugodie_id+' and '+
'data=#'+FormatDateTime('MM.DD.YYYY',strtodate(ListView1.Selected.Caption))+'#';
pS  := StringReplace(pS, '.', '/',[rfReplaceAll, rfIgnoreCase]);

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
//Clipboard.astext:=ADQ.SQL.Text;
//ShowMessage(ADQ.SQL.Text);
ADQ.Open;

if ADQ.RecordCount>0 then begin
  ShowMessage('�� ���� '+ListView1.Selected.Caption+' � ������������� '+pCurrent_Combo_prepodavatelmz_id.Text+' ��� ������� ����');
//  ClearRecord;
  abort;
end;

end;


procedure TfFORM_RASPISANIEf.UpdateRecord;
var
pFIZKULTURA:string;
pDate:string;
begin

GetCurrentElement;


if pCurrent_Combo_distsiplina_id.Text='-' then begin
  pCurrent_Combo_distsiplina_id.SetFocus;
  ShowMessage('������� ����������');
  abort;
end;

if pCurrent_Combo_prepodavatelmz_id.Text='' then begin
  pCurrent_Combo_prepodavatelmz_id.SetFocus;
  ShowMessage('������������� �� ���������');
  abort;
end;

if pCurrent_Combo_podgruppa_id.Text='' then begin
  pCurrent_Combo_podgruppa_id.SetFocus;
  ShowMessage('������/��������� �� ����������');
  abort;
end;

if pCurrent_Combo_tip_zanyatiya_id.Text='-' then begin
  pCurrent_Combo_tip_zanyatiya_id.SetFocus;
  ShowMessage('��� ������� �� ���������');
  abort;
end;

if pCurrent_Combo_kabinet_id.Text='-' then begin
  pCurrent_Combo_kabinet_id.SetFocus;
  ShowMessage('������� �� ���������');
  abort;
end;

Check_Kabinet_Zanyat;     //�������� �� ���������� ��������
Check_1;                  //�������� �� ���������������� ������������� ����� ��������

//������� ������������� �� ����� �������� � ���� 3 ��������� ������������ ������� � ����� ������ �������� 2
if Inttostr(Integer(pCurrent_Combo_tip_zanyatiya_id.Items.Objects[pCurrent_Combo_tip_zanyatiya_id.ItemIndex]))='3' then Check_23;


//� ��������� ����� ���������� �� ����� 2-� �����
if Inttostr(Integer(pCurrent_Combo_kabinet_id.Items.Objects[pCurrent_Combo_kabinet_id.ItemIndex]))='33' then Check_24;



//Check_25;    //���������� �� ����� ��������� ������������� ������� �� 5-� ����
//Check_14;    //���� ������������� � ���� � �� �� ����� �� ����� ����� ���������� � ������ �������
//Check_15_16; //������� �������� �� �������� ������������� ���������� �� ����� 4 ���
//Check_17;    //������� �������� �� ������ �� ����� 4 ���
//Check_18;    //����������� �� ����� 2-� ��� � ������ � �� ����� 1 ���� � ����
//Check_19;    //�� ����� ���������� ������ �� ����� 1 ���� � ����
//Check_20;    //�� ����� ���������� �������� �� ����� 2 ��� � ����
//Check_21;    //�������� �� ����� ���������� ����� ���������� ����� ����������� �� ����� 50 ��������� ������������� �����



ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select fizkultura from distsiplina where id='+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex])));
ADQ.Open;
pFIZKULTURA:=ADQ.fieldbyname('fizkultura').AsString;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select sportzal from kabinet where id='+Inttostr(Integer(pCurrent_Combo_kabinet_id.Items.Objects[pCurrent_Combo_kabinet_id.ItemIndex])));
ADQ.Open;
pSPORTZAL:=ADQ.fieldbyname('sportzal').AsString;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select vid_nagruzki_id from nagruzka_na_prepodavatelya where '+
'god_id='+pCurrent_God_id+' and '+
'polugodie_id='+pCurrent_Polugodie_id+' and '+
'gruppa_id='+pGRUPPA_ID+' and '+
'distsiplina_id='+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex]))+' and '+
'podgruppa_id='+Inttostr(Integer(pCurrent_Combo_podgruppa_id.Items.Objects[pCurrent_Combo_podgruppa_id.ItemIndex]))+' and '+
'prepodavatelmz_id='+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex])) );
//ShowMessage(ADQ.SQL.Text);
ADQ.Open;
pVID_NAGRUZKI_ID:=ADQ.fieldbyname('vid_nagruzki_id').AsString;

if trim(pVID_NAGRUZKI_ID)='' then begin
ShowMessage('�� ������ ���������� � �������� ������������� �� ������ ��������� �������� �����������');
abort;
end;

pS:='UPDATE '+
  'raspisanie set ' +

  'distsiplina_id='''+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex]))+''','+
  'prepodavatelmz_id='''+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]))+''','+
  'kabinet_id='''+Inttostr(Integer(pCurrent_Combo_kabinet_id.Items.Objects[pCurrent_Combo_kabinet_id.ItemIndex]))+''','+
  'tip_zanyatiya_id='''+Inttostr(Integer(pCurrent_Combo_tip_zanyatiya_id.Items.Objects[pCurrent_Combo_tip_zanyatiya_id.ItemIndex]))+''','+
  'podgruppa_id='''+Inttostr(Integer(pCurrent_Combo_podgruppa_id.Items.Objects[pCurrent_Combo_podgruppa_id.ItemIndex]))+''','+
  'fizkultura='''+pFIZKULTURA+''','+
  'sportzal='''+pSPORTZAL+''','+
  'vid_nagruzki_id='''+pVID_NAGRUZKI_ID+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where '+

  'para_id='+pPara_id+' and '+
  'stroka='+pStroka+' and '+
  'gruppa_id='+pGRUPPA_ID+' and '+
  'data=#'+FormatDateTime('MM.DD.YYYY',strtodate(ListView1.Selected.Caption))+'#';
  pS  := StringReplace(pS, '.', '/',[rfReplaceAll, rfIgnoreCase]);




//  Clipboard.AsText:=pS;
//  ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

procedure TfFORM_RASPISANIEf.GetCurrentElement;
begin

if (pPara_id='1') and (pStroka='1') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina11_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz11_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa11_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya11_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet11_id;
end;

if (pPara_id='1') and (pStroka='2') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina12_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz12_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa12_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya12_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet12_id;
end;

if (pPara_id='1') and (pStroka='3') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina13_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz13_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa13_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya13_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet13_id;
end;

if (pPara_id='2') and (pStroka='1') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina21_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz21_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa21_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya21_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet21_id;
end;

if (pPara_id='2') and (pStroka='2') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina22_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz22_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa22_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya22_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet22_id;
end;

if (pPara_id='2') and (pStroka='3') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina23_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz23_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa23_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya23_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet23_id;
end;

if (pPara_id='3') and (pStroka='1') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina31_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz31_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa31_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya31_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet31_id;
end;

if (pPara_id='3') and (pStroka='2') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina32_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz32_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa32_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya32_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet32_id;
end;

if (pPara_id='3') and (pStroka='3') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina33_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz33_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa33_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya33_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet33_id;
end;

if (pPara_id='4') and (pStroka='1') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina41_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz41_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa41_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya41_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet41_id;
end;

if (pPara_id='4') and (pStroka='2') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina42_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz42_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa42_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya42_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet42_id;
end;

if (pPara_id='4') and (pStroka='3') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina43_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz43_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa43_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya43_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet43_id;
end;

if (pPara_id='5') and (pStroka='1') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina51_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz51_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa51_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya51_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet51_id;
end;

if (pPara_id='5') and (pStroka='2') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina52_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz52_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa52_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya52_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet52_id;
end;

if (pPara_id='5') and (pStroka='3') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina53_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz53_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa53_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya53_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet53_id;
end;

if (pPara_id='6') and (pStroka='1') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina61_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz61_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa61_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya61_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet61_id;
end;

if (pPara_id='6') and (pStroka='2') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina62_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz62_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa62_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya62_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet62_id;
end;

if (pPara_id='6') and (pStroka='3') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina63_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz63_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa63_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya63_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet63_id;
end;

if (pPara_id='7') and (pStroka='1') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina71_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz71_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa71_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya71_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet71_id;
end;

if (pPara_id='7') and (pStroka='2') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina72_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz72_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa72_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya72_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet72_id;
end;

if (pPara_id='7') and (pStroka='3') then begin
  pCurrent_Combo_distsiplina_id:=Combo_distsiplina73_id;
  pCurrent_Combo_prepodavatelmz_id:=Combo_prepodavatelmz73_id;
  pCurrent_Combo_podgruppa_id:=Combo_podgruppa73_id;
  pCurrent_Combo_tip_zanyatiya_id:=Combo_tip_zanyatiya73_id;
  pCurrent_Combo_kabinet_id:=Combo_kabinet73_id;
end;

end;

procedure TfFORM_RASPISANIEf.LoadCombo2;
begin

if pCurrent_Combo_distsiplina_id.Text='-' then exit;
GetCurrentElement;


pCurrent_Combo_podgruppa_id.Clear;

ADQ1.Close;
ADQ1.SQL.Clear;
ADQ1.SQL.Add(
'Select '+
'distinct '+
'podgruppa.id,'+
'podgruppa.podgruppa '+
'from '+
'podgruppa,'+
'nagruzka_na_prepodavatelya '+
'where '+
'nagruzka_na_prepodavatelya.deleted=0 and  '+
'podgruppa.id=nagruzka_na_prepodavatelya.podgruppa_id and '+
'nagruzka_na_prepodavatelya.prepodavatelmz_id='+Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]))+' and '+
'nagruzka_na_prepodavatelya.distsiplina_id='+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex]))+' and '+
'nagruzka_na_prepodavatelya.god_id='+pCurrent_God_id+' and '+
'nagruzka_na_prepodavatelya.polugodie_id='+pCurrent_Polugodie_id+' and '+
'nagruzka_na_prepodavatelya.gruppa_id='+pGRUPPA_ID
);

ADQ1.Open;

if ADQ1.RecordCount=0 then begin
pCurrent_Combo_podgruppa_id.Clear;
exit;
end;

ADQ1.Last;
ADQ1.First;

while not ADQ1.Eof do begin
  pCurrent_Combo_podgruppa_id.AddItem(ADQ1.FieldByName('podgruppa').AsString, Pointer(ADQ1.FieldByName('id').AsInteger));
  ADQ1.Next;
end;

if ADQ1.RecordCount>0 then pCurrent_Combo_podgruppa_id.ItemIndex:=0;



end;

procedure TfFORM_RASPISANIEf.LoadCombo1;
begin

ListView2.Clear;
ListView3.Clear;

GetCurrentElement;

if pCurrent_Combo_distsiplina_id.Text='-' then begin
  pCurrent_Combo_prepodavatelmz_id.Clear;
  pCurrent_Combo_podgruppa_id.Clear;
  exit;
end;



pCurrent_Combo_prepodavatelmz_id.Clear;
pCurrent_Combo_podgruppa_id.Clear;

ADQ1.Close;
ADQ1.SQL.Clear;
ADQ1.SQL.Add(
'Select distinct '+
'prepodavatelmz.id,'+
'prepodavatelmz.fio '+
'from '+
'prepodavatelmz,'+
'nagruzka_na_prepodavatelya '+
'where '+
'nagruzka_na_prepodavatelya.deleted=0 and '+
'prepodavatelmz.id=nagruzka_na_prepodavatelya.prepodavatelmz_id and '+
'nagruzka_na_prepodavatelya.distsiplina_id='+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex]))+' and '+
'nagruzka_na_prepodavatelya.god_id='+pCurrent_God_id+' and '+
'nagruzka_na_prepodavatelya.polugodie_id='+pCurrent_Polugodie_id+' and '+
'nagruzka_na_prepodavatelya.gruppa_id='+pGRUPPA_ID+
' order by prepodavatelmz.fio'
);

//Clipboard.AsText:=ADQ1.SQL.Text;

ADQ1.Open;

ADQ1.Last;
ADQ1.First;

while not ADQ1.Eof do begin
  pCurrent_Combo_prepodavatelmz_id.AddItem(ADQ1.FieldByName('fio').AsString, Pointer(ADQ1.FieldByName('id').AsInteger));
  ADQ1.Next;
end;

if ADQ1.RecordCount>0 then pCurrent_Combo_prepodavatelmz_id.ItemIndex:=0;

{
pCurrent_Combo_kabinet_id.Clear;

ADQ1.Close;
ADQ1.SQL.Clear;


ADQ1.SQL.Add(
'Select '+
'kabinet.id,'+
'kabinet.nomer_kabineta '+
'from '+
'kabinet,'+
'kabinety_distsiplin '+
'where '+
'kabinet.id=kabinety_distsiplin.kabinet_id and '+
'kabinety_distsiplin.distsiplina_id='+Inttostr(Integer(pCurrent_Combo_distsiplina_id.Items.Objects[pCurrent_Combo_distsiplina_id.ItemIndex]))
);


ADQ1.SQL.Add(
'Select '+
'kabinet.id,'+
'kabinet.nomer_kabineta '+
'from '+
'kabinet '+
'where '+
'kabinet.deleted=0'
);

ADQ1.Open;

ADQ1.Last;
ADQ1.First;

while not ADQ1.Eof do begin
  pCurrent_Combo_kabinet_id.AddItem(ADQ1.FieldByName('nomer_kabineta').AsString, Pointer(ADQ1.FieldByName('id').AsInteger));
  ADQ1.Next;
end;

if ADQ1.RecordCount>0 then pCurrent_Combo_kabinet_id.ItemIndex:=0;
}
end;


procedure TfFORM_RASPISANIEf.GetComboRecord(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfFORM_RASPISANIEf.LoadTIP_ZANYATIYA;
begin

Combo_tip_zanyatiya11_id.Clear;
Combo_tip_zanyatiya12_id.Clear;
Combo_tip_zanyatiya13_id.Clear;

Combo_tip_zanyatiya21_id.Clear;
Combo_tip_zanyatiya22_id.Clear;
Combo_tip_zanyatiya23_id.Clear;

Combo_tip_zanyatiya31_id.Clear;
Combo_tip_zanyatiya32_id.Clear;
Combo_tip_zanyatiya33_id.Clear;

Combo_tip_zanyatiya41_id.Clear;
Combo_tip_zanyatiya42_id.Clear;
Combo_tip_zanyatiya43_id.Clear;

Combo_tip_zanyatiya51_id.Clear;
Combo_tip_zanyatiya52_id.Clear;
Combo_tip_zanyatiya53_id.Clear;

Combo_tip_zanyatiya61_id.Clear;
Combo_tip_zanyatiya62_id.Clear;
Combo_tip_zanyatiya63_id.Clear;

Combo_tip_zanyatiya71_id.Clear;
Combo_tip_zanyatiya72_id.Clear;
Combo_tip_zanyatiya73_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from tip_zanyatiya where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin

  Combo_tip_zanyatiya11_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_tip_zanyatiya12_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_tip_zanyatiya13_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_tip_zanyatiya21_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_tip_zanyatiya22_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_tip_zanyatiya23_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_tip_zanyatiya31_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_tip_zanyatiya32_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_tip_zanyatiya33_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_tip_zanyatiya41_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_tip_zanyatiya42_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_tip_zanyatiya43_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_tip_zanyatiya51_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_tip_zanyatiya52_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_tip_zanyatiya53_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_tip_zanyatiya61_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_tip_zanyatiya62_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_tip_zanyatiya63_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_tip_zanyatiya71_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_tip_zanyatiya72_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_tip_zanyatiya73_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_tip_zanyatiya11_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_tip_zanyatiya12_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_tip_zanyatiya13_id.ItemIndex:=0;

if ADQ.RecordCount>0 then Combo_tip_zanyatiya21_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_tip_zanyatiya22_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_tip_zanyatiya23_id.ItemIndex:=0;

if ADQ.RecordCount>0 then Combo_tip_zanyatiya31_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_tip_zanyatiya32_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_tip_zanyatiya33_id.ItemIndex:=0;

if ADQ.RecordCount>0 then Combo_tip_zanyatiya41_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_tip_zanyatiya42_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_tip_zanyatiya43_id.ItemIndex:=0;

if ADQ.RecordCount>0 then Combo_tip_zanyatiya51_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_tip_zanyatiya52_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_tip_zanyatiya53_id.ItemIndex:=0;

if ADQ.RecordCount>0 then Combo_tip_zanyatiya61_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_tip_zanyatiya62_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_tip_zanyatiya63_id.ItemIndex:=0;

if ADQ.RecordCount>0 then Combo_tip_zanyatiya71_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_tip_zanyatiya72_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_tip_zanyatiya73_id.ItemIndex:=0;

end;

procedure TfFORM_RASPISANIEf.LoadDISTSIPLINA;
begin

Combo_distsiplina11_id.Clear;
Combo_distsiplina12_id.Clear;
Combo_distsiplina13_id.Clear;

Combo_distsiplina21_id.Clear;
Combo_distsiplina22_id.Clear;
Combo_distsiplina23_id.Clear;

Combo_distsiplina31_id.Clear;
Combo_distsiplina32_id.Clear;
Combo_distsiplina33_id.Clear;

Combo_distsiplina41_id.Clear;
Combo_distsiplina42_id.Clear;
Combo_distsiplina43_id.Clear;

Combo_distsiplina51_id.Clear;
Combo_distsiplina52_id.Clear;
Combo_distsiplina53_id.Clear;

Combo_distsiplina61_id.Clear;
Combo_distsiplina62_id.Clear;
Combo_distsiplina63_id.Clear;

Combo_distsiplina71_id.Clear;
Combo_distsiplina72_id.Clear;
Combo_distsiplina73_id.Clear;

//'Select * from distsiplina where deleted=0 order by id'


if pCurrentLanguage='1' then
pS:=
'Select distinct distsiplina.distsiplina_na_russkom,'+
'distsiplina.id as id  '+
'from '+
'distsiplina,nagruzka_na_prepodavatelya '+
'where ' +
'nagruzka_na_prepodavatelya.deleted=0 and '+
'nagruzka_na_prepodavatelya.distsiplina_id=distsiplina.id and '+
'nagruzka_na_prepodavatelya.gruppa_id='+pGRUPPA_ID+ ' and '+
'nagruzka_na_prepodavatelya.deleted=0 and '+
'god_id='+pCurrent_God_id+' and '+
'polugodie_id='+pCurrent_Polugodie_id+
' order by distsiplina.distsiplina_na_russkom';




if pCurrentLanguage='2' then
pS:=
'Select distinct distsiplina.distsiplina_na_kazakhskom as distsiplina_na_russkom,'+
'distsiplina.id as id  '+
'from '+
'distsiplina,nagruzka_na_prepodavatelya '+
'where ' +
'nagruzka_na_prepodavatelya.deleted=0 and '+
'nagruzka_na_prepodavatelya.distsiplina_id=distsiplina.id and '+
'nagruzka_na_prepodavatelya.gruppa_id='+pGRUPPA_ID+ ' and '+
'nagruzka_na_prepodavatelya.deleted=0 and '+
'god_id='+pCurrent_God_id+' and '+
'polugodie_id='+pCurrent_Polugodie_id+
' order by distsiplina.distsiplina_na_kazakhskom';










//Clipboard.AsText:=pS;
//ShowMessage(pS);

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.Open;

ADQ.First;


  Combo_distsiplina11_id.AddItem('-', Pointer(strtoint('1')));
  Combo_distsiplina12_id.AddItem('-', Pointer(strtoint('1')));
  Combo_distsiplina13_id.AddItem('-', Pointer(strtoint('1')));

  Combo_distsiplina21_id.AddItem('-', Pointer(strtoint('1')));
  Combo_distsiplina22_id.AddItem('-', Pointer(strtoint('1')));
  Combo_distsiplina23_id.AddItem('-', Pointer(strtoint('1')));

  Combo_distsiplina31_id.AddItem('-', Pointer(strtoint('1')));
  Combo_distsiplina32_id.AddItem('-', Pointer(strtoint('1')));
  Combo_distsiplina33_id.AddItem('-', Pointer(strtoint('1')));

  Combo_distsiplina41_id.AddItem('-', Pointer(strtoint('1')));
  Combo_distsiplina42_id.AddItem('-', Pointer(strtoint('1')));
  Combo_distsiplina43_id.AddItem('-', Pointer(strtoint('1')));

  Combo_distsiplina51_id.AddItem('-', Pointer(strtoint('1')));
  Combo_distsiplina52_id.AddItem('-', Pointer(strtoint('1')));
  Combo_distsiplina53_id.AddItem('-', Pointer(strtoint('1')));

  Combo_distsiplina61_id.AddItem('-', Pointer(strtoint('1')));
  Combo_distsiplina62_id.AddItem('-', Pointer(strtoint('1')));
  Combo_distsiplina63_id.AddItem('-', Pointer(strtoint('1')));

  Combo_distsiplina71_id.AddItem('-', Pointer(strtoint('1')));
  Combo_distsiplina72_id.AddItem('-', Pointer(strtoint('1')));
  Combo_distsiplina73_id.AddItem('-', Pointer(strtoint('1')));

while not ADQ.Eof do begin
  Combo_distsiplina11_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_distsiplina12_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_distsiplina13_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_distsiplina21_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_distsiplina22_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_distsiplina23_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_distsiplina31_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_distsiplina32_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_distsiplina33_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_distsiplina41_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_distsiplina42_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_distsiplina43_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_distsiplina51_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_distsiplina52_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_distsiplina53_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_distsiplina61_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_distsiplina62_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_distsiplina63_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  Combo_distsiplina71_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_distsiplina72_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  Combo_distsiplina73_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));

  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_distsiplina11_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_distsiplina12_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_distsiplina13_id.ItemIndex:=0;

if ADQ.RecordCount>0 then Combo_distsiplina21_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_distsiplina22_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_distsiplina23_id.ItemIndex:=0;

if ADQ.RecordCount>0 then Combo_distsiplina31_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_distsiplina32_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_distsiplina33_id.ItemIndex:=0;

if ADQ.RecordCount>0 then Combo_distsiplina41_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_distsiplina42_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_distsiplina43_id.ItemIndex:=0;

if ADQ.RecordCount>0 then Combo_distsiplina51_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_distsiplina52_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_distsiplina53_id.ItemIndex:=0;

if ADQ.RecordCount>0 then Combo_distsiplina61_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_distsiplina62_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_distsiplina63_id.ItemIndex:=0;

if ADQ.RecordCount>0 then Combo_distsiplina71_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_distsiplina72_id.ItemIndex:=0;
if ADQ.RecordCount>0 then Combo_distsiplina73_id.ItemIndex:=0;


if ADQ.RecordCount>0 then Combo_distsiplina11_id.DropDownCount:=ADQ.RecordCount+1;
if ADQ.RecordCount>0 then Combo_distsiplina12_id.DropDownCount:=ADQ.RecordCount+1;
if ADQ.RecordCount>0 then Combo_distsiplina13_id.DropDownCount:=ADQ.RecordCount+1;

if ADQ.RecordCount>0 then Combo_distsiplina21_id.DropDownCount:=ADQ.RecordCount+1;
if ADQ.RecordCount>0 then Combo_distsiplina22_id.DropDownCount:=ADQ.RecordCount+1;
if ADQ.RecordCount>0 then Combo_distsiplina23_id.DropDownCount:=ADQ.RecordCount+1;

if ADQ.RecordCount>0 then Combo_distsiplina31_id.DropDownCount:=ADQ.RecordCount+1;
if ADQ.RecordCount>0 then Combo_distsiplina32_id.DropDownCount:=ADQ.RecordCount+1;
if ADQ.RecordCount>0 then Combo_distsiplina33_id.DropDownCount:=ADQ.RecordCount+1;

if ADQ.RecordCount>0 then Combo_distsiplina41_id.DropDownCount:=ADQ.RecordCount+1;
if ADQ.RecordCount>0 then Combo_distsiplina42_id.DropDownCount:=ADQ.RecordCount+1;
if ADQ.RecordCount>0 then Combo_distsiplina43_id.DropDownCount:=ADQ.RecordCount+1;

if ADQ.RecordCount>0 then Combo_distsiplina51_id.DropDownCount:=ADQ.RecordCount+1;
if ADQ.RecordCount>0 then Combo_distsiplina52_id.DropDownCount:=ADQ.RecordCount+1;
if ADQ.RecordCount>0 then Combo_distsiplina53_id.DropDownCount:=ADQ.RecordCount+1;

if ADQ.RecordCount>0 then Combo_distsiplina61_id.DropDownCount:=ADQ.RecordCount+1;
if ADQ.RecordCount>0 then Combo_distsiplina62_id.DropDownCount:=ADQ.RecordCount+1;
if ADQ.RecordCount>0 then Combo_distsiplina63_id.DropDownCount:=ADQ.RecordCount+1;

if ADQ.RecordCount>0 then Combo_distsiplina71_id.DropDownCount:=ADQ.RecordCount+1;
if ADQ.RecordCount>0 then Combo_distsiplina72_id.DropDownCount:=ADQ.RecordCount+1;
if ADQ.RecordCount>0 then Combo_distsiplina73_id.DropDownCount:=ADQ.RecordCount+1;

end;


procedure TfFORM_RASPISANIEf.DISTINCT_DATA;
begin

  ListView1.Clear;
  Screen.Cursor := crHourglass;
  Application.ProcessMessages;

  pS:='Select distinct data from raspisanie where '+
  'deleted=0 and '+
  'gruppa_id='+pGRUPPA_ID+' and '+
  'god_id='+pCurrent_God_id+' and '+
  'polugodie_id='+pCurrent_Polugodie_id;

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS+' order by data desc');
  ADQ.Open;

  if ADQ.RecordCount = 0 then
  begin
    ListView1.Clear;
    Panel5.Visible:=false;
    Screen.Cursor := crDefault;
    exit;
  end;

  ListView1.Visible := false;
  ListView1.Items.Clear;

  ADQ.First;
  while not ADQ.Eof do
  begin
    L := ListView1.Items.Add;
    L.Caption := trim(ADQ.fieldbyname('data').AsString);
    L.SubItems.Append(LongDayNames[DayOfWeek(ADQ.fieldbyname('data').AsDateTime)]);
    L.SubItems.Append(IntToStr(DayOfWeek(ADQ.fieldbyname('data').AsDateTime)-1));    
    ADQ.Next;
  end;

  ListView1.Visible := true;

  if ListView1.Items.Count>0 then begin
    ListView1.Items[0].Selected:=true;
    ListView1Click(Self);
    Panel5.Visible:=true;
  end;

  Screen.Cursor := crDefault;

end;

procedure TfFORM_RASPISANIEf.FormActivate(Sender: TObject);
begin
PageControl1.ActivePageIndex:=0;
label10.Caption:='����� ��������: #';
label11.Caption:='����� �� �����: #';
label13.Caption:='#';
ClearCombo;
LoadGRUPPA;
LOAD_KABINET;
LoadTIP_ZANYATIYA;
//Combo_gruppaChange(Self);
end;

procedure TfFORM_RASPISANIEf.ListView1Click(Sender: TObject);
begin
ListView1.Enabled:=false;

ListView2.Clear;
//ListView3.Clear;
label10.Caption:='����� ��������: #';
label11.Caption:='����� �� �����: #';

if ListView1.ItemIndex=-1 then begin
Panel5.Visible:=false;
ListView2.Clear;
abort;
end;
Panel5.Visible:=true;

Screen.Cursor := crHourglass;

Application.ProcessMessages;
//ClearCombo;
//LoadDISTSIPLINA;
//LoadTIP_ZANYATIYA;

pS:='Select * from raspisanie where deleted=0 and gruppa_id='+pGRUPPA_ID+' and data=#'+FormatDateTime('MM.DD.YYYY',strtodate(ListView1.Selected.Caption))+'# order by id ';
pS  := StringReplace(pS, '.', '/',[rfReplaceAll, rfIgnoreCase]);

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.Open;
ADQ.First;

//------------------------------------------------------------------------------
pPara_id:='1';
pStroka:='1';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;

pPara_id:='1';
pStroka:='2';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;

pPara_id:='1';
pStroka:='3';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;
//------------------------------------------------------------------------------
pPara_id:='2';
pStroka:='1';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;

pPara_id:='2';
pStroka:='2';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;

pPara_id:='2';
pStroka:='3';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;
//------------------------------------------------------------------------------

pPara_id:='3';
pStroka:='1';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;

pPara_id:='3';
pStroka:='2';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;

pPara_id:='3';
pStroka:='3';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;
//------------------------------------------------------------------------------

pPara_id:='4';
pStroka:='1';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;

pPara_id:='4';
pStroka:='2';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;

pPara_id:='4';
pStroka:='3';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;
//------------------------------------------------------------------------------
pPara_id:='5';
pStroka:='1';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;

pPara_id:='5';
pStroka:='2';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;

pPara_id:='5';
pStroka:='3';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;
//------------------------------------------------------------------------------
pPara_id:='6';
pStroka:='1';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;

pPara_id:='6';
pStroka:='2';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;

pPara_id:='6';
pStroka:='3';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;
//------------------------------------------------------------------------------
pPara_id:='7';
pStroka:='1';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;

pPara_id:='7';
pStroka:='2';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;

pPara_id:='7';
pStroka:='3';
GetCurrentElement;
GetComboRecord(pCurrent_Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
LoadCombo1;
GetComboRecord(pCurrent_Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
LoadCombo2;
GetComboRecord(pCurrent_Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
GetComboRecord(pCurrent_Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
GetComboRecord(pCurrent_Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
ADQ.Next;
//------------------------------------------------------------------------------


ListView1.Enabled:=true;
Screen.Cursor := crDefault;

end;

procedure TfFORM_RASPISANIEf.SpeedButton1Click(Sender: TObject);
begin
pPara_id:='1';
pStroka:='1';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.ListView1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
ListView1Click(Self);
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina11_idChange(Sender: TObject);
begin
  pPara_id:='1';
  pStroka:='1';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz11_idChange(
  Sender: TObject);
begin
  pPara_id:='1';
  pStroka:='1';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.SpeedButton2Click(Sender: TObject);
begin
pPara_id:='1';
pStroka:='2';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton3Click(Sender: TObject);
begin
pPara_id:='1';
pStroka:='3';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton4Click(Sender: TObject);
begin
pPara_id:='2';
pStroka:='1';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton7Click(Sender: TObject);
begin
pPara_id:='3';
pStroka:='1';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton10Click(Sender: TObject);
begin
pPara_id:='4';
pStroka:='1';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton13Click(Sender: TObject);
begin
pPara_id:='5';
pStroka:='1';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton16Click(Sender: TObject);
begin
pPara_id:='6';
pStroka:='1';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton19Click(Sender: TObject);
begin
pPara_id:='7';
pStroka:='1';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton5Click(Sender: TObject);
begin
pPara_id:='2';
pStroka:='2';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton8Click(Sender: TObject);
begin
pPara_id:='3';
pStroka:='2';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton11Click(Sender: TObject);
begin
pPara_id:='4';
pStroka:='2';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton14Click(Sender: TObject);
begin
pPara_id:='5';
pStroka:='2';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton17Click(Sender: TObject);
begin
pPara_id:='6';
pStroka:='2';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton20Click(Sender: TObject);
begin
pPara_id:='7';
pStroka:='2';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton6Click(Sender: TObject);
begin
pPara_id:='2';
pStroka:='3';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton12Click(Sender: TObject);
begin
pPara_id:='4';
pStroka:='3';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton15Click(Sender: TObject);
begin
pPara_id:='5';
pStroka:='3';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton18Click(Sender: TObject);
begin
pPara_id:='6';
pStroka:='3';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton9Click(Sender: TObject);
begin
pPara_id:='3';
pStroka:='3';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton21Click(Sender: TObject);
begin
pPara_id:='7';
pStroka:='3';
UpdateRecord;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina12_idChange(Sender: TObject);
begin
  pPara_id:='1';
  pStroka:='2';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina13_idChange(Sender: TObject);
begin
  pPara_id:='1';
  pStroka:='3';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina21_idChange(Sender: TObject);
begin
  pPara_id:='2';
  pStroka:='1';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina22_idChange(Sender: TObject);
begin
  pPara_id:='2';
  pStroka:='2';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina23_idChange(Sender: TObject);
begin
  pPara_id:='2';
  pStroka:='3';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina31_idChange(Sender: TObject);
begin
  pPara_id:='3';
  pStroka:='1';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina32_idChange(Sender: TObject);
begin
  pPara_id:='3';
  pStroka:='2';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina33_idChange(Sender: TObject);
begin
  pPara_id:='3';
  pStroka:='3';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina41_idChange(Sender: TObject);
begin
  pPara_id:='4';
  pStroka:='1';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina42_idChange(Sender: TObject);
begin
  pPara_id:='4';
  pStroka:='2';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina43_idChange(Sender: TObject);
begin
  pPara_id:='4';
  pStroka:='3';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina51_idChange(Sender: TObject);
begin
  pPara_id:='5';
  pStroka:='1';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina52_idChange(Sender: TObject);
begin
  pPara_id:='5';
  pStroka:='2';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina53_idChange(Sender: TObject);
begin
  pPara_id:='5';
  pStroka:='3';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina61_idChange(Sender: TObject);
begin
  pPara_id:='6';
  pStroka:='1';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina62_idChange(Sender: TObject);
begin
  pPara_id:='6';
  pStroka:='2';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina63_idChange(Sender: TObject);
begin
  pPara_id:='6';
  pStroka:='3';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina71_idChange(Sender: TObject);
begin
  pPara_id:='7';
  pStroka:='1';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina72_idChange(Sender: TObject);
begin
  pPara_id:='7';
  pStroka:='2';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_distsiplina73_idChange(Sender: TObject);
begin
  pPara_id:='7';
  pStroka:='3';
  LoadCombo1;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz12_idChange(
  Sender: TObject);
begin
  pPara_id:='1';
  pStroka:='2';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz13_idChange(
  Sender: TObject);
begin
  pPara_id:='1';
  pStroka:='3';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz21_idChange(
  Sender: TObject);
begin
  pPara_id:='2';
  pStroka:='1';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz22_idChange(
  Sender: TObject);
begin
  pPara_id:='2';
  pStroka:='2';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz23_idChange(
  Sender: TObject);
begin
  pPara_id:='2';
  pStroka:='3';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz31_idChange(
  Sender: TObject);
begin
  pPara_id:='3';
  pStroka:='1';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz32_idChange(
  Sender: TObject);
begin
  pPara_id:='3';
  pStroka:='2';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz33_idChange(
  Sender: TObject);
begin
  pPara_id:='3';
  pStroka:='3';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz41_idChange(
  Sender: TObject);
begin
  pPara_id:='4';
  pStroka:='1';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz42_idChange(
  Sender: TObject);
begin
  pPara_id:='4';
  pStroka:='2';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz43_idChange(
  Sender: TObject);
begin
  pPara_id:='4';
  pStroka:='3';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz51_idChange(
  Sender: TObject);
begin
  pPara_id:='5';
  pStroka:='1';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz52_idChange(
  Sender: TObject);
begin
  pPara_id:='5';
  pStroka:='2';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz53_idChange(
  Sender: TObject);
begin
  pPara_id:='5';
  pStroka:='3';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz61_idChange(
  Sender: TObject);
begin
  pPara_id:='6';
  pStroka:='1';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz62_idChange(
  Sender: TObject);
begin
  pPara_id:='6';
  pStroka:='2';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz63_idChange(
  Sender: TObject);
begin
  pPara_id:='6';
  pStroka:='3';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz71_idChange(
  Sender: TObject);
begin
  pPara_id:='7';
  pStroka:='1';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz72_idChange(
  Sender: TObject);
begin
  pPara_id:='7';
  pStroka:='2';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz73_idChange(
  Sender: TObject);
begin
  pPara_id:='7';
  pStroka:='3';
  LoadCombo2;
end;

procedure TfFORM_RASPISANIEf.SpeedButton22Click(Sender: TObject);
begin
pPara_id:='1';
pStroka:='1';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton23Click(Sender: TObject);
begin
pPara_id:='1';
pStroka:='2';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton24Click(Sender: TObject);
begin
pPara_id:='1';
pStroka:='3';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton25Click(Sender: TObject);
begin
pPara_id:='2';
pStroka:='1';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton26Click(Sender: TObject);
begin
pPara_id:='2';
pStroka:='2';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton27Click(Sender: TObject);
begin
pPara_id:='2';
pStroka:='3';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton28Click(Sender: TObject);
begin
pPara_id:='3';
pStroka:='1';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton29Click(Sender: TObject);
begin
pPara_id:='3';
pStroka:='2';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton30Click(Sender: TObject);
begin
pPara_id:='3';
pStroka:='3';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton31Click(Sender: TObject);
begin
pPara_id:='4';
pStroka:='1';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton32Click(Sender: TObject);
begin
pPara_id:='4';
pStroka:='2';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton33Click(Sender: TObject);
begin
pPara_id:='4';
pStroka:='3';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton34Click(Sender: TObject);
begin
pPara_id:='5';
pStroka:='1';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton35Click(Sender: TObject);
begin
pPara_id:='5';
pStroka:='2';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton36Click(Sender: TObject);
begin
pPara_id:='5';
pStroka:='3';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton37Click(Sender: TObject);
begin
pPara_id:='6';
pStroka:='1';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton38Click(Sender: TObject);
begin
pPara_id:='6';
pStroka:='2';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton39Click(Sender: TObject);
begin
pPara_id:='6';
pStroka:='3';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton40Click(Sender: TObject);
begin
pPara_id:='7';
pStroka:='1';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton41Click(Sender: TObject);
begin
pPara_id:='7';
pStroka:='2';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton42Click(Sender: TObject);
begin
pPara_id:='7';
pStroka:='3';
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton43Click(Sender: TObject);
begin
pPara_id:='1';
pStroka:='1';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton44Click(Sender: TObject);
begin
pPara_id:='1';
pStroka:='2';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton45Click(Sender: TObject);
begin
pPara_id:='1';
pStroka:='3';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton46Click(Sender: TObject);
begin
pPara_id:='2';
pStroka:='1';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton47Click(Sender: TObject);
begin
pPara_id:='2';
pStroka:='2';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton48Click(Sender: TObject);
begin
pPara_id:='2';
pStroka:='3';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton49Click(Sender: TObject);
begin
pPara_id:='3';
pStroka:='1';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton50Click(Sender: TObject);
begin
pPara_id:='3';
pStroka:='2';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton51Click(Sender: TObject);
begin
pPara_id:='3';
pStroka:='3';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton52Click(Sender: TObject);
begin
pPara_id:='4';
pStroka:='1';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton53Click(Sender: TObject);
begin
pPara_id:='4';
pStroka:='2';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton54Click(Sender: TObject);
begin
pPara_id:='4';
pStroka:='3';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton55Click(Sender: TObject);
begin
pPara_id:='5';
pStroka:='1';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton56Click(Sender: TObject);
begin
pPara_id:='5';
pStroka:='2';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton57Click(Sender: TObject);
begin
pPara_id:='5';
pStroka:='3';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton58Click(Sender: TObject);
begin
pPara_id:='6';
pStroka:='1';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton59Click(Sender: TObject);
begin
pPara_id:='6';
pStroka:='2';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton60Click(Sender: TObject);
begin
pPara_id:='6';
pStroka:='3';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton61Click(Sender: TObject);
begin
pPara_id:='7';
pStroka:='1';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton62Click(Sender: TObject);
begin
pPara_id:='7';
pStroka:='2';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.SpeedButton63Click(Sender: TObject);
begin
pPara_id:='7';
pStroka:='3';
Progul;
ClearRecord;
end;

procedure TfFORM_RASPISANIEf.Combo_gruppaChange(Sender: TObject);
begin
   ListView2.Clear;
   pGRUPPA_ID:=Inttostr(Integer(Combo_gruppa.Items.Objects[Combo_gruppa.ItemIndex]));
   pGRUPPA_NAME:=Combo_gruppa.Text;

   ADQ.Close;
   ADQ.SQL.Clear;
   ADQ.SQL.Add('Select * from gruppa where id='+pGRUPPA_ID);
   ADQ.Open;

   pCurrentLanguage:=ADQ.FieldByName('yazyk_obucheniya_id').AsString;


   fFORM_RASPISANIEf.Caption:='���������� ��� ������ '+pGRUPPA_NAME;
   LoadDISTSIPLINA;
   DISTINCT_DATA;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya11_idChange(
  Sender: TObject);
begin
  pPara_id:='1';
  pStroka:='1';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya12_idChange(
  Sender: TObject);
begin
  pPara_id:='1';
  pStroka:='2';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya13_idChange(
  Sender: TObject);
begin
  pPara_id:='1';
  pStroka:='3';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya21_idChange(
  Sender: TObject);
begin
  pPara_id:='2';
  pStroka:='1';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya22_idChange(
  Sender: TObject);
begin
  pPara_id:='2';
  pStroka:='2';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya23_idChange(
  Sender: TObject);
begin
  pPara_id:='2';
  pStroka:='3';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya31_idChange(
  Sender: TObject);
begin
  pPara_id:='3';
  pStroka:='1';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya32_idChange(
  Sender: TObject);
begin
  pPara_id:='3';
  pStroka:='2';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya33_idChange(
  Sender: TObject);
begin
  pPara_id:='3';
  pStroka:='3';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya41_idChange(
  Sender: TObject);
begin
  pPara_id:='4';
  pStroka:='1';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya42_idChange(
  Sender: TObject);
begin
  pPara_id:='4';
  pStroka:='2';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya43_idChange(
  Sender: TObject);
begin
  pPara_id:='4';
  pStroka:='3';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya51_idChange(
  Sender: TObject);
begin
  pPara_id:='5';
  pStroka:='1';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya52_idChange(
  Sender: TObject);
begin
  pPara_id:='5';
  pStroka:='2';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya53_idChange(
  Sender: TObject);
begin
  pPara_id:='5';
  pStroka:='3';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya61_idChange(
  Sender: TObject);
begin
  pPara_id:='6';
  pStroka:='1';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya62_idChange(
  Sender: TObject);
begin
  pPara_id:='6';
  pStroka:='2';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya63_idChange(
  Sender: TObject);
begin
  pPara_id:='6';
  pStroka:='3';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya71_idChange(
  Sender: TObject);
begin
  pPara_id:='7';
  pStroka:='1';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya72_idChange(
  Sender: TObject);
begin
  pPara_id:='7';
  pStroka:='2';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.Combo_tip_zanyatiya73_idChange(
  Sender: TObject);
begin
  pPara_id:='7';
  pStroka:='3';
  Raschet;
end;

procedure TfFORM_RASPISANIEf.SpeedButton64Click(Sender: TObject);
begin
pPara_id:='1';
pStroka:='1';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton65Click(Sender: TObject);
begin
pPara_id:='1';
pStroka:='2';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton66Click(Sender: TObject);
begin
pPara_id:='1';
pStroka:='3';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton67Click(Sender: TObject);
begin
pPara_id:='2';
pStroka:='1';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton68Click(Sender: TObject);
begin
pPara_id:='2';
pStroka:='2';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton69Click(Sender: TObject);
begin
pPara_id:='2';
pStroka:='3';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton70Click(Sender: TObject);
begin
pPara_id:='3';
pStroka:='1';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton71Click(Sender: TObject);
begin
pPara_id:='3';
pStroka:='2';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton72Click(Sender: TObject);
begin
pPara_id:='3';
pStroka:='3';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton73Click(Sender: TObject);
begin
pPara_id:='4';
pStroka:='1';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton74Click(Sender: TObject);
begin
pPara_id:='4';
pStroka:='2';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton75Click(Sender: TObject);
begin
pPara_id:='4';
pStroka:='3';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton76Click(Sender: TObject);
begin
pPara_id:='5';
pStroka:='1';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton77Click(Sender: TObject);
begin
pPara_id:='5';
pStroka:='2';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton78Click(Sender: TObject);
begin
pPara_id:='5';
pStroka:='3';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton79Click(Sender: TObject);
begin
pPara_id:='6';
pStroka:='1';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton80Click(Sender: TObject);
begin
pPara_id:='6';
pStroka:='2';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton81Click(Sender: TObject);
begin
pPara_id:='6';
pStroka:='3';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton82Click(Sender: TObject);
begin
pPara_id:='7';
pStroka:='1';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton83Click(Sender: TObject);
begin
pPara_id:='7';
pStroka:='2';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.SpeedButton84Click(Sender: TObject);
begin
pPara_id:='7';
pStroka:='3';
DobavNagrNaPrepod;
end;

procedure TfFORM_RASPISANIEf.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
//Check_22;
end;

procedure TfFORM_RASPISANIEf.N1Click(Sender: TObject);
var
pCurrentNum:integer;
pSQL:widestring;
day,pChasy,pDistsiplina : string;
xlapp, xlbook, xlsht: OleVariant;
i,j: word;
rc, FirstI: integer;
pPodgruppa:string;
begin

xlapp:=CreateOleObject('Excel.Application');
xlapp.Visible:=false;
xlbook:=xlapp.WorkBooks.Open(ExtractFilePath(Application.exeName)+'report.xls');
xlsht := xlapp.Workbooks[1].Worksheets['report1'];
xlsht := xlapp.Sheets;
xlsht.Item['report1'].Activate;
xlsht:=xlbook.ActiveSheet;

pCurrentNum:=fCREATE_DATEf.WeekNum(strtodate(ListView1.Selected.Caption));

xlsht.Cells(5,2):='���������� ������� '+copy(Combo_gruppa.Text,1,length(Combo_gruppa.Text)-4);

Screen.Cursor := crHourGlass;

pS:='Select '+
'raspisanie.id as raspisanie_id,'+
'raspisanie.podgruppa_id,'+
'para.para as para,'+
'para.budnie_chasy,'+
'para.vykhodnye_chasy,'+
'raspisanie.stroka as stroka,'+
'raspisanie.nomer_nedeli,'+
'distsiplina.distsiplina_na_russkom as distsiplina_na_russkom,'+
'distsiplina.distsiplina_na_kazakhskom as distsiplina_na_kazakhskom,'+
'prepodavatelmz.fio as fio,'+
'kabinet.nomer_kabineta as nomer_kabineta,'+
'raspisanie.kabinet_id,'+
'tip_zanyatiya.tip_zanyatiya as tip_zanyatiya,'+
'raspisanie.data as data,'+
'gruppa.nazvanie_gruppy as gruppa_nazvanie_gruppy,'+
'gruppa.yazyk_obucheniya_id as yazyk_obucheniya_id,'+
'podgruppa.podgruppa as podgruppa,'+
'vid_nagruzki.vid_nagruzki as vid_nagruzki '+
'from '+
'raspisanie,para,distsiplina,prepodavatelmz,kabinet,tip_zanyatiya,gruppa,podgruppa,vid_nagruzki '+
'where '+
'raspisanie.para_id=para.id and '+
'raspisanie.distsiplina_id=distsiplina.id and '+
'raspisanie.prepodavatelmz_id=prepodavatelmz.id and '+
'raspisanie.kabinet_id=kabinet.id and '+
'raspisanie.tip_zanyatiya_id=tip_zanyatiya.id and '+
'raspisanie.gruppa_id=gruppa.id and '+
'raspisanie.podgruppa_id=podgruppa.id and '+
'raspisanie.vid_nagruzki_id=vid_nagruzki.id and '+
'raspisanie.deleted=0 and '+
'raspisanie.nomer_nedeli='+inttostr(pCurrentNum);

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS+' order by  raspisanie.id');
ADQ.Open;

ADQ.Last;
ADQ.First;

pSQL:='';
i:=7;

while not ADQ.Eof do begin
if ADQ.FieldByName('yazyk_obucheniya_id').AsString='1' then pDistsiplina:=ADQ.FieldByName('distsiplina_na_russkom').AsString;
if ADQ.FieldByName('yazyk_obucheniya_id').AsString='2' then pDistsiplina:=ADQ.FieldByName('distsiplina_na_kazakhskom').AsString;

if ADQ.FieldByName('podgruppa_id').AsString='1' then pPodgruppa:='';
if ADQ.FieldByName('podgruppa_id').AsString='2' then pPodgruppa:='I';
if ADQ.FieldByName('podgruppa_id').AsString='3' then pPodgruppa:='II';
if ADQ.FieldByName('podgruppa_id').AsString='4' then pPodgruppa:='III';

xlsht.Cells(i-1,2):=ADQ.FieldByName('data').AsString;
xlsht.Cells(i,6):=pPodgruppa+' '+pDistsiplina;
xlsht.Cells(i,8):=ADQ.FieldByName('fio').AsString;
if ADQ.FieldByName('kabinet_id').AsInteger>34 then xlsht.Cells(i,9):='����' else xlsht.Cells(i,9):=ADQ.FieldByName('nomer_kabineta').AsString;
inc(i);
ADQ.Next;
end;


//2 ������----------------------------------------------------------------------

xlsht.Cells(5,11):='���������� ������� '+copy(Combo_gruppa.Text,1,length(Combo_gruppa.Text)-4);

pS:='Select '+
'raspisanie.id as raspisanie_id,'+
'raspisanie.podgruppa_id,'+
'para.para as para,'+
'para.budnie_chasy,'+
'para.vykhodnye_chasy,'+
'raspisanie.stroka as stroka,'+
'raspisanie.nomer_nedeli,'+
'distsiplina.distsiplina_na_russkom as distsiplina_na_russkom,'+
'distsiplina.distsiplina_na_kazakhskom as distsiplina_na_kazakhskom,'+
'prepodavatelmz.fio as fio,'+
'kabinet.nomer_kabineta as nomer_kabineta,'+
'raspisanie.kabinet_id,'+
'tip_zanyatiya.tip_zanyatiya as tip_zanyatiya,'+
'raspisanie.data as data,'+
'gruppa.nazvanie_gruppy as gruppa_nazvanie_gruppy,'+
'gruppa.yazyk_obucheniya_id as yazyk_obucheniya_id,'+
'podgruppa.podgruppa as podgruppa,'+
'vid_nagruzki.vid_nagruzki as vid_nagruzki '+
'from '+
'raspisanie,para,distsiplina,prepodavatelmz,kabinet,tip_zanyatiya,gruppa,podgruppa,vid_nagruzki '+
'where '+
'raspisanie.para_id=para.id and '+
'raspisanie.distsiplina_id=distsiplina.id and '+
'raspisanie.prepodavatelmz_id=prepodavatelmz.id and '+
'raspisanie.kabinet_id=kabinet.id and '+
'raspisanie.tip_zanyatiya_id=tip_zanyatiya.id and '+
'raspisanie.gruppa_id=gruppa.id and '+
'raspisanie.podgruppa_id=podgruppa.id and '+
'raspisanie.vid_nagruzki_id=vid_nagruzki.id and '+
'raspisanie.deleted=0 and '+
'raspisanie.nomer_nedeli='+inttostr(pCurrentNum+1);

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS+' order by raspisanie.id');
ADQ.Open;

ADQ.Last;
ADQ.First;

pSQL:='';
i:=7;

while not ADQ.Eof do begin

if ADQ.FieldByName('yazyk_obucheniya_id').AsString='1' then pDistsiplina:=ADQ.FieldByName('distsiplina_na_russkom').AsString;
if ADQ.FieldByName('yazyk_obucheniya_id').AsString='2' then pDistsiplina:=ADQ.FieldByName('distsiplina_na_kazakhskom').AsString;

if ADQ.FieldByName('podgruppa_id').AsString='1' then pPodgruppa:='';
if ADQ.FieldByName('podgruppa_id').AsString='2' then pPodgruppa:='I';
if ADQ.FieldByName('podgruppa_id').AsString='3' then pPodgruppa:='II';
if ADQ.FieldByName('podgruppa_id').AsString='4' then pPodgruppa:='III';

xlsht.Cells(i-1,11):=ADQ.FieldByName('data').AsString;
xlsht.Cells(i,15):=pPodgruppa+' '+pDistsiplina;
xlsht.Cells(i,17):=ADQ.FieldByName('fio').AsString;
if ADQ.FieldByName('kabinet_id').AsInteger>34 then xlsht.Cells(i,9):='����' else xlsht.Cells(i,18):=ADQ.FieldByName('nomer_kabineta').AsString;
inc(i);
ADQ.Next;
end;
//2 ������----------------------------------------------------------------------

xlapp.Visible:=true;

//xlsht.PageSetup.Orientation := 1;
//xlsht.PageSetup.Zoom := false;
//xlsht.PageSetup.Order := 1;
//xlsht.PrintPreview;
xlapp.ActiveWindow.Zoom := 100;

Screen.Cursor := crDefault;

end;

procedure TfFORM_RASPISANIEf.N11Click(Sender: TObject);
var
pCurrentNum:integer;
pSQL:widestring;
day,pChasy,pDistsiplina : string;
xlapp, xlbook, xlsht: OleVariant;
i,j,z: word;
rc, FirstI: integer;

begin

if ListView1.Items.Count=0 then abort;

if ListView1.Selected.SubItems[1]<>'1' then begin
ShowMessage('������ ���������� ����������� ������ ��������� ������ �� ����������� (������ ���� ������)'+#13#10+'���������� ������ ������ ��������� ���� ��������� ���� ��� ���������� ������� �������� ��� ������� ���� ������� �������� �����������');
abort;
end;

xlapp:=CreateOleObject('Excel.Application');
xlapp.Visible:=false;
xlbook:=xlapp.WorkBooks.Open(ExtractFilePath(Application.exeName)+'report.xls');
xlsht := xlapp.Workbooks[1].Worksheets['report2'];
xlsht := xlapp.Sheets;
xlsht.Item['report2'].Activate;
xlsht:=xlbook.ActiveSheet;

pCurrentNum:=fCREATE_DATEf.WeekNum(strtodate(ListView1.Selected.Caption));

xlsht.Cells(2,2):='���������� ������� '+copy(Combo_gruppa.Text,1,length(Combo_gruppa.Text)-4);

Screen.Cursor := crHourGlass;

pS:='Select '+
'raspisanie.id as raspisanie_id,'+
'raspisanie.podgruppa_id,'+
'para.para as para,'+
'para.budnie_chasy,'+
'para.vykhodnye_chasy,'+
'raspisanie.stroka as stroka,'+
'raspisanie.nomer_nedeli,'+
'distsiplina.distsiplina_na_russkom as distsiplina_na_russkom,'+
'distsiplina.distsiplina_na_kazakhskom as distsiplina_na_kazakhskom,'+
'prepodavatelmz.fio as fio,'+
'kabinet.nomer_kabineta as nomer_kabineta,'+
'raspisanie.kabinet_id,'+
'tip_zanyatiya.tip_zanyatiya as tip_zanyatiya,'+
'raspisanie.data as data,'+
'gruppa.nazvanie_gruppy as gruppa_nazvanie_gruppy,'+
'gruppa.yazyk_obucheniya_id as yazyk_obucheniya_id,'+
'podgruppa.podgruppa as podgruppa,'+
'vid_nagruzki.vid_nagruzki as vid_nagruzki '+
'from '+
'raspisanie,para,distsiplina,prepodavatelmz,kabinet,tip_zanyatiya,gruppa,podgruppa,vid_nagruzki '+
'where '+
'raspisanie.para_id=para.id and '+
'raspisanie.distsiplina_id=distsiplina.id and '+
'raspisanie.prepodavatelmz_id=prepodavatelmz.id and '+
'raspisanie.kabinet_id=kabinet.id and '+
'raspisanie.tip_zanyatiya_id=tip_zanyatiya.id and '+
'raspisanie.gruppa_id=gruppa.id and '+
'raspisanie.podgruppa_id=podgruppa.id and '+
'raspisanie.vid_nagruzki_id=vid_nagruzki.id and '+
'raspisanie.deleted=0 and '+
'raspisanie.gruppa_id='+pGRUPPA_ID+ ' and '+
'raspisanie.god_id='+pCurrent_God_id+' and '+
'raspisanie.polugodie_id='+pCurrent_Polugodie_id+' and '+
'raspisanie.nomer_nedeli='+inttostr(pCurrentNum);

pS:=pS+' order by raspisanie.data, raspisanie.para_id, raspisanie.stroka';

//Clipboard.AsText:=pS;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.Open;

ADQ.Last;
ADQ.First;

pSQL:='';
i:=4;

for z:=1 to 42 do begin

GetOneString;
ADQ.Next;
GetOneString;
ADQ.Next;
GetOneString;
ADQ.Next;


pD  := StringReplace(pD, '-;', '',[rfReplaceAll, rfIgnoreCase]);
pP  := StringReplace(pP, '-;', '',[rfReplaceAll, rfIgnoreCase]);
pK  := StringReplace(pK, '-;', '',[rfReplaceAll, rfIgnoreCase]);

xlsht.Cells(i,5):=trim(pD);
xlsht.Cells(i,7):=trim(pP);
xlsht.Cells(i,8):=trim(pK);

pD:='';
pP:='';
pK:='';

xlsht.Cells(i-1,2):=ADQ.FieldByName('data').AsString;

inc(i);

end;




xlapp.Visible:=true;

//xlsht.PageSetup.Orientation := 1;
//xlsht.PageSetup.Zoom := false;
//xlsht.PageSetup.Order := 1;
//xlsht.PrintPreview;
xlapp.ActiveWindow.Zoom := 100;

Screen.Cursor := crDefault;

end;

procedure TfFORM_RASPISANIEf.SpeedButton85Click(Sender: TObject);
begin
fCREATE_DATEf.ShowModal;
end;

procedure TfFORM_RASPISANIEf.N2Click(Sender: TObject);
var
i,j:integer;
begin

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select id from svobod_chasy where prepodavatelmz_id='+pCurrent_Combo_prepodavatelmz_id_x);
ADQ.Open;

if ADQ.RecordCount=0 then begin

for i:=1 to 6 do begin

    for j:=1 to 7 do begin

    pS:='Insert into svobod_chasy(prepodavatelmz_id,dayz_of_week,para_id,svoboden) values ('+pCurrent_Combo_prepodavatelmz_id_x+','+inttostr(i)+','+inttostr(j)+',''1'')';

    ADQ1.Close;
    ADQ1.SQL.Clear;
    ADQ1.SQL.Add(pS);
    ADQ1.ExecSQL;

    end;

end;

end;

fSVOBOD_CHASYf.ShowModal;

LV2;

end;

procedure TfFORM_RASPISANIEf.N4Click(Sender: TObject);
begin
if ListView1.Items.Count=0 then abort;
fBUSY_KABINETf.ShowModal;
end;

procedure TfFORM_RASPISANIEf.N21Click(Sender: TObject);
var
pCurrentNum:integer;
pSQL:widestring;
day,pChasy,pDistsiplina : string;
xlapp, xlbook, xlsht: OleVariant;
i,j,z: word;
rc, FirstI: integer;
pName,pGruppaName:string;
begin

if ListView1.Items.Count=0 then abort;

if ListView1.Selected.SubItems[1]<>'1' then begin
ShowMessage('������ ���������� ����������� ������ ��������� ������ �� ����������� (������ ���� ������)'+#13#10+'���������� ������ ������ ��������� ���� ��������� ���� ��� ���������� ������� �������� ��� ������� ���� ������� �������� �����������');
abort;
end;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from gruppa where deleted=0 and id='+Inttostr(Integer(Combo_gruppa.Items.Objects[Combo_gruppa.ItemIndex])));
ADQ.Open;

ADQ.First;

pGruppaName:=ADQ.FieldByName('nazvanie_gruppy').AsString;

if ADQ.FieldByName('yazyk_obucheniya_id').AsString='1' then pName:='rus';
if ADQ.FieldByName('yazyk_obucheniya_id').AsString='2' then pName:='kaz';

xlapp:=CreateOleObject('Excel.Application');
xlapp.Visible:=false;
xlbook:=xlapp.WorkBooks.Open(ExtractFilePath(Application.exeName)+'report.xls');
xlsht := xlapp.Workbooks[1].Worksheets['report3'];
xlsht := xlapp.Sheets;
xlsht.Item['report3'].Activate;
xlsht:=xlbook.ActiveSheet;

pCurrentNum:=fCREATE_DATEf.WeekNum(strtodate(ListView1.Selected.Caption));

xlsht.Cells(2,2):='���������� ������� '+copy(Combo_gruppa.Text,1,length(Combo_gruppa.Text)-4);

xlsht.Cells(5,1):=datetostr(now);

if pName='rus' then begin
  xlsht.Cells(1,1):='���������';
  xlsht.Cells(2,1):='�������� ';
  xlsht.Cells(3,1):=' �������';
  xlsht.Cells(4,1):='_______________';

  xlsht.Cells(6,1):='���������� ������� '+pGruppaName;
  xlsht.Cells(7,2):='�����������';
  xlsht.Cells(15,2):='�������';
  xlsht.Cells(22,2):='�����';
  xlsht.Cells(29,2):='�������';
  xlsht.Cells(36,2):='�������';
  xlsht.Cells(43,2):='�������';

  xlsht.Cells(7,4):='����������';
  xlsht.Cells(7,5):='�����';
  xlsht.Cells(7,6):='�������������';
  xlsht.Cells(7,7):='�������';

  xlsht.Cells(50,1):='����������� ��������� �� ����������������  ������                                   �.�.';
end;


if pName='kaz' then begin
  xlsht.Cells(1,1):='��ʲ��̲�';
  xlsht.Cells(2,1):='� ';
  xlsht.Cells(3,1):='';
  xlsht.Cells(4,1):='_______________�.�.';
  xlsht.Cells(6,1):='����� ������ '+pGruppaName;

  xlsht.Cells(7,2):='ğ�����';
  xlsht.Cells(15,2):='�������';
  xlsht.Cells(22,2):='Ѿ�����';
  xlsht.Cells(29,2):='�������';
  xlsht.Cells(36,2):='Ɛ��';
  xlsht.Cells(43,2):='����';

  xlsht.Cells(7,4):='Ͼ��';
  xlsht.Cells(7,5):='������';
  xlsht.Cells(7,6):='Ν�����';
  xlsht.Cells(7,7):='�������';

  xlsht.Cells(50,1):='���                                   �.� ';
end;

Screen.Cursor := crHourGlass;


//������� ������
pS:='Select '+
'raspisanie.id as raspisanie_id,'+
'raspisanie.podgruppa_id,'+
'para.para as para,'+
'para.budnie_chasy,'+
'para.vykhodnye_chasy,'+
'raspisanie.stroka as stroka,'+
'raspisanie.nomer_nedeli,'+
'distsiplina.distsiplina_na_russkom as distsiplina_na_russkom,'+
'distsiplina.distsiplina_na_kazakhskom as distsiplina_na_kazakhskom,'+
'prepodavatelmz.fio as fio,'+
'kabinet.nomer_kabineta as nomer_kabineta,'+
'raspisanie.kabinet_id,'+
'tip_zanyatiya.tip_zanyatiya as tip_zanyatiya,'+
'raspisanie.data as data,'+
'gruppa.nazvanie_gruppy as gruppa_nazvanie_gruppy,'+
'gruppa.yazyk_obucheniya_id as yazyk_obucheniya_id,'+
'podgruppa.podgruppa as podgruppa,'+
'vid_nagruzki.vid_nagruzki as vid_nagruzki '+
'from '+
'raspisanie,para,distsiplina,prepodavatelmz,kabinet,tip_zanyatiya,gruppa,podgruppa,vid_nagruzki '+
'where '+
'raspisanie.para_id=para.id and '+
'raspisanie.distsiplina_id=distsiplina.id and '+
'raspisanie.prepodavatelmz_id=prepodavatelmz.id and '+
'raspisanie.kabinet_id=kabinet.id and '+
'raspisanie.tip_zanyatiya_id=tip_zanyatiya.id and '+
'raspisanie.gruppa_id=gruppa.id and '+
'raspisanie.podgruppa_id=podgruppa.id and '+
'raspisanie.vid_nagruzki_id=vid_nagruzki.id and '+
'raspisanie.deleted=0 and '+
'raspisanie.gruppa_id='+pGRUPPA_ID+ ' and '+
'raspisanie.god_id='+pCurrent_God_id+' and '+
'raspisanie.polugodie_id='+pCurrent_Polugodie_id+' and '+
'raspisanie.nomer_nedeli = '+inttostr(pCurrentNum);

pS:=pS+' order by raspisanie.data, raspisanie.para_id, raspisanie.stroka';

//Clipboard.AsText:=pS;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.Open;

ADQ.Last;
ADQ.First;

pSQL:='';

i:=8;

pX:='';
pD:='';
pP:='';
pK:='';

for z:=1 to 42 do begin

GetOneString1;
ADQ.Next;
GetOneString1;
ADQ.Next;
GetOneString1;
ADQ.Next;


pX:=trim(pX);
pX  := StringReplace(pX, ',,,', '',[rfReplaceAll, rfIgnoreCase]);
pX  := StringReplace(pX, ',,', '',[rfReplaceAll, rfIgnoreCase]);
if copy(pX,length(pX),length(pX))=',' then pX:=copy(pX,1,length(pX)-1);

pD  := StringReplace(pD, '-;', '',[rfReplaceAll, rfIgnoreCase]);
pP  := StringReplace(pP, '-;', '',[rfReplaceAll, rfIgnoreCase]);
pK  := StringReplace(pK, '-;', '',[rfReplaceAll, rfIgnoreCase]);

xlsht.Cells(i,4):=trim(trim(pX)+' '+trim(pD));
xlsht.Cells(i,6):=trim(pP);
xlsht.Cells(i,7):=trim(pK);

pX:='';
pD:='';
pP:='';
pK:='';

xlsht.Cells(i-1,1):=ADQ.FieldByName('data').AsString;

inc(i);

end;


{
ADQ.Next;

i:=8;

pX:='';
pD:='';
pP:='';
pK:='';

for z:=1 to 42 do begin

GetOneString1;
ADQ.Next;
GetOneString1;
ADQ.Next;
GetOneString1;
ADQ.Next;


pX:=trim(pX);
pX  := StringReplace(pX, ',,,', '',[rfReplaceAll, rfIgnoreCase]);
pX  := StringReplace(pX, ',,', '',[rfReplaceAll, rfIgnoreCase]);
if copy(pX,length(pX),length(pX))=',' then pX:=copy(pX,1,length(pX)-1);

pD  := StringReplace(pD, '-;', '',[rfReplaceAll, rfIgnoreCase]);
pP  := StringReplace(pP, '-;', '',[rfReplaceAll, rfIgnoreCase]);
pK  := StringReplace(pK, '-;', '',[rfReplaceAll, rfIgnoreCase]);

xlsht.Cells(i,12):=trim(trim(pX)+' '+trim(pD));
xlsht.Cells(i,14):=trim(pP);
xlsht.Cells(i,15):=trim(pK);

pX:='';
pD:='';
pP:='';
pK:='';

xlsht.Cells(i-1,9):=ADQ.FieldByName('data').AsString;

inc(i);

end;
}












//��������� ������
pS:='Select '+
'raspisanie.id as raspisanie_id,'+
'raspisanie.podgruppa_id,'+
'para.para as para,'+
'para.budnie_chasy,'+
'para.vykhodnye_chasy,'+
'raspisanie.stroka as stroka,'+
'raspisanie.nomer_nedeli,'+
'distsiplina.distsiplina_na_russkom as distsiplina_na_russkom,'+
'distsiplina.distsiplina_na_kazakhskom as distsiplina_na_kazakhskom,'+
'prepodavatelmz.fio as fio,'+
'kabinet.nomer_kabineta as nomer_kabineta,'+
'raspisanie.kabinet_id,'+
'tip_zanyatiya.tip_zanyatiya as tip_zanyatiya,'+
'raspisanie.data as data,'+
'gruppa.nazvanie_gruppy as gruppa_nazvanie_gruppy,'+
'gruppa.yazyk_obucheniya_id as yazyk_obucheniya_id,'+
'podgruppa.podgruppa as podgruppa,'+
'vid_nagruzki.vid_nagruzki as vid_nagruzki '+
'from '+
'raspisanie,para,distsiplina,prepodavatelmz,kabinet,tip_zanyatiya,gruppa,podgruppa,vid_nagruzki '+
'where '+
'raspisanie.para_id=para.id and '+
'raspisanie.distsiplina_id=distsiplina.id and '+
'raspisanie.prepodavatelmz_id=prepodavatelmz.id and '+
'raspisanie.kabinet_id=kabinet.id and '+
'raspisanie.tip_zanyatiya_id=tip_zanyatiya.id and '+
'raspisanie.gruppa_id=gruppa.id and '+
'raspisanie.podgruppa_id=podgruppa.id and '+
'raspisanie.vid_nagruzki_id=vid_nagruzki.id and '+
'raspisanie.deleted=0 and '+
'raspisanie.gruppa_id='+pGRUPPA_ID+ ' and '+
'raspisanie.god_id='+pCurrent_God_id+' and '+
'raspisanie.polugodie_id='+pCurrent_Polugodie_id+' and '+
'raspisanie.nomer_nedeli='+inttostr(pCurrentNum+1);

pS:=pS+' order by raspisanie.data, raspisanie.para_id, raspisanie.stroka';

//Clipboard.AsText:=pS;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.Open;

ADQ.Last;
ADQ.First;

pSQL:='';

i:=8;

pX:='';
pD:='';
pP:='';
pK:='';

for z:=1 to 42 do begin

GetOneString1;
ADQ.Next;
GetOneString1;
ADQ.Next;
GetOneString1;
ADQ.Next;


pX:=trim(pX);
pX  := StringReplace(pX, ',,,', '',[rfReplaceAll, rfIgnoreCase]);
pX  := StringReplace(pX, ',,', '',[rfReplaceAll, rfIgnoreCase]);
if copy(pX,length(pX),length(pX))=',' then pX:=copy(pX,1,length(pX)-1);

pD  := StringReplace(pD, '-;', '',[rfReplaceAll, rfIgnoreCase]);
pP  := StringReplace(pP, '-;', '',[rfReplaceAll, rfIgnoreCase]);
pK  := StringReplace(pK, '-;', '',[rfReplaceAll, rfIgnoreCase]);

xlsht.Cells(i,12):=trim(trim(pX)+' '+trim(pD));
xlsht.Cells(i,14):=trim(pP);
xlsht.Cells(i,15):=trim(pK);

pX:='';
pD:='';
pP:='';
pK:='';

xlsht.Cells(i-1,9):=ADQ.FieldByName('data').AsString;

inc(i);

end;


xlapp.Visible:=true;

//xlsht.PageSetup.Orientation := 1;
//xlsht.PageSetup.Zoom := false;
//xlsht.PageSetup.Order := 1;
//xlsht.PrintPreview;
xlapp.ActiveWindow.Zoom := 100;

Screen.Cursor := crDefault;

end;

procedure TfFORM_RASPISANIEf.Button1Click(Sender: TObject);
begin
fLENTAf.ShowModal;
end;

procedure TfFORM_RASPISANIEf.Button2Click(Sender: TObject);
begin
fLENTA_GRUPPf.ShowModal;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz11_idClick(
  Sender: TObject);
begin
  pPara_id:='1';
  pStroka:='1';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz12_idClick(
  Sender: TObject);
begin
  pPara_id:='1';
  pStroka:='2';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz13_idClick(
  Sender: TObject);
begin
  pPara_id:='1';
  pStroka:='3';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz21_idClick(
  Sender: TObject);
begin
  pPara_id:='2';
  pStroka:='1';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz22_idClick(
  Sender: TObject);
begin
  pPara_id:='2';
  pStroka:='2';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz23_idClick(
  Sender: TObject);
begin
  pPara_id:='2';
  pStroka:='3';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz31_idClick(
  Sender: TObject);
begin
  pPara_id:='3';
  pStroka:='1';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz32_idClick(
  Sender: TObject);
begin
  pPara_id:='3';
  pStroka:='2';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz33_idClick(
  Sender: TObject);
begin
  pPara_id:='3';
  pStroka:='3';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz41_idClick(
  Sender: TObject);
begin
  pPara_id:='4';
  pStroka:='1';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz42_idClick(
  Sender: TObject);
begin
  pPara_id:='4';
  pStroka:='2';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz43_idClick(
  Sender: TObject);
begin
  pPara_id:='4';
  pStroka:='3';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz51_idClick(
  Sender: TObject);
begin
  pPara_id:='5';
  pStroka:='1';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz52_idClick(
  Sender: TObject);
begin
  pPara_id:='5';
  pStroka:='2';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz53_idClick(
  Sender: TObject);
begin
  pPara_id:='5';
  pStroka:='3';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz61_idClick(
  Sender: TObject);
begin
  pPara_id:='6';
  pStroka:='1';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz62_idClick(
  Sender: TObject);
begin
  pPara_id:='6';
  pStroka:='2';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz63_idClick(
  Sender: TObject);
begin
  pPara_id:='6';
  pStroka:='3';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz71_idClick(
  Sender: TObject);
begin
  pPara_id:='7';
  pStroka:='1';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz72_idClick(
  Sender: TObject);
begin
  pPara_id:='7';
  pStroka:='2';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

procedure TfFORM_RASPISANIEf.Combo_prepodavatelmz73_idClick(
  Sender: TObject);
begin
  pPara_id:='7';
  pStroka:='3';
  GetCurrentElement;
  pCurrent_Combo_prepodavatelmz_id_x:=Inttostr(Integer(pCurrent_Combo_prepodavatelmz_id.Items.Objects[pCurrent_Combo_prepodavatelmz_id.ItemIndex]));
  LV2;
end;

end.
