unit fSELECT_DATE_INTO_RASP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, StdCtrls,clipbrd;

type
  TfSELECT_DATE_INTO_RASPf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    MonthCalendar1: TMonthCalendar;
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    ProgressBar1: TProgressBar;
    procedure Button2Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fSELECT_DATE_INTO_RASPf: TfSELECT_DATE_INTO_RASPf;

implementation

uses fMW, fCREATE_DATE;

{$R *.dfm}

procedure TfSELECT_DATE_INTO_RASPf.Button2Click(Sender: TObject);
var
pSQL:widestring;
i,pCurrentNum:integer;
day,pChasy,pDistsiplina : string;
begin

pCurrentNum:=fCREATE_DATEf.WeekNum(MonthCalendar1.Date);

Screen.Cursor := crHourGlass;

fMWf.PostDATA('Delete from raspisanie');

pS:='Select '+
'raspisanie.id as raspisanie_id,'+
'para.para as para,'+
'para.budnie_chasy,'+
'para.vykhodnye_chasy,'+
'raspisanie.stroka as stroka,'+
'raspisanie.nomer_nedeli,'+
'distsiplina.distsiplina_na_russkom as distsiplina_na_russkom,'+
'distsiplina.distsiplina_na_kazakhskom as distsiplina_na_kazakhskom,'+
'prepodavatelmz.fio as fio,'+
'kabinet.nomer_kabineta as nomer_kabineta,'+
'tip_zanyatiya.tip_zanyatiya as tip_zanyatiya,'+
'raspisanie.data as data,'+
'gruppa.nazvanie_gruppy as gruppa_nazvanie_gruppy,'+
'gruppa.yazyk_obucheniya_id as yazyk_obucheniya_id,'+
'podgruppa.podgruppa as podgruppa,'+
'vid_nagruzki.vid_nagruzki as vid_nagruzki '+
'from '+
'raspisanie,para,distsiplina,prepodavatelmz,kabinet,tip_zanyatiya,gruppa,podgruppa,vid_nagruzki '+
'where '+
'raspisanie.para_id=para.id and '+
'raspisanie.distsiplina_id=distsiplina.id and '+
'raspisanie.prepodavatelmz_id=prepodavatelmz.id and '+
'raspisanie.kabinet_id=kabinet.id and '+
'raspisanie.tip_zanyatiya_id=tip_zanyatiya.id and '+
'raspisanie.gruppa_id=gruppa.id and '+
'raspisanie.podgruppa_id=podgruppa.id and '+
'raspisanie.vid_nagruzki_id=vid_nagruzki.id and '+
'raspisanie.deleted=0 and '+
'raspisanie.god_id='+pCurrent_God_id+' and '+
'raspisanie.polugodie_id='+pCurrent_Polugodie_id+' and '+
'raspisanie.nomer_nedeli in ('+inttostr(pCurrentNum)+','+inttostr(pCurrentNum+1)+')';

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS+' order by raspisanie.data,raspisanie.id,para,stroka');
ADQ.Open;

ADQ.Last;
ADQ.First;

ProgressBar1.Position:=0;  //2478
//ShowMessage(inttostr(ADQ.RecordCount)+'   / '+ADQ.SQL.Text);
ProgressBar1.Max:=ADQ.RecordCount;
ProgressBar1.Visible:=true;



pSQL:='';
i:=0;

while not ADQ.Eof do begin

day := LongDayNames[DayOfWeek(strtodate(ADQ.FieldByName('data').AsString))];

if day<>'�������' then pChasy:=ADQ.FieldByName('budnie_chasy').AsString;
if day='�������'  then pChasy:=ADQ.FieldByName('vykhodnye_chasy').AsString;

if ADQ.FieldByName('yazyk_obucheniya_id').AsString='1' then pDistsiplina:=ADQ.FieldByName('distsiplina_na_russkom').AsString;
if ADQ.FieldByName('yazyk_obucheniya_id').AsString='2' then pDistsiplina:=ADQ.FieldByName('distsiplina_na_kazakhskom').AsString;

pSQL:=pSQL+#13#10+ 'INSERT into raspisanie '+
'('+
'data,'+
'raspisanie_id,'+
'para,'+
'stroka,'+
'distsiplina_na_russkom,'+
'fio,'+
'nomer_kabineta,'+
'tip_zanyatiya,'+
'nomer_nedeli,'+
'den_nedeli,'+
'gruppa_nazvanie_gruppy,'+
'podgruppa,'+
'vid_nagruzki, '+
'chasy '+
') '+
'values'+
' ('+
''''+ADQ.FieldByName('data').AsString+''','+
''''+ADQ.FieldByName('raspisanie_id').AsString+''','+
''''+ADQ.FieldByName('para').AsString+''','+
''''+ADQ.FieldByName('stroka').AsString+''','+
''''+pDistsiplina+''','+
''''+ADQ.FieldByName('fio').AsString+''','+
''''+ADQ.FieldByName('nomer_kabineta').AsString+''','+
''''+ADQ.FieldByName('tip_zanyatiya').AsString+''','+
''''+inttostr(fCREATE_DATEf.WeekNum(ADQ.FieldByName('data').AsDateTime))+''','+
''''+day+''','+
''''+ADQ.FieldByName('gruppa_nazvanie_gruppy').AsString+''','+
''''+ADQ.FieldByName('podgruppa').AsString+''','+
''''+ADQ.FieldByName('vid_nagruzki').AsString+''','+
''''+pChasy+''');';

ProgressBar1.Position:=i;
inc(i);
ADQ.Next;
end;

fMWf.PostDATA(trim(pSQL));
//clipboard.astext:=pSQL;
ProgressBar1.Position:=0;
ProgressBar1.Visible:=false;
Screen.Cursor := crDefault;
ShowMessage('������ ������� ���������� �� ������');

end;

procedure TfSELECT_DATE_INTO_RASPf.FormActivate(Sender: TObject);
begin
MonthCalendar1.Date:=now;
ProgressBar1.Visible:=false;
end;

procedure TfSELECT_DATE_INTO_RASPf.Button1Click(Sender: TObject);
begin
Close
end;

end.
