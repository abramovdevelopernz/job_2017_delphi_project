object fTEMPLATE_USERf: TfTEMPLATE_USERf
  Left = 690
  Top = 334
  Width = 660
  Height = 500
  Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 454
    Width = 652
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 652
    Height = 49
    Align = alTop
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 168
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 248
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      TabOrder = 2
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 328
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100
      TabOrder = 3
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 488
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1055#1086#1080#1089#1082
      TabOrder = 4
      OnClick = Button5Click
    end
    object Button6: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1055#1077#1095#1072#1090#1100
      TabOrder = 5
      OnClick = Button6Click
    end
    object Button7: TButton
      Left = 568
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1042#1089#1077' '#1079#1072#1087#1080#1089#1080
      TabOrder = 6
      OnClick = Button7Click
    end
    object Button8: TButton
      Left = 408
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      TabOrder = 7
      OnClick = Button8Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 49
    Width = 652
    Height = 405
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 2
    object ListView1: TListView
      Left = 1
      Top = 1
      Width = 650
      Height = 403
      Align = alClient
      Columns = <>
      Ctl3D = False
      FlatScrollBars = True
      GridLines = True
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      ViewStyle = vsReport
      OnClick = ListView1Click
    end
  end
end
