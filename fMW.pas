unit fMW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, ComCtrls, ExtCtrls, Menus, ADODB, DB,comobj, StdCtrls,
  IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, IdBaseComponent,
  IdAntiFreezeBase, IdAntiFreeze,clipbrd,ShellApi;

type
  TfMWf = class(TForm)
    Panel1: TPanel;
    StatusBar1: TStatusBar;
    MainMenu1: TMainMenu;
    N_APP: TMenuItem;
    N_TEMPLATE_USER: TMenuItem;
    N_LINE_1: TMenuItem;
    N_LINE_2: TMenuItem;
    N_EXIT: TMenuItem;
    N_SPRAVKA: TMenuItem;
    N_SPRAVKA1: TMenuItem;
    N_ABOUT: TMenuItem;
    N_GRAPH: TMenuItem;
    N_TABLES: TMenuItem;
    N_PREPODAVATELMZ : TMenuItem;
    N_SPETSIALMZNOSTMZ : TMenuItem;
    N_VOZMOZHNOSTMZ_RABOTY : TMenuItem;
    N_KABINET : TMenuItem;
    N_TIP_DISTSIPLINY : TMenuItem;
    N_GRUPPA : TMenuItem;
    N_YAZYK_OBUCHENIYA : TMenuItem;
    N_PODGRUPPA : TMenuItem;
    N_GOD : TMenuItem;
    N_POLUGODIE : TMenuItem;
    N_TIP_ZANYATIYA : TMenuItem;
    N_PRAZDNIKI_I_VYKHODNYE : TMenuItem;
    N_PARA : TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    Panel2: TPanel;
    Label1: TLabel;
    Combo_god_id: TComboBox;
    Label2: TLabel;
    Combo_polugodie_id: TComboBox;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    Image1: TImage;
    N8: TMenuItem;
    IdAntiFreeze2: TIdAntiFreeze;
    IdHTTP1: TIdHTTP;
    N9: TMenuItem;
    N10: TMenuItem;
    N12: TMenuItem;
    N11: TMenuItem;
    N13: TMenuItem;
    N21: TMenuItem;
    N31: TMenuItem;

    procedure ShowRecord(lstv: TListview);     //��������� ������ ������ Listview � �� ������ �� ��������� �����
    procedure ConnectDB;                       //��������� ����������� � ���� ������
    procedure LVToExcel(ListView: TListView);  //��������� ������ ������ � Excel �� Listview
    procedure SetLCW(Listview:TListview);
    procedure LoadPOLUGODIE;
    procedure LoadGOD;
    procedure GetR(pCombo:TComboBox;pSID:integer);
    procedure PostDATA(pSQL:widestring);

    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure N_TEMPLATE_USERClick(Sender: TObject);
    procedure N_EXITClick(Sender: TObject);
    procedure N_GRAPHClick(Sender: TObject);
    procedure N_PREPODAVATELMZClick(Sender: TObject);
    procedure N_SPETSIALMZNOSTMZClick(Sender: TObject);
    procedure N_VOZMOZHNOSTMZ_RABOTYClick(Sender: TObject);
    procedure N_KABINETClick(Sender: TObject);
    procedure N_TIP_DISTSIPLINYClick(Sender: TObject);
    procedure N_GRUPPAClick(Sender: TObject);
    procedure N_YAZYK_OBUCHENIYAClick(Sender: TObject);
    procedure N_PODGRUPPAClick(Sender: TObject);
    procedure N_GODClick(Sender: TObject);
    procedure N_POLUGODIEClick(Sender: TObject);
    procedure N_TIP_ZANYATIYAClick(Sender: TObject);
    procedure N_PRAZDNIKI_I_VYKHODNYEClick(Sender: TObject);
    procedure N_PARAClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Combo_god_idChange(Sender: TObject);
    procedure Combo_polugodie_idChange(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N_SPRAVKA1Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure N31Click(Sender: TObject);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fMWf: TfMWf;
  pS:widestring;         //������ �������
  L : TListItem;         //�����
  ADC1:TADOConnection;   //�����������
  ADQ,ADQ1,ADQ2:TADOQuery;    //������
  pProject:string;       //�������� �������
  pAction:string;        //CRUD
  pID:string;            //������� ������������� ������
  pMyUser:string;        //������� ������������
  pNoAction:boolean;     //������ ��������
  pSearch:string;        //������ ������
  pAppStarted,pOtmena:boolean;   //����������� ��� �������� ����� � ���������� �������� ����������� ������ ���� ���
  pLink:string;          //����������� ��� ������ ����� ������� � ������
  pLinkVrem:string;      //����������� ��� ��������� �������� ������ ����� ������� � ������ 
  pSelectedID:string;    //����������� ��� ���������� ������ �� ������������ ������� ���������� Combobox
  pSelectedSID:string;   //����������� ��� ���������� ������ �� ����������� � Listview
  pPhoto:boolean;        //����������� ��� ����� ������� � ����������� ������
  pPhotoVrem:boolean;    //����������� ��� ��������� �������� ����� ������� � ����������� ������
  pTableName:string;     //����������� ��� ����������� ������������ �����
  pTableNameVrem:string; //����������� ��� ��������� �������� �������� ����
  pActionREESTR_PROGULOV,pActionVID_NAGRUZKI,pActionPREPODAVATELMZ,pActionSPETSIALMZNOSTMZ,pActionVOZMOZHNOSTMZ_RABOTY,pActionDISTSIPLINA,pActionKABINET,pActionKABINETY_DISTSIPLIN,pActionTIP_DISTSIPLINY,pActionNAGRUZKA_NA_PREPODAVATELYA,pActionGRUPPA,pActionYAZYK_OBUCHENIYA,pActionRASPISANIE,pActionPODGRUPPA,pActionGOD,pActionPOLUGODIE,pActionTIP_ZANYATIYA,pActionPRAZDNIKI_I_VYKHODNYE,pActionPARA:string;
  pLinkREESTR_PROGULOV,pLinkVID_NAGRUZKI,pLinkPREPODAVATELMZ,pLinkSPETSIALMZNOSTMZ,pLinkVOZMOZHNOSTMZ_RABOTY,pLinkDISTSIPLINA,pLinkKABINET,pLinkKABINETY_DISTSIPLIN,pLinkTIP_DISTSIPLINY,pLinkNAGRUZKA_NA_PREPODAVATELYA,pLinkGRUPPA,pLinkYAZYK_OBUCHENIYA,pLinkRASPISANIE,pLinkPODGRUPPA,pLinkGOD,pLinkPOLUGODIE,pLinkTIP_ZANYATIYA,pLinkPRAZDNIKI_I_VYKHODNYE,pLinkPARA:string;
  pREESTR_PROGULOV_ID,pSPORTZAL,pFIZKULTURA,pVID_NAGRUZKI_ID,pGRUPPA_NAME,pPREPODAVATELMZ_ID,pSPETSIALMZNOSTMZ_ID,pVOZMOZHNOSTMZ_RABOTY_ID,pDISTSIPLINA_ID,pKABINET_ID,pKABINETY_DISTSIPLIN_ID,pTIP_DISTSIPLINY_ID,pNAGRUZKA_NA_PREPODAVATELYA_ID,pGRUPPA_ID,pYAZYK_OBUCHENIYA_ID,pRASPISANIE_ID,pPODGRUPPA_ID,pGOD_ID,pPOLUGODIE_ID,pTIP_ZANYATIYA_ID,pPRAZDNIKI_I_VYKHODNYE_ID,pPARA_ID:string;
  pPRICHINA_MSG:string;
  pAutorization:boolean; //����������� �� ������ � ������ ��� �������� �����������
  pCurrent_God_id,pCurrent_Polugodie_id:string;

  pSum_praktika_chasov_itogo:string;
  pSum_teoriya_chasov_itogo:string;
  pCount_par_teoriya,pCount_par_praktika:string;
  pCurrentLanguage:string;
  pzPREPODAVATELMZ:string;
  pInterval_Till:integer;
  pCurrent_Combo_prepodavatelmz_id_x,pInterval,pInterval_X:string;

implementation

uses
fPREPODAVATELMZ,fSPETSIALMZNOSTMZ,fVOZMOZHNOSTMZ_RABOTY,fDISTSIPLINA,fKABINET,fKABINETY_DISTSIPLIN,fTIP_DISTSIPLINY,fNAGRUZKA_NA_PREPODAVATELYA,fGRUPPA,fYAZYK_OBUCHENIYA,fRASPISANIE,fPODGRUPPA,fGOD,fPOLUGODIE,fTIP_ZANYATIYA,fPRAZDNIKI_I_VYKHODNYE,fPARA,
fLGN,fTEMPLATE_USER,fSHOWRECORD,fGRAPH, fFORM_RASPISANIE, fREESTR_PROGULOV, fPLAN_FACT,fCREATE_DATE,
fLENTA_GRUPP, fSELECT_DATE_INTO_RASP, fREPORT_FORM2, fREPORT_FORM3;

{$R *.dfm}

procedure TfMWf.PostDATA(pSQL:widestring);
var
PostData:TStringList;
begin
PostData:= TStringList.Create;
//ShowMessage(pSQL);
pSQL:=AnsiToUtf8(pSQL);
PostData.Add('sql='+pSQL);
IdHTTP1.Post('http://xxx/sql.php',PostData);
end;

procedure TfMWf.GetR(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfMWf.LoadPOLUGODIE;
begin

Combo_polugodie_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from polugodie where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_polugodie_id.AddItem(ADQ.FieldByName('polugodie').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_polugodie_id.ItemIndex:=0;

end;

procedure TfMWf.LoadGOD;
begin

Combo_god_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from god where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_god_id.AddItem(ADQ.FieldByName('god').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_god_id.ItemIndex:=0;

end;

procedure TfMWf.SetLCW(Listview:TListview);
var
  i,pNoCol:integer;
//  a,b,c:real;
begin

  Listview.Visible:=false;
  pNoCol:=0;

  for i:=0 to Listview.Columns.Count-1 do begin
    if (copy(trim(Listview.Columns.Items[i].Caption),length(Listview.Columns.Items[i].Caption)-2,length(Listview.Columns.Items[i].Caption))='_id') or (Listview.Columns.Items[i].Caption='ID') then pNoCol:=pNoCol+1;
  end;

  for i:=0 to Listview.Columns.Count-1 do begin
    Listview.Column[i].Width:=Trunc(Listview.Width/(Listview.Columns.Count-pNoCol));
  end;

  for i:=0 to Listview.Columns.Count-1 do begin
   if (copy(trim(Listview.Columns.Items[i].Caption),length(Listview.Columns.Items[i].Caption)-2,length(Listview.Columns.Items[i].Caption))='_id') or (Listview.Columns.Items[i].Caption='ID') then Listview.Column[i].Width:=0;
  end;

//  Listview.Column[0].Width:=50;
//  Listview.Column[Listview.Columns.Count-1].Width:= Listview.Column[Listview.Columns.Count-1].Width-50;

  Listview.Visible:=true;

end;


//��� ��������� �������� ����������
procedure TfMWf.FormCreate(Sender: TObject);
begin
//  Image1.Picture.LoadFromFile(ExtractFilePath(Application.ExeName)+'logo.jpg');
  pAppStarted:=false;
  pAutorization:=false;
  pProject:='';
end;


//�������� ���� �������������
procedure TfMWf.N_PREPODAVATELMZClick(Sender: TObject);
begin
pSearch:='';
fPREPODAVATELMZf.Caption:='�������������';
fPREPODAVATELMZf.ShowModal;
end;

//�������� ���� �������������
procedure TfMWf.N_SPETSIALMZNOSTMZClick(Sender: TObject);
begin
pSearch:='';
fSPETSIALMZNOSTMZf.Caption:='�������������';
fSPETSIALMZNOSTMZf.ShowModal;
end;

//�������� ���� ����������� ������
procedure TfMWf.N_VOZMOZHNOSTMZ_RABOTYClick(Sender: TObject);
begin
pSearch:='';
fVOZMOZHNOSTMZ_RABOTYf.Caption:='����������� ������';
fVOZMOZHNOSTMZ_RABOTYf.ShowModal;
end;

//�������� ���� �������
procedure TfMWf.N_KABINETClick(Sender: TObject);
begin
pSearch:='';
fKABINETf.Caption:='�������';
fKABINETf.ShowModal;
end;

//�������� ���� ��� ����������
procedure TfMWf.N_TIP_DISTSIPLINYClick(Sender: TObject);
begin
pSearch:='';
fTIP_DISTSIPLINYf.Caption:='��� ����������';
fTIP_DISTSIPLINYf.ShowModal;
end;

//�������� ���� ������
procedure TfMWf.N_GRUPPAClick(Sender: TObject);
begin
pSearch:='';
fGRUPPAf.Caption:='������';
fGRUPPAf.ShowModal;
end;

//�������� ���� ���� ��������
procedure TfMWf.N_YAZYK_OBUCHENIYAClick(Sender: TObject);
begin
pSearch:='';
fYAZYK_OBUCHENIYAf.Caption:='���� ��������';
fYAZYK_OBUCHENIYAf.ShowModal;
end;

//�������� ���� ���������
procedure TfMWf.N_PODGRUPPAClick(Sender: TObject);
begin
pSearch:='';
fPODGRUPPAf.Caption:='���������';
fPODGRUPPAf.ShowModal;
end;

//�������� ���� ���
procedure TfMWf.N_GODClick(Sender: TObject);
begin
pSearch:='';
fGODf.Caption:='���';
fGODf.ShowModal;
end;

//�������� ���� ���������
procedure TfMWf.N_POLUGODIEClick(Sender: TObject);
begin
pSearch:='';
fPOLUGODIEf.Caption:='���������';
fPOLUGODIEf.ShowModal;
end;

//�������� ���� ��� �������
procedure TfMWf.N_TIP_ZANYATIYAClick(Sender: TObject);
begin
pSearch:='';
fTIP_ZANYATIYAf.Caption:='��� �������';
fTIP_ZANYATIYAf.ShowModal;
end;

//�������� ���� ��������� � ��������
procedure TfMWf.N_PRAZDNIKI_I_VYKHODNYEClick(Sender: TObject);
begin
pSearch:='';
fPRAZDNIKI_I_VYKHODNYEf.Caption:='��������� � ��������';
fPRAZDNIKI_I_VYKHODNYEf.ShowModal;
end;

//�������� ���� ����
procedure TfMWf.N_PARAClick(Sender: TObject);
begin
pSearch:='';
fPARAf.Caption:='����';
fPARAf.ShowModal;
end;


//��������� ������ ������ Listview � �� ������ � ��������� listview
procedure TfMWf.ShowRecord(lstv: TListview);
var
i:integer;
begin

fSHOWRECORDf.ListView1.Clear;

for i:=0 to lstv.Columns.Count-1 do begin

if copy(trim(lstv.Columns.Items[i].Caption),length(lstv.Columns.Items[i].Caption)-2,length(lstv.Columns.Items[i].Caption))<>'_id' then begin 
  L:=fSHOWRECORDf.ListView1.Items.add;
  L.Caption :=lstv.Columns.Items[i].Caption;
  if i=0 then  L.SubItems.Append(lstv.Selected.Caption) else  L.SubItems.Append(lstv.Items[lstv.Selected.index].SubItems[i-1]);
end;

end;

end;

//��������� ����������� � ���� ������
procedure TfMWf.ConnectDB;
begin
  ADC1:=TADOConnection.Create(nil);
  ADC1.LoginPrompt:=false;
  ADC1.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=db1.mdb;Persist Security Info=False';
  ADC1.Connected:=true;
  ADQ := TADOQuery.Create(nil);
  ADQ.Connection := ADC1;
  ADQ1 := TADOQuery.Create(nil);
  ADQ1.Connection := ADC1;
  ADQ2 := TADOQuery.Create(nil);
  ADQ2.Connection := ADC1;
end;

//��������� ������ ������ � ������ �� listview
procedure TfMWf.LVToExcel(ListView: TListView);
var
Sheet,objExcel : Variant;
Title : String;
x,y: Integer;
begin
if ListView.Items.Count=0 then abort;
Screen.Cursor := crHourGlass;
try
Title := '������';
objExcel := CreateOleObject('Excel.Application');
objExcel.Caption := '������';
objExcel.Workbooks.Add;
objExcel.Workbooks[1].Sheets.Add;
objExcel.Workbooks[1].WorkSheets[1].Name := Title;
Sheet := objExcel.Workbooks[1].WorkSheets[Title];
for x := 0 to (ListView.Columns.Count -1) do
 Sheet.Cells[1, (x + 1)] := ListView.Columns.Items[x].Caption;
for y := 0 to (ListView.Items.Count -1) do
begin
Sheet.Cells[(y + 3),1] := ListView.Items.Item[y].Caption;
for x := 0 to (ListView.Columns.Count - 2) do
begin
Sheet.Cells[(y + 3), (x + 2)] := ListView.Items.Item[y].SubItems.Strings[x];
end;
end;
objExcel.Cells.select;
objExcel.Selection.Font.Name:='Arial';
objExcel.Selection.Font.Size:=9;
objExcel.selection.Columns.AutoFit;
except
begin
Screen.Cursor := crDefault;
MessageDlg('Excel transaction cancelled.',mtInformation, [mbOK],0);
exit;
end;
end;
objExcel.Visible := true;
Screen.Cursor := crDefault;
end;

//��� ������ �� ��������� ����� ����>�����
procedure TfMWf.N_EXITClick(Sender: TObject);
begin
  if MessageBox(Self.Handle, '������������� ����� �� ���������?', '�����', MB_OKCANCEL) =  IDOK then Application.Terminate;
end;

//��� ������� �� ������� ESC
procedure TfMWf.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=vk_escape then close;
end;

//��� ������ ������� �����
procedure TfMWf.FormShow(Sender: TObject);
begin

  pMyUser:='Administrator';

  if pAppStarted=false then begin
    ConnectDB;



    if pAutorization=true then begin
      fLGNf.ShowModal;
      N_TEMPLATE_USER.visible:=true;
    end else begin
      N_TEMPLATE_USER.visible:=false;
    end;

    pAppStarted:=true;
    Self.Caption:=pProject;
  end;

end;

//�������� ���� �������������� �������������
procedure TfMWf.N_TEMPLATE_USERClick(Sender: TObject);
begin
  fTEMPLATE_USERf.ShowModal;
end;

//�������� ���� ������ ��������
procedure TfMWf.N_GRAPHClick(Sender: TObject);
begin
  fGRAPHf.ShowModal;
end;

procedure TfMWf.FormActivate(Sender: TObject);
begin

LoadGOD;
LoadPOLUGODIE;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from nastr where id = 1');
ADQ.Open;
GetR(Combo_god_id,ADQ.fieldbyname('god_id').AsInteger);
GetR(Combo_polugodie_id,ADQ.fieldbyname('polugodie_id').AsInteger);
Combo_god_idChange(Self);
end;

procedure TfMWf.Combo_god_idChange(Sender: TObject);
begin
pCurrent_God_id:=Inttostr(Integer(Combo_god_id.Items.Objects[Combo_god_id.ItemIndex]));
pCurrent_Polugodie_id:=Inttostr(Integer(Combo_polugodie_id.Items.Objects[Combo_polugodie_id.ItemIndex]));

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Update nastr set god_id='+pCurrent_God_id+' ,'+
'polugodie_id='+pCurrent_Polugodie_id+' where id=1');
ADQ.ExecSQL;

StatusBar1.Panels[0].Text:='���: '+ Combo_god_id.Text+' ���������: '+Combo_polugodie_id.Text;

end;

procedure TfMWf.Combo_polugodie_idChange(Sender: TObject);
begin
Combo_god_idChange(Self);
end;

procedure TfMWf.N6Click(Sender: TObject);
begin
fREESTR_PROGULOVf.ShowModal;
end;

procedure TfMWf.N7Click(Sender: TObject);
begin
fPLAN_FACTf.ShowModal;
end;

procedure TfMWf.N8Click(Sender: TObject);
begin
fSELECT_DATE_INTO_RASPf.ShowModal;
end;

procedure TfMWf.N9Click(Sender: TObject);
begin
   fFORM_RASPISANIEf.ShowModal;
end;

procedure TfMWf.N10Click(Sender: TObject);
begin
ShellExecute(0,'Open','',nil,nil,SW_SHOWNORMAL);
end;



procedure TfMWf.N_SPRAVKA1Click(Sender: TObject);
begin
ShellExecute(Handle, nil, PChar(ExtractFilePath(Application.exename)+'�������.doc'), nil, nil, SW_RESTORE);
end;

procedure TfMWf.N11Click(Sender: TObject);
begin
fLENTA_GRUPPf.ShowModal;
end;

procedure TfMWf.N21Click(Sender: TObject);
begin
fREPORT_FORM2f.ShowModal;
end;

procedure TfMWf.N31Click(Sender: TObject);
begin
fREPORT_FORM3f.ShowModal;
end;

end.
