unit fNagruzkaPlus;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfNagruzkaPlusf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Button1: TButton;
    Button2: TButton;
    Combo_prepodavatelmz_id: TComboBox;
    Combo_distsiplina_id: TComboBox;
    Combo_podgruppa_id: TComboBox;
    Combo_gruppa_id: TComboBox;
    Label5: TLabel;
    Combo_tip_zanyatiya_id: TComboBox;

    procedure LoadDISTSIPLINA;
    procedure LoadPREPODAVALELMZ;
    procedure LoadGRUPPA;
    procedure LoadPODGRUPPA;
    procedure LoadTIP_ZANYATIYA;

    procedure Button1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fNagruzkaPlusf: TfNagruzkaPlusf;

implementation
uses fMW;
{$R *.dfm}

procedure TfNagruzkaPlusf.LoadDISTSIPLINA;
begin

Combo_distsiplina_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from distsiplina where deleted=0 order by distsiplina_na_russkom');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_distsiplina_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_distsiplina_id.ItemIndex:=0;

end;

procedure TfNagruzkaPlusf.LoadPREPODAVALELMZ;
begin

Combo_prepodavatelmz_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from prepodavatelmz where deleted=0 order by fio');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_prepodavatelmz_id.AddItem(ADQ.FieldByName('fio').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_prepodavatelmz_id.ItemIndex:=0;

end;

procedure TfNagruzkaPlusf.LoadGRUPPA;
begin

Combo_gruppa_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from gruppa where deleted=0 order by nazvanie_gruppy');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_gruppa_id.AddItem(ADQ.FieldByName('nazvanie_gruppy').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_gruppa_id.ItemIndex:=0;

end;

procedure TfNagruzkaPlusf.LoadPODGRUPPA;
begin

Combo_podgruppa_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from podgruppa where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_podgruppa_id.AddItem(ADQ.FieldByName('podgruppa').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_podgruppa_id.ItemIndex:=0;

end;

procedure TfNagruzkaPlusf.LoadTIP_ZANYATIYA;
begin

Combo_tip_zanyatiya_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from tip_zanyatiya where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_tip_zanyatiya_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_tip_zanyatiya_id.ItemIndex:=0;

end;




procedure TfNagruzkaPlusf.Button1Click(Sender: TObject);
begin
Close;
end;

procedure TfNagruzkaPlusf.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=vk_escape then close;
end;

procedure TfNagruzkaPlusf.FormActivate(Sender: TObject);
begin
LoadDISTSIPLINA;
LoadPREPODAVALELMZ;
LoadGRUPPA;
LoadPODGRUPPA;
LoadTIP_ZANYATIYA;
end;

procedure TfNagruzkaPlusf.Button2Click(Sender: TObject);
var
pZ1,pZ2:string;
begin

 if Inttostr(Integer(Combo_tip_zanyatiya_id.Items.Objects[Combo_tip_zanyatiya_id.ItemIndex]))='2' then begin
  pZ1:='2';
  pZ2:='0';
 end;

 if Inttostr(Integer(Combo_tip_zanyatiya_id.Items.Objects[Combo_tip_zanyatiya_id.ItemIndex]))='3' then begin
  pZ1:='0';
  pZ2:='2';
 end;

  pS:='INSERT into '+
  'nagruzka_na_prepodavatelya '+
  '('+
  'prepodavatelmz_id,'+
  'god_id,'+
  'polugodie_id,'+
  'distsiplina_id,'+
  'gruppa_id,'+
  'podgruppa_id,'+
  'vid_nagruzki_id,'+
  'teoriya_chasov_itogo,'+
  'praktika_chasov_itogo,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+

  ' '''+Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+''', '+
  ' '''+pCurrent_God_id+''', '+
  ' '''+pCurrent_Polugodie_id+''', '+
  ' '''+Inttostr(Integer(Combo_distsiplina_id.Items.Objects[Combo_distsiplina_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_gruppa_id.Items.Objects[Combo_gruppa_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_podgruppa_id.Items.Objects[Combo_podgruppa_id.ItemIndex]))+''', '+
  ' ''2'', '+
  ' '''+pZ1+''', '+
  ' '''+pZ2+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

  Close;

end;

end.
