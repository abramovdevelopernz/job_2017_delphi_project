unit fTEMPLATE_USER_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, StdCtrls, ExtCtrls;

type
  TfTEMPLATE_USER_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label_name: TLabel;
    Label_login: TLabel;
    Label_ps: TLabel;
    Check_name: TCheckBox;
    Check_lgn: TCheckBox;   
    Check_ps: TCheckBox;
    Button1: TButton;
    Button2: TButton;
    Edit_name: TEdit;
    Edit_lgn: TEdit;
    Edit_ps: TEdit;

    procedure FormActivated;
    procedure CheckInput;
    procedure CLALL;
    procedure BtnOK;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fTEMPLATE_USER_RECORDf: TfTEMPLATE_USER_RECORDf;

implementation

uses fMW, fTEMPLATE_USER;
{$R *.dfm}

procedure TfTEMPLATE_USER_RECORDf.CheckInput;
begin

if trim(Edit_name.Text)='' then begin
  Edit_name.SetFocus;
  abort;
end;

if trim(Edit_lgn.Text)='' then begin
  Edit_lgn.SetFocus;
  abort;
end;

if trim(Edit_ps.Text)='' then begin
  Edit_ps.SetFocus;
  abort;
end;

end;

procedure TfTEMPLATE_USER_RECORDf.CLALL;
begin

  Check_name.Checked:=false;
  Check_name.Visible:=false;
  Check_lgn.Checked:=false;
  Check_lgn.Visible:=false;
  Check_ps.Checked:=false;
  Check_ps.Visible:=false;

  Edit_name.Clear;
  Edit_lgn.Clear;
  Edit_ps.Clear;

end;

procedure TfTEMPLATE_USER_RECORDf.FormActivated;
begin

  CLALL;

if pAction='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pAction='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from template_user where id='+pID);
  ADQ.Open;

  ADQ.First;

  Edit_name.Text:=ADQ.fieldbyname('name').AsString;
  Edit_lgn.Text:=ADQ.fieldbyname('lgn').AsString;
  Edit_ps.Text:=ADQ.fieldbyname('ps').AsString;

end;

if pAction='SEARCH' then begin
  Self.Caption:='����� ������';
  Check_name.Visible:=true;
  Check_lgn.Visible:=true;
  Check_ps.Visible:=true;
end;

  Edit_name.SetFocus;

end;

procedure TfTEMPLATE_USER_RECORDf.BtnOK;
begin

if pAction='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'template_user '+
  '('+

  'name,'+
  'lgn,'+
  'ps,'+

  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+

  ' '''+trim(Edit_name.Text)+''', '+
  ' '''+trim(Edit_lgn.Text)+''', '+
  ' '''+trim(Edit_ps.Text)+''', '+

  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pAction='UPDATE' then begin

  CheckInput;

  pS:='UPDATE '+
  'template_user set '+

  'name='''+trim(Edit_name.Text)+''','+
  'lgn='''+trim(Edit_lgn.Text)+''','+
  'ps='''+trim(Edit_ps.Text)+''','+

  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

  fTEMPLATE_USERf.ListView1.Selected.SubItems[0]:=trim(Edit_name.Text);
  fTEMPLATE_USERf.ListView1.Selected.SubItems[1]:=trim(Edit_lgn.Text);
  fTEMPLATE_USERf.ListView1.Selected.SubItems[2]:=trim(Edit_ps.Text);

end;

if pAction='SEARCH' then begin

  if (Check_name.Checked=false) and  (Check_lgn.Checked=false) and (Check_ps.Checked=false) then abort;
  pSearch:='';
  if Check_name.Checked=true then pSearch:=pSearch+' and template_user.name like '''+trim(Edit_name.Text)+''' ';
  if Check_lgn.Checked=true then pSearch:=pSearch+' and template_user.lgn like '''+trim(Edit_lgn.Text)+''' ';
  if Check_ps.Checked=true then pSearch:=pSearch+' and template_user.ps like '''+trim(Edit_ps.Text)+''' ';

end;

Close;

end;

procedure TfTEMPLATE_USER_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

procedure TfTEMPLATE_USER_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfTEMPLATE_USER_RECORDf.FormKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfTEMPLATE_USER_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

end.
