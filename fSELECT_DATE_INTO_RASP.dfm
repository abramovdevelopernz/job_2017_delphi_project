object fSELECT_DATE_INTO_RASPf: TfSELECT_DATE_INTO_RASPf
  Left = 480
  Top = 197
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1100' '#1085#1072' '#1089#1072#1081#1090' - http://ukmed.kz'
  ClientHeight = 325
  ClientWidth = 308
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 272
    Width = 308
    Height = 53
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWindow
    TabOrder = 0
    object Button1: TButton
      Left = 16
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 120
      Top = 8
      Width = 171
      Height = 33
      Cursor = crHandPoint
      Caption = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1085#1072' '#1089#1072#1081#1090
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 308
    Height = 272
    Align = alClient
    BevelOuter = bvLowered
    Color = clWindow
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 273
      Height = 52
      Alignment = taCenter
      Caption = 
        #1044#1083#1103' '#1086#1090#1087#1088#1072#1074#1082#1080' '#1076#1072#1085#1085#1099#1093' '#1085#1072' '#1089#1072#1081#1090' '#1085#1077#1086#1073#1093#1086#1076#1080#1084#1086' '#1074#1099#1073#1088#1072#1090#1100' '#1074' '#1082#1072#1095#1077#1089#1090#1074#1077' '#1076#1072#1090#1099' -' +
        ' ['#1087#1086#1085#1077#1076#1077#1083#1100#1085#1080#1082'],  '#1080' '#1086#1090' '#1076#1072#1090#1099' '#1082#1086#1090#1086#1088#1091#1102' '#1074#1099' '#1074#1099#1073#1077#1088#1080#1090#1077' '#1087#1088#1086#1080#1079#1086#1081#1076#1077#1090' '#1076#1074#1091#1093#1085#1077 +
        #1076#1077#1083#1100#1085#1072#1103' '#1074#1099#1075#1088#1091#1079#1082#1072
      WordWrap = True
    end
    object MonthCalendar1: TMonthCalendar
      Left = 64
      Top = 72
      Width = 177
      Height = 154
      Date = 42842.514880335650000000
      TabOrder = 0
    end
    object ProgressBar1: TProgressBar
      Left = 8
      Top = 233
      Width = 281
      Height = 16
      TabOrder = 1
    end
  end
end
