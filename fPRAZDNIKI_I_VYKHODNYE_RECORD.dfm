object fPRAZDNIKI_I_VYKHODNYE_RECORDf: TfPRAZDNIKI_I_VYKHODNYE_RECORDf
  Left = 538
  Top = 254
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  ClientHeight = 145
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 97
    Width = 472
    Height = 48
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = 'OK'
      TabOrder = 0
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 472
    Height = 97
    Align = alClient
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label_god_id: TLabel
      Left = 120
      Top = 16
      Width = 18
      Height = 13
      Caption = #1043#1086#1076
    end
    object Label_polugodie_id: TLabel
      Left = 84
      Top = 40
      Width = 54
      Height = 13
      Caption = #1055#1086#1083#1091#1075#1086#1076#1080#1077
    end
    object Label_data: TLabel
      Left = 112
      Top = 64
      Width = 26
      Height = 13
      Caption = #1044#1072#1090#1072
    end
    object Check_god_id: TCheckBox
      Left = 448
      Top = 16
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 0
    end
    object Check_polugodie_id: TCheckBox
      Left = 448
      Top = 40
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 1
    end
    object Check_data: TCheckBox
      Left = 448
      Top = 64
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 3
    end
    object Combo_god_id: TComboBox
      Left = 144
      Top = 16
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 2
    end
    object Button_god_id: TButton
      Left = 416
      Top = 16
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 4
      OnClick = Button_god_idClick
    end
    object Combo_polugodie_id: TComboBox
      Left = 144
      Top = 40
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 5
    end
    object Button_polugodie_id: TButton
      Left = 416
      Top = 40
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 6
      OnClick = Button_polugodie_idClick
    end
    object DateTime_data: TDateTimePicker
      Left = 144
      Top = 64
      Width = 297
      Height = 21
      Date = 40887.604251655100000000
      Time = 40887.604251655100000000
      TabOrder = 7
    end
  end
end
