object fBUSY_KABINETf: TfBUSY_KABINETf
  Left = 291
  Top = 201
  Width = 928
  Height = 480
  Caption = #1047#1072#1085#1103#1090#1099#1077' '#1082#1072#1073#1080#1085#1077#1090#1099' '#1085#1072' '#1090#1077#1082#1091#1097#1091#1102' '#1076#1072#1090#1091
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 434
    Width = 920
    Height = 19
    Color = clWindow
    Panels = <>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 920
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    Color = clWindow
    TabOrder = 1
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Caption = #1057#1090#1072#1088#1090
      TabOrder = 1
      OnClick = Button2Click
    end
    object DateTimePicker1: TDateTimePicker
      Left = 168
      Top = 12
      Width = 89
      Height = 21
      Date = 42815.863121250000000000
      Time = 42815.863121250000000000
      TabOrder = 2
      OnChange = DateTimePicker1Change
    end
    object Button3: TButton
      Left = 264
      Top = 8
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Caption = #1055#1077#1095#1072#1090#1100
      TabOrder = 3
      OnClick = Button3Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 920
    Height = 393
    Align = alClient
    BevelOuter = bvLowered
    Color = clWindow
    TabOrder = 2
    object ListView4: TListView
      Left = 1
      Top = 17
      Width = 918
      Height = 375
      Align = alClient
      Columns = <
        item
          Caption = 'ID'
          Width = 1
        end
        item
          Caption = #1040#1091#1076#1080#1090#1086#1088#1080#1103
          Width = 85
        end
        item
          Caption = '1'
          Width = 40
        end
        item
          Caption = '2'
        end
        item
          Caption = '3'
        end
        item
          Caption = '4'
        end
        item
          Caption = '5'
        end
        item
          Caption = '6'
        end
        item
          Caption = '7'
        end>
      Ctl3D = False
      FlatScrollBars = True
      GridLines = True
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      ViewStyle = vsReport
    end
    object ProgressBar1: TProgressBar
      Left = 1
      Top = 1
      Width = 918
      Height = 16
      Align = alTop
      TabOrder = 1
    end
  end
end
