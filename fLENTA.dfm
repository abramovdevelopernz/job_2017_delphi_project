object fLENTAf: TfLENTAf
  Left = 235
  Top = 128
  Width = 928
  Height = 480
  Caption = #1051#1077#1085#1090#1072' '#1087#1088#1077#1087#1086#1076#1072#1074#1072#1090#1077#1083#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 920
    Height = 49
    Align = alTop
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1055#1077#1095#1072#1090#1100
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 168
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1055#1086#1080#1089#1082
      TabOrder = 2
      OnClick = Button3Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 49
    Width = 920
    Height = 385
    Align = alClient
    BevelOuter = bvLowered
    Color = clHighlightText
    TabOrder = 1
    object ListView1: TListView
      Left = 1
      Top = 1
      Width = 918
      Height = 383
      Align = alClient
      Columns = <
        item
          Caption = 'ID'
        end
        item
          Caption = #1055#1088#1077#1087#1086#1076#1072#1074#1072#1090#1077#1083#1100
          Width = 100
        end
        item
          Caption = #1044#1072#1090#1072
          Width = 100
        end
        item
          Caption = #1055#1072#1088#1072
          Width = 100
        end
        item
          Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1072
          Width = 100
        end
        item
          Caption = #1043#1088#1091#1087#1087#1072
          Width = 100
        end
        item
          Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
          Width = 100
        end
        item
          Caption = #1058#1080#1087' '#1079#1072#1085#1103#1090#1080#1103
          Width = 100
        end
        item
          Caption = #1053#1086#1084#1077#1088' '#1082#1072#1073#1080#1085#1077#1090#1072
          Width = 100
        end>
      Ctl3D = False
      FlatScrollBars = True
      GridLines = True
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      ViewStyle = vsReport
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 434
    Width = 920
    Height = 19
    Color = clWindow
    Panels = <
      item
        Width = 300
      end>
  end
end
