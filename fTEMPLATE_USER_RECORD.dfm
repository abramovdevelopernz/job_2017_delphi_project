object fTEMPLATE_USER_RECORDf: TfTEMPLATE_USER_RECORDf
  Left = 739
  Top = 150
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = 'fTEMPLATE_USER_RECORDf'
  ClientHeight = 143
  ClientWidth = 357
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 357
    Height = 95
    Align = alClient
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label_name: TLabel
      Left = 64
      Top = 16
      Width = 27
      Height = 13
      Caption = #1060#1048#1054
    end
    object Label_login: TLabel
      Left = 60
      Top = 40
      Width = 31
      Height = 13
      Caption = #1051#1086#1075#1080#1085
    end
    object Label_ps: TLabel
      Left = 53
      Top = 64
      Width = 38
      Height = 13
      Caption = #1055#1072#1088#1086#1083#1100
    end
    object Edit_name: TEdit
      Left = 96
      Top = 16
      Width = 233
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 0
      Text = 'Edit_name'
    end
    object Check_name: TCheckBox
      Left = 336
      Top = 16
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 1
    end
    object Edit_lgn: TEdit
      Left = 96
      Top = 40
      Width = 233
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 2
      Text = 'Edit_lgn'
    end
    object Check_lgn: TCheckBox
      Left = 336
      Top = 40
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 3
    end
    object Edit_ps: TEdit
      Left = 96
      Top = 64
      Width = 233
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 4
      Text = 'Edit_ps'
    end
    object Check_ps: TCheckBox
      Left = 336
      Top = 64
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 5
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 95
    Width = 357
    Height = 48
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = 'OK'
      TabOrder = 1
      OnClick = Button2Click
    end
  end
end
