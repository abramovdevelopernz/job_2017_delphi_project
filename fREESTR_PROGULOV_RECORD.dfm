object fREESTR_PROGULOV_RECORDf: TfREESTR_PROGULOV_RECORDf

  Width = 480
  Left = 552
  Top = 215
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  ClientHeight = 345
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel

    Color = clWindow
    Left = 0
    Top = 286
    Width = 472
    Height = 48
    Align = alBottom
    BevelOuter = bvLowered
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = '�������'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = 'OK'
      TabOrder = 0
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel

    Color = clWindow
    Left = 0
    Top = 0
    Width = 472
    Height = 286
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
   object Label_god_id: TLabel
      Left = 8
      Top = 16
      Width = 27
      Height = 13
      Caption = '���'
    end
   object Label_polugodie_id: TLabel
      Left = 8
      Top = 40
      Width = 27
      Height = 13
      Caption = '���������'
    end
   object Label_data_progula: TLabel
      Left = 8
      Top = 64
      Width = 27
      Height = 13
      Caption = '���� �������'
    end
   object Label_para_id: TLabel
      Left = 8
      Top = 88
      Width = 27
      Height = 13
      Caption = '����'
    end
   object Label_nomer_stroki: TLabel
      Left = 8
      Top = 112
      Width = 27
      Height = 13
      Caption = '����� ������'
    end
   object Label_prepodavatelmz_id: TLabel
      Left = 8
      Top = 136
      Width = 27
      Height = 13
      Caption = '�������������'
    end
   object Label_distsiplina_id: TLabel
      Left = 8
      Top = 160
      Width = 27
      Height = 13
      Caption = '����������'
    end
   object Label_podgruppa_id: TLabel
      Left = 8
      Top = 184
      Width = 27
      Height = 13
      Caption = '���������'
    end
   object Label_tip_zanyatiya_id: TLabel
      Left = 8
      Top = 208
      Width = 27
      Height = 13
      Caption = '��� �������'
    end
   object Label_kabinet_id: TLabel
      Left = 8
      Top = 232
      Width = 27
      Height = 13
      Caption = '�������'
    end
   object Label_prichina_progula: TLabel
      Left = 8
      Top = 256
      Width = 27
      Height = 13
      Caption = '������� �������'
    end
   object Check_god_id: TCheckBox
      Left = 448
      Top = 16
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 1
    end
   object Check_polugodie_id: TCheckBox
      Left = 448
      Top = 40
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 2
    end
   object Check_data_progula: TCheckBox
      Left = 448
      Top = 64
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 3
    end
   object Check_para_id: TCheckBox
      Left = 448
      Top = 88
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 4
    end
   object Check_nomer_stroki: TCheckBox
      Left = 448
      Top = 112
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 5
    end
   object Check_prepodavatelmz_id: TCheckBox
      Left = 448
      Top = 136
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 6
    end
   object Check_distsiplina_id: TCheckBox
      Left = 448
      Top = 160
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 7
    end
   object Check_podgruppa_id: TCheckBox
      Left = 448
      Top = 184
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 8
    end
   object Check_tip_zanyatiya_id: TCheckBox
      Left = 448
      Top = 208
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 9
    end
   object Check_kabinet_id: TCheckBox
      Left = 448
      Top = 232
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 10
    end
   object Check_prichina_progula: TCheckBox
      Left = 448
      Top = 256
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 11
    end
    object Combo_god_id: TComboBox
      Left = 144
      Top = 16
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
    end
    object Button_god_id: TButton
      Left = 416
      Top = 16
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      OnClick = Button_god_idClick
    end
    object Combo_polugodie_id: TComboBox
      Left = 144
      Top = 40
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
    end
    object Button_polugodie_id: TButton
      Left = 416
      Top = 40
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      OnClick = Button_polugodie_idClick
    end
    object DateTime_data_progula: TDateTimePicker
      Left = 144
      Top = 64
      Width = 297
      Height = 21
      Date = 40887.604251655100000000
      Time = 40887.604251655100000000
    end
    object Combo_para_id: TComboBox
      Left = 144
      Top = 88
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
    end
    object Button_para_id: TButton
      Left = 416
      Top = 88
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      OnClick = Button_para_idClick
    end
    object Edit_nomer_stroki: TEdit
      Left = 144
      Top = 112
      Width = 297
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      Text = 'Edit_nomer_stroki'
    end
    object Combo_prepodavatelmz_id: TComboBox
      Left = 144
      Top = 136
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
    end
    object Button_prepodavatelmz_id: TButton
      Left = 416
      Top = 136
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      OnClick = Button_prepodavatelmz_idClick
    end
    object Combo_distsiplina_id: TComboBox
      Left = 144
      Top = 160
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
    end
    object Button_distsiplina_id: TButton
      Left = 416
      Top = 160
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      OnClick = Button_distsiplina_idClick
    end
    object Combo_podgruppa_id: TComboBox
      Left = 144
      Top = 184
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
    end
    object Button_podgruppa_id: TButton
      Left = 416
      Top = 184
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      OnClick = Button_podgruppa_idClick
    end
    object Combo_tip_zanyatiya_id: TComboBox
      Left = 144
      Top = 208
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
    end
    object Button_tip_zanyatiya_id: TButton
      Left = 416
      Top = 208
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      OnClick = Button_tip_zanyatiya_idClick
    end
    object Combo_kabinet_id: TComboBox
      Left = 144
      Top = 232
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
    end
    object Button_kabinet_id: TButton
      Left = 416
      Top = 232
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      OnClick = Button_kabinet_idClick
    end
    object Edit_prichina_progula: TEdit
      Left = 144
      Top = 256
      Width = 297
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      Text = 'Edit_prichina_progula'
    end

  end
end