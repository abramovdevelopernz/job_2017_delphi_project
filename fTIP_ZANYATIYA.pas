unit fTIP_ZANYATIYA;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, ComCtrls, StdCtrls, Menus, Clipbrd, Idglobal, JPEG;

type
  TfTIP_ZANYATIYAf = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    ListView1: TListView;
    PopupMenu1: TPopupMenu;
    Button8: TButton;
    Panel3: TPanel;
    Image1: TImage;
    Splitter1: TSplitter;
    N_PHOTO: TMenuItem;
    OpenDialog1: TOpenDialog;
    PopupMenu2: TPopupMenu;
    N_LOAD_IMAGE: TMenuItem;
    N_DELETE_IMAGE: TMenuItem;

    procedure CreateCol;                 //�������� ������� ��� ������
    procedure LV1(pSearchString:string); //�������� ������ ��������
    procedure SearchRecord;              //����� �������
    procedure UpdateRecord;              //���������� �������
    procedure InsertRecord;              //���������� �������
    procedure DeleteRecord;              //�������� �������

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure ListView1Click(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure ListView1KeyUp(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure Image1Click(Sender: TObject);
    procedure N_LOAD_IMAGEClick(Sender: TObject);
    procedure N_PHOTOClick(Sender: TObject);
    procedure N_DELETE_IMAGEClick(Sender: TObject);
    procedure FormResize(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fTIP_ZANYATIYAf: TfTIP_ZANYATIYAf;

implementation

uses
fMW,
fSHOWRECORD,fTIP_ZANYATIYA_RECORD;
{$R *.dfm}

//���������� �������
procedure TfTIP_ZANYATIYAf.InsertRecord;
begin
  pNoAction:=false;
  pActionTIP_ZANYATIYA:='INSERT';
  fTIP_ZANYATIYA_RECORDf.showmodal;
  if pNoAction=false then LV1('');
end;

//���������� �������
procedure TfTIP_ZANYATIYAf.UpdateRecord;
begin
  if ListView1.ItemIndex=-1 then abort;
  pNoAction:=false;
  pID:=ListView1.Selected.Caption;
  pActionTIP_ZANYATIYA:='UPDATE';
  fTIP_ZANYATIYA_RECORDf.showmodal;
end;

//����� �������
procedure TfTIP_ZANYATIYAf.SearchRecord;
begin
  pNoAction:=false;
  pActionTIP_ZANYATIYA:='SEARCH';
  fTIP_ZANYATIYA_RECORDf.showmodal;

  if pNoAction=false then begin
    LV1(pSearch);
    Button7.Visible:=true;
  end;

end;

//�������� �������
procedure TfTIP_ZANYATIYAf.DeleteRecord;
begin

if ListView1.ItemIndex=-1 then abort;

if MessageBox(Self.Handle, '������������� �������� ������?', '�������� ������', MB_OKCANCEL) =  IDOK then begin

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Update tip_zanyatiya set deleted=1,deleteuser='''+pMyUser+''',deletedate='''+datetostr(now)+' '+timetostr(now)+''' where id='+ListView1.Selected.Caption);
  ADQ.ExecSQL;

LV1('');

end;

end;

//�������� ������ ��������
procedure TfTIP_ZANYATIYAf.LV1(pSearchString:string);
begin

  ListView1.Clear;
  Screen.Cursor := crHourglass;
  Application.ProcessMessages;

pS:='Select '+
' tip_zanyatiya.tip_zanyatiya,'+ 
' tip_zanyatiya.id '+
' from '+
' tip_zanyatiya '+
' where '+
' tip_zanyatiya.deleted=0';

  if trim(pSearchString)<>'' then pS:=pS+pSearchString;

  if trim(pLinkTIP_ZANYATIYA)<>'' then begin 
   pS:=pS+pLinkTIP_ZANYATIYA;
   Button5.Visible:=false;
  end;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS+' order by tip_zanyatiya.id');
  ADQ.Open;

  if ADQ.RecordCount = 0 then
  begin
    ListView1.Clear;
    StatusBar1.panels[0].text:='����� ������� : 0';
    Screen.Cursor := crDefault;
    exit;
  end;

  ListView1.Visible := false;
  ListView1.Items.Clear;

  ADQ.First;
  while not ADQ.Eof do
  begin
    L := ListView1.Items.Add;
    L.Caption := trim(ADQ.fieldbyname('id').AsString);
    L.SubItems.Append(ADQ.fieldbyname('tip_zanyatiya').AsString); 
    ADQ.Next;
  end;

  ListView1.Visible := true;

  if ListView1.Items.Count>0 then begin
    ListView1.Items[ListView1.Items.Count-1].Selected:=true;
    Button3.Enabled:=true;
    Button4.Enabled:=true;
    Button8.Enabled:=true;
  end else begin
    Button3.Enabled:=false;
    Button4.Enabled:=false;
    Button8.Enabled:=false;
  end;

  StatusBar1.panels[0].text:='����� ������� : '+inttostr(ADQ.recordcount);

  if Self.Caption='����� ��� �������' then begin
    L := ListView1.FindCaption(1,pSelectedSID,True,True,False);
      if L <> nil then
        begin
          ListView1.Selected := L;
          L.MakeVisible(True);
          ListView1.SetFocus;
        end;
  end;

  Screen.Cursor := crDefault;

end;

//�������� ������� ��� ������ 
procedure TfTIP_ZANYATIYAf.CreateCol;
var
  i:integer;
begin

  pTableName:='TIP_ZANYATIYA';

  for i:=0 to ListView1.Columns.Count-1 do begin
    ListView1.Columns.Delete(0);
  end;

  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='��� �������';
  ListView1.Column[1].Width:=100;

end;

procedure TfTIP_ZANYATIYAf.ListView1DblClick(Sender: TObject);
begin

  if Self.Caption='����� ��� �������' then begin
    pNoAction:=false;
    pSelectedID:=ListView1.Selected.Caption;
    Close;
  end;

  if Self.Caption<>'����� ��� �������' then  Button3Click(Self);

end;

procedure TfTIP_ZANYATIYAf.FormActivate(Sender: TObject);
begin
  if pLink<>'' then begin
    Button5.Visible:=false;
    Button7.Visible:=false;
  end else begin
    Button5.Visible:=true;
    Button7.Visible:=true;
  end;

  Button7.Visible:=false;

  pPhoto:=false;

  Splitter1.Visible:=false;
  if pPhoto=true then begin
    Panel3.Visible:=false;
    N_PHOTO.Visible:=true;
    N_PHOTO.Checked:=false;

  end else begin
    Panel3.Visible:=false;
    N_PHOTO.Visible:=false;
  end;

  CreateCol;

  LV1('');
  fMWf.SetLCW(ListView1);  
end;

procedure TfTIP_ZANYATIYAf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close;
end;

procedure TfTIP_ZANYATIYAf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_insert then Button2Click(Self);
  if key=vk_f2 then Button3Click(Self);
  if key=vk_delete then Button4Click(Self);
  if key=vk_f3 then Button5Click(Self);
  if key=vk_escape then close;
end;

procedure TfTIP_ZANYATIYAf.Button2Click(Sender: TObject);
begin
  InsertRecord;
end;

procedure TfTIP_ZANYATIYAf.Button3Click(Sender: TObject);
begin
  UpdateRecord;
end;

procedure TfTIP_ZANYATIYAf.Button4Click(Sender: TObject);
begin
  DeleteRecord;
end;

procedure TfTIP_ZANYATIYAf.Button5Click(Sender: TObject);
begin
  SearchRecord;
end;

procedure TfTIP_ZANYATIYAf.Button6Click(Sender: TObject);
begin
  fMWf.LVToExcel(ListView1);
end;

procedure TfTIP_ZANYATIYAf.ListView1Click(Sender: TObject);
begin
if (ListView1.ItemIndex<0) then begin
  Button3.Enabled:=false;
  Button4.Enabled:=false;
  Button8.Enabled:=false;
  Panel3.Visible:=false;
end else begin
  Button3.Enabled:=true;
  Button4.Enabled:=true;
  Button8.Enabled:=true;

  if N_PHOTO.Checked=true then begin
    Panel3.Visible:=true;
    Splitter1.Visible:=true;
    Splitter1.Align:=alNone;
    Panel3.Align:=alNone;
    ListView1.Align:=alNone;
    Panel3.Align:=alRight;
    Splitter1.Align:=alRight;
    ListView1.Align:=alClient;

    if FileExists(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg')= true then begin
      Image1.Picture.LoadFromFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg');
    end else begin
      image1.Picture.Bitmap:=nil;
    end;

  end else begin
    Panel3.Visible:=false;
    Splitter1.Visible:=false;
  end;

end;
end;

procedure TfTIP_ZANYATIYAf.Button7Click(Sender: TObject);
begin
  Button7.Visible:=false;
  LV1('');
end;

procedure TfTIP_ZANYATIYAf.Button8Click(Sender: TObject);
begin
if ListView1.ItemIndex>-1 then begin
  fMWf.ShowRecord(ListView1);
  fSHOWRECORDf.ShowModal;
end;
end;

procedure TfTIP_ZANYATIYAf.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  pPhoto:=false;
end;

procedure TfTIP_ZANYATIYAf.N_PHOTOClick(Sender: TObject);
begin
if N_PHOTO.Checked=true then begin
  Splitter1.Visible:=false;
  N_PHOTO.Checked:=false;
  Panel3.Visible:=false;
end else begin
  N_PHOTO.Checked:=true;
  Panel3.Visible:=true;
  Splitter1.Visible:=true;
  Splitter1.Align:=alNone;
  Panel3.Align:=alNone;
  ListView1.Align:=alNone;
  Panel3.Align:=alRight;
  Splitter1.Align:=alRight;
  ListView1.Align:=alClient;
end;
end;

procedure TfTIP_ZANYATIYAf.N_LOAD_IMAGEClick(Sender: TObject);
begin
  if ListView1.Items.Count=0 then abort;
  if MessageBox(Self.Handle, '������� �����������?', '����� �����������', MB_OKCANCEL) =  IDCANCEL then abort;
  OpenDialog1.Execute;
  if trim(OpenDialog1.FileName)='' then abort;
  Application.ProcessMessages;
  Screen.Cursor := crHourglass;
  DeleteFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg');
  CopyFileTo(trim(OpenDialog1.FileName),ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg');
  Image1.Picture.LoadFromFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg');
  Screen.Cursor := crDefault;
end;

procedure TfTIP_ZANYATIYAf.N_DELETE_IMAGEClick(Sender: TObject);
begin
 if ListView1.Items.Count=0 then abort;
 if MessageBox(Self.Handle, '������� �����������?', '�������� �����', MB_OKCANCEL) =  IDCANCEL then abort;
   if FileExists(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg')= true then begin
     DeleteFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg');
     Image1.Picture.Bitmap:=nil;
   end;
end;

procedure TfTIP_ZANYATIYAf.ListView1KeyUp(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  ListView1Click(Self);
end;

procedure TfTIP_ZANYATIYAf.Image1Click(Sender: TObject);
begin
  Button8Click(Self);
end;

procedure TfTIP_ZANYATIYAf.FormResize(Sender: TObject);
begin
if ListView1.Columns.Count>0 then fMWf.SetLCW(ListView1);
end;

end.