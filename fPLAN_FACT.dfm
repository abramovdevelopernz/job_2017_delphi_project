object fPLAN_FACTf: TfPLAN_FACTf
  Left = 273
  Top = 137
  Width = 821
  Height = 340
  Caption = #1055#1083#1072#1085'/'#1060#1072#1082#1090' '#1074#1099#1095#1080#1090#1072#1085#1085#1086#1081' '#1090#1077#1086#1088#1080#1080'/'#1087#1088#1072#1082#1090#1080#1082#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 294
    Width = 813
    Height = 19
    Color = clWindow
    Panels = <>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 813
    Height = 49
    Align = alTop
    BevelOuter = bvLowered
    Color = clWindow
    TabOrder = 1
    object Label3: TLabel
      Left = 176
      Top = 6
      Width = 79
      Height = 13
      Caption = #1055#1088#1077#1087#1086#1076#1072#1074#1072#1090#1077#1083#1100
    end
    object Label1: TLabel
      Left = 480
      Top = 6
      Width = 68
      Height = 13
      Caption = #1042#1080#1076' '#1085#1072#1075#1088#1091#1079#1082#1080
    end
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1056#1072#1089#1095#1077#1090
      TabOrder = 1
      OnClick = Button2Click
    end
    object Combo_prepodavatelmz_id: TComboBox
      Left = 176
      Top = 20
      Width = 265
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 2
    end
    object Button_prepodavatelmz_id: TButton
      Left = 448
      Top = 19
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 3
      OnClick = Button_prepodavatelmz_idClick
    end
    object Combo_vid_nagruzki_id: TComboBox
      Left = 480
      Top = 20
      Width = 265
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 4
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 49
    Width = 813
    Height = 245
    Align = alClient
    BevelOuter = bvLowered
    Color = clWindow
    TabOrder = 2
    object ListView1: TListView
      Left = 1
      Top = 1
      Width = 811
      Height = 243
      Align = alClient
      Columns = <
        item
          Caption = 'ID'
        end
        item
          Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1072
        end
        item
          Caption = #1058#1077#1086#1088#1080#1103' '#1055#1051#1040#1053'/'#1060#1040#1050#1058
        end
        item
          Caption = #1055#1088#1072#1082#1090#1080#1082#1072' '#1055#1051#1040#1053'/'#1060#1040#1050#1058
        end>
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FlatScrollBars = True
      GridLines = True
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      ParentFont = False
      TabOrder = 0
      ViewStyle = vsReport
    end
  end
end
