object fGRAPHf: TfGRAPHf
  Left = 322
  Top = 178
  Width = 660
  Height = 500
  Caption = '������'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Color = clWhite
    Left = 0
    Top = 454
    Width = 652
    Height = 19
    Panels = <
      item
        Width = 300
      end>
  end
  object Panel1: TPanel

    Color = clWhite
    Left = 0
    Top = 0
    Width = 652
    Height = 49
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 1
    DesignSize = (
      652
      49)
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = '�������'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = '������'
      TabOrder = 1
      OnClick = Button2Click
    end
    object ComboBox1: TComboBox
      Left = 176
      Top = 16
      Width = 465
      Height = 21
      Cursor = crHandPoint
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight] 
      ItemHeight = 13
      TabOrder = 2
      OnChange = ComboBox1Change
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 49
    Width = 652
    Height = 405
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 2
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 650
      Height = 403
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = '������'
        object ListView1: TListView
          Left = 0
          Top = 0
          Width = 642
          Height = 375
          Align = alClient
          Columns = <>
          Ctl3D = False
          FlatScrollBars = True
          GridLines = True
          HideSelection = False
          ReadOnly = True
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
        end
      end
      object TabSheet2: TTabSheet
        Caption = '������'
        ImageIndex = 1
        object Chart1: TChart

    Color = clWhite
          Left = 0
          Top = 0
          Width = 642
          Height = 375
          AllowPanning = pmNone
          AllowZoom = False
          BackWall.Brush.Color = clWhite
          BackWall.Brush.Style = bsClear
          BackWall.Pen.Visible = False
          Title.Text.Strings = ( 
            ' ')
          AxisVisible = False
          ClipPoints = False
          Frame.Visible = False
          View3DOptions.Elevation = 315
          View3DOptions.Orthogonal = False
          View3DOptions.Perspective = 0
          View3DOptions.Rotation = 360
          View3DWalls = False
          Align = alClient
          BorderStyle = bsSingle
          TabOrder = 0
          object Series1: TPieSeries
            Marks.ArrowLength = 8
            Marks.Visible = True 
            SeriesColor = clRed
            OtherSlice.Text = 'Other'
            PieValues.DateTime = False
            PieValues.Name = 'Pie'
            PieValues.Multiplier = 1.000000000000000000
            PieValues.Order = loNone
          end
        end
      end 
    end
  end
end