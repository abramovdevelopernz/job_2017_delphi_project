unit fGRAPH;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, Menus, ComCtrls, StdCtrls, ExtCtrls, TeeProcs, TeEngine, Chart, Series, Clipbrd;

type
  TfGRAPHf = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    ListView1: TListView;
    Chart1: TChart;
    ComboBox1: TComboBox;
    Series1: TPieSeries;

    procedure LoadCombo;  //��������� �������� ����������� ������
    procedure LoadRecord; //��������� �������� �������

    procedure Button1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fGRAPHf: TfGRAPHf;

implementation
uses fMW;
{$R *.dfm}

//��������� �������� ����������� ������
procedure TfGRAPHf.LoadCombo;
begin
  ComboBox1.Clear;
  ComboBox1.Items.Add('������� [���-�� ������� �� �������������] � ������� ����������� ������');
  ComboBox1.Items.Add('������� [���-�� ������� �� �������������] � ������� ����');
  ComboBox1.Items.Add('������� [���-�� ������� �� �������� ���������] � ������� ����������');
  ComboBox1.Items.Add('������� [���-�� ������� �� �������� �� �������������] � ������� ���');
  ComboBox1.Items.Add('������� [������ ����� ����� �� �������� �� �������������] � ������� ���');
  ComboBox1.Items.Add('������� [�������� ����� ����� �� �������� �� �������������] � ������� ���');
  ComboBox1.Items.Add('������� [���-�� ������� �� �������� �� �������������] � ������� ���������');
  ComboBox1.Items.Add('������� [������ ����� ����� �� �������� �� �������������] � ������� ���������');
  ComboBox1.Items.Add('������� [�������� ����� ����� �� �������� �� �������������] � ������� ���������');
  ComboBox1.Items.Add('������� [���-�� ������� �� �������� �� �������������] � ������� ����������');
  ComboBox1.Items.Add('������� [������ ����� ����� �� �������� �� �������������] � ������� ����������');
  ComboBox1.Items.Add('������� [�������� ����� ����� �� �������� �� �������������] � ������� ����������');
  ComboBox1.Items.Add('������� [���-�� ������� �� �������� �� �������������] � ������� ������');
  ComboBox1.Items.Add('������� [������ ����� ����� �� �������� �� �������������] � ������� ������');
  ComboBox1.Items.Add('������� [�������� ����� ����� �� �������� �� �������������] � ������� ������');
  ComboBox1.Items.Add('������� [���-�� ������� �� ������] � ������� �������������');
  ComboBox1.Items.Add('������� [��� ������ �������� �� ������] � ������� �������������');
  ComboBox1.Items.Add('������� [���������� ��������� �� ������] � ������� �������������');
  ComboBox1.Items.Add('������� [���-�� ������� �� ������] � ������� ���� ��������');
  ComboBox1.Items.Add('������� [��� ������ �������� �� ������] � ������� ���� ��������');
  ComboBox1.Items.Add('������� [���������� ��������� �� ������] � ������� ���� ��������');
  ComboBox1.Items.Add('������� [���-�� ������� �� ����������] � ������� ����');
  ComboBox1.Items.Add('������� [���-�� ������� �� ����������] � ������� ����������');
  ComboBox1.Items.Add('������� [���-�� ������� �� ����������] � ������� �������������');
  ComboBox1.Items.Add('������� [���-�� ������� �� ����������] � ������� �������');
  ComboBox1.Items.Add('������� [���-�� ������� �� ����������] � ������� ��� �������');
  ComboBox1.Items.Add('������� [���-�� ������� �� ��������� � ��������] � ������� ���');
  ComboBox1.Items.Add('������� [���-�� ������� �� ��������� � ��������] � ������� ���������');
  ComboBox1.ItemIndex:=0;
end;

//��������� �������� �������
procedure TfGRAPHf.LoadRecord;
var
  i:integer;
begin

  for i:=0 to ListView1.Columns.Count-1 do begin

    ListView1.Columns.Delete(0); 

  end;


if ComboBox1.ItemIndex=0 then begin
  pS:='Select vozmozhnostmz_raboty.id,vozmozhnostmz_raboty.vozmozhnostmz_raboty as name , count(prepodavatelmz.id) as countz from vozmozhnostmz_raboty,prepodavatelmz where '+
'vozmozhnostmz_raboty.id =prepodavatelmz.vozmozhnostmz_raboty_id and vozmozhnostmz_raboty.deleted=0 and prepodavatelmz.deleted=0 group by vozmozhnostmz_raboty.id, vozmozhnostmz_raboty.vozmozhnostmz_raboty';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='����������� ������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=1 then begin
  pS:='Select para.id,para.para as name , count(prepodavatelmz.id) as countz from para,prepodavatelmz where '+
'para.id =prepodavatelmz.para_id and para.deleted=0 and prepodavatelmz.deleted=0 group by para.id, para.para';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='����';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=2 then begin
  pS:='Select distsiplina.id,distsiplina.distsiplina_na_russkom as name , count(kabinety_distsiplin.id) as countz from distsiplina,kabinety_distsiplin where '+
'distsiplina.id =kabinety_distsiplin.distsiplina_id and distsiplina.deleted=0 and kabinety_distsiplin.deleted=0 group by distsiplina.id, distsiplina.distsiplina_na_russkom';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='����������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=3 then begin
  pS:='Select god.id,god.god as name , count(nagruzka_na_prepodavatelya.id) as countz from god,nagruzka_na_prepodavatelya where '+
'god.id =nagruzka_na_prepodavatelya.god_id and god.deleted=0 and nagruzka_na_prepodavatelya.deleted=0 group by god.id, god.god';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='���';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=4 then begin
  pS:='Select GOD.id,GOD.GOD as name , sum(NAGRUZKA_NA_PREPODAVATELYA.teoriya_chasov_itogo) as countz from GOD,NAGRUZKA_NA_PREPODAVATELYA where '+
'GOD.id =NAGRUZKA_NA_PREPODAVATELYA.god_id and GOD.deleted=0 and NAGRUZKA_NA_PREPODAVATELYA.deleted=0 group by GOD.id, GOD.GOD';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='���';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='������ ����� �����';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=5 then begin
  pS:='Select GOD.id,GOD.GOD as name , sum(NAGRUZKA_NA_PREPODAVATELYA.praktika_chasov_itogo) as countz from GOD,NAGRUZKA_NA_PREPODAVATELYA where '+
'GOD.id =NAGRUZKA_NA_PREPODAVATELYA.god_id and GOD.deleted=0 and NAGRUZKA_NA_PREPODAVATELYA.deleted=0 group by GOD.id, GOD.GOD';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='���';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='�������� ����� �����';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=6 then begin
  pS:='Select polugodie.id,polugodie.polugodie as name , count(nagruzka_na_prepodavatelya.id) as countz from polugodie,nagruzka_na_prepodavatelya where '+
'polugodie.id =nagruzka_na_prepodavatelya.polugodie_id and polugodie.deleted=0 and nagruzka_na_prepodavatelya.deleted=0 group by polugodie.id, polugodie.polugodie';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='���������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=7 then begin
  pS:='Select POLUGODIE.id,POLUGODIE.POLUGODIE as name , sum(NAGRUZKA_NA_PREPODAVATELYA.teoriya_chasov_itogo) as countz from POLUGODIE,NAGRUZKA_NA_PREPODAVATELYA where '+
'POLUGODIE.id =NAGRUZKA_NA_PREPODAVATELYA.polugodie_id and POLUGODIE.deleted=0 and NAGRUZKA_NA_PREPODAVATELYA.deleted=0 group by POLUGODIE.id, POLUGODIE.POLUGODIE';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='���������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='������ ����� �����';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=8 then begin
  pS:='Select POLUGODIE.id,POLUGODIE.POLUGODIE as name , sum(NAGRUZKA_NA_PREPODAVATELYA.praktika_chasov_itogo) as countz from POLUGODIE,NAGRUZKA_NA_PREPODAVATELYA where '+
'POLUGODIE.id =NAGRUZKA_NA_PREPODAVATELYA.polugodie_id and POLUGODIE.deleted=0 and NAGRUZKA_NA_PREPODAVATELYA.deleted=0 group by POLUGODIE.id, POLUGODIE.POLUGODIE';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='���������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='�������� ����� �����';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=9 then begin
  pS:='Select distsiplina.id,distsiplina.distsiplina as name , count(nagruzka_na_prepodavatelya.id) as countz from distsiplina,nagruzka_na_prepodavatelya where '+
'distsiplina.id =nagruzka_na_prepodavatelya.distsiplina and distsiplina.deleted=0 and nagruzka_na_prepodavatelya.deleted=0 group by distsiplina.id, distsiplina.distsiplina';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='����������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=10 then begin
  pS:='Select DISTSIPLINA.id,DISTSIPLINA.DISTSIPLINA as name , sum(NAGRUZKA_NA_PREPODAVATELYA.teoriya_chasov_itogo) as countz from DISTSIPLINA,NAGRUZKA_NA_PREPODAVATELYA where '+
'DISTSIPLINA.id =NAGRUZKA_NA_PREPODAVATELYA.DISTSIPLINA and DISTSIPLINA.deleted=0 and NAGRUZKA_NA_PREPODAVATELYA.deleted=0 group by DISTSIPLINA.id, DISTSIPLINA.DISTSIPLINA';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='����������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='������ ����� �����';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=11 then begin
  pS:='Select DISTSIPLINA.id,DISTSIPLINA.DISTSIPLINA as name , sum(NAGRUZKA_NA_PREPODAVATELYA.praktika_chasov_itogo) as countz from DISTSIPLINA,NAGRUZKA_NA_PREPODAVATELYA where '+
'DISTSIPLINA.id =NAGRUZKA_NA_PREPODAVATELYA.DISTSIPLINA and DISTSIPLINA.deleted=0 and NAGRUZKA_NA_PREPODAVATELYA.deleted=0 group by DISTSIPLINA.id, DISTSIPLINA.DISTSIPLINA';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='����������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='�������� ����� �����';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=12 then begin
  pS:='Select gruppa.id,gruppa.nazvanie_gruppy as name , count(nagruzka_na_prepodavatelya.id) as countz from gruppa,nagruzka_na_prepodavatelya where '+
'gruppa.id =nagruzka_na_prepodavatelya.gruppa_id and gruppa.deleted=0 and nagruzka_na_prepodavatelya.deleted=0 group by gruppa.id, gruppa.nazvanie_gruppy';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=13 then begin
  pS:='Select GRUPPA.id,GRUPPA.NAZVANIE_GRUPPY as name , sum(NAGRUZKA_NA_PREPODAVATELYA.teoriya_chasov_itogo) as countz from GRUPPA,NAGRUZKA_NA_PREPODAVATELYA where '+
'GRUPPA.id =NAGRUZKA_NA_PREPODAVATELYA.gruppa_id and GRUPPA.deleted=0 and NAGRUZKA_NA_PREPODAVATELYA.deleted=0 group by GRUPPA.id, GRUPPA.NAZVANIE_GRUPPY';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='������ ����� �����';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=14 then begin
  pS:='Select GRUPPA.id,GRUPPA.NAZVANIE_GRUPPY as name , sum(NAGRUZKA_NA_PREPODAVATELYA.praktika_chasov_itogo) as countz from GRUPPA,NAGRUZKA_NA_PREPODAVATELYA where '+
'GRUPPA.id =NAGRUZKA_NA_PREPODAVATELYA.gruppa_id and GRUPPA.deleted=0 and NAGRUZKA_NA_PREPODAVATELYA.deleted=0 group by GRUPPA.id, GRUPPA.NAZVANIE_GRUPPY';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='�������� ����� �����';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=15 then begin
  pS:='Select spetsialmznostmz.id,spetsialmznostmz.spetsialmznostmz_na_russkom as name , count(gruppa.id) as countz from spetsialmznostmz,gruppa where '+
'spetsialmznostmz.id =gruppa.spetsialmznostmz_id and spetsialmznostmz.deleted=0 and gruppa.deleted=0 group by spetsialmznostmz.id, spetsialmznostmz.spetsialmznostmz_na_russkom';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='�������������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=16 then begin
  pS:='Select SPETSIALMZNOSTMZ.id,SPETSIALMZNOSTMZ.SPETSIALMZNOSTMZ_NA_RUSSKOM as name , sum(GRUPPA.god_nachala_obucheniya) as countz from SPETSIALMZNOSTMZ,GRUPPA where '+
'SPETSIALMZNOSTMZ.id =GRUPPA.spetsialmznostmz_id and SPETSIALMZNOSTMZ.deleted=0 and GRUPPA.deleted=0 group by SPETSIALMZNOSTMZ.id, SPETSIALMZNOSTMZ.SPETSIALMZNOSTMZ_NA_RUSSKOM';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='�������������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='��� ������ ��������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=17 then begin
  pS:='Select SPETSIALMZNOSTMZ.id,SPETSIALMZNOSTMZ.SPETSIALMZNOSTMZ_NA_RUSSKOM as name , sum(GRUPPA.kolichestvo_studentov) as countz from SPETSIALMZNOSTMZ,GRUPPA where '+
'SPETSIALMZNOSTMZ.id =GRUPPA.spetsialmznostmz_id and SPETSIALMZNOSTMZ.deleted=0 and GRUPPA.deleted=0 group by SPETSIALMZNOSTMZ.id, SPETSIALMZNOSTMZ.SPETSIALMZNOSTMZ_NA_RUSSKOM';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='�������������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���������� ���������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=18 then begin
  pS:='Select yazyk_obucheniya.id,yazyk_obucheniya.yazyk_obucheniya as name , count(gruppa.id) as countz from yazyk_obucheniya,gruppa where '+
'yazyk_obucheniya.id =gruppa.yazyk_obucheniya_id and yazyk_obucheniya.deleted=0 and gruppa.deleted=0 group by yazyk_obucheniya.id, yazyk_obucheniya.yazyk_obucheniya';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='���� ��������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=19 then begin
  pS:='Select YAZYK_OBUCHENIYA.id,YAZYK_OBUCHENIYA.YAZYK_OBUCHENIYA as name , sum(GRUPPA.god_nachala_obucheniya) as countz from YAZYK_OBUCHENIYA,GRUPPA where '+
'YAZYK_OBUCHENIYA.id =GRUPPA.yazyk_obucheniya_id and YAZYK_OBUCHENIYA.deleted=0 and GRUPPA.deleted=0 group by YAZYK_OBUCHENIYA.id, YAZYK_OBUCHENIYA.YAZYK_OBUCHENIYA';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='���� ��������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='��� ������ ��������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=20 then begin
  pS:='Select YAZYK_OBUCHENIYA.id,YAZYK_OBUCHENIYA.YAZYK_OBUCHENIYA as name , sum(GRUPPA.kolichestvo_studentov) as countz from YAZYK_OBUCHENIYA,GRUPPA where '+
'YAZYK_OBUCHENIYA.id =GRUPPA.yazyk_obucheniya_id and YAZYK_OBUCHENIYA.deleted=0 and GRUPPA.deleted=0 group by YAZYK_OBUCHENIYA.id, YAZYK_OBUCHENIYA.YAZYK_OBUCHENIYA';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='���� ��������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���������� ���������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=21 then begin
  pS:='Select para.id,para.para as name , count(raspisanie.id) as countz from para,raspisanie where '+
'para.id =raspisanie.para_id and para.deleted=0 and raspisanie.deleted=0 group by para.id, para.para';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='����';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=22 then begin
  pS:='Select distsiplina.id,distsiplina.distsiplina as name , count(raspisanie.id) as countz from distsiplina,raspisanie where '+
'distsiplina.id =raspisanie.distsiplina and distsiplina.deleted=0 and raspisanie.deleted=0 group by distsiplina.id, distsiplina.distsiplina';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='����������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=23 then begin
  pS:='Select prepodavatelmz.id,prepodavatelmz.fio as name , count(raspisanie.id) as countz from prepodavatelmz,raspisanie where '+
'prepodavatelmz.id =raspisanie.prepodavatelmz_id and prepodavatelmz.deleted=0 and raspisanie.deleted=0 group by prepodavatelmz.id, prepodavatelmz.fio';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='�������������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=24 then begin
  pS:='Select kabinet.id,kabinet.nomer_kabineta as name , count(raspisanie.id) as countz from kabinet,raspisanie where '+
'kabinet.id =raspisanie.kabinet_id and kabinet.deleted=0 and raspisanie.deleted=0 group by kabinet.id, kabinet.nomer_kabineta';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='�������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=25 then begin
  pS:='Select tip_zanyatiya.id,tip_zanyatiya.tip_zanyatiya as name , count(raspisanie.id) as countz from tip_zanyatiya,raspisanie where '+
'tip_zanyatiya.id =raspisanie.tip_zanyatiya_id and tip_zanyatiya.deleted=0 and raspisanie.deleted=0 group by tip_zanyatiya.id, tip_zanyatiya.tip_zanyatiya';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='��� �������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=26 then begin
  pS:='Select god.id,god.god as name , count(prazdniki_i_vykhodnye.id) as countz from god,prazdniki_i_vykhodnye where '+
'god.id =prazdniki_i_vykhodnye.god_id and god.deleted=0 and prazdniki_i_vykhodnye.deleted=0 group by god.id, god.god';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='���';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 


if ComboBox1.ItemIndex=27 then begin
  pS:='Select polugodie.id,polugodie.polugodie as name , count(prazdniki_i_vykhodnye.id) as countz from polugodie,prazdniki_i_vykhodnye where '+
'polugodie.id =prazdniki_i_vykhodnye.polugodie_id and polugodie.deleted=0 and prazdniki_i_vykhodnye.deleted=0 group by polugodie.id, polugodie.polugodie';
  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='���������';
  ListView1.Column[1].Width:=200;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���-�� �������';
  ListView1.Column[2].Width:=150;
end; 

  Series1.Clear;

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.Open;

  if ADQ.RecordCount = 0 then
  begin
    ListView1.Clear; 
    StatusBar1.panels[0].text:='����� ������� : 0';
    Screen.Cursor := crDefault;
  end;

  ListView1.Visible := false;
  ListView1.Items.Clear;

  ADQ.First;

  while not ADQ.Eof do
  begin
    L := ListView1.Items.Add;
    L.Caption := trim(ADQ.fieldbyname('id').AsString);
    L.SubItems.Append(ADQ.fieldbyname('name').AsString);
    L.SubItems.Append(ADQ.fieldbyname('countz').AsString);
    Series1.Add(ADQ.fieldbyname('countz').AsInteger,ADQ.fieldbyname('name').AsString);
    ADQ.Next;
  end;

  ListView1.Visible := true;

  if ListView1.Items.Count>0 then begin
    ListView1.Items[ListView1.Items.Count-1].Selected:=true;
  end;

  StatusBar1.panels[0].text:='����� ������� : '+inttostr(ADQ.recordcount);

  Screen.Cursor := crDefault;

end;

procedure TfGRAPHf.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TfGRAPHf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfGRAPHf.FormActivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex:=0;
  LoadCombo;
  LoadRecord;
end;

procedure TfGRAPHf.Button2Click(Sender: TObject);
begin
  fMWf.LVToExcel(ListView1);
end;

procedure TfGRAPHf.ComboBox1Change(Sender: TObject);
begin
  LoadRecord;
end;

end.