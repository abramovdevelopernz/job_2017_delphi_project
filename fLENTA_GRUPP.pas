unit fLENTA_GRUPP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls;

type
  TfLENTA_GRUPPf = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Combo_gruppa: TComboBox;
    Label1: TLabel;
    ListView1: TListView;
    Button3: TButton;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DateTimePicker1: TDateTimePicker;
    Combo_distsiplina: TComboBox;
    Combo_prepodavatelmz: TComboBox;
    Combo_podgruppa: TComboBox;
    Combo_tip_zanyatiya: TComboBox;
    Combo_kabinet: TComboBox;
    Combo_para: TComboBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    Button2: TButton;

    procedure LoadGRUPPA;
    procedure LoadPARA;
    procedure LoadDISTSIPLINA;
    procedure LoadPREPODAVATELMZ;
    procedure LoadPODGRUPPA;
    procedure LoadTIP;
    procedure LoadKABINET;


    procedure FormActivate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fLENTA_GRUPPf: TfLENTA_GRUPPf;

implementation
uses fMW;
{$R *.dfm}

procedure TfLENTA_GRUPPf.LoadKABINET;
begin

Combo_kabinet.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from kabinet where deleted=0 order by nomer_kabineta');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_kabinet.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then begin
  Combo_kabinet.ItemIndex:=0;
  Combo_kabinet.DropDownCount:=20;
end;

end;

procedure TfLENTA_GRUPPf.LoadTIP;
begin

Combo_tip_zanyatiya.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from tip_zanyatiya where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_tip_zanyatiya.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then begin
  Combo_tip_zanyatiya.ItemIndex:=0;
  Combo_tip_zanyatiya.DropDownCount:=20;
end;

end;

procedure TfLENTA_GRUPPf.LoadPODGRUPPA;
begin

Combo_podgruppa.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from podgruppa where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_podgruppa.AddItem(ADQ.FieldByName('podgruppa').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then begin
  Combo_podgruppa.ItemIndex:=0;
  Combo_podgruppa.DropDownCount:=20;
end;

end;

procedure TfLENTA_GRUPPf.LoadPREPODAVATELMZ;
begin

Combo_prepodavatelmz.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from prepodavatelmz where deleted=0 order by fio');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_prepodavatelmz.AddItem(ADQ.FieldByName('fio').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then begin
  Combo_prepodavatelmz.ItemIndex:=0;
  Combo_prepodavatelmz.DropDownCount:=20;
end;

end;

procedure TfLENTA_GRUPPf.LoadDISTSIPLINA;
begin

Combo_distsiplina.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from distsiplina where deleted=0 order by distsiplina_na_russkom');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_distsiplina.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then begin
  Combo_distsiplina.ItemIndex:=0;
  Combo_distsiplina.DropDownCount:=20;
end;

end;

procedure TfLENTA_GRUPPf.LoadPARA;
begin

Combo_para.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from para where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_para.AddItem(ADQ.FieldByName('para').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then begin
  Combo_para.ItemIndex:=0;
  Combo_para.DropDownCount:=20;
end;

end;

procedure TfLENTA_GRUPPf.LoadGRUPPA;
begin

Combo_gruppa.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from gruppa where deleted=0 order by nazvanie_gruppy');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  if ADQ.FieldByName('yazyk_obucheniya_id').AsString='1' then Combo_gruppa.AddItem(ADQ.FieldByName('nazvanie_gruppy').AsString+'-���', Pointer(ADQ.FieldByName('id').AsInteger));
  if ADQ.FieldByName('yazyk_obucheniya_id').AsString='2' then Combo_gruppa.AddItem(ADQ.FieldByName('nazvanie_gruppy').AsString+'-���', Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then begin
  Combo_gruppa.ItemIndex:=0;
  pGRUPPA_ID:=Inttostr(Integer(Combo_gruppa.Items.Objects[Combo_gruppa.ItemIndex]));
  Combo_gruppa.DropDownCount:=20;
end;

end;

procedure TfLENTA_GRUPPf.FormActivate(Sender: TObject);
begin

CheckBox1.Checked:=false;
CheckBox2.Checked:=false;
CheckBox3.Checked:=false;
CheckBox4.Checked:=false;
CheckBox5.Checked:=false;
CheckBox6.Checked:=false;
CheckBox7.Checked:=false;
CheckBox8.Checked:=false;

ListView1.Clear;
fMWf.SetLCW(Listview1);
StatusBar1.panels[0].text:='����� ������� : 0';
LoadGRUPPA;
LoadPARA;
LoadDISTSIPLINA;
LoadPREPODAVATELMZ;
LoadPODGRUPPA;
LoadTIP;
LoadKABINET;

end;

procedure TfLENTA_GRUPPf.Button1Click(Sender: TObject);
begin
Close
end;

procedure TfLENTA_GRUPPf.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=vk_escape then close;
end;

procedure TfLENTA_GRUPPf.Button3Click(Sender: TObject);
begin
  fMWf.LVToExcel(ListView1);
end;

procedure TfLENTA_GRUPPf.Button2Click(Sender: TObject);
var
  pName:string;
  pData:string;
begin

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from gruppa where deleted=0 and id='+Inttostr(Integer(Combo_gruppa.Items.Objects[Combo_gruppa.ItemIndex])));
ADQ.Open;

ADQ.First;


if ADQ.FieldByName('yazyk_obucheniya_id').AsString='1' then pName:='rus';
if ADQ.FieldByName('yazyk_obucheniya_id').AsString='2' then pName:='kaz';


  ListView1.Clear;
  Screen.Cursor := crHourglass;
  Application.ProcessMessages;

  pS:='Select '+
  'gruppa.nazvanie_gruppy as gruppa'+
  ',raspisanie.data'+
  ',raspisanie.id as raspisanie_id'+
  ',raspisanie.para_id as para_id'+
  ',distsiplina.distsiplina_na_russkom as distsiplina_rus'+
  ',distsiplina.distsiplina_na_kazakhskom as distsiplina_kaz'+
  ',prepodavatelmz.fio as prepodavatelmz'+
  ',podgruppa.podgruppa as podgruppa'+
  ',tip_zanyatiya.tip_zanyatiya as tip_zanyatiya'+
  ',kabinet.nomer_kabineta as kabinet '+

  'from '+

  'raspisanie'+
  ',gruppa'+
  ',podgruppa'+
  ',kabinet'+
  ',distsiplina'+
  ',prepodavatelmz'+
  ',tip_zanyatiya'+

  ' where ' +

  '     raspisanie.gruppa_id=gruppa.id'+
  ' and raspisanie.podgruppa_id=podgruppa.id'+
  ' and raspisanie.kabinet_id=kabinet.id'+
  ' and raspisanie.distsiplina_id=distsiplina.id'+
  ' and raspisanie.prepodavatelmz_id=prepodavatelmz.id'+
  ' and raspisanie.tip_zanyatiya_id=tip_zanyatiya.id'+
  ' and raspisanie.deleted=0'+
  ' and raspisanie.distsiplina_id<>1'+
  ' and god_id='+pCurrent_God_id+
  ' and polugodie_id='+pCurrent_Polugodie_id;


  pData:='data=#'+FormatDateTime('MM/DD/YYYY',DateTimePicker1.Date)+'#';
  pData:= StringReplace(pData, '.', '/',[rfReplaceAll, rfIgnoreCase]);

  if CheckBox1.Checked=true then
  pS:=pS+' and '+pData;

  if CheckBox2.Checked=true then
  pS:=pS+' and raspisanie.para_id='+Inttostr(Integer(Combo_para.Items.Objects[Combo_para.ItemIndex]));

  if CheckBox3.Checked=true then
  pS:=pS+' and raspisanie.distsiplina_id='+Inttostr(Integer(Combo_distsiplina.Items.Objects[Combo_distsiplina.ItemIndex]));

  if CheckBox4.Checked=true then
  pS:=pS+' and raspisanie.prepodavatelmz_id='+Inttostr(Integer(Combo_prepodavatelmz.Items.Objects[Combo_prepodavatelmz.ItemIndex]));

  if CheckBox5.Checked=true then
  pS:=pS+' and raspisanie.gruppa_id='+Inttostr(Integer(Combo_gruppa.Items.Objects[Combo_gruppa.ItemIndex]));

  if CheckBox6.Checked=true then
  pS:=pS+' and raspisanie.podgruppa_id='+Inttostr(Integer(Combo_podgruppa.Items.Objects[Combo_podgruppa.ItemIndex]));

  if CheckBox7.Checked=true then
  pS:=pS+' and raspisanie.tip_zanyatiya_id='+Inttostr(Integer(Combo_tip_zanyatiya.Items.Objects[Combo_tip_zanyatiya.ItemIndex]));

  if CheckBox8.Checked=true then
  pS:=pS+' and kabinet_id='+Inttostr(Integer(Combo_kabinet.Items.Objects[Combo_kabinet.ItemIndex]));

//  ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS+' order by raspisanie.data desc');
  ADQ.Open;

  if ADQ.RecordCount = 0 then
  begin
    ListView1.Clear;
    StatusBar1.panels[0].text:='����� ������� : 0';
    Screen.Cursor := crDefault;
    exit;
  end;

  ListView1.Visible := false;
  ListView1.Items.Clear;

  ADQ.First;
  while not ADQ.Eof do
  begin
    L := ListView1.Items.Add;
    L.Caption := trim(ADQ.fieldbyname('raspisanie_id').AsString);
    L.SubItems.Append(ADQ.fieldbyname('data').AsString);
    L.SubItems.Append(ADQ.fieldbyname('para_id').AsString);

    if pName='rus' then L.SubItems.Append(ADQ.fieldbyname('distsiplina_rus').AsString);
    if pName='kaz' then L.SubItems.Append(ADQ.fieldbyname('distsiplina_kaz').AsString);

    L.SubItems.Append(ADQ.fieldbyname('prepodavatelmz').AsString);
    L.SubItems.Append(ADQ.fieldbyname('gruppa').AsString);    
    L.SubItems.Append(ADQ.fieldbyname('podgruppa').AsString);
    L.SubItems.Append(ADQ.fieldbyname('tip_zanyatiya').AsString);
    L.SubItems.Append(ADQ.fieldbyname('kabinet').AsString);

    ADQ.Next;
  end;

  ListView1.Visible := true;
  StatusBar1.panels[0].text:='����� ������� : '+inttostr(ADQ.RecordCount);
  if ListView1.Items.Count>0 then ListView1.Items[0].Selected:=true;

  Screen.Cursor := crDefault;


end;

procedure TfLENTA_GRUPPf.FormResize(Sender: TObject);
begin
fMWf.SetLCW(Listview1);
end;

end.
