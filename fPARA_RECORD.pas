unit fPARA_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfPARA_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_para: TLabel;
    Label_budnie_chasy: TLabel;
    Label_vykhodnye_chasy: TLabel;

    Edit_para: TEdit;
    Edit_budnie_chasy: TEdit;
    Edit_vykhodnye_chasy: TEdit;

    Check_para: TCheckBox;
    Check_budnie_chasy: TCheckBox;
    Check_vykhodnye_chasy: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fPARA_RECORDf: TfPARA_RECORDf;

implementation

uses  
    fPARA,
    fMW;

{$R *.dfm}

procedure TfPARA_RECORDf.CheckInput;
begin

if trim(Edit_para.Text)='' then begin
  Edit_para.SetFocus;
  abort;
end;

if trim(Edit_budnie_chasy.Text)='' then begin
  Edit_budnie_chasy.SetFocus;
  abort;
end;

if trim(Edit_vykhodnye_chasy.Text)='' then begin
  Edit_vykhodnye_chasy.SetFocus;
  abort;
end;

end;
procedure TfPARA_RECORDf.CLALL;
begin


  Check_para.Checked:=false;
  Check_para.Visible:=false;
  Check_budnie_chasy.Checked:=false;
  Check_budnie_chasy.Visible:=false;
  Check_vykhodnye_chasy.Checked:=false;
  Check_vykhodnye_chasy.Visible:=false;

  Edit_para.Clear;
  Edit_budnie_chasy.Clear;
  Edit_vykhodnye_chasy.Clear;

  Edit_para.Setfocus;

end;

procedure TfPARA_RECORDf.FormActivated;
begin

  CLALL;

if pActionPARA='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionPARA='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from para where id='+pID);
  ADQ.Open;
  ADQ.First;

  Edit_para.Text:=ADQ.fieldbyname('para').AsString;
  Edit_budnie_chasy.Text:=ADQ.fieldbyname('budnie_chasy').AsString;
  Edit_vykhodnye_chasy.Text:=ADQ.fieldbyname('vykhodnye_chasy').AsString;

end;

if pActionPARA='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_para.Visible:=true;
  Check_budnie_chasy.Visible:=true;
  Check_vykhodnye_chasy.Visible:=true;

end;

end;

procedure TfPARA_RECORDf.BtnOK;
begin

if pActionPARA='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'para '+
  '('+
  'para,'+
  'budnie_chasy,'+
  'vykhodnye_chasy,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+trim(Edit_para.Text)+''', '+
  ' '''+trim(Edit_budnie_chasy.Text)+''', '+
  ' '''+trim(Edit_vykhodnye_chasy.Text)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionPARA='UPDATE' then begin

 CheckInput;

 pS:='UPDATE '+
  'para set ' +
  'para='''+trim(Edit_para.Text)+''','+
  'budnie_chasy='''+trim(Edit_budnie_chasy.Text)+''','+
  'vykhodnye_chasy='''+trim(Edit_vykhodnye_chasy.Text)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fPARAf.ListView1.Selected.SubItems[0]:=trim(Edit_para.Text);
   fPARAf.ListView1.Selected.SubItems[1]:=trim(Edit_budnie_chasy.Text);
   fPARAf.ListView1.Selected.SubItems[2]:=trim(Edit_vykhodnye_chasy.Text);

end;

if pActionPARA='SEARCH' then begin

if (Check_para.Checked=false) and (Check_budnie_chasy.Checked=false) and (Check_vykhodnye_chasy.Checked=false) then abort;

   pSearch:='';

   if Check_para.Checked=true then pSearch:=pSearch+' and para.para like '''+trim(Edit_para.Text)+''' ';
   if Check_budnie_chasy.Checked=true then pSearch:=pSearch+' and para.budnie_chasy like '''+trim(Edit_budnie_chasy.Text)+''' ';
   if Check_vykhodnye_chasy.Checked=true then pSearch:=pSearch+' and para.vykhodnye_chasy like '''+trim(Edit_vykhodnye_chasy.Text)+''' ';

end;

Close;

end;

procedure TfPARA_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfPARA_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfPARA_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfPARA_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.