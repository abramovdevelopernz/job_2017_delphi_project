unit fNAGRUZKA_NA_PREPODAVATELYA;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, ComCtrls, StdCtrls, Menus, Clipbrd, Idglobal, JPEG;

type
  TfNAGRUZKA_NA_PREPODAVATELYAf = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    ListView1: TListView;
    PopupMenu1: TPopupMenu;
    Button8: TButton;
    Panel3: TPanel;
    Image1: TImage;
    Splitter1: TSplitter;
    N_PHOTO: TMenuItem;
    OpenDialog1: TOpenDialog;
    PopupMenu2: TPopupMenu;
    N_LOAD_IMAGE: TMenuItem;
    N_DELETE_IMAGE: TMenuItem;
    N1: TMenuItem;

    procedure CreateCol;                 //�������� ������� ��� ������
    procedure LV1(pSearchString:string); //�������� ������ ��������
    procedure SearchRecord;              //����� �������
    procedure UpdateRecord;              //���������� �������
    procedure InsertRecord;              //���������� �������
    procedure DeleteRecord;              //�������� �������

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure ListView1Click(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure ListView1KeyUp(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure Image1Click(Sender: TObject);
    procedure N_LOAD_IMAGEClick(Sender: TObject);
    procedure N_PHOTOClick(Sender: TObject);
    procedure N_DELETE_IMAGEClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure N1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fNAGRUZKA_NA_PREPODAVATELYAf: TfNAGRUZKA_NA_PREPODAVATELYAf;

implementation

uses
fMW,
fSHOWRECORD,fNAGRUZKA_NA_PREPODAVATELYA_RECORD, fPERENOS_CHASOV;
{$R *.dfm}

//���������� �������
procedure TfNAGRUZKA_NA_PREPODAVATELYAf.InsertRecord;
begin
  pNoAction:=false;
  pActionNAGRUZKA_NA_PREPODAVATELYA:='INSERT';
  fNAGRUZKA_NA_PREPODAVATELYA_RECORDf.showmodal;
  if pNoAction=false then LV1('');
end;

//���������� �������
procedure TfNAGRUZKA_NA_PREPODAVATELYAf.UpdateRecord;
begin
  if ListView1.ItemIndex=-1 then abort;
  pNoAction:=false;
  pID:=ListView1.Selected.Caption;
  pActionNAGRUZKA_NA_PREPODAVATELYA:='UPDATE';
  fNAGRUZKA_NA_PREPODAVATELYA_RECORDf.showmodal;
end;

//����� �������
procedure TfNAGRUZKA_NA_PREPODAVATELYAf.SearchRecord;
begin
  pNoAction:=false;
  pActionNAGRUZKA_NA_PREPODAVATELYA:='SEARCH';
  fNAGRUZKA_NA_PREPODAVATELYA_RECORDf.showmodal;

  if pNoAction=false then begin
    LV1(pSearch);
    Button7.Visible:=true;
  end;

end;

//�������� �������
procedure TfNAGRUZKA_NA_PREPODAVATELYAf.DeleteRecord;
begin

if ListView1.ItemIndex=-1 then abort;

if MessageBox(Self.Handle, '������������� �������� ������?', '�������� ������', MB_OKCANCEL) =  IDOK then begin

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Update nagruzka_na_prepodavatelya set deleted=1,deleteuser='''+pMyUser+''',deletedate='''+datetostr(now)+' '+timetostr(now)+''' where id='+ListView1.Selected.Caption);
  ADQ.ExecSQL;

LV1('');

end;

end;

//�������� ������ ��������
procedure TfNAGRUZKA_NA_PREPODAVATELYAf.LV1(pSearchString:string);
var
pCount_par_praktikaX,pCount_par_teoriyaX:integer;
begin

  ListView1.Clear;
  Screen.Cursor := crHourglass;
  Application.ProcessMessages;

pS:='Select '+
' god.god as god_name,'+ 
' polugodie.polugodie as polugodie_name,'+
' distsiplina.id as distsiplina_id,'+
' distsiplina.distsiplina_na_russkom as distsiplina_name,'+
' gruppa.nazvanie_gruppy as gruppa_name,'+
' podgruppa.podgruppa as podgruppa_name,'+
' gruppa.id as gruppa_id,'+
' podgruppa.id as podgruppa_id,'+
' vid_nagruzki.id as vid_nagruzki_id,'+

' vid_nagruzki.vid_nagruzki as vid_nagruzki_name,'+
' nagruzka_na_prepodavatelya.teoriya_chasov_itogo,'+ 
' nagruzka_na_prepodavatelya.praktika_chasov_itogo,'+
' nagruzka_na_prepodavatelya.id '+
' from '+
' god,'+ 
' polugodie,'+ 
' distsiplina,'+ 
' gruppa,'+ 
' podgruppa,'+ 
' vid_nagruzki,'+ 
' nagruzka_na_prepodavatelya '+
' where '+
' god.id=nagruzka_na_prepodavatelya.god_id and ' +
' polugodie.id=nagruzka_na_prepodavatelya.polugodie_id and ' +
' distsiplina.id=nagruzka_na_prepodavatelya.distsiplina_id and ' +
' gruppa.id=nagruzka_na_prepodavatelya.gruppa_id and ' +
' podgruppa.id=nagruzka_na_prepodavatelya.podgruppa_id and ' +
' vid_nagruzki.id=nagruzka_na_prepodavatelya.vid_nagruzki_id and ' +

' nagruzka_na_prepodavatelya.god_id='+pCurrent_God_id+' and ' +
' nagruzka_na_prepodavatelya.polugodie_id='+pCurrent_Polugodie_id+' and ' +
' vid_nagruzki.id=nagruzka_na_prepodavatelya.vid_nagruzki_id and ' +

' nagruzka_na_prepodavatelya.deleted=0 ';

  if trim(pSearchString)<>'' then pS:=pS+pSearchString;

  if trim(pLinkNAGRUZKA_NA_PREPODAVATELYA)<>'' then begin 
   pS:=pS+pLinkNAGRUZKA_NA_PREPODAVATELYA;
   Button5.Visible:=false;
  end;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS+' order by nagruzka_na_prepodavatelya.id');
  ADQ.Open;

  if ADQ.RecordCount = 0 then
  begin
    ListView1.Clear;
    StatusBar1.panels[0].text:='����� ������� : 0';
    Screen.Cursor := crDefault;
    exit;
  end;

  ListView1.Visible := false;
  ListView1.Items.Clear;

  ADQ.First;
  while not ADQ.Eof do
  begin
    L := ListView1.Items.Add;
    L.Caption := trim(ADQ.fieldbyname('id').AsString);
    L.SubItems.Append(ADQ.fieldbyname('god_name').AsString); 
    L.SubItems.Append(ADQ.fieldbyname('polugodie_name').AsString); 
    L.SubItems.Append(ADQ.fieldbyname('distsiplina_name').AsString); 
    L.SubItems.Append(ADQ.fieldbyname('gruppa_name').AsString); 
    L.SubItems.Append(ADQ.fieldbyname('podgruppa_name').AsString); 
    L.SubItems.Append(ADQ.fieldbyname('vid_nagruzki_name').AsString); 
    L.SubItems.Append(ADQ.fieldbyname('teoriya_chasov_itogo').AsString); 
    L.SubItems.Append(ADQ.fieldbyname('praktika_chasov_itogo').AsString);

   //������
   pS:=
   'Select count(id) as count_par_teoriya '+
   'from raspisanie where '+
   'tip_zanyatiya_id=2 and '+
   'gruppa_id='+ADQ.fieldbyname('gruppa_id').AsString+' and '+
   'podgruppa_id='+ADQ.fieldbyname('podgruppa_id').AsString+' and '+
   'distsiplina_id='+ADQ.fieldbyname('distsiplina_id').AsString+' and '+
   'prepodavatelmz_id='+pPREPODAVATELMZ_ID+' and '+
   'vid_nagruzki_id='+ADQ.fieldbyname('vid_nagruzki_id').AsString+' and '+
   'god_id='+pCurrent_God_id+' and '+
   'polugodie_id='+pCurrent_Polugodie_id;

   ADQ1.Close;
   ADQ1.SQL.Clear;
   ADQ1.SQL.Add(pS);
   ADQ1.Open;

   pCount_par_teoriyaX:=ADQ1.fieldbyname('count_par_teoriya').asinteger;
   L.SubItems.Append(inttostr(pCount_par_teoriyaX*2));

   //��������
   pS:=
   'Select count(id) as count_par_praktika '+
   'from raspisanie where '+
   'tip_zanyatiya_id=3 and '+
   'gruppa_id='+ADQ.fieldbyname('gruppa_id').AsString+' and '+
   'podgruppa_id='+ADQ.fieldbyname('podgruppa_id').AsString+' and '+
   'distsiplina_id='+ADQ.fieldbyname('distsiplina_id').AsString+' and '+
   'prepodavatelmz_id='+pPREPODAVATELMZ_ID+' and '+
   'vid_nagruzki_id='+ADQ.fieldbyname('vid_nagruzki_id').AsString+' and '+
   'god_id='+pCurrent_God_id+' and '+
   'polugodie_id='+pCurrent_Polugodie_id;

   ADQ1.Close;
   ADQ1.SQL.Clear;
   ADQ1.SQL.Add(pS);
   ADQ1.Open;

   pCount_par_praktikaX:=ADQ1.fieldbyname('count_par_praktika').asinteger;
   L.SubItems.Append(inttostr(pCount_par_praktikaX*2));

    ADQ.Next;
  end;

  ListView1.Visible := true;

  if ListView1.Items.Count>0 then begin
    ListView1.Items[ListView1.Items.Count-1].Selected:=true;
    Button3.Enabled:=true;
    Button4.Enabled:=true;
    Button8.Enabled:=true;
  end else begin
    Button3.Enabled:=false;
    Button4.Enabled:=false;
    Button8.Enabled:=false;
  end;

  StatusBar1.panels[0].text:='����� ������� : '+inttostr(ADQ.recordcount);

  if Self.Caption='����� �������� �� �������������' then begin
    L := ListView1.FindCaption(1,pSelectedSID,True,True,False);
      if L <> nil then
        begin
          ListView1.Selected := L;
          L.MakeVisible(True);
          ListView1.SetFocus;
        end;
  end;

  Screen.Cursor := crDefault;

end;

//�������� ������� ��� ������ 
procedure TfNAGRUZKA_NA_PREPODAVATELYAf.CreateCol;
var
  i:integer;
begin

  pTableName:='NAGRUZKA_NA_PREPODAVATELYA';

  for i:=0 to ListView1.Columns.Count-1 do begin
    ListView1.Columns.Delete(0);
  end;

  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='���';
  ListView1.Column[1].Width:=100;
  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='���������';
  ListView1.Column[2].Width:=100;
  ListView1.Columns.Add;
  ListView1.Column[3].Caption:='����������';
  ListView1.Column[3].Width:=100;
  ListView1.Columns.Add;
  ListView1.Column[4].Caption:='������';
  ListView1.Column[4].Width:=100;
  ListView1.Columns.Add;
  ListView1.Column[5].Caption:='���������';
  ListView1.Column[5].Width:=100;
  ListView1.Columns.Add;
  ListView1.Column[6].Caption:='��� ��������';
  ListView1.Column[6].Width:=100;
  ListView1.Columns.Add;
  ListView1.Column[7].Caption:='������ ����� �����';
  ListView1.Column[7].Width:=100;
  ListView1.Columns.Add;
  ListView1.Column[8].Caption:='�������� ����� �����';
  ListView1.Column[8].Width:=100;



  ListView1.Columns.Add;
  ListView1.Column[9].Caption:='�������� ������ ����� �����';
  ListView1.Column[9].Width:=100;
  ListView1.Columns.Add;
  ListView1.Column[10].Caption:='�������� �������� ����� �����';
  ListView1.Column[10].Width:=100;





end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.ListView1DblClick(Sender: TObject);
begin

  if Self.Caption='����� �������� �� �������������' then begin
    pNoAction:=false;
    pSelectedID:=ListView1.Selected.Caption;
    Close;
  end;

  if Self.Caption<>'����� �������� �� �������������' then  Button3Click(Self);

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.FormActivate(Sender: TObject);
begin
  if pLink<>'' then begin
    Button5.Visible:=false;
    Button7.Visible:=false;
  end else begin
    Button5.Visible:=true;
    Button7.Visible:=true;
  end;

  Button7.Visible:=false;

  pPhoto:=false;

  Splitter1.Visible:=false;
  if pPhoto=true then begin
    Panel3.Visible:=false;
    N_PHOTO.Visible:=true;
    N_PHOTO.Checked:=false;

  end else begin
    Panel3.Visible:=false;
    N_PHOTO.Visible:=false;
  end;

  CreateCol;

  LV1('');
  fMWf.SetLCW(ListView1);  
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close;
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_insert then Button2Click(Self);
  if key=vk_f2 then Button3Click(Self);
  if key=vk_delete then Button4Click(Self);
  if key=vk_f3 then Button5Click(Self);
  if key=vk_escape then close;
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.Button2Click(Sender: TObject);
begin
  InsertRecord;
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.Button3Click(Sender: TObject);
begin
  UpdateRecord;
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.Button4Click(Sender: TObject);
begin
  DeleteRecord;
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.Button5Click(Sender: TObject);
begin
  SearchRecord;
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.Button6Click(Sender: TObject);
begin
  fMWf.LVToExcel(ListView1);
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.ListView1Click(Sender: TObject);
begin
if (ListView1.ItemIndex<0) then begin
  Button3.Enabled:=false;
  Button4.Enabled:=false;
  Button8.Enabled:=false;
  Panel3.Visible:=false;
end else begin
  Button3.Enabled:=true;
  Button4.Enabled:=true;
  Button8.Enabled:=true;

  if N_PHOTO.Checked=true then begin
    Panel3.Visible:=true;
    Splitter1.Visible:=true;
    Splitter1.Align:=alNone;
    Panel3.Align:=alNone;
    ListView1.Align:=alNone;
    Panel3.Align:=alRight;
    Splitter1.Align:=alRight;
    ListView1.Align:=alClient;

    if FileExists(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg')= true then begin
      Image1.Picture.LoadFromFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg');
    end else begin
      image1.Picture.Bitmap:=nil;
    end;

  end else begin
    Panel3.Visible:=false;
    Splitter1.Visible:=false;
  end;

end;
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.Button7Click(Sender: TObject);
begin
  Button7.Visible:=false;
  LV1('');
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.Button8Click(Sender: TObject);
begin
if ListView1.ItemIndex>-1 then begin
  fMWf.ShowRecord(ListView1);
  fSHOWRECORDf.ShowModal;
end;
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  pPhoto:=false;
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.N_PHOTOClick(Sender: TObject);
begin
if N_PHOTO.Checked=true then begin
  Splitter1.Visible:=false;
  N_PHOTO.Checked:=false;
  Panel3.Visible:=false;
end else begin
  N_PHOTO.Checked:=true;
  Panel3.Visible:=true;
  Splitter1.Visible:=true;
  Splitter1.Align:=alNone;
  Panel3.Align:=alNone;
  ListView1.Align:=alNone;
  Panel3.Align:=alRight;
  Splitter1.Align:=alRight;
  ListView1.Align:=alClient;
end;
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.N_LOAD_IMAGEClick(Sender: TObject);
begin
  if ListView1.Items.Count=0 then abort;
  if MessageBox(Self.Handle, '������� �����������?', '����� �����������', MB_OKCANCEL) =  IDCANCEL then abort;
  OpenDialog1.Execute;
  if trim(OpenDialog1.FileName)='' then abort;
  Application.ProcessMessages;
  Screen.Cursor := crHourglass;
  DeleteFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg');
  CopyFileTo(trim(OpenDialog1.FileName),ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg');
  Image1.Picture.LoadFromFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg');
  Screen.Cursor := crDefault;
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.N_DELETE_IMAGEClick(Sender: TObject);
begin
 if ListView1.Items.Count=0 then abort;
 if MessageBox(Self.Handle, '������� �����������?', '�������� �����', MB_OKCANCEL) =  IDCANCEL then abort;
   if FileExists(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg')= true then begin
     DeleteFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Selected.Caption+'.jpg');
     Image1.Picture.Bitmap:=nil;
   end;
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.ListView1KeyUp(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  ListView1Click(Self);
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.Image1Click(Sender: TObject);
begin
  Button8Click(Self);
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.FormResize(Sender: TObject);
begin
if ListView1.Columns.Count>0 then fMWf.SetLCW(ListView1);
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYAf.N1Click(Sender: TObject);
begin
fPERENOS_CHASOVf.ShowModal;
end;

end.
