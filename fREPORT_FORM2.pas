unit fREPORT_FORM2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,comobj, ComCtrls,clipbrd;

type
  TfREPORT_FORM2f = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Combo_month: TComboBox;
    Combo_prepodavatelmz_id: TComboBox;
    Combo_za: TComboBox;
    Panel3: TPanel;
    Button1: TButton;
    Button2: TButton;
    ProgressBar1: TProgressBar;
    procedure Button1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fREPORT_FORM2f: TfREPORT_FORM2f;

implementation
uses fMW;
{$R *.dfm}

procedure TfREPORT_FORM2f.Button1Click(Sender: TObject);
begin
Close
end;

procedure TfREPORT_FORM2f.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfREPORT_FORM2f.FormActivate(Sender: TObject);
begin
Combo_month.ItemIndex:=0;
Combo_za.ItemIndex:=0;
ProgressBar1.Visible:=false;
Combo_prepodavatelmz_id.Clear;
Combo_prepodavatelmz_id.Clear;

ADQ1.Close;
ADQ1.SQL.Clear;
ADQ1.SQL.Add('Select * from prepodavatelmz where prepodavatelmz.deleted=0 order by prepodavatelmz.fio');
ADQ1.Open;

ADQ1.Last;
ADQ1.First;

while not ADQ1.Eof do begin
  Combo_prepodavatelmz_id.AddItem(ADQ1.FieldByName('fio').AsString, Pointer(ADQ1.FieldByName('id').AsInteger));
  ADQ1.Next;
end;

if ADQ1.RecordCount>0 then Combo_prepodavatelmz_id.ItemIndex:=0;

end;

procedure TfREPORT_FORM2f.Button2Click(Sender: TObject);
var
pCurrentNum:integer;
pSQL:widestring;
pSem : string;
xlapp, xlbook, xlsht: OleVariant;
i,j,z,l: word;
rc, FirstI,pMonthNumber,day: integer;
pName,pGruppaName,pZa:string;
begin

Screen.Cursor := crHourglass;

pMonthNumber:=Combo_month.ItemIndex+1;

if pMonthNumber=1 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.01.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('31.01.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=2 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.02.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('28.02.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=3 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.03.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('31.03.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=4 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.04.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('30.04.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=5 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.05.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('31.05.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=6 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.06.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('30.06.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=7 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.07.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('31.07.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=8 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.08.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('31.08.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=9 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.09.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('30.09.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=10 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.10.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('31.10.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=11 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.11.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('30.11.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=12 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.12.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('31.12.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';

if Combo_za.ItemIndex=0 then pZa:='1';
if Combo_za.ItemIndex=1 then pZa:='2';

pS:='Select distinct '+
' gruppa.id as gruppa_id,'+
' raspisanie.distsiplina_id as distsiplina_id,'+
' distsiplina.distsiplina_na_russkom as distsiplina_na_russkom, '+
' gruppa.nazvanie_gruppy as gruppa_nazvanie_gruppy '+
' from '+
' raspisanie,'+
' distsiplina,'+
' gruppa '+
' where '+
' gruppa.id=raspisanie.gruppa_id and ' +
' distsiplina.id=raspisanie.distsiplina_id and ' +
' gruppa.za_id= ' +pZa+' and ' +
' raspisanie.prepodavatelmz_id= ' +Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+' and ' +
' raspisanie.god_id='+pCurrent_God_id+' and ' +pInterval+
' raspisanie.deleted=0 ';

//ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.Open;

  if ADQ.RecordCount = 0 then
  begin
    ShowMessage('�� ��������� ������ ��� ���������� �� �������� ���������� �������������');
    Screen.Cursor := crDefault;
    exit;
  end;

xlapp:=CreateOleObject('Excel.Application');
xlapp.Visible:=false;
xlbook:=xlapp.WorkBooks.Open(ExtractFilePath(Application.exeName)+'report.xls');
xlsht := xlapp.Workbooks[1].Worksheets['form2'];
xlsht := xlapp.Sheets;
xlsht.Item['form2'].Activate;
xlsht:=xlbook.ActiveSheet;

if fMWf.Combo_polugodie_id.ItemIndex=0 then pSem:='1-� �������';
if fMWf.Combo_polugodie_id.ItemIndex=1 then pSem:='2-� �������';
xlsht.Cells(8,4):='�� '+pSem+'  '+fMWf.Combo_god_id.Text+' - '+inttostr(strtoint(fMWf.Combo_god_id.Text)+1)+'  ����.';

ProgressBar1.Visible:=true;
ProgressBar1.Max:=ADQ.RecordCount;
ProgressBar1.Position:=0;
l:=0;

ADQ.Last;
ADQ.First;
i:=12;

while not ADQ.Eof do begin
  xlsht.Cells(i,2):=inttostr(l+1);
  xlsht.Cells(i,3):=ADQ.FieldByName('distsiplina_na_russkom').AsString;
  xlsht.Cells(i,4):=ADQ.FieldByName('gruppa_nazvanie_gruppy').AsString;

//������� ��� �� ����
if pMonthNumber=1 then pInterval_Till:=31;
if pMonthNumber=2 then pInterval_Till:=28;
if pMonthNumber=3 then pInterval_Till:=31;
if pMonthNumber=4 then pInterval_Till:=30;
if pMonthNumber=5 then pInterval_Till:=31;
if pMonthNumber=6 then pInterval_Till:=30;
if pMonthNumber=7 then pInterval_Till:=31;
if pMonthNumber=8 then pInterval_Till:=31;
if pMonthNumber=9 then pInterval_Till:=30;
if pMonthNumber=10 then pInterval_Till:=31;
if pMonthNumber=11 then pInterval_Till:=30;
if pMonthNumber=12 then pInterval_Till:=31;

for day:=1 to pInterval_Till do begin
if pMonthNumber=1 then pInterval_X:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.01.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.01.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=2 then pInterval_X:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.02.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.02.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=3 then pInterval_X:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.03.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.03.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=4 then pInterval_X:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.04.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.04.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=5 then pInterval_X:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.05.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.05.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=6 then pInterval_X:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.06.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.06.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=7 then pInterval_X:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.07.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.07.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=8 then pInterval_X:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.08.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.08.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=9 then pInterval_X:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.09.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.09.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=10 then pInterval_X:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.10.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.10.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=11 then pInterval_X:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.11.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.11.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=12 then pInterval_X:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.12.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate(inttostr(day)+'.12.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';

pS:='Select count(id) as count_id from raspisanie where '+
' god_id='+pCurrent_God_id+' and ' +
' distsiplina_id= ' +ADQ.FieldByName('distsiplina_id').AsString+' and ' +
' gruppa_id= ' +ADQ.FieldByName('gruppa_id').AsString+' and ' +
' prepodavatelmz_id= ' +Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+' and ' +
  pInterval_X+
' deleted=0 ' ;
ADQ1.Close;
ADQ1.SQL.Clear;
ADQ1.SQL.Add(pS);
ADQ1.Open;
xlsht.Cells(i,day+4):=inttostr(ADQ1.fieldbyname('count_id').AsInteger*2);
end;

  ProgressBar1.Position:=l;
  inc(l);
  inc(i);
  ADQ.Next;
end;



xlapp.Visible:=true;
//xlsht.PageSetup.Orientation := 1;
//xlsht.PageSetup.Zoom := false;
//xlsht.PageSetup.Order := 1;
//xlsht.PrintPreview;
xlapp.ActiveWindow.Zoom := 100;
ProgressBar1.Visible:=false;
Screen.Cursor := crDefault;

end;

end.
