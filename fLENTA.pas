unit fLENTA;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, StdCtrls;

type
  TfLENTAf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Button1: TButton;
    Button2: TButton;
    ListView1: TListView;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button2Click(Sender: TObject);
    procedure LV1;
    procedure Button3Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fLENTAf: TfLENTAf;

implementation

uses fMW;

{$R *.dfm}

procedure TfLENTAf.LV1;
begin


  ListView1.Clear;
  Screen.Cursor := crHourglass;
  Application.ProcessMessages;

pS:='Select '+
'raspisanie.data as raspisanie_data,'+
'raspisanie.id as raspisanie_id,'+
'raspisanie.podgruppa_id,'+
'para.para as para,'+
'para.budnie_chasy,'+
'para.vykhodnye_chasy,'+
'raspisanie.stroka as stroka,'+
'raspisanie.nomer_nedeli,'+
'distsiplina.distsiplina_na_russkom as distsiplina_na_russkom,'+
'distsiplina.distsiplina_na_kazakhskom as distsiplina_na_kazakhskom,'+
'prepodavatelmz.fio as fio,'+
'kabinet.nomer_kabineta as nomer_kabineta,'+
'raspisanie.kabinet_id,'+
'tip_zanyatiya.tip_zanyatiya as tip_zanyatiya,'+
'raspisanie.data as data,'+
'gruppa.nazvanie_gruppy as gruppa_nazvanie_gruppy,'+
'gruppa.yazyk_obucheniya_id as yazyk_obucheniya_id,'+
'podgruppa.podgruppa as podgruppa,'+
'vid_nagruzki.vid_nagruzki as vid_nagruzki '+
'from '+
'raspisanie,para,distsiplina,prepodavatelmz,kabinet,tip_zanyatiya,gruppa,podgruppa,vid_nagruzki '+
'where '+
'raspisanie.para_id=para.id and '+
'raspisanie.distsiplina_id=distsiplina.id and '+
'raspisanie.prepodavatelmz_id=prepodavatelmz.id and '+
'raspisanie.kabinet_id=kabinet.id and '+
'raspisanie.tip_zanyatiya_id=tip_zanyatiya.id and '+
'raspisanie.gruppa_id=gruppa.id and '+
'raspisanie.podgruppa_id=podgruppa.id and '+
'raspisanie.vid_nagruzki_id=vid_nagruzki.id and '+
'raspisanie.deleted=0 and '+
//'raspisanie.gruppa_id='+pGRUPPA_ID+ ' and '+
'raspisanie.god_id='+pCurrent_God_id+' and '+
'raspisanie.polugodie_id='+pCurrent_Polugodie_id+' and '+
'raspisanie.prepodavatelmz_id='+pCurrent_Combo_prepodavatelmz_id_x;

pS:=pS+' order by raspisanie.data, raspisanie.para_id';

//Clipboard.AsText:=pS;

ADQ1.Close;
ADQ1.SQL.Clear;
ADQ1.SQL.Add(pS);
ADQ1.Open;

  if ADQ1.RecordCount = 0 then
  begin
    ListView1.Clear;
    StatusBar1.panels[0].text:='����� ������� : 0';
    Screen.Cursor := crDefault;
    exit;
  end;


  ListView1.Visible := false;
  ListView1.Items.Clear;

  ADQ1.Last;
  ADQ1.First;

  while not ADQ1.Eof do
  begin
    L := ListView1.Items.Add;
    L.Caption := trim(ADQ1.fieldbyname('raspisanie_id').AsString);
    L.SubItems.Append(ADQ1.fieldbyname('fio').AsString);
    L.SubItems.Append(ADQ1.fieldbyname('raspisanie_data').AsString);
    L.SubItems.Append(ADQ1.fieldbyname('para').AsString);
    if pCurrentLanguage='1' then L.SubItems.Append(ADQ1.fieldbyname('distsiplina_na_russkom').AsString);
    if pCurrentLanguage='2' then L.SubItems.Append(ADQ1.fieldbyname('distsiplina_na_kazakhskom').AsString);
    L.SubItems.Append(ADQ1.fieldbyname('gruppa_nazvanie_gruppy').AsString);
    L.SubItems.Append(ADQ1.fieldbyname('podgruppa').AsString);
    L.SubItems.Append(ADQ1.fieldbyname('tip_zanyatiya').AsString);
    L.SubItems.Append(ADQ1.fieldbyname('nomer_kabineta').AsString);

    ADQ1.Next;
  end;

  ListView1.Visible := true;
  StatusBar1.panels[0].text:='����� ������� : '+inttostr(ADQ.RecordCount);
  
  if ListView1.Items.Count>0 then begin
    ListView1.Items[0].Selected:=true;
  end;

  Screen.Cursor := crDefault;

end;


procedure TfLENTAf.Button1Click(Sender: TObject);
begin
Close;
end;

procedure TfLENTAf.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=vk_escape then close;
end;

procedure TfLENTAf.Button2Click(Sender: TObject);
begin
  fMWf.LVToExcel(ListView1);
end;

procedure TfLENTAf.Button3Click(Sender: TObject);
begin
LV1;
end;

procedure TfLENTAf.FormActivate(Sender: TObject);
begin
ListView1.Clear;
fMWf.SetLCW(Listview1);
StatusBar1.panels[0].text:='����� ������� : 0';
end;

procedure TfLENTAf.FormResize(Sender: TObject);
begin
fMWf.SetLCW(Listview1);
end;

end.
