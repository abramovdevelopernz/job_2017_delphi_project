unit fPREPODAVATELMZ_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfPREPODAVATELMZ_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_fio: TLabel;
    Label_mesto_raboty: TLabel;
    Label_shtatnyy: TLabel;
    Label_vozmozhnostmz_raboty_id: TLabel;
    Label_para_id: TLabel;

    Edit_fio: TEdit;
    Edit_mesto_raboty: TEdit;
    CheckBox_shtatnyy: TCheckBox;
    Combo_vozmozhnostmz_raboty_id: TComboBox;
    Button_vozmozhnostmz_raboty_id: TButton;
    Combo_para_id: TComboBox;
    Button_para_id: TButton;

    Check_fio: TCheckBox;
    Check_mesto_raboty: TCheckBox;
    Check_shtatnyy: TCheckBox;
    Check_vozmozhnostmz_raboty_id: TCheckBox;
    Check_para_id: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;
    procedure LoadVOZMOZHNOSTMZ_RABOTY;
    procedure GetVOZMOZHNOSTMZ_RABOTY(pCombo:TComboBox;pSID:integer);
    procedure Button_vozmozhnostmz_raboty_idClick(Sender: TObject);

    procedure LoadPARA;
    procedure GetPARA(pCombo:TComboBox;pSID:integer);
    procedure Button_para_idClick(Sender: TObject);


    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fPREPODAVATELMZ_RECORDf: TfPREPODAVATELMZ_RECORDf;

implementation

uses  
    fPREPODAVATELMZ,
    fVOZMOZHNOSTMZ_RABOTY,
    fPARA,
    fMW;

{$R *.dfm}

procedure TfPREPODAVATELMZ_RECORDf.GetVOZMOZHNOSTMZ_RABOTY(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfPREPODAVATELMZ_RECORDf.LoadVOZMOZHNOSTMZ_RABOTY;
begin

Combo_vozmozhnostmz_raboty_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from vozmozhnostmz_raboty where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_vozmozhnostmz_raboty_id.AddItem(ADQ.FieldByName('vozmozhnostmz_raboty').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_vozmozhnostmz_raboty_id.ItemIndex:=0;

end;

procedure TfPREPODAVATELMZ_RECORDf.Button_vozmozhnostmz_raboty_idClick(Sender: TObject);
begin

  fVOZMOZHNOSTMZ_RABOTYf.Caption:='����� ����������� ������';
  if Combo_vozmozhnostmz_raboty_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_vozmozhnostmz_raboty_id.Items.Objects[Combo_vozmozhnostmz_raboty_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fVOZMOZHNOSTMZ_RABOTYf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='PREPODAVATELMZ';

  if pNoAction=false then begin
    LoadVOZMOZHNOSTMZ_RABOTY;
    if pSelectedID<>'' then GetVOZMOZHNOSTMZ_RABOTY(Combo_vozmozhnostmz_raboty_id,strtoint(pSelectedID));
  end;

end;

procedure TfPREPODAVATELMZ_RECORDf.GetPARA(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfPREPODAVATELMZ_RECORDf.LoadPARA;
begin

Combo_para_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from para where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_para_id.AddItem(ADQ.FieldByName('para').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_para_id.ItemIndex:=0;

end;

procedure TfPREPODAVATELMZ_RECORDf.Button_para_idClick(Sender: TObject);
begin

  fPARAf.Caption:='����� ����';
  if Combo_para_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_para_id.Items.Objects[Combo_para_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fPARAf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='PREPODAVATELMZ';

  if pNoAction=false then begin
    LoadPARA;
    if pSelectedID<>'' then GetPARA(Combo_para_id,strtoint(pSelectedID));
  end;

end;

procedure TfPREPODAVATELMZ_RECORDf.CheckInput;
begin

if trim(Edit_fio.Text)='' then begin
  Edit_fio.SetFocus;
  abort;
end;

if trim(Combo_vozmozhnostmz_raboty_id.Text)='' then begin
  Combo_vozmozhnostmz_raboty_id.SetFocus;
  abort;
end;

if trim(Combo_para_id.Text)='' then begin
  Combo_para_id.SetFocus;
  abort;
end;

end;
procedure TfPREPODAVATELMZ_RECORDf.CLALL;
begin

  LoadVOZMOZHNOSTMZ_RABOTY;
  LoadPARA;

  Check_fio.Checked:=false;
  Check_fio.Visible:=false;
  Check_mesto_raboty.Checked:=false;
  Check_mesto_raboty.Visible:=false;
  Check_shtatnyy.Checked:=false;
  Check_shtatnyy.Visible:=false;
  Check_vozmozhnostmz_raboty_id.Checked:=false;
  Check_vozmozhnostmz_raboty_id.Visible:=false;
  Check_para_id.Checked:=false;
  Check_para_id.Visible:=false;

  Edit_fio.Clear;
  Edit_mesto_raboty.Clear;
  Check_shtatnyy.Checked:=false;

  Edit_fio.Setfocus;

end;

procedure TfPREPODAVATELMZ_RECORDf.FormActivated;
begin

  CLALL;

if pActionPREPODAVATELMZ='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionPREPODAVATELMZ='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from prepodavatelmz where id='+pID);
  ADQ.Open;
  ADQ.First;

  Edit_fio.Text:=ADQ.fieldbyname('fio').AsString;
  Edit_mesto_raboty.Text:=ADQ.fieldbyname('mesto_raboty').AsString;
  CheckBox_shtatnyy.Checked:=ADQ.fieldbyname('shtatnyy').AsBoolean;
  GetVOZMOZHNOSTMZ_RABOTY(Combo_vozmozhnostmz_raboty_id,ADQ.fieldbyname('vozmozhnostmz_raboty_id').AsInteger);
  GetPARA(Combo_para_id,ADQ.fieldbyname('para_id').AsInteger);

end;

if pActionPREPODAVATELMZ='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_fio.Visible:=true;
  Check_mesto_raboty.Visible:=true;
  Check_shtatnyy.Visible:=true;
  Check_vozmozhnostmz_raboty_id.Visible:=true;
  Check_para_id.Visible:=true;

end;

end;

procedure TfPREPODAVATELMZ_RECORDf.BtnOK;
begin

if pActionPREPODAVATELMZ='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'prepodavatelmz '+
  '('+
  'fio,'+
  'mesto_raboty,'+
  'shtatnyy,'+
  'vozmozhnostmz_raboty_id,'+
  'para_id,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+trim(Edit_fio.Text)+''', '+
  ' '''+trim(Edit_mesto_raboty.Text)+''', '+
  ' '''+booltoStr(CheckBox_shtatnyy.Checked)+''', '+
  ' '''+Inttostr(Integer(Combo_vozmozhnostmz_raboty_id.Items.Objects[Combo_vozmozhnostmz_raboty_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_para_id.Items.Objects[Combo_para_id.ItemIndex]))+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionPREPODAVATELMZ='UPDATE' then begin

 CheckInput;

 pS:='UPDATE '+
  'prepodavatelmz set ' +
  'fio='''+trim(Edit_fio.Text)+''','+
  'mesto_raboty='''+trim(Edit_mesto_raboty.Text)+''','+
  'shtatnyy='''+BoolToStr(CheckBox_shtatnyy.Checked)+''','+
  'vozmozhnostmz_raboty_id='''+Inttostr(Integer(Combo_vozmozhnostmz_raboty_id.Items.Objects[Combo_vozmozhnostmz_raboty_id.ItemIndex]))+''','+
  'para_id='''+Inttostr(Integer(Combo_para_id.Items.Objects[Combo_para_id.ItemIndex]))+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fPREPODAVATELMZf.ListView1.Selected.SubItems[0]:=trim(Edit_fio.Text);
   fPREPODAVATELMZf.ListView1.Selected.SubItems[1]:=trim(Edit_mesto_raboty.Text);
   if CheckBox_shtatnyy.Checked=true then fPREPODAVATELMZf.ListView1.Selected.SubItems[2]:='��' else fPREPODAVATELMZf.ListView1.Selected.SubItems[2]:='���';
   fPREPODAVATELMZf.ListView1.Selected.SubItems[3]:=trim(Combo_vozmozhnostmz_raboty_id.Text);
   fPREPODAVATELMZf.ListView1.Selected.SubItems[4]:=trim(Combo_para_id.Text);

end;

if pActionPREPODAVATELMZ='SEARCH' then begin

if (Check_fio.Checked=false) and (Check_mesto_raboty.Checked=false) and (Check_shtatnyy.Checked=false) and (Check_vozmozhnostmz_raboty_id.Checked=false) and (Check_para_id.Checked=false) then abort;

   pSearch:='';

   if Check_fio.Checked=true then pSearch:=pSearch+' and prepodavatelmz.fio like '''+trim(Edit_fio.Text)+''' ';
   if Check_mesto_raboty.Checked=true then pSearch:=pSearch+' and prepodavatelmz.mesto_raboty like '''+trim(Edit_mesto_raboty.Text)+''' ';
   if Check_shtatnyy.Checked=true then pSearch:=pSearch+' and prepodavatelmz.shtatnyy = '+BoolToStr(CheckBox_shtatnyy.Checked)+' ';
   if Check_vozmozhnostmz_raboty_id.Checked=true then pSearch:=pSearch+' and prepodavatelmz.vozmozhnostmz_raboty_id = '+Inttostr(Integer(Combo_vozmozhnostmz_raboty_id.Items.Objects[Combo_vozmozhnostmz_raboty_id.ItemIndex]))+' ';
   if Check_para_id.Checked=true then pSearch:=pSearch+' and prepodavatelmz.para_id = '+Inttostr(Integer(Combo_para_id.Items.Objects[Combo_para_id.ItemIndex]))+' ';

end;

Close;

end;

procedure TfPREPODAVATELMZ_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfPREPODAVATELMZ_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfPREPODAVATELMZ_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfPREPODAVATELMZ_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.