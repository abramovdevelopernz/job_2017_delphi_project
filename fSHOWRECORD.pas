unit fSHOWRECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Idglobal, JPEG, Menus;

type
  TfSHOWRECORDf = class(TForm)
    Panel1: TPanel;
    ListView1: TListView;
    Button1: TButton;
    Button2: TButton;
    Panel2: TPanel;
    Splitter1: TSplitter;
    Image1: TImage;
    OpenDialog1: TOpenDialog;
    PopupMenu1: TPopupMenu;
    N_LOAD_IMAGE: TMenuItem;
    N_DELETE_IMAGE: TMenuItem;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure N_LOAD_IMAGEClick(Sender: TObject);
    procedure N_DELETE_IMAGEClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fSHOWRECORDf: TfSHOWRECORDf;

implementation

uses fMW;

{$R *.dfm}

procedure TfSHOWRECORDf.FormActivate(Sender: TObject);
begin

if pPhoto=false then begin
  Splitter1.Visible:=false;
  Panel2.Visible:=false;
end else begin
  Panel2.Visible:=true;
  Splitter1.Visible:=true;
  Splitter1.Align:=alNone;
  Panel2.Align:=alNone;
  ListView1.Align:=alNone;
  Panel2.Align:=alRight;
  Splitter1.Align:=alRight;
  ListView1.Align:=alClient;

  if FileExists(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Items[0].SubItems[0]+'.jpg')= true then begin
      Image1.Picture.LoadFromFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Items[0].SubItems[0]+'.jpg');
      Image1.Visible:=true;
  end else begin
      Image1.Picture.Bitmap:=nil;
      Splitter1.Visible:=false;
      Panel2.Visible:=false;
  end;

end;

end;

procedure TfSHOWRECORDf.N_LOAD_IMAGEClick(Sender: TObject);
begin
  if ListView1.Items.Count=0 then abort;
  if MessageBox(Self.Handle, '������� �����������?', '����� �����', MB_OKCANCEL) =  IDCANCEL then abort;
  OpenDialog1.Execute;
  if trim(OpenDialog1.FileName)='' then abort;
  Application.ProcessMessages;
  Screen.Cursor := crHourglass;
  DeleteFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Items[0].SubItems[0]+'.jpg');
  CopyFileTo(trim(OpenDialog1.FileName),ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Items[0].SubItems[0]+'.jpg');
  Image1.Picture.LoadFromFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Items[0].SubItems[0]+'.jpg');
  Screen.Cursor := crDefault;
end;

procedure TfSHOWRECORDf.N_DELETE_IMAGEClick(Sender: TObject);
begin
if MessageBox(Self.Handle, '������� �����������?', '�������� �����', MB_OKCANCEL) =  IDCANCEL then abort;
if FileExists(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Items[0].SubItems[0]+'.jpg')= true then begin
  DeleteFile(ExtractFilePath(Application.ExeName)+'FILES\'+pTableName+ListView1.Items[0].SubItems[0]+'.jpg');
  Image1.Picture.Bitmap:=nil;
end;
end;

procedure TfSHOWRECORDf.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TfSHOWRECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Close;
end;

procedure TfSHOWRECORDf.Button2Click(Sender: TObject);
begin
  fMWf.LVToExcel(ListView1);
end;

end.