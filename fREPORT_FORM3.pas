unit fREPORT_FORM3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,comobj;

type
  TfREPORT_FORM3f = class(TForm)
    Panel3: TPanel;
    Button1: TButton;
    Button2: TButton;
    Panel1: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Combo_prepodavatelmz_id: TComboBox;
    Combo_predmet: TComboBox;
    ProgressBar1: TProgressBar;
    procedure FormActivate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LoadPredmet;
    function CountPara(pMonthNumber:integer;pGruppa_id:string): string;
    procedure Combo_prepodavatelmz_idChange(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fREPORT_FORM3f: TfREPORT_FORM3f;

implementation
uses fMW;
{$R *.dfm}

function TfREPORT_FORM3f.CountPara(pMonthNumber:integer;pGruppa_id:string): string;
var
pSQL_Interval:widestring;
begin

if pMonthNumber=1 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.01.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('31.01.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=2 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.02.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('28.02.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=3 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.03.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('31.03.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=4 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.04.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('30.04.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=5 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.05.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('31.05.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=6 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.06.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('30.06.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=7 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.07.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('31.07.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=8 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.08.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('31.08.'+inttostr(strtoint(fMWf.Combo_god_id.Text)+1))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=9 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.09.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('30.09.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=10 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.10.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('31.10.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=11 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.11.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('30.11.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';
if pMonthNumber=12 then pInterval:='raspisanie.data between #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('01.12.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'#  and #'+StringReplace(FormatDateTime('MM.DD.YYYY',strtodate('31.12.'+inttostr(strtoint(fMWf.Combo_god_id.Text)))), '.', '/',[rfReplaceAll, rfIgnoreCase])+'# and ';

pSQL_Interval:=
' Select count(id) as count_id '+
' from '+
' raspisanie '+
' where '+
' raspisanie.prepodavatelmz_id= ' +Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+' and ' +
' raspisanie.distsiplina_id= ' +Inttostr(Integer(Combo_predmet.Items.Objects[Combo_predmet.ItemIndex]))+' and ' +
' raspisanie.gruppa_id= ' +pGruppa_id+' and ' +
' raspisanie.god_id='+pCurrent_God_id+' and ' +pInterval+
' raspisanie.deleted=0 ';

//ShowMessage(pSQL_Interval);

ADQ1.Close;
ADQ1.SQL.Clear;
ADQ1.SQL.Add(pSQL_Interval);
ADQ1.Open;

Result := inttostr(ADQ1.FieldByName('count_id').AsInteger*2);
end;

procedure TfREPORT_FORM3f.LoadPredmet;
begin
  Combo_predmet.Clear;
  Screen.Cursor := crHourglass;
  Application.ProcessMessages;

pS:='Select distinct '+
' distsiplina.id as distsiplina_id,'+
' distsiplina.distsiplina_na_russkom as distsiplina_name '+
' from '+
' god,'+ 
' polugodie,'+ 
' distsiplina,'+ 
' gruppa,'+ 
' podgruppa,'+ 
' vid_nagruzki,'+ 
' nagruzka_na_prepodavatelya '+
' where '+
' god.id=nagruzka_na_prepodavatelya.god_id and ' +
' polugodie.id=nagruzka_na_prepodavatelya.polugodie_id and ' +
' distsiplina.id=nagruzka_na_prepodavatelya.distsiplina_id and ' +
' gruppa.id=nagruzka_na_prepodavatelya.gruppa_id and ' +
' podgruppa.id=nagruzka_na_prepodavatelya.podgruppa_id and ' +
' vid_nagruzki.id=nagruzka_na_prepodavatelya.vid_nagruzki_id and ' +
' nagruzka_na_prepodavatelya.prepodavatelmz_id= ' +Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+' and ' +
' nagruzka_na_prepodavatelya.god_id='+pCurrent_God_id+' and ' +
' nagruzka_na_prepodavatelya.polugodie_id='+pCurrent_Polugodie_id+' and ' +
' vid_nagruzki.id=nagruzka_na_prepodavatelya.vid_nagruzki_id and ' +
' nagruzka_na_prepodavatelya.deleted=0 ';

//ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.Open;

  if ADQ.RecordCount = 0 then
  begin
    Combo_predmet.Clear;
    ShowMessage('�� ��������� ������ ��� ���������� �� �������� ���������� �������������');
    Screen.Cursor := crDefault;
    exit;
  end;

ADQ.Last;
ADQ.First;

while not ADQ.Eof do begin
  Combo_predmet.AddItem(ADQ.FieldByName('distsiplina_name').AsString, Pointer(ADQ.FieldByName('distsiplina_id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_predmet.ItemIndex:=0;

Screen.Cursor := crDefault;

end;


procedure TfREPORT_FORM3f.FormActivate(Sender: TObject);
begin
ProgressBar1.Visible:=false;
Combo_predmet.Clear;
Combo_prepodavatelmz_id.Clear;

ADQ1.Close;
ADQ1.SQL.Clear;
ADQ1.SQL.Add('Select * from prepodavatelmz where prepodavatelmz.deleted=0 order by prepodavatelmz.fio');
ADQ1.Open;

ADQ1.Last;
ADQ1.First;

while not ADQ1.Eof do begin
  Combo_prepodavatelmz_id.AddItem(ADQ1.FieldByName('fio').AsString, Pointer(ADQ1.FieldByName('id').AsInteger));
  ADQ1.Next;
end;

if ADQ1.RecordCount>0 then Combo_prepodavatelmz_id.ItemIndex:=0;

end;

procedure TfREPORT_FORM3f.Button1Click(Sender: TObject);
begin
Close
end;

procedure TfREPORT_FORM3f.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfREPORT_FORM3f.Combo_prepodavatelmz_idChange(Sender: TObject);
begin
LoadPredmet;
end;

procedure TfREPORT_FORM3f.Button2Click(Sender: TObject);
var
pCurrentNum:integer;
pSQL:widestring;
day,pChasy,pDistsiplina : string;
xlapp, xlbook, xlsht: OleVariant;
i,j,z,l: word;
rc, FirstI: integer;
pName,pGruppaName:string;
begin


pS:='Select distinct '+
' gruppa.id as gruppa_id,'+
' gruppa.nazvanie_gruppy as gruppa_nazvanie_gruppy '+
' from '+
' god,'+
' polugodie,'+
' distsiplina,'+
' gruppa,'+
' podgruppa,'+
' vid_nagruzki,'+
' nagruzka_na_prepodavatelya '+
' where '+
' god.id=nagruzka_na_prepodavatelya.god_id and ' +
' polugodie.id=nagruzka_na_prepodavatelya.polugodie_id and ' +
' distsiplina.id=nagruzka_na_prepodavatelya.distsiplina_id and ' +
' gruppa.id=nagruzka_na_prepodavatelya.gruppa_id and ' +
' podgruppa.id=nagruzka_na_prepodavatelya.podgruppa_id and ' +
' vid_nagruzki.id=nagruzka_na_prepodavatelya.vid_nagruzki_id and ' +
' nagruzka_na_prepodavatelya.distsiplina_id= ' +Inttostr(Integer(Combo_predmet.Items.Objects[Combo_predmet.ItemIndex]))+' and ' +
' nagruzka_na_prepodavatelya.prepodavatelmz_id= ' +Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+' and ' +
' nagruzka_na_prepodavatelya.god_id='+pCurrent_God_id+' and ' +
//' nagruzka_na_prepodavatelya.polugodie_id='+pCurrent_Polugodie_id+' and ' +
' vid_nagruzki.id=nagruzka_na_prepodavatelya.vid_nagruzki_id and ' +
' nagruzka_na_prepodavatelya.deleted=0 ';

//ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.Open;

  if ADQ.RecordCount = 0 then
  begin
    Combo_predmet.Clear;
    ShowMessage('�� ��������� ������ ��� ���������� �� �������� ���������� �������������');
    Screen.Cursor := crDefault;
    exit;
  end;

ProgressBar1.Visible:=true;
ProgressBar1.Max:=ADQ.RecordCount;
ProgressBar1.Position:=0;
l:=0;

xlapp:=CreateOleObject('Excel.Application');
xlapp.Visible:=false;
xlbook:=xlapp.WorkBooks.Open(ExtractFilePath(Application.exeName)+'report.xls');
xlsht := xlapp.Workbooks[1].Worksheets['form3'];
xlsht := xlapp.Sheets;
xlsht.Item['form3'].Activate;
xlsht:=xlbook.ActiveSheet;

xlsht.Cells(7,3):='���� ����� ������ �� '+fMWf.Combo_god_id.Text+' - '+inttostr(strtoint(fMWf.Combo_god_id.Text)+1);

xlsht.Cells(9,3):=Combo_prepodavatelmz_id.Text;
xlsht.Cells(10,3):=Combo_predmet.Text;

//ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.Open;

ADQ.Last;
ADQ.First;
i:=3;

//ShowMessage(inttostr(ADQ.recordcount));

while not ADQ.Eof do begin
  xlsht.Cells(14,i):=ADQ.FieldByName('gruppa_nazvanie_gruppy').AsString;
  xlsht.Cells(15,i):=CountPara(9,ADQ.FieldByName('gruppa_id').AsString);
  xlsht.Cells(16,i):=CountPara(10,ADQ.FieldByName('gruppa_id').AsString);
  xlsht.Cells(17,i):=CountPara(11,ADQ.FieldByName('gruppa_id').AsString);
  xlsht.Cells(18,i):=CountPara(12,ADQ.FieldByName('gruppa_id').AsString);
  xlsht.Cells(19,i):=CountPara(1,ADQ.FieldByName('gruppa_id').AsString);
  xlsht.Cells(20,i):=CountPara(2,ADQ.FieldByName('gruppa_id').AsString);
  xlsht.Cells(21,i):=CountPara(3,ADQ.FieldByName('gruppa_id').AsString);
  xlsht.Cells(22,i):=CountPara(4,ADQ.FieldByName('gruppa_id').AsString);
  xlsht.Cells(23,i):=CountPara(5,ADQ.FieldByName('gruppa_id').AsString);
  xlsht.Cells(24,i):=CountPara(6,ADQ.FieldByName('gruppa_id').AsString);
  xlsht.Cells(25,i):=CountPara(7,ADQ.FieldByName('gruppa_id').AsString);
  xlsht.Cells(26,i):=CountPara(8,ADQ.FieldByName('gruppa_id').AsString);

//����� ���� �����
pS:='Select '+
' sum(nagruzka_na_prepodavatelya.teoriya_chasov_itogo) as sum_teoriya_chasov_itogo,'+
' sum(nagruzka_na_prepodavatelya.praktika_chasov_itogo) as sum_praktika_chasov_itogo '+
' from '+
' god,'+
' polugodie,'+
' distsiplina,'+
' gruppa,'+
' podgruppa,'+
' vid_nagruzki,'+
' nagruzka_na_prepodavatelya '+
' where '+
' god.id=nagruzka_na_prepodavatelya.god_id and ' +
' polugodie.id=nagruzka_na_prepodavatelya.polugodie_id and ' +
' distsiplina.id=nagruzka_na_prepodavatelya.distsiplina_id and ' +
' gruppa.id=nagruzka_na_prepodavatelya.gruppa_id and ' +
' podgruppa.id=nagruzka_na_prepodavatelya.podgruppa_id and ' +
' vid_nagruzki.id=nagruzka_na_prepodavatelya.vid_nagruzki_id and ' +
' nagruzka_na_prepodavatelya.god_id='+pCurrent_God_id+' and ' +
' nagruzka_na_prepodavatelya.distsiplina_id= ' +Inttostr(Integer(Combo_predmet.Items.Objects[Combo_predmet.ItemIndex]))+' and ' +
' nagruzka_na_prepodavatelya.prepodavatelmz_id= ' +Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+' and ' +
' nagruzka_na_prepodavatelya.gruppa_id= ' +ADQ.FieldByName('gruppa_id').AsString+' and ' +
' vid_nagruzki.id=nagruzka_na_prepodavatelya.vid_nagruzki_id and ' +
' nagruzka_na_prepodavatelya.deleted=0 ';
  ADQ1.Close;
  ADQ1.SQL.Clear;
  ADQ1.SQL.Add(pS);
  ADQ1.Open;
  xlsht.Cells(28,i):=inttostr(ADQ1.fieldbyname('sum_teoriya_chasov_itogo').AsInteger*2+ADQ1.fieldbyname('sum_praktika_chasov_itogo').AsInteger*2);

//����� ����� �� �����
pS:='Select '+
' sum(nagruzka_na_prepodavatelya.teoriya_chasov_itogo) as sum_teoriya_chasov_itogo,'+
' sum(nagruzka_na_prepodavatelya.praktika_chasov_itogo) as sum_praktika_chasov_itogo '+
' from '+
' god,'+
' polugodie,'+
' distsiplina,'+
' gruppa,'+
' podgruppa,'+
' vid_nagruzki,'+
' nagruzka_na_prepodavatelya '+
' where '+
' god.id=nagruzka_na_prepodavatelya.god_id and ' +
' polugodie.id=nagruzka_na_prepodavatelya.polugodie_id and ' +
' distsiplina.id=nagruzka_na_prepodavatelya.distsiplina_id and ' +
' gruppa.id=nagruzka_na_prepodavatelya.gruppa_id and ' +
' podgruppa.id=nagruzka_na_prepodavatelya.podgruppa_id and ' +
' vid_nagruzki.id=nagruzka_na_prepodavatelya.vid_nagruzki_id and ' +
' nagruzka_na_prepodavatelya.vid_nagruzki_id=1 and ' +
' nagruzka_na_prepodavatelya.god_id='+pCurrent_God_id+' and ' +
' nagruzka_na_prepodavatelya.distsiplina_id= ' +Inttostr(Integer(Combo_predmet.Items.Objects[Combo_predmet.ItemIndex]))+' and ' +
' nagruzka_na_prepodavatelya.prepodavatelmz_id= ' +Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+' and ' +
' nagruzka_na_prepodavatelya.gruppa_id= ' +ADQ.FieldByName('gruppa_id').AsString+' and ' +
' vid_nagruzki.id=nagruzka_na_prepodavatelya.vid_nagruzki_id and ' +
' nagruzka_na_prepodavatelya.deleted=0 ';
  ADQ1.Close;
  ADQ1.SQL.Clear;
  ADQ1.SQL.Add(pS);
  ADQ1.Open;
  xlsht.Cells(29,i):=inttostr(ADQ1.fieldbyname('sum_teoriya_chasov_itogo').AsInteger*2+ADQ1.fieldbyname('sum_praktika_chasov_itogo').AsInteger*2);

//�� ��������� �����
pS:='Select count(id) as count_id from reestr_progulov where '+
' god_id='+pCurrent_God_id+' and ' +
' distsiplina_id= ' +Inttostr(Integer(Combo_predmet.Items.Objects[Combo_predmet.ItemIndex]))+' and ' +
' prepodavatelmz_id= ' +Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+' and ' +
' gruppa_id= ' +ADQ.FieldByName('gruppa_id').AsString+' and ' +
' deleted=0 ' ;
  ADQ1.Close;
  ADQ1.SQL.Clear;
  ADQ1.SQL.Add(pS);
  ADQ1.Open;
  xlsht.Cells(30,i):=inttostr(ADQ1.fieldbyname('count_id').AsInteger*2);

//���� ����� ����� �����
pS:='Select '+
' sum(nagruzka_na_prepodavatelya.teoriya_chasov_itogo) as sum_teoriya_chasov_itogo,'+
' sum(nagruzka_na_prepodavatelya.praktika_chasov_itogo) as sum_praktika_chasov_itogo '+
' from '+
' god,'+
' polugodie,'+
' distsiplina,'+
' gruppa,'+
' podgruppa,'+
' vid_nagruzki,'+
' nagruzka_na_prepodavatelya '+
' where '+
' god.id=nagruzka_na_prepodavatelya.god_id and ' +
' polugodie.id=nagruzka_na_prepodavatelya.polugodie_id and ' +
' distsiplina.id=nagruzka_na_prepodavatelya.distsiplina_id and ' +
' gruppa.id=nagruzka_na_prepodavatelya.gruppa_id and ' +
' podgruppa.id=nagruzka_na_prepodavatelya.podgruppa_id and ' +
' vid_nagruzki.id=nagruzka_na_prepodavatelya.vid_nagruzki_id and ' +
' nagruzka_na_prepodavatelya.vid_nagruzki_id=2 and ' +
' nagruzka_na_prepodavatelya.god_id='+pCurrent_God_id+' and ' +
' nagruzka_na_prepodavatelya.distsiplina_id= ' +Inttostr(Integer(Combo_predmet.Items.Objects[Combo_predmet.ItemIndex]))+' and ' +
' nagruzka_na_prepodavatelya.prepodavatelmz_id= ' +Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+' and ' +
' nagruzka_na_prepodavatelya.gruppa_id= ' +ADQ.FieldByName('gruppa_id').AsString+' and ' +
' vid_nagruzki.id=nagruzka_na_prepodavatelya.vid_nagruzki_id and ' +
' nagruzka_na_prepodavatelya.deleted=0 ';
  ADQ1.Close;
  ADQ1.SQL.Clear;
  ADQ1.SQL.Add(pS);
  ADQ1.Open;
  xlsht.Cells(31,i):=inttostr(ADQ1.fieldbyname('sum_teoriya_chasov_itogo').AsInteger+ADQ1.fieldbyname('sum_praktika_chasov_itogo').AsInteger);

  ProgressBar1.Position:=l;
  inc(l);
  inc(i);
  ADQ.Next;
end;

xlapp.Visible:=true;

//xlsht.PageSetup.Orientation := 1;
//xlsht.PageSetup.Zoom := false;
//xlsht.PageSetup.Order := 1;
//xlsht.PrintPreview;
xlapp.ActiveWindow.Zoom := 100;

ProgressBar1.Visible:=false;
Screen.Cursor := crDefault;


end;

end.
