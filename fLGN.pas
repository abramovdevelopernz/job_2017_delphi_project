unit fLGN;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, ExtCtrls, StdCtrls;

type
  TfLGNf = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Button1: TButton;
    Button2: TButton;
    LabelProject: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure Edit2KeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fLGNf: TfLGNf;

implementation
uses fMW;
{$R *.dfm}

procedure TfLGNf.FormActivate(Sender: TObject);
begin
  LabelProject.Caption:=pProject;
  Edit1.Clear;
  Edit2.Clear;
  Edit1.SetFocus;
end;
procedure TfLGNf.Edit1KeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_return then if trim(Edit1.Text)<>'' then Edit2.SetFocus;
end;

procedure TfLGNf.Edit2KeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_return then if trim(Edit2.Text)<>'' then Button1.SetFocus;
end;

procedure TfLGNf.Button2Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfLGNf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if ((ssAlt in Shift) and (Key = VK_F4)) then Key := 0;
  if key=vk_escape then Application.Terminate;
end;

procedure TfLGNf.Button1Click(Sender: TObject);
begin

if trim(Edit1.Text)='' then begin
  Edit1.SetFocus;
  abort;
end;

if trim(Edit2.Text)='' then begin
  Edit2.SetFocus;
  abort;
end;

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from TEMPLATE_USER where deleted=0 and lgn='''+trim(Edit1.Text)+''' and ps ='''+trim(Edit2.Text)+''' ');
  ADQ.Open;

  if ADQ.RecordCount=0 then begin
    MessageDlg('����� ������������ ����������� � ������� ���� ������ �������� ������.'+#13#10+'���������� ��� ���', mtInformation, [mbOK], 0);
    Edit1.SetFocus;
    Abort;
  end;

  pMyUser:=ADQ.fieldbyname('name').AsString;
  fMWf.StatusBar1.Panels[0].Text:='������� ������������ - '+pMyUser;
  Close;

end;

end.