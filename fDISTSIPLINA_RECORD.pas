unit fDISTSIPLINA_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfDISTSIPLINA_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_distsiplina_na_russkom: TLabel;
    Label_distsiplina_na_kazakhskom: TLabel;
    Label_indeks_na_russkom: TLabel;
    Label_indeks_na_kazakhskom: TLabel;
    Label_kol_podgrupp_po_praktike: TLabel;
    Label_itogo_chasov_za_vse_semestry: TLabel;
    Label_semestry: TLabel;

    Edit_distsiplina_na_russkom: TEdit;
    Edit_distsiplina_na_kazakhskom: TEdit;
    Edit_indeks_na_russkom: TEdit;
    Edit_indeks_na_kazakhskom: TEdit;
    Edit_kol_podgrupp_po_praktike: TEdit;
    Edit_itogo_chasov_za_vse_semestry: TEdit;
    Edit_semestry: TEdit;

    Check_distsiplina_na_russkom: TCheckBox;
    Check_distsiplina_na_kazakhskom: TCheckBox;
    Check_indeks_na_russkom: TCheckBox;
    Check_indeks_na_kazakhskom: TCheckBox;
    Check_kol_podgrupp_po_praktike: TCheckBox;
    Check_itogo_chasov_za_vse_semestry: TCheckBox;
    Check_semestry: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);

    procedure Edit_kol_podgrupp_po_praktikeKeyPress(Sender: TObject;var Key: Char);
    procedure Edit_itogo_chasov_za_vse_semestryKeyPress(Sender: TObject;var Key: Char);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fDISTSIPLINA_RECORDf: TfDISTSIPLINA_RECORDf;

implementation

uses  
    fDISTSIPLINA,
    fMW;

{$R *.dfm}

procedure TfDISTSIPLINA_RECORDf.Edit_kol_podgrupp_po_praktikeKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'.. '9',#8]) then key:= #0;
end;

procedure TfDISTSIPLINA_RECORDf.Edit_itogo_chasov_za_vse_semestryKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'.. '9',#8]) then key:= #0;
end;

procedure TfDISTSIPLINA_RECORDf.CheckInput;
begin

if trim(Edit_distsiplina_na_russkom.Text)='' then begin
  Edit_distsiplina_na_russkom.SetFocus;
  abort;
end;

if trim(Edit_distsiplina_na_kazakhskom.Text)='' then begin
  Edit_distsiplina_na_kazakhskom.SetFocus;
  abort;
end;

if trim(Edit_indeks_na_russkom.Text)='' then begin
  Edit_indeks_na_russkom.SetFocus;
  abort;
end;

if trim(Edit_indeks_na_kazakhskom.Text)='' then begin
  Edit_indeks_na_kazakhskom.SetFocus;
  abort;
end;

if trim(Edit_kol_podgrupp_po_praktike.Text)='' then begin
  Edit_kol_podgrupp_po_praktike.SetFocus;
  abort;
end;

if trim(Edit_itogo_chasov_za_vse_semestry.Text)='' then begin
  Edit_itogo_chasov_za_vse_semestry.SetFocus;
  abort;
end;

if trim(Edit_semestry.Text)='' then begin
  Edit_semestry.SetFocus;
  abort;
end;

end;
procedure TfDISTSIPLINA_RECORDf.CLALL;
begin


  Check_distsiplina_na_russkom.Checked:=false;
  Check_distsiplina_na_russkom.Visible:=false;
  Check_distsiplina_na_kazakhskom.Checked:=false;
  Check_distsiplina_na_kazakhskom.Visible:=false;
  Check_indeks_na_russkom.Checked:=false;
  Check_indeks_na_russkom.Visible:=false;
  Check_indeks_na_kazakhskom.Checked:=false;
  Check_indeks_na_kazakhskom.Visible:=false;
  Check_kol_podgrupp_po_praktike.Checked:=false;
  Check_kol_podgrupp_po_praktike.Visible:=false;
  Check_itogo_chasov_za_vse_semestry.Checked:=false;
  Check_itogo_chasov_za_vse_semestry.Visible:=false;
  Check_semestry.Checked:=false;
  Check_semestry.Visible:=false;

  Edit_distsiplina_na_russkom.Clear;
  Edit_distsiplina_na_kazakhskom.Clear;
  Edit_indeks_na_russkom.Clear;
  Edit_indeks_na_kazakhskom.Clear;
  Edit_kol_podgrupp_po_praktike.Clear;
  Edit_itogo_chasov_za_vse_semestry.Clear;
  Edit_semestry.Clear;

  Edit_distsiplina_na_russkom.Setfocus;

end;

procedure TfDISTSIPLINA_RECORDf.FormActivated;
begin

  CLALL;

if pActionDISTSIPLINA='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionDISTSIPLINA='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from distsiplina where id='+pID);
  ADQ.Open;
  ADQ.First;

  Edit_distsiplina_na_russkom.Text:=ADQ.fieldbyname('distsiplina_na_russkom').AsString;
  Edit_distsiplina_na_kazakhskom.Text:=ADQ.fieldbyname('distsiplina_na_kazakhskom').AsString;
  Edit_indeks_na_russkom.Text:=ADQ.fieldbyname('indeks_na_russkom').AsString;
  Edit_indeks_na_kazakhskom.Text:=ADQ.fieldbyname('indeks_na_kazakhskom').AsString;
  Edit_kol_podgrupp_po_praktike.Text:=ADQ.fieldbyname('kol_podgrupp_po_praktike').AsString;
  Edit_itogo_chasov_za_vse_semestry.Text:=ADQ.fieldbyname('itogo_chasov_za_vse_semestry').AsString;
  Edit_semestry.Text:=ADQ.fieldbyname('semestry').AsString;

end;

if pActionDISTSIPLINA='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_distsiplina_na_russkom.Visible:=true;
  Check_distsiplina_na_kazakhskom.Visible:=true;
  Check_indeks_na_russkom.Visible:=true;
  Check_indeks_na_kazakhskom.Visible:=true;
  Check_kol_podgrupp_po_praktike.Visible:=true;
  Check_itogo_chasov_za_vse_semestry.Visible:=true;
  Check_semestry.Visible:=true;

end;

end;

procedure TfDISTSIPLINA_RECORDf.BtnOK;
begin

if pActionDISTSIPLINA='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'distsiplina '+
  '('+
  'tip_distsipliny_id,'+
  'distsiplina_na_russkom,'+
  'distsiplina_na_kazakhskom,'+
  'indeks_na_russkom,'+
  'indeks_na_kazakhskom,'+
  'kol_podgrupp_po_praktike,'+
  'itogo_chasov_za_vse_semestry,'+
  'semestry,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+ptip_distsipliny_ID+''', '+
  ' '''+trim(Edit_distsiplina_na_russkom.Text)+''', '+
  ' '''+trim(Edit_distsiplina_na_kazakhskom.Text)+''', '+
  ' '''+trim(Edit_indeks_na_russkom.Text)+''', '+
  ' '''+trim(Edit_indeks_na_kazakhskom.Text)+''', '+
  ' '''+trim(Edit_kol_podgrupp_po_praktike.Text)+''', '+
  ' '''+trim(Edit_itogo_chasov_za_vse_semestry.Text)+''', '+
  ' '''+trim(Edit_semestry.Text)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

//  Clipboard.AsText:=pS;
//  ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionDISTSIPLINA='UPDATE' then begin

 CheckInput;

 pS:='UPDATE '+
  'distsiplina set ' +
  'distsiplina_na_russkom='''+trim(Edit_distsiplina_na_russkom.Text)+''','+
  'distsiplina_na_kazakhskom='''+trim(Edit_distsiplina_na_kazakhskom.Text)+''','+
  'indeks_na_russkom='''+trim(Edit_indeks_na_russkom.Text)+''','+
  'indeks_na_kazakhskom='''+trim(Edit_indeks_na_kazakhskom.Text)+''','+
  'kol_podgrupp_po_praktike='''+trim(Edit_kol_podgrupp_po_praktike.Text)+''','+
  'itogo_chasov_za_vse_semestry='''+trim(Edit_itogo_chasov_za_vse_semestry.Text)+''','+
  'semestry='''+trim(Edit_semestry.Text)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fDISTSIPLINAf.ListView1.Selected.SubItems[0]:=trim(Edit_distsiplina_na_russkom.Text);
   fDISTSIPLINAf.ListView1.Selected.SubItems[1]:=trim(Edit_distsiplina_na_kazakhskom.Text);
   fDISTSIPLINAf.ListView1.Selected.SubItems[2]:=trim(Edit_indeks_na_russkom.Text);
   fDISTSIPLINAf.ListView1.Selected.SubItems[3]:=trim(Edit_indeks_na_kazakhskom.Text);
   fDISTSIPLINAf.ListView1.Selected.SubItems[4]:=trim(Edit_kol_podgrupp_po_praktike.Text);
   fDISTSIPLINAf.ListView1.Selected.SubItems[5]:=trim(Edit_itogo_chasov_za_vse_semestry.Text);
   fDISTSIPLINAf.ListView1.Selected.SubItems[6]:=trim(Edit_semestry.Text);

end;

if pActionDISTSIPLINA='SEARCH' then begin

if (Check_distsiplina_na_russkom.Checked=false) and (Check_distsiplina_na_kazakhskom.Checked=false) and (Check_indeks_na_russkom.Checked=false) and (Check_indeks_na_kazakhskom.Checked=false) and (Check_kol_podgrupp_po_praktike.Checked=false) and (Check_itogo_chasov_za_vse_semestry.Checked=false) and (Check_semestry.Checked=false) then abort;

   pSearch:='';

   if Check_distsiplina_na_russkom.Checked=true then pSearch:=pSearch+' and distsiplina.distsiplina_na_russkom like '''+trim(Edit_distsiplina_na_russkom.Text)+''' ';
   if Check_distsiplina_na_kazakhskom.Checked=true then pSearch:=pSearch+' and distsiplina.distsiplina_na_kazakhskom like '''+trim(Edit_distsiplina_na_kazakhskom.Text)+''' ';
   if Check_indeks_na_russkom.Checked=true then pSearch:=pSearch+' and distsiplina.indeks_na_russkom like '''+trim(Edit_indeks_na_russkom.Text)+''' ';
   if Check_indeks_na_kazakhskom.Checked=true then pSearch:=pSearch+' and distsiplina.indeks_na_kazakhskom like '''+trim(Edit_indeks_na_kazakhskom.Text)+''' ';
   if Check_kol_podgrupp_po_praktike.Checked=true then pSearch:=pSearch+' and distsiplina.kol_podgrupp_po_praktike like '''+trim(Edit_kol_podgrupp_po_praktike.Text)+''' ';
   if Check_itogo_chasov_za_vse_semestry.Checked=true then pSearch:=pSearch+' and distsiplina.itogo_chasov_za_vse_semestry like '''+trim(Edit_itogo_chasov_za_vse_semestry.Text)+''' ';
   if Check_semestry.Checked=true then pSearch:=pSearch+' and distsiplina.semestry like '''+trim(Edit_semestry.Text)+''' ';

end;

Close;

end;

procedure TfDISTSIPLINA_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfDISTSIPLINA_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfDISTSIPLINA_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfDISTSIPLINA_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.