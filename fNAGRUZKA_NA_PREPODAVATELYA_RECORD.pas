unit fNAGRUZKA_NA_PREPODAVATELYA_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    Label_DISTSIPLINA: TLabel;
    Label_gruppa_id: TLabel;
    Label_podgruppa_id: TLabel;
    Label_vid_nagruzki_id: TLabel;
    Label_teoriya_chasov_itogo: TLabel;
    Label_praktika_chasov_itogo: TLabel;
    Combo_distsiplina_id: TComboBox;
    Button_DISTSIPLINA: TButton;
    Combo_gruppa_id: TComboBox;
    Button_gruppa_id: TButton;
    Combo_podgruppa_id: TComboBox;
    Button_podgruppa_id: TButton;
    Combo_vid_nagruzki_id: TComboBox;
    Button_vid_nagruzki_id: TButton;
    Edit_teoriya_chasov_itogo: TEdit;
    Edit_praktika_chasov_itogo: TEdit;
    Check_DISTSIPLINA: TCheckBox;
    Check_gruppa_id: TCheckBox;
    Check_podgruppa_id: TCheckBox;
    Check_vid_nagruzki_id: TCheckBox;
    Check_teoriya_chasov_itogo: TCheckBox;
    Check_praktika_chasov_itogo: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;

    procedure GetGOD(pCombo:TComboBox;pSID:integer);


    procedure GetPOLUGODIE(pCombo:TComboBox;pSID:integer);

    procedure LoadDISTSIPLINA;
    procedure GetDISTSIPLINA(pCombo:TComboBox;pSID:integer);
    procedure Button_DISTSIPLINAClick(Sender: TObject);

    procedure LoadGRUPPA;
    procedure GetGRUPPA(pCombo:TComboBox;pSID:integer);
    procedure Button_gruppa_idClick(Sender: TObject);

    procedure Loadpodgruppa;
    procedure Getpodgruppa(pCombo:TComboBox;pSID:integer);
    procedure Button_podgruppa_idClick(Sender: TObject);

    procedure Loadvid_nagruzki;
    procedure Getvid_nagruzki(pCombo:TComboBox;pSID:integer);
    procedure Button_vid_nagruzki_idClick(Sender: TObject);


    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);

    procedure Edit_teoriya_chasov_itogoKeyPress(Sender: TObject;var Key: Char);
    procedure Edit_praktika_chasov_itogoKeyPress(Sender: TObject;var Key: Char);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fNAGRUZKA_NA_PREPODAVATELYA_RECORDf: TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf;

implementation

uses  
    fNAGRUZKA_NA_PREPODAVATELYA,
    fGOD,
    fPOLUGODIE,
    fDISTSIPLINA,
    fGRUPPA,
    fpodgruppa,
    fvid_nagruzki,
    fMW;

{$R *.dfm}

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.Edit_teoriya_chasov_itogoKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'.. '9',#8]) then key:= #0;
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.Edit_praktika_chasov_itogoKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'.. '9',#8]) then key:= #0;
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.GetGOD(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.GetPOLUGODIE(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.GetDISTSIPLINA(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.LoadDISTSIPLINA;
begin

Combo_distsiplina_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from distsiplina where deleted=0 order by distsiplina_na_russkom');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_distsiplina_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_distsiplina_id.ItemIndex:=0;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.Button_DISTSIPLINAClick(Sender: TObject);
begin

  fDISTSIPLINAf.Caption:='����� ����������';
  if Combo_distsiplina_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_distsiplina_id.Items.Objects[Combo_distsiplina_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fDISTSIPLINAf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='NAGRUZKA_NA_PREPODAVATELYA';

  if pNoAction=false then begin
    LoadDISTSIPLINA;
    if pSelectedID<>'' then GetDISTSIPLINA(Combo_distsiplina_id,strtoint(pSelectedID));
  end;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.GetGRUPPA(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.LoadGRUPPA;
begin

Combo_gruppa_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from gruppa where deleted=0 order by nazvanie_gruppy');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_gruppa_id.AddItem(ADQ.FieldByName('nazvanie_gruppy').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_gruppa_id.ItemIndex:=0;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.Button_gruppa_idClick(Sender: TObject);
begin

  fGRUPPAf.Caption:='����� ������';
  if Combo_gruppa_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_gruppa_id.Items.Objects[Combo_gruppa_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fGRUPPAf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='NAGRUZKA_NA_PREPODAVATELYA';

  if pNoAction=false then begin
    LoadGRUPPA;
    if pSelectedID<>'' then GetGRUPPA(Combo_gruppa_id,strtoint(pSelectedID));
  end;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.Getpodgruppa(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.Loadpodgruppa;
begin

Combo_podgruppa_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from podgruppa where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_podgruppa_id.AddItem(ADQ.FieldByName('podgruppa').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_podgruppa_id.ItemIndex:=0;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.Button_podgruppa_idClick(Sender: TObject);
begin

  fpodgruppaf.Caption:='����� ���������';
  if Combo_podgruppa_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_podgruppa_id.Items.Objects[Combo_podgruppa_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fpodgruppaf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='NAGRUZKA_NA_PREPODAVATELYA';

  if pNoAction=false then begin
    Loadpodgruppa;
    if pSelectedID<>'' then Getpodgruppa(Combo_podgruppa_id,strtoint(pSelectedID));
  end;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.Getvid_nagruzki(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.Loadvid_nagruzki;
begin

Combo_vid_nagruzki_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from vid_nagruzki where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_vid_nagruzki_id.AddItem(ADQ.FieldByName('vid_nagruzki').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_vid_nagruzki_id.ItemIndex:=0;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.Button_vid_nagruzki_idClick(Sender: TObject);
begin

  fvid_nagruzkif.Caption:='����� ��� ��������';
  if Combo_vid_nagruzki_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_vid_nagruzki_id.Items.Objects[Combo_vid_nagruzki_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fVID_NAGRUZKIf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='NAGRUZKA_NA_PREPODAVATELYA';

  if pNoAction=false then begin
    LoadVID_NAGRUZKI;
    if pSelectedID<>'' then Getvid_nagruzki(Combo_vid_nagruzki_id,strtoint(pSelectedID));
  end;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.CheckInput;
begin

if trim(Combo_distsiplina_id.Text)='' then begin
  Combo_distsiplina_id.SetFocus;
  abort;
end;

if trim(Combo_gruppa_id.Text)='' then begin
  Combo_gruppa_id.SetFocus;
  abort;
end;

if trim(Combo_podgruppa_id.Text)='' then begin
  Combo_podgruppa_id.SetFocus;
  abort;
end;

if trim(Combo_vid_nagruzki_id.Text)='' then begin
  Combo_vid_nagruzki_id.SetFocus;
  abort;
end;

if trim(Edit_teoriya_chasov_itogo.Text)='' then begin
  Edit_teoriya_chasov_itogo.SetFocus;
  abort;
end;

if trim(Edit_praktika_chasov_itogo.Text)='' then begin
  Edit_praktika_chasov_itogo.SetFocus;
  abort;
end;

end;
procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.CLALL;
begin

  LoadDISTSIPLINA;
  LoadGRUPPA;
  Loadpodgruppa;
  Loadvid_nagruzki;

  Check_DISTSIPLINA.Checked:=false;
  Check_DISTSIPLINA.Visible:=false;
  Check_gruppa_id.Checked:=false;
  Check_gruppa_id.Visible:=false;
  Check_podgruppa_id.Checked:=false;
  Check_podgruppa_id.Visible:=false;
  Check_vid_nagruzki_id.Checked:=false;
  Check_vid_nagruzki_id.Visible:=false;
  Check_teoriya_chasov_itogo.Checked:=false;
  Check_teoriya_chasov_itogo.Visible:=false;
  Check_praktika_chasov_itogo.Checked:=false;
  Check_praktika_chasov_itogo.Visible:=false;

  Edit_teoriya_chasov_itogo.Clear;
  Edit_praktika_chasov_itogo.Clear;

  Combo_distsiplina_id.Setfocus;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.FormActivated;
begin

  CLALL;

if pActionNAGRUZKA_NA_PREPODAVATELYA='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionNAGRUZKA_NA_PREPODAVATELYA='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from nagruzka_na_prepodavatelya where id='+pID);
  ADQ.Open;
  ADQ.First;

  GetDISTSIPLINA(Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
  GetGRUPPA(Combo_gruppa_id,ADQ.fieldbyname('gruppa_id').AsInteger);
  Getpodgruppa(Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
  Getvid_nagruzki(Combo_vid_nagruzki_id,ADQ.fieldbyname('vid_nagruzki_id').AsInteger);
  Edit_teoriya_chasov_itogo.Text:=ADQ.fieldbyname('teoriya_chasov_itogo').AsString;
  Edit_praktika_chasov_itogo.Text:=ADQ.fieldbyname('praktika_chasov_itogo').AsString;

end;

if pActionNAGRUZKA_NA_PREPODAVATELYA='SEARCH' then begin

  Self.Caption:='����� ������';
  Check_DISTSIPLINA.Visible:=true;
  Check_gruppa_id.Visible:=true;
  Check_podgruppa_id.Visible:=true;
  Check_vid_nagruzki_id.Visible:=true;
  Check_teoriya_chasov_itogo.Visible:=true;
  Check_praktika_chasov_itogo.Visible:=true;

end;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.BtnOK;
begin

if pActionNAGRUZKA_NA_PREPODAVATELYA='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'nagruzka_na_prepodavatelya '+
  '('+
  'prepodavatelmz_id,'+
  'god_id,'+
  'polugodie_id,'+
  'distsiplina_id,'+
  'gruppa_id,'+
  'podgruppa_id,'+
  'vid_nagruzki_id,'+
  'teoriya_chasov_itogo,'+
  'praktika_chasov_itogo,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+pprepodavatelmz_ID+''', '+
  ' '''+pCurrent_God_id+''', '+
  ' '''+pCurrent_Polugodie_id+''', '+
  ' '''+Inttostr(Integer(Combo_distsiplina_id.Items.Objects[Combo_distsiplina_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_gruppa_id.Items.Objects[Combo_gruppa_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_podgruppa_id.Items.Objects[Combo_podgruppa_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_vid_nagruzki_id.Items.Objects[Combo_vid_nagruzki_id.ItemIndex]))+''', '+
  ' '''+trim(Edit_teoriya_chasov_itogo.Text)+''', '+
  ' '''+trim(Edit_praktika_chasov_itogo.Text)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionNAGRUZKA_NA_PREPODAVATELYA='UPDATE' then begin

 CheckInput;

 pS:='UPDATE '+
  'nagruzka_na_prepodavatelya set ' +
  'god_id='''+pCurrent_God_id+''','+
  'polugodie_id='''+pCurrent_Polugodie_id+''','+
  'distsiplina_id='''+Inttostr(Integer(Combo_distsiplina_id.Items.Objects[Combo_distsiplina_id.ItemIndex]))+''','+
  'gruppa_id='''+Inttostr(Integer(Combo_gruppa_id.Items.Objects[Combo_gruppa_id.ItemIndex]))+''','+
  'podgruppa_id='''+Inttostr(Integer(Combo_podgruppa_id.Items.Objects[Combo_podgruppa_id.ItemIndex]))+''','+
  'vid_nagruzki_id='''+Inttostr(Integer(Combo_vid_nagruzki_id.Items.Objects[Combo_vid_nagruzki_id.ItemIndex]))+''','+
  'teoriya_chasov_itogo='''+trim(Edit_teoriya_chasov_itogo.Text)+''','+
  'praktika_chasov_itogo='''+trim(Edit_praktika_chasov_itogo.Text)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;


   fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[2]:=trim(Combo_distsiplina_id.Text);
   fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[3]:=trim(Combo_gruppa_id.Text);
   fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[4]:=trim(Combo_podgruppa_id.Text);
   fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[5]:=trim(Combo_vid_nagruzki_id.Text);
   fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[6]:=trim(Edit_teoriya_chasov_itogo.Text);
   fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[7]:=trim(Edit_praktika_chasov_itogo.Text);

end;

if pActionNAGRUZKA_NA_PREPODAVATELYA='SEARCH' then begin

if (Check_DISTSIPLINA.Checked=false) and (Check_gruppa_id.Checked=false) and (Check_podgruppa_id.Checked=false) and (Check_vid_nagruzki_id.Checked=false) and (Check_teoriya_chasov_itogo.Checked=false) and (Check_praktika_chasov_itogo.Checked=false) then abort;

   pSearch:='';

   if Check_DISTSIPLINA.Checked=true then pSearch:=pSearch+' and nagruzka_na_prepodavatelya.distsiplina = '+Inttostr(Integer(Combo_distsiplina_id.Items.Objects[Combo_distsiplina_id.ItemIndex]))+' ';
   if Check_gruppa_id.Checked=true then pSearch:=pSearch+' and nagruzka_na_prepodavatelya.gruppa_id = '+Inttostr(Integer(Combo_gruppa_id.Items.Objects[Combo_gruppa_id.ItemIndex]))+' ';
   if Check_podgruppa_id.Checked=true then pSearch:=pSearch+' and nagruzka_na_prepodavatelya.podgruppa_id = '+Inttostr(Integer(Combo_podgruppa_id.Items.Objects[Combo_podgruppa_id.ItemIndex]))+' ';
   if Check_vid_nagruzki_id.Checked=true then pSearch:=pSearch+' and nagruzka_na_prepodavatelya.vid_nagruzki_id = '+Inttostr(Integer(Combo_vid_nagruzki_id.Items.Objects[Combo_vid_nagruzki_id.ItemIndex]))+' ';
   if Check_teoriya_chasov_itogo.Checked=true then pSearch:=pSearch+' and nagruzka_na_prepodavatelya.teoriya_chasov_itogo like '''+trim(Edit_teoriya_chasov_itogo.Text)+''' ';
   if Check_praktika_chasov_itogo.Checked=true then pSearch:=pSearch+' and nagruzka_na_prepodavatelya.praktika_chasov_itogo like '''+trim(Edit_praktika_chasov_itogo.Text)+''' ';

end;

Close;

end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.