unit fSVOBOD_CHASY;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfSVOBOD_CHASYf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    CheckBox9: TCheckBox;
    CheckBox10: TCheckBox;
    CheckBox11: TCheckBox;
    CheckBox12: TCheckBox;
    CheckBox13: TCheckBox;
    CheckBox14: TCheckBox;
    CheckBox15: TCheckBox;
    CheckBox16: TCheckBox;
    CheckBox17: TCheckBox;
    CheckBox18: TCheckBox;
    CheckBox19: TCheckBox;
    CheckBox20: TCheckBox;
    CheckBox21: TCheckBox;
    CheckBox22: TCheckBox;
    CheckBox23: TCheckBox;
    CheckBox24: TCheckBox;
    CheckBox25: TCheckBox;
    CheckBox26: TCheckBox;
    CheckBox27: TCheckBox;
    CheckBox28: TCheckBox;
    CheckBox29: TCheckBox;
    CheckBox30: TCheckBox;
    CheckBox31: TCheckBox;
    CheckBox32: TCheckBox;
    CheckBox33: TCheckBox;
    CheckBox34: TCheckBox;
    CheckBox35: TCheckBox;
    CheckBox36: TCheckBox;
    CheckBox37: TCheckBox;
    CheckBox38: TCheckBox;
    CheckBox39: TCheckBox;
    CheckBox40: TCheckBox;
    CheckBox41: TCheckBox;
    CheckBox42: TCheckBox;
    procedure Button1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure SetPara;
    procedure CheckBox2Click(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure CheckBox4Click(Sender: TObject);
    procedure CheckBox5Click(Sender: TObject);
    procedure CheckBox6Click(Sender: TObject);
    procedure CheckBox7Click(Sender: TObject);
    procedure CheckBox8Click(Sender: TObject);
    procedure CheckBox9Click(Sender: TObject);
    procedure CheckBox10Click(Sender: TObject);
    procedure CheckBox11Click(Sender: TObject);
    procedure CheckBox12Click(Sender: TObject);
    procedure CheckBox13Click(Sender: TObject);
    procedure CheckBox14Click(Sender: TObject);
    procedure CheckBox15Click(Sender: TObject);
    procedure CheckBox16Click(Sender: TObject);
    procedure CheckBox17Click(Sender: TObject);
    procedure CheckBox18Click(Sender: TObject);
    procedure CheckBox19Click(Sender: TObject);
    procedure CheckBox20Click(Sender: TObject);
    procedure CheckBox21Click(Sender: TObject);
    procedure CheckBox22Click(Sender: TObject);
    procedure CheckBox23Click(Sender: TObject);
    procedure CheckBox24Click(Sender: TObject);
    procedure CheckBox25Click(Sender: TObject);
    procedure CheckBox26Click(Sender: TObject);
    procedure CheckBox27Click(Sender: TObject);
    procedure CheckBox28Click(Sender: TObject);
    procedure CheckBox29Click(Sender: TObject);
    procedure CheckBox30Click(Sender: TObject);
    procedure CheckBox31Click(Sender: TObject);
    procedure CheckBox32Click(Sender: TObject);
    procedure CheckBox33Click(Sender: TObject);
    procedure CheckBox34Click(Sender: TObject);
    procedure CheckBox35Click(Sender: TObject);
    procedure CheckBox36Click(Sender: TObject);
    procedure CheckBox37Click(Sender: TObject);
    procedure CheckBox38Click(Sender: TObject);
    procedure CheckBox39Click(Sender: TObject);
    procedure CheckBox40Click(Sender: TObject);
    procedure CheckBox41Click(Sender: TObject);
    procedure CheckBox42Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fSVOBOD_CHASYf: TfSVOBOD_CHASYf;
  pzDayz_of_week,pzPara_id,pzSvoboden:string;
  pStopIt:integer;

implementation

uses fMW,fPREPODAVATELMZ;

{$R *.dfm}



procedure TfSVOBOD_CHASYf.SetPara;
begin

if pStopIt=0 then exit;

pS:='Update svobod_chasy set svoboden='+pzSvoboden+' where dayz_of_week='+pzDayz_of_week+' and para_id='+pzPara_id+' and prepodavatelmz_id='+pCurrent_Combo_prepodavatelmz_id_x;

//ShowMessage(pS);

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.ExecSQL;

end;

procedure TfSVOBOD_CHASYf.Button1Click(Sender: TObject);
begin
Close
end;

procedure TfSVOBOD_CHASYf.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=vk_escape then close;
end;

procedure TfSVOBOD_CHASYf.FormActivate(Sender: TObject);
var
i:integer;
begin

pStopIt:=0;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from svobod_chasy where prepodavatelmz_id='+pCurrent_Combo_prepodavatelmz_id_x+' order by dayz_of_week,para_id');
ADQ.Open;

ADQ.Last;
ADQ.First;

i:=1;

while not ADQ.Eof do begin

if ADQ.Fieldbyname('svoboden').AsInteger=1 then
(FindComponent( Format( 'CheckBox%d', [i] ) ) as TCheckBox ).Checked := true else
(FindComponent( Format( 'CheckBox%d', [i] ) ) as TCheckBox ).Checked := false;
ADQ.Next;
inc(i);
end;

pStopIt:=1; 

end;

procedure TfSVOBOD_CHASYf.CheckBox1Click(Sender: TObject);
begin
pzDayz_of_week:='1';
pzPara_id:='1';
if CheckBox1.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox2Click(Sender: TObject);
begin
pzDayz_of_week:='1';
pzPara_id:='2';
if CheckBox2.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox3Click(Sender: TObject);
begin
pzDayz_of_week:='1';
pzPara_id:='3';
if CheckBox3.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox4Click(Sender: TObject);
begin
pzDayz_of_week:='1';
pzPara_id:='4';
if CheckBox4.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox5Click(Sender: TObject);
begin
pzDayz_of_week:='1';
pzPara_id:='5';
if CheckBox5.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox6Click(Sender: TObject);
begin
pzDayz_of_week:='1';
pzPara_id:='6';
if CheckBox6.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox7Click(Sender: TObject);
begin
pzDayz_of_week:='1';
pzPara_id:='7';
if CheckBox7.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox8Click(Sender: TObject);
begin
pzDayz_of_week:='2';
pzPara_id:='1';
if CheckBox8.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox9Click(Sender: TObject);
begin
pzDayz_of_week:='2';
pzPara_id:='2';
if CheckBox9.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox10Click(Sender: TObject);
begin
pzDayz_of_week:='2';
pzPara_id:='3';
if CheckBox10.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox11Click(Sender: TObject);
begin
pzDayz_of_week:='2';
pzPara_id:='4';
if CheckBox11.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox12Click(Sender: TObject);
begin
pzDayz_of_week:='2';
pzPara_id:='5';
if CheckBox12.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox13Click(Sender: TObject);
begin
pzDayz_of_week:='2';
pzPara_id:='6';
if CheckBox13.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox14Click(Sender: TObject);
begin
pzDayz_of_week:='2';
pzPara_id:='7';
if CheckBox14.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox15Click(Sender: TObject);
begin
pzDayz_of_week:='3';
pzPara_id:='1';
if CheckBox15.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox16Click(Sender: TObject);
begin
pzDayz_of_week:='3';
pzPara_id:='2';
if CheckBox16.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox17Click(Sender: TObject);
begin
pzDayz_of_week:='3';
pzPara_id:='3';
if CheckBox17.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox18Click(Sender: TObject);
begin
pzDayz_of_week:='3';
pzPara_id:='4';
if CheckBox18.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox19Click(Sender: TObject);
begin
pzDayz_of_week:='3';
pzPara_id:='5';
if CheckBox19.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox20Click(Sender: TObject);
begin
pzDayz_of_week:='3';
pzPara_id:='6';
if CheckBox20.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox21Click(Sender: TObject);
begin
pzDayz_of_week:='3';
pzPara_id:='7';
if CheckBox21.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox22Click(Sender: TObject);
begin
pzDayz_of_week:='4';
pzPara_id:='1';
if CheckBox22.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox23Click(Sender: TObject);
begin
pzDayz_of_week:='4';
pzPara_id:='2';
if CheckBox23.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox24Click(Sender: TObject);
begin
pzDayz_of_week:='4';
pzPara_id:='3';
if CheckBox24.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox25Click(Sender: TObject);
begin
pzDayz_of_week:='4';
pzPara_id:='4';
if CheckBox25.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox26Click(Sender: TObject);
begin
pzDayz_of_week:='4';
pzPara_id:='5';
if CheckBox26.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox27Click(Sender: TObject);
begin
pzDayz_of_week:='4';
pzPara_id:='6';
if CheckBox27.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox28Click(Sender: TObject);
begin
pzDayz_of_week:='4';
pzPara_id:='7';
if CheckBox28.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox29Click(Sender: TObject);
begin
pzDayz_of_week:='5';
pzPara_id:='1';
if CheckBox29.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox30Click(Sender: TObject);
begin
pzDayz_of_week:='5';
pzPara_id:='2';
if CheckBox30.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox31Click(Sender: TObject);
begin
pzDayz_of_week:='5';
pzPara_id:='3';
if CheckBox31.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox32Click(Sender: TObject);
begin
pzDayz_of_week:='5';
pzPara_id:='4';
if CheckBox32.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox33Click(Sender: TObject);
begin
pzDayz_of_week:='5';
pzPara_id:='5';
if CheckBox33.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox34Click(Sender: TObject);
begin
pzDayz_of_week:='5';
pzPara_id:='6';
if CheckBox34.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox35Click(Sender: TObject);
begin
pzDayz_of_week:='5';
pzPara_id:='7';
if CheckBox35.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox36Click(Sender: TObject);
begin
pzDayz_of_week:='6';
pzPara_id:='1';
if CheckBox36.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox37Click(Sender: TObject);
begin
pzDayz_of_week:='6';
pzPara_id:='2';
if CheckBox37.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox38Click(Sender: TObject);
begin
pzDayz_of_week:='6';
pzPara_id:='3';
if CheckBox38.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox39Click(Sender: TObject);
begin
pzDayz_of_week:='6';
pzPara_id:='4';
if CheckBox39.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox40Click(Sender: TObject);
begin
pzDayz_of_week:='6';
pzPara_id:='5';
if CheckBox40.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox41Click(Sender: TObject);
begin
pzDayz_of_week:='6';
pzPara_id:='6';
if CheckBox41.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

procedure TfSVOBOD_CHASYf.CheckBox42Click(Sender: TObject);
begin
pzDayz_of_week:='6';
pzPara_id:='7';
if CheckBox42.Checked=true then pzSvoboden:='1' else pzSvoboden:='0';
SetPara;
end;

end.
