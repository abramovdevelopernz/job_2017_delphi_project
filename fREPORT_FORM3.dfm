object fREPORT_FORM3f: TfREPORT_FORM3f
  Left = 519
  Top = 358
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1055#1077#1095#1072#1090#1100' '#1060#1086#1088#1084#1072' 3'
  ClientHeight = 113
  ClientWidth = 570
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 61
    Width = 570
    Height = 52
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 12
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 12
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = 'OK'
      TabOrder = 0
      OnClick = Button2Click
    end
    object ProgressBar1: TProgressBar
      Left = 168
      Top = 16
      Width = 393
      Height = 16
      TabOrder = 2
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 570
    Height = 61
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    TabOrder = 1
    object Label2: TLabel
      Left = 55
      Top = 8
      Width = 79
      Height = 13
      Caption = #1055#1088#1077#1087#1086#1076#1072#1074#1072#1090#1077#1083#1100
    end
    object Label3: TLabel
      Left = 89
      Top = 32
      Width = 45
      Height = 13
      Caption = #1055#1088#1077#1076#1084#1077#1090
    end
    object Combo_prepodavatelmz_id: TComboBox
      Left = 145
      Top = 8
      Width = 416
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 0
      OnChange = Combo_prepodavatelmz_idChange
    end
    object Combo_predmet: TComboBox
      Left = 144
      Top = 33
      Width = 417
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 1
      Items.Strings = (
        #1041#1070#1044#1046#1045#1058
        #1044#1054#1043#1054#1042#1054#1056)
    end
  end
end
