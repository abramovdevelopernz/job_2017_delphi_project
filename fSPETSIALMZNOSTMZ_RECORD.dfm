object fSPETSIALMZNOSTMZ_RECORDf: TfSPETSIALMZNOSTMZ_RECORDf
  Left = 552
  Top = 215
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  ClientHeight = 170
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 122
    Width = 472
    Height = 48
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = 'OK'
      TabOrder = 0
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 472
    Height = 122
    Align = alClient
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label_spetsialmznostmz_na_russkom: TLabel
      Left = 32
      Top = 16
      Width = 139
      Height = 13
      Caption = #1057#1087#1077#1094#1080#1072#1083#1100#1085#1086#1089#1090#1100' '#1085#1072' '#1088#1091#1089#1089#1082#1086#1084
    end
    object Label_abbreviatura_na_russkom: TLabel
      Left = 39
      Top = 40
      Width = 132
      Height = 13
      Caption = #1040#1073#1073#1088#1077#1074#1080#1072#1090#1091#1088#1072' '#1085#1072' '#1088#1091#1089#1089#1082#1086#1084
    end
    object Label_spetsialmznostmz_na_kazakhskom: TLabel
      Left = 20
      Top = 64
      Width = 151
      Height = 13
      Caption = #1057#1087#1077#1094#1080#1072#1083#1100#1085#1086#1089#1090#1100' '#1085#1072' '#1082#1072#1079#1072#1093#1089#1082#1086#1084
    end
    object Label_abbreviatura_na_kazakhskom: TLabel
      Left = 27
      Top = 88
      Width = 144
      Height = 13
      Caption = #1040#1073#1073#1088#1077#1074#1080#1072#1090#1091#1088#1072' '#1085#1072' '#1082#1072#1079#1072#1093#1089#1082#1086#1084
    end
    object Check_spetsialmznostmz_na_russkom: TCheckBox
      Left = 448
      Top = 16
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 0
    end
    object Check_abbreviatura_na_russkom: TCheckBox
      Left = 448
      Top = 40
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 1
    end
    object Check_spetsialmznostmz_na_kazakhskom: TCheckBox
      Left = 448
      Top = 64
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 2
    end
    object Check_abbreviatura_na_kazakhskom: TCheckBox
      Left = 448
      Top = 88
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 4
    end
    object Edit_spetsialmznostmz_na_russkom: TEdit
      Left = 176
      Top = 16
      Width = 265
      Height = 25
      Ctl3D = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      Text = 'Edit_spetsialmznostmz_na_russkom'
    end
    object Edit_abbreviatura_na_russkom: TEdit
      Left = 176
      Top = 40
      Width = 265
      Height = 25
      Ctl3D = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 5
      Text = 'Edit_abbreviatura_na_russkom'
    end
    object Edit_spetsialmznostmz_na_kazakhskom: TEdit
      Left = 176
      Top = 64
      Width = 265
      Height = 25
      Ctl3D = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 6
      Text = 'Edit_spetsialmznostmz_na_kazakhskom'
    end
    object Edit_abbreviatura_na_kazakhskom: TEdit
      Left = 176
      Top = 88
      Width = 265
      Height = 25
      Ctl3D = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 7
      Text = 'Edit_abbreviatura_na_kazakhskom'
    end
  end
end
