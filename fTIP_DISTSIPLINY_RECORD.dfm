object fTIP_DISTSIPLINY_RECORDf: TfTIP_DISTSIPLINY_RECORDf
  Left = 552
  Top = 215
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  ClientHeight = 120
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 72
    Width = 472
    Height = 48
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = 'OK'
      TabOrder = 0
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 472
    Height = 72
    Align = alClient
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label_tip_na_russkom: TLabel
      Left = 56
      Top = 16
      Width = 80
      Height = 13
      Caption = #1058#1080#1087' '#1085#1072' '#1088#1091#1089#1089#1082#1086#1084
    end
    object Label_tip_na_kazakhskom: TLabel
      Left = 44
      Top = 40
      Width = 92
      Height = 13
      Caption = #1058#1080#1087' '#1085#1072' '#1082#1072#1079#1072#1093#1089#1082#1086#1084
    end
    object Check_tip_na_russkom: TCheckBox
      Left = 448
      Top = 16
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 0
    end
    object Check_tip_na_kazakhskom: TCheckBox
      Left = 448
      Top = 40
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 2
    end
    object Edit_tip_na_russkom: TEdit
      Left = 144
      Top = 16
      Width = 297
      Height = 25
      Ctl3D = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      Text = 'Edit_tip_na_russkom'
    end
    object Edit_tip_na_kazakhskom: TEdit
      Left = 144
      Top = 40
      Width = 297
      Height = 25
      Ctl3D = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      Text = 'Edit_tip_na_kazakhskom'
    end
  end
end
