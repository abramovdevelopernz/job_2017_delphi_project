object fPREPODAVATELMZ_RECORDf: TfPREPODAVATELMZ_RECORDf
  Left = 552
  Top = 215
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  ClientHeight = 195
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 147
    Width = 472
    Height = 48
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = 'OK'
      TabOrder = 0
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 472
    Height = 147
    Align = alClient
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label_fio: TLabel
      Left = 112
      Top = 16
      Width = 23
      Height = 13
      Caption = #1060#1080#1086
    end
    object Label_mesto_raboty: TLabel
      Left = 63
      Top = 40
      Width = 72
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1088#1072#1073#1086#1090#1099
    end
    object Label_shtatnyy: TLabel
      Left = 90
      Top = 64
      Width = 45
      Height = 13
      Caption = #1064#1090#1072#1090#1085#1099#1081
    end
    object Label_vozmozhnostmz_raboty_id: TLabel
      Left = 25
      Top = 88
      Width = 110
      Height = 13
      Caption = #1042#1086#1079#1084#1086#1078#1085#1086#1089#1090#1100' '#1088#1072#1073#1086#1090#1099
    end
    object Label_para_id: TLabel
      Left = 109
      Top = 112
      Width = 26
      Height = 13
      Caption = #1055#1072#1088#1072
    end
    object Check_fio: TCheckBox
      Left = 448
      Top = 16
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 0
    end
    object Check_mesto_raboty: TCheckBox
      Left = 448
      Top = 40
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 1
    end
    object Check_shtatnyy: TCheckBox
      Left = 448
      Top = 64
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 2
    end
    object Check_vozmozhnostmz_raboty_id: TCheckBox
      Left = 448
      Top = 88
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 3
    end
    object Check_para_id: TCheckBox
      Left = 448
      Top = 112
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 5
    end
    object Edit_fio: TEdit
      Left = 144
      Top = 16
      Width = 297
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 4
      Text = 'Edit_fio'
    end
    object Edit_mesto_raboty: TEdit
      Left = 144
      Top = 40
      Width = 297
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 6
      Text = 'Edit_mesto_raboty'
    end
    object CheckBox_shtatnyy: TCheckBox
      Left = 144
      Top = 64
      Width = 17
      Height = 17
      Cursor = crHandPoint
      TabOrder = 7
    end
    object Combo_vozmozhnostmz_raboty_id: TComboBox
      Left = 144
      Top = 88
      Width = 265
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 8
    end
    object Button_vozmozhnostmz_raboty_id: TButton
      Left = 416
      Top = 88
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 9
      OnClick = Button_vozmozhnostmz_raboty_idClick
    end
    object Combo_para_id: TComboBox
      Left = 144
      Top = 112
      Width = 265
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 10
    end
    object Button_para_id: TButton
      Left = 416
      Top = 112
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 11
      OnClick = Button_para_idClick
    end
  end
end
