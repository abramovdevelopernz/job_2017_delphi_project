unit fPERENOS_CHASOV;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,clipbrd;

type
  TfPERENOS_CHASOVf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Combo_prepodavatelmz: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Button1: TButton;
    Button2: TButton;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    procedure LoadPREPODAVATELMZ;
    procedure FormActivate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fPERENOS_CHASOVf: TfPERENOS_CHASOVf;

implementation
uses fMW, fNAGRUZKA_NA_PREPODAVATELYA;
{$R *.dfm}

procedure TfPERENOS_CHASOVf.LoadPREPODAVATELMZ;
begin

Combo_prepodavatelmz.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from prepodavatelmz where deleted=0 and id<>'+pPREPODAVATELMZ_ID+' order by fio');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_prepodavatelmz.AddItem(ADQ.FieldByName('fio').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then begin
  Combo_prepodavatelmz.ItemIndex:=0;
end;

end;

procedure TfPERENOS_CHASOVf.FormActivate(Sender: TObject);
begin
LoadPREPODAVATELMZ;
Label6.Caption:='����������: '+fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[2];
Label4.Caption:='��� ������: '+fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[3];
Label5.Caption:='��� ���������: '+fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[4];
Label2.Caption:='������ ����� �����: '+fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[6];
Label3.Caption:='�������� ����� �����: '+fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[7];

Label8.Caption:='�������� ������ ����� �����: '+fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[8];
Label9.Caption:='�������� �������� ����� �����: '+fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[9];

Label10.Caption:='������ �����: '+inttostr(strtoint(fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[6])-strtoint(fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[8]));
Label11.Caption:='�������� �����: '+inttostr(strtoint(fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[7])-strtoint(fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[9]));

end;

procedure TfPERENOS_CHASOVf.Button2Click(Sender: TObject);
begin
Close;
end;

procedure TfPERENOS_CHASOVf.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=vk_escape then close;
end;

procedure TfPERENOS_CHASOVf.Button1Click(Sender: TObject);
begin

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from nagruzka_na_prepodavatelya where id='+fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.Caption);
  ADQ.Open;

  pS:='INSERT into '+
  'nagruzka_na_prepodavatelya '+
  '('+
  'prepodavatelmz_id,'+
  'god_id,'+
  'polugodie_id,'+
  'distsiplina_id,'+
  'gruppa_id,'+
  'podgruppa_id,'+
  'vid_nagruzki_id,'+
  'teoriya_chasov_itogo,'+
  'praktika_chasov_itogo,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+Inttostr(Integer(Combo_prepodavatelmz.Items.Objects[Combo_prepodavatelmz.ItemIndex]))+''', '+
  ' '''+ADQ.fieldbyname('god_id').asstring+''', '+
  ' '''+ADQ.fieldbyname('polugodie_id').asstring+''', '+
  ' '''+ADQ.fieldbyname('distsiplina_id').asstring+''', '+
  ' '''+ADQ.fieldbyname('gruppa_id').asstring+''', '+
  ' '''+ADQ.fieldbyname('podgruppa_id').asstring+''', '+
  ' '''+ADQ.fieldbyname('vid_nagruzki_id').asstring+''', '+
  ' '''+inttostr(strtoint(fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[6])-strtoint(fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[8]))+''', '+
  ' '''+inttostr(strtoint(fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[7])-strtoint(fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[9]))+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

//  Clipboard.AsText:=pS;
  ShowMessage(pS);

  ADQ1.Close;
  ADQ1.SQL.Clear;
  ADQ1.SQL.Add(pS);
  ADQ1.ExecSQL;



  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(
  'Update nagruzka_na_prepodavatelya set '+
  ' teoriya_chasov_itogo='+fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[8]+
  ',praktika_chasov_itogo='+fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[9]+
  ' where id='+fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.Caption);
  ADQ.ExecSQL;

  fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[6]:=fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[8];
  fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[7]:=fNAGRUZKA_NA_PREPODAVATELYAf.ListView1.Selected.SubItems[9];

Close;

end;

end.
