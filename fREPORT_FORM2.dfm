object fREPORT_FORM2f: TfREPORT_FORM2f
  Left = 474
  Top = 216
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1055#1077#1095#1072#1090#1100' '#1060#1086#1088#1084#1072' 2'
  ClientHeight = 148
  ClientWidth = 367
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 367
    Height = 96
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    TabOrder = 0
    object Label1: TLabel
      Left = 104
      Top = 16
      Width = 33
      Height = 13
      Caption = #1052#1077#1089#1103#1094
    end
    object Label2: TLabel
      Left = 58
      Top = 40
      Width = 79
      Height = 13
      Caption = #1055#1088#1077#1087#1086#1076#1072#1074#1072#1090#1077#1083#1100
    end
    object Label3: TLabel
      Left = 8
      Top = 64
      Width = 129
      Height = 13
      Caption = #1060#1080#1085#1072#1085#1089#1080#1088#1086#1074#1072#1085#1080#1077' '#1079#1072' '#1089#1095#1077#1090
    end
    object Combo_month: TComboBox
      Left = 144
      Top = 16
      Width = 209
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        #1071#1085#1074#1072#1088#1100
        #1060#1077#1074#1088#1072#1083#1100
        #1052#1072#1088#1090
        #1040#1087#1088#1077#1083#1100
        #1052#1072#1081
        #1048#1102#1085#1100
        #1048#1102#1083#1100
        #1040#1074#1075#1091#1089#1090
        #1057#1077#1085#1090#1103#1073#1088#1100
        #1054#1082#1090#1103#1073#1088#1100
        #1053#1086#1103#1073#1088#1100
        #1044#1077#1082#1072#1073#1088#1100)
    end
    object Combo_prepodavatelmz_id: TComboBox
      Left = 145
      Top = 40
      Width = 208
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 1
    end
    object Combo_za: TComboBox
      Left = 144
      Top = 65
      Width = 209
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 2
      Items.Strings = (
        #1041#1070#1044#1046#1045#1058
        #1044#1054#1043#1054#1042#1054#1056)
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 96
    Width = 367
    Height = 52
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Button1: TButton
      Left = 8
      Top = 12
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 12
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = 'OK'
      TabOrder = 0
      OnClick = Button2Click
    end
    object ProgressBar1: TProgressBar
      Left = 168
      Top = 24
      Width = 193
      Height = 16
      TabOrder = 2
    end
  end
end
