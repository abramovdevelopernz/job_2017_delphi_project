unit fRASPISANIE_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfRASPISANIE_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_para_id: TLabel;
    Label_DISTSIPLINA: TLabel;
    Label_prepodavatelmz_id: TLabel;
    Label_kabinet_id: TLabel;
    Label_tip_zanyatiya_id: TLabel;
    Label_data: TLabel;

    Combo_para_id: TComboBox;
    Button_para_id: TButton;
    Combo_DISTSIPLINA: TComboBox;
    Button_DISTSIPLINA: TButton;
    Combo_prepodavatelmz_id: TComboBox;
    Button_prepodavatelmz_id: TButton;
    Combo_kabinet_id: TComboBox;
    Button_kabinet_id: TButton;
    Combo_tip_zanyatiya_id: TComboBox;
    Button_tip_zanyatiya_id: TButton;
    DateTime_data: TDateTimePicker;

    Check_para_id: TCheckBox;
    Check_DISTSIPLINA: TCheckBox;
    Check_prepodavatelmz_id: TCheckBox;
    Check_kabinet_id: TCheckBox;
    Check_tip_zanyatiya_id: TCheckBox;
    Check_data: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;
    procedure LoadPARA;
    procedure GetPARA(pCombo:TComboBox;pSID:integer);
    procedure Button_para_idClick(Sender: TObject);

    procedure LoadDISTSIPLINA;
    procedure GetDISTSIPLINA(pCombo:TComboBox;pSID:integer);
    procedure Button_DISTSIPLINAClick(Sender: TObject);

    procedure LoadPREPODAVATELMZ;
    procedure GetPREPODAVATELMZ(pCombo:TComboBox;pSID:integer);
    procedure Button_prepodavatelmz_idClick(Sender: TObject);

    procedure LoadKABINET;
    procedure GetKABINET(pCombo:TComboBox;pSID:integer);
    procedure Button_kabinet_idClick(Sender: TObject);

    procedure LoadTIP_ZANYATIYA;
    procedure GetTIP_ZANYATIYA(pCombo:TComboBox;pSID:integer);
    procedure Button_tip_zanyatiya_idClick(Sender: TObject);


    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fRASPISANIE_RECORDf: TfRASPISANIE_RECORDf;

implementation

uses  
    fRASPISANIE,
    fPARA,
    fDISTSIPLINA,
    fPREPODAVATELMZ,
    fKABINET,
    fTIP_ZANYATIYA,
    fMW;

{$R *.dfm}

procedure TfRASPISANIE_RECORDf.GetPARA(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfRASPISANIE_RECORDf.LoadPARA;
begin

Combo_para_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from para where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_para_id.AddItem(ADQ.FieldByName('para').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_para_id.ItemIndex:=0;

end;

procedure TfRASPISANIE_RECORDf.Button_para_idClick(Sender: TObject);
begin

  fPARAf.Caption:='����� ����';
  if Combo_para_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_para_id.Items.Objects[Combo_para_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fPARAf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='RASPISANIE';

  if pNoAction=false then begin
    LoadPARA;
    if pSelectedID<>'' then GetPARA(Combo_para_id,strtoint(pSelectedID));
  end;

end;

procedure TfRASPISANIE_RECORDf.GetDISTSIPLINA(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfRASPISANIE_RECORDf.LoadDISTSIPLINA;
begin

Combo_distsiplina.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from distsiplina where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_DISTSIPLINA.AddItem(ADQ.FieldByName('distsiplina').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_DISTSIPLINA.ItemIndex:=0;

end;

procedure TfRASPISANIE_RECORDf.Button_DISTSIPLINAClick(Sender: TObject);
begin

  fDISTSIPLINAf.Caption:='����� ����������';
  if Combo_DISTSIPLINA.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_DISTSIPLINA.Items.Objects[Combo_DISTSIPLINA.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fDISTSIPLINAf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='RASPISANIE';

  if pNoAction=false then begin
    LoadDISTSIPLINA;
    if pSelectedID<>'' then GetDISTSIPLINA(Combo_DISTSIPLINA,strtoint(pSelectedID));
  end;

end;

procedure TfRASPISANIE_RECORDf.GetPREPODAVATELMZ(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfRASPISANIE_RECORDf.LoadPREPODAVATELMZ;
begin

Combo_prepodavatelmz_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from prepodavatelmz where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_prepodavatelmz_id.AddItem(ADQ.FieldByName('fio').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_prepodavatelmz_id.ItemIndex:=0;

end;

procedure TfRASPISANIE_RECORDf.Button_prepodavatelmz_idClick(Sender: TObject);
begin

  fPREPODAVATELMZf.Caption:='����� �������������';
  if Combo_prepodavatelmz_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fPREPODAVATELMZf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='RASPISANIE';

  if pNoAction=false then begin
    LoadPREPODAVATELMZ;
    if pSelectedID<>'' then GetPREPODAVATELMZ(Combo_prepodavatelmz_id,strtoint(pSelectedID));
  end;

end;

procedure TfRASPISANIE_RECORDf.GetKABINET(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfRASPISANIE_RECORDf.LoadKABINET;
begin

Combo_kabinet_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from kabinet where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_kabinet_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_kabinet_id.ItemIndex:=0;

end;

procedure TfRASPISANIE_RECORDf.Button_kabinet_idClick(Sender: TObject);
begin

  fKABINETf.Caption:='����� �������';
  if Combo_kabinet_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_kabinet_id.Items.Objects[Combo_kabinet_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fKABINETf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='RASPISANIE';

  if pNoAction=false then begin
    LoadKABINET;
    if pSelectedID<>'' then GetKABINET(Combo_kabinet_id,strtoint(pSelectedID));
  end;

end;

procedure TfRASPISANIE_RECORDf.GetTIP_ZANYATIYA(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfRASPISANIE_RECORDf.LoadTIP_ZANYATIYA;
begin

Combo_tip_zanyatiya_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from tip_zanyatiya where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_tip_zanyatiya_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_tip_zanyatiya_id.ItemIndex:=0;

end;

procedure TfRASPISANIE_RECORDf.Button_tip_zanyatiya_idClick(Sender: TObject);
begin

  fTIP_ZANYATIYAf.Caption:='����� ��� �������';
  if Combo_tip_zanyatiya_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_tip_zanyatiya_id.Items.Objects[Combo_tip_zanyatiya_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fTIP_ZANYATIYAf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='RASPISANIE';

  if pNoAction=false then begin
    LoadTIP_ZANYATIYA;
    if pSelectedID<>'' then GetTIP_ZANYATIYA(Combo_tip_zanyatiya_id,strtoint(pSelectedID));
  end;

end;

procedure TfRASPISANIE_RECORDf.CheckInput;
begin

if trim(Combo_para_id.Text)='' then begin
  Combo_para_id.SetFocus;
  abort;
end;

if trim(Combo_DISTSIPLINA.Text)='' then begin
  Combo_DISTSIPLINA.SetFocus;
  abort;
end;

if trim(Combo_prepodavatelmz_id.Text)='' then begin
  Combo_prepodavatelmz_id.SetFocus;
  abort;
end;

if trim(Combo_kabinet_id.Text)='' then begin
  Combo_kabinet_id.SetFocus;
  abort;
end;

if trim(Combo_tip_zanyatiya_id.Text)='' then begin
  Combo_tip_zanyatiya_id.SetFocus;
  abort;
end;

end;
procedure TfRASPISANIE_RECORDf.CLALL;
begin

  LoadPARA;
  LoadDISTSIPLINA;
  LoadPREPODAVATELMZ;
  LoadKABINET;
  LoadTIP_ZANYATIYA;

  Check_para_id.Checked:=false;
  Check_para_id.Visible:=false;
  Check_DISTSIPLINA.Checked:=false;
  Check_DISTSIPLINA.Visible:=false;
  Check_prepodavatelmz_id.Checked:=false;
  Check_prepodavatelmz_id.Visible:=false;
  Check_kabinet_id.Checked:=false;
  Check_kabinet_id.Visible:=false;
  Check_tip_zanyatiya_id.Checked:=false;
  Check_tip_zanyatiya_id.Visible:=false;
  Check_data.Checked:=false;
  Check_data.Visible:=false;

  DateTime_data.Date:=now;

  Combo_para_id.Setfocus;

end;

procedure TfRASPISANIE_RECORDf.FormActivated;
begin

  CLALL;

if pActionRASPISANIE='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionRASPISANIE='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from raspisanie where id='+pID);
  ADQ.Open;
  ADQ.First;

  GetPARA(Combo_para_id,ADQ.fieldbyname('para_id').AsInteger);
  GetDISTSIPLINA(Combo_DISTSIPLINA,ADQ.fieldbyname('distsiplina').AsInteger);
  GetPREPODAVATELMZ(Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
  GetKABINET(Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
  GetTIP_ZANYATIYA(Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
  DateTime_data.Date:=ADQ.fieldbyname('data').AsDateTime;

end;

if pActionRASPISANIE='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_para_id.Visible:=true;
  Check_DISTSIPLINA.Visible:=true;
  Check_prepodavatelmz_id.Visible:=true;
  Check_kabinet_id.Visible:=true;
  Check_tip_zanyatiya_id.Visible:=true;
  Check_data.Visible:=true;

end;

end;

procedure TfRASPISANIE_RECORDf.BtnOK;
begin

if pActionRASPISANIE='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'raspisanie '+
  '('+
  'gruppa_id,'+
  'para_id,'+
  'distsiplina,'+
  'prepodavatelmz_id,'+
  'kabinet_id,'+
  'tip_zanyatiya_id,'+
  'data,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+pgruppa_ID+''', '+
  ' '''+Inttostr(Integer(Combo_para_id.Items.Objects[Combo_para_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_DISTSIPLINA.Items.Objects[Combo_DISTSIPLINA.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_kabinet_id.Items.Objects[Combo_kabinet_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_tip_zanyatiya_id.Items.Objects[Combo_tip_zanyatiya_id.ItemIndex]))+''', '+
  ' '''+datetostr(DateTime_data.Date)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionRASPISANIE='UPDATE' then begin


 pS:='UPDATE '+
  'raspisanie set ' +
  'para_id='''+Inttostr(Integer(Combo_para_id.Items.Objects[Combo_para_id.ItemIndex]))+''','+
  'distsiplina='''+Inttostr(Integer(Combo_DISTSIPLINA.Items.Objects[Combo_DISTSIPLINA.ItemIndex]))+''','+
  'prepodavatelmz_id='''+Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+''','+
  'kabinet_id='''+Inttostr(Integer(Combo_kabinet_id.Items.Objects[Combo_kabinet_id.ItemIndex]))+''','+
  'tip_zanyatiya_id='''+Inttostr(Integer(Combo_tip_zanyatiya_id.Items.Objects[Combo_tip_zanyatiya_id.ItemIndex]))+''','+
  'data='''+datetostr(DateTime_data.date)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fRASPISANIEf.ListView1.Selected.SubItems[0]:=trim(Combo_para_id.Text);
   fRASPISANIEf.ListView1.Selected.SubItems[1]:=trim(Combo_DISTSIPLINA.Text);
   fRASPISANIEf.ListView1.Selected.SubItems[2]:=trim(Combo_prepodavatelmz_id.Text);
   fRASPISANIEf.ListView1.Selected.SubItems[3]:=trim(Combo_kabinet_id.Text);
   fRASPISANIEf.ListView1.Selected.SubItems[4]:=trim(Combo_tip_zanyatiya_id.Text);
   fRASPISANIEf.ListView1.Selected.SubItems[5]:=datetostr(DateTime_data.date);

end;

if pActionRASPISANIE='SEARCH' then begin

if (Check_para_id.Checked=false) and (Check_DISTSIPLINA.Checked=false) and (Check_prepodavatelmz_id.Checked=false) and (Check_kabinet_id.Checked=false) and (Check_tip_zanyatiya_id.Checked=false) and (Check_data.Checked=false) then abort;

   pSearch:='';

   if Check_para_id.Checked=true then pSearch:=pSearch+' and raspisanie.para_id = '+Inttostr(Integer(Combo_para_id.Items.Objects[Combo_para_id.ItemIndex]))+' ';
   if Check_DISTSIPLINA.Checked=true then pSearch:=pSearch+' and raspisanie.distsiplina = '+Inttostr(Integer(Combo_DISTSIPLINA.Items.Objects[Combo_DISTSIPLINA.ItemIndex]))+' ';
   if Check_prepodavatelmz_id.Checked=true then pSearch:=pSearch+' and raspisanie.prepodavatelmz_id = '+Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+' ';
   if Check_kabinet_id.Checked=true then pSearch:=pSearch+' and raspisanie.kabinet_id = '+Inttostr(Integer(Combo_kabinet_id.Items.Objects[Combo_kabinet_id.ItemIndex]))+' ';
   if Check_tip_zanyatiya_id.Checked=true then pSearch:=pSearch+' and raspisanie.tip_zanyatiya_id = '+Inttostr(Integer(Combo_tip_zanyatiya_id.Items.Objects[Combo_tip_zanyatiya_id.ItemIndex]))+' ';
   if Check_data.Checked=true then pSearch:=pSearch+' and raspisanie.data = #'+FormatDateTime('YYYY-MM-DD',DateTime_data.Date)+'#';

end;

Close;

end;

procedure TfRASPISANIE_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfRASPISANIE_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfRASPISANIE_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfRASPISANIE_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.