unit fVOZMOZHNOSTMZ_RABOTY_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfVOZMOZHNOSTMZ_RABOTY_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_vozmozhnostmz_raboty: TLabel;

    Edit_vozmozhnostmz_raboty: TEdit;

    Check_vozmozhnostmz_raboty: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fVOZMOZHNOSTMZ_RABOTY_RECORDf: TfVOZMOZHNOSTMZ_RABOTY_RECORDf;

implementation

uses  
    fVOZMOZHNOSTMZ_RABOTY,
    fMW;

{$R *.dfm}

procedure TfVOZMOZHNOSTMZ_RABOTY_RECORDf.CheckInput;
begin

if trim(Edit_vozmozhnostmz_raboty.Text)='' then begin
  Edit_vozmozhnostmz_raboty.SetFocus;
  abort;
end;

end;
procedure TfVOZMOZHNOSTMZ_RABOTY_RECORDf.CLALL;
begin


  Check_vozmozhnostmz_raboty.Checked:=false;
  Check_vozmozhnostmz_raboty.Visible:=false;

  Edit_vozmozhnostmz_raboty.Clear;

  Edit_vozmozhnostmz_raboty.Setfocus;

end;

procedure TfVOZMOZHNOSTMZ_RABOTY_RECORDf.FormActivated;
begin

  CLALL;

if pActionVOZMOZHNOSTMZ_RABOTY='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionVOZMOZHNOSTMZ_RABOTY='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from vozmozhnostmz_raboty where id='+pID);
  ADQ.Open;
  ADQ.First;

  Edit_vozmozhnostmz_raboty.Text:=ADQ.fieldbyname('vozmozhnostmz_raboty').AsString;

end;

if pActionVOZMOZHNOSTMZ_RABOTY='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_vozmozhnostmz_raboty.Visible:=true;

end;

end;

procedure TfVOZMOZHNOSTMZ_RABOTY_RECORDf.BtnOK;
begin

if pActionVOZMOZHNOSTMZ_RABOTY='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'vozmozhnostmz_raboty '+
  '('+
  'vozmozhnostmz_raboty,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+trim(Edit_vozmozhnostmz_raboty.Text)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionVOZMOZHNOSTMZ_RABOTY='UPDATE' then begin

 CheckInput;

 pS:='UPDATE '+
  'vozmozhnostmz_raboty set ' +
  'vozmozhnostmz_raboty='''+trim(Edit_vozmozhnostmz_raboty.Text)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fVOZMOZHNOSTMZ_RABOTYf.ListView1.Selected.SubItems[0]:=trim(Edit_vozmozhnostmz_raboty.Text);

end;

if pActionVOZMOZHNOSTMZ_RABOTY='SEARCH' then begin

if (Check_vozmozhnostmz_raboty.Checked=false) then abort;

   pSearch:='';

   if Check_vozmozhnostmz_raboty.Checked=true then pSearch:=pSearch+' and vozmozhnostmz_raboty.vozmozhnostmz_raboty like '''+trim(Edit_vozmozhnostmz_raboty.Text)+''' ';

end;

Close;

end;

procedure TfVOZMOZHNOSTMZ_RABOTY_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfVOZMOZHNOSTMZ_RABOTY_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfVOZMOZHNOSTMZ_RABOTY_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfVOZMOZHNOSTMZ_RABOTY_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.