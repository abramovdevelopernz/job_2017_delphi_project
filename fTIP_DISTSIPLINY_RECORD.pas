unit fTIP_DISTSIPLINY_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfTIP_DISTSIPLINY_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_tip_na_russkom: TLabel;
    Label_tip_na_kazakhskom: TLabel;

    Edit_tip_na_russkom: TEdit;
    Edit_tip_na_kazakhskom: TEdit;

    Check_tip_na_russkom: TCheckBox;
    Check_tip_na_kazakhskom: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fTIP_DISTSIPLINY_RECORDf: TfTIP_DISTSIPLINY_RECORDf;

implementation

uses  
    fTIP_DISTSIPLINY,
    fMW;

{$R *.dfm}

procedure TfTIP_DISTSIPLINY_RECORDf.CheckInput;
begin

if trim(Edit_tip_na_russkom.Text)='' then begin
  Edit_tip_na_russkom.SetFocus;
  abort;
end;

if trim(Edit_tip_na_kazakhskom.Text)='' then begin
  Edit_tip_na_kazakhskom.SetFocus;
  abort;
end;

end;
procedure TfTIP_DISTSIPLINY_RECORDf.CLALL;
begin


  Check_tip_na_russkom.Checked:=false;
  Check_tip_na_russkom.Visible:=false;
  Check_tip_na_kazakhskom.Checked:=false;
  Check_tip_na_kazakhskom.Visible:=false;

  Edit_tip_na_russkom.Clear;
  Edit_tip_na_kazakhskom.Clear;

  Edit_tip_na_russkom.Setfocus;

end;

procedure TfTIP_DISTSIPLINY_RECORDf.FormActivated;
begin

  CLALL;

if pActionTIP_DISTSIPLINY='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionTIP_DISTSIPLINY='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from tip_distsipliny where id='+pID);
  ADQ.Open;
  ADQ.First;

  Edit_tip_na_russkom.Text:=ADQ.fieldbyname('tip_na_russkom').AsString;
  Edit_tip_na_kazakhskom.Text:=ADQ.fieldbyname('tip_na_kazakhskom').AsString;

end;

if pActionTIP_DISTSIPLINY='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_tip_na_russkom.Visible:=true;
  Check_tip_na_kazakhskom.Visible:=true;

end;

end;

procedure TfTIP_DISTSIPLINY_RECORDf.BtnOK;
begin

if pActionTIP_DISTSIPLINY='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'tip_distsipliny '+
  '('+
  'tip_na_russkom,'+
  'tip_na_kazakhskom,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+trim(Edit_tip_na_russkom.Text)+''', '+
  ' '''+trim(Edit_tip_na_kazakhskom.Text)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionTIP_DISTSIPLINY='UPDATE' then begin

 CheckInput;

 pS:='UPDATE '+
  'tip_distsipliny set ' +
  'tip_na_russkom='''+trim(Edit_tip_na_russkom.Text)+''','+
  'tip_na_kazakhskom='''+trim(Edit_tip_na_kazakhskom.Text)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fTIP_DISTSIPLINYf.ListView1.Selected.SubItems[0]:=trim(Edit_tip_na_russkom.Text);
   fTIP_DISTSIPLINYf.ListView1.Selected.SubItems[1]:=trim(Edit_tip_na_kazakhskom.Text);

end;

if pActionTIP_DISTSIPLINY='SEARCH' then begin

if (Check_tip_na_russkom.Checked=false) and (Check_tip_na_kazakhskom.Checked=false) then abort;

   pSearch:='';

   if Check_tip_na_russkom.Checked=true then pSearch:=pSearch+' and tip_distsipliny.tip_na_russkom like '''+trim(Edit_tip_na_russkom.Text)+''' ';
   if Check_tip_na_kazakhskom.Checked=true then pSearch:=pSearch+' and tip_distsipliny.tip_na_kazakhskom like '''+trim(Edit_tip_na_kazakhskom.Text)+''' ';

end;

Close;

end;

procedure TfTIP_DISTSIPLINY_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfTIP_DISTSIPLINY_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfTIP_DISTSIPLINY_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfTIP_DISTSIPLINY_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.