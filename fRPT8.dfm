object fRPT8f: TfRPT8f
  Left = 361
  Top = 49
  Width = 928
  Height = 480
  Caption = #1054#1090#1095#1077#1090' '#1076#1083#1103' '#1088#1077#1074#1080#1079#1080#1086#1085#1085#1086#1081' '#1082#1086#1084#1080#1089#1089#1080#1080' '#1087#1086' '#1074#1099#1096#1077#1076#1096#1080#1084' '#1087#1086' '#1089#1088#1086#1082#1072#1084' '#1088#1072#1079#1088#1077#1096#1077#1085#1080#1103#1084
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 434
    Width = 920
    Height = 19
    Color = clWindow
    Panels = <
      item
        Width = 300
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 920
    Height = 33
    Align = alTop
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 448
      Top = 8
      Width = 347
      Height = 13
      Caption = #1059#1082#1072#1078#1080#1090#1077' '#1074' '#1082#1072#1082#1086#1084' '#1080#1085#1090#1077#1088#1074#1072#1083#1077' '#1088#1072#1079#1088#1077#1096#1077#1085#1080#1077' '#1076#1086#1083#1078#1085#1086' '#1073#1099#1083#1086' '#1079#1072#1082#1086#1085#1095#1080#1090#1100#1089#1103
      Visible = False
    end
    object Button1: TButton
      Left = 8
      Top = 4
      Width = 75
      Height = 25
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = Button1Click
    end
    object DateTimePicker1: TDateTimePicker
      Left = 448
      Top = 32
      Width = 81
      Height = 21
      Date = 42804.395688807870000000
      Time = 42804.395688807870000000
      TabOrder = 1
      Visible = False
    end
    object DateTimePicker2: TDateTimePicker
      Left = 536
      Top = 32
      Width = 81
      Height = 21
      Date = 42804.395837037040000000
      Time = 42804.395837037040000000
      TabOrder = 2
      Visible = False
    end
    object Button2: TButton
      Left = 88
      Top = 4
      Width = 75
      Height = 25
      Caption = #1055#1086#1080#1089#1082
      TabOrder = 3
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 168
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Excel'
      TabOrder = 4
      OnClick = Button3Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 33
    Width = 920
    Height = 401
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 2
    object ListView1: TListView
      Left = 1
      Top = 1
      Width = 918
      Height = 399
      Align = alClient
      Columns = <
        item
          Caption = #1047#1072#1103#1074#1082#1072'_ID'
          Width = 0
        end
        item
          Caption = #1048#1048#1053'/'#1041#1048#1053
        end
        item
          Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1088#1077#1076#1087#1088#1080#1103#1090#1080#1103
        end
        item
          Caption = #1057#1077#1088#1080#1103
        end
        item
          Caption = #1053#1086#1084#1077#1088' '#1088#1072#1079#1088#1077#1096#1077#1085#1080#1103
        end
        item
          Caption = #1044#1072#1090#1072' '#1088#1072#1079#1088#1077#1096#1077#1085#1080#1103
        end
        item
          Caption = #1057#1088#1086#1082' '#1086#1090
        end
        item
          Caption = #1057#1088#1086#1082' '#1076#1086
        end
        item
          Caption = #1050#1083#1072#1089#1089' '#1086#1087#1072#1089#1085#1086#1089#1090#1080
        end
        item
          Caption = #1056#1072#1081#1086#1085
        end
        item
          Caption = #1053#1086#1084#1077#1088' - '#1087#1086#1074#1090#1086#1088#1085#1086' '#1087#1086#1083#1091#1095#1080#1074#1096#1080#1077' '#1088#1072#1079#1088#1077#1096#1077#1085#1080#1077
        end
        item
          Caption = #1044#1072#1090#1072' - '#1087#1086#1074#1090#1086#1088#1085#1086' '#1087#1086#1083#1091#1095#1080#1074#1096#1080#1077' '#1088#1072#1079#1088#1077#1096#1077#1085#1080#1077
        end
        item
          Caption = #1057#1088#1086#1082' '#1076#1077#1081#1089#1090#1074#1080#1103' '#1086#1090' - '#1087#1086#1074#1090#1086#1088#1085#1086' '#1087#1086#1083#1091#1095#1080#1074#1096#1080#1077' '#1088#1072#1079#1088#1077#1096#1077#1085#1080#1077
        end
        item
          Caption = #1057#1088#1086#1082' '#1076#1077#1081#1089#1090#1074#1080#1103' '#1076#1086' - '#1087#1086#1074#1090#1086#1088#1085#1086' '#1087#1086#1083#1091#1095#1080#1074#1096#1080#1077' '#1088#1072#1079#1088#1077#1096#1077#1085#1080#1077
        end>
      FlatScrollBars = True
      GridLines = True
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      ViewStyle = vsReport
      OnColumnClick = ListView1ColumnClick
    end
  end
end
