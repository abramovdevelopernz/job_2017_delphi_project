unit fKABINET_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfKABINET_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_nomer_kabineta: TLabel;
    Label_kolichestvo_mest: TLabel;

    Edit_nomer_kabineta: TEdit;
    Edit_kolichestvo_mest: TEdit;

    Check_nomer_kabineta: TCheckBox;
    Check_kolichestvo_mest: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);

    procedure Edit_kolichestvo_mestKeyPress(Sender: TObject;var Key: Char);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fKABINET_RECORDf: TfKABINET_RECORDf;

implementation

uses  
    fKABINET,
    fMW;

{$R *.dfm}

procedure TfKABINET_RECORDf.Edit_kolichestvo_mestKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'.. '9',#8]) then key:= #0;
end;

procedure TfKABINET_RECORDf.CheckInput;
begin

if trim(Edit_nomer_kabineta.Text)='' then begin
  Edit_nomer_kabineta.SetFocus;
  abort;
end;

if trim(Edit_kolichestvo_mest.Text)='' then begin
  Edit_kolichestvo_mest.SetFocus;
  abort;
end;

end;
procedure TfKABINET_RECORDf.CLALL;
begin


  Check_nomer_kabineta.Checked:=false;
  Check_nomer_kabineta.Visible:=false;
  Check_kolichestvo_mest.Checked:=false;
  Check_kolichestvo_mest.Visible:=false;

  Edit_nomer_kabineta.Clear;
  Edit_kolichestvo_mest.Clear;

  Edit_nomer_kabineta.Setfocus;

end;

procedure TfKABINET_RECORDf.FormActivated;
begin

  CLALL;

if pActionKABINET='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionKABINET='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from kabinet where id='+pID);
  ADQ.Open;
  ADQ.First;

  Edit_nomer_kabineta.Text:=ADQ.fieldbyname('nomer_kabineta').AsString;
  Edit_kolichestvo_mest.Text:=ADQ.fieldbyname('kolichestvo_mest').AsString;

end;

if pActionKABINET='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_nomer_kabineta.Visible:=true;
  Check_kolichestvo_mest.Visible:=true;

end;

end;

procedure TfKABINET_RECORDf.BtnOK;
begin

if pActionKABINET='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'kabinet '+
  '('+
  'nomer_kabineta,'+
  'kolichestvo_mest,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+trim(Edit_nomer_kabineta.Text)+''', '+
  ' '''+trim(Edit_kolichestvo_mest.Text)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionKABINET='UPDATE' then begin

 CheckInput;

 pS:='UPDATE '+
  'kabinet set ' +
  'nomer_kabineta='''+trim(Edit_nomer_kabineta.Text)+''','+
  'kolichestvo_mest='''+trim(Edit_kolichestvo_mest.Text)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fKABINETf.ListView1.Selected.SubItems[0]:=trim(Edit_nomer_kabineta.Text);
   fKABINETf.ListView1.Selected.SubItems[1]:=trim(Edit_kolichestvo_mest.Text);

end;

if pActionKABINET='SEARCH' then begin

if (Check_nomer_kabineta.Checked=false) and (Check_kolichestvo_mest.Checked=false) then abort;

   pSearch:='';

   if Check_nomer_kabineta.Checked=true then pSearch:=pSearch+' and kabinet.nomer_kabineta like '''+trim(Edit_nomer_kabineta.Text)+''' ';
   if Check_kolichestvo_mest.Checked=true then pSearch:=pSearch+' and kabinet.kolichestvo_mest like '''+trim(Edit_kolichestvo_mest.Text)+''' ';

end;

Close;

end;

procedure TfKABINET_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfKABINET_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfKABINET_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfKABINET_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.