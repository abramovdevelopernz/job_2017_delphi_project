unit fVID_NAGRUZKI_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  Tfvid_nagruzki_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_Vid_nagruzki: TLabel;

    Edit_Vid_nagruzki: TEdit;

    Check_Vid_nagruzki: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;


    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fvid_nagruzki_RECORDf: Tfvid_nagruzki_RECORDf;

implementation

uses  
    fvid_nagruzki,
    fMW;

{$R *.dfm}


procedure Tfvid_nagruzki_RECORDf.CLALL;
begin


  Check_Vid_nagruzki.Checked:=false;
  Check_Vid_nagruzki.Visible:=false;

  Edit_Vid_nagruzki.Clear;

  Edit_Vid_nagruzki.Setfocus;

end;

procedure Tfvid_nagruzki_RECORDf.FormActivated;
begin

  CLALL;

if pActionVID_NAGRUZKI='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionVID_NAGRUZKI='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from vid_nagruzki where id='+pID);
  ADQ.Open;
  ADQ.First;

  Edit_Vid_nagruzki.Text:=ADQ.fieldbyname('vid_nagruzki').AsString;

end;

if pActionVID_NAGRUZKI='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_Vid_nagruzki.Visible:=true;

end;

end;

procedure Tfvid_nagruzki_RECORDf.BtnOK;
begin

if pActionVID_NAGRUZKI='INSERT' then begin

  pS:='INSERT into '+
  'vid_nagruzki '+
  '('+
  'vid_nagruzki,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+trim(Edit_Vid_nagruzki.Text)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionVID_NAGRUZKI='UPDATE' then begin


 pS:='UPDATE '+
  'vid_nagruzki set ' +
  'vid_nagruzki='''+trim(Edit_Vid_nagruzki.Text)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fvid_nagruzkif.ListView1.Selected.SubItems[0]:=trim(Edit_Vid_nagruzki.Text);

end;

if pActionVID_NAGRUZKI='SEARCH' then begin

if (Check_Vid_nagruzki.Checked=false) then abort;

   pSearch:='';

   if Check_Vid_nagruzki.Checked=true then pSearch:=pSearch+' and vid_nagruzki.vid_nagruzki like '''+trim(Edit_Vid_nagruzki.Text)+''' ';

end;

Close;

end;

procedure Tfvid_nagruzki_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure Tfvid_nagruzki_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure Tfvid_nagruzki_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure Tfvid_nagruzki_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.