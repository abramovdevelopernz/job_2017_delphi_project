unit fPODGRUPPA_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfPODGRUPPA_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_podgruppa: TLabel;

    Edit_podgruppa: TEdit;

    Check_podgruppa: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fPODGRUPPA_RECORDf: TfPODGRUPPA_RECORDf;

implementation

uses  
    fPODGRUPPA,
    fMW;

{$R *.dfm}

procedure TfPODGRUPPA_RECORDf.CheckInput;
begin

if trim(Edit_podgruppa.Text)='' then begin
  Edit_podgruppa.SetFocus;
  abort;
end;

end;
procedure TfPODGRUPPA_RECORDf.CLALL;
begin


  Check_podgruppa.Checked:=false;
  Check_podgruppa.Visible:=false;

  Edit_podgruppa.Clear;

  Edit_podgruppa.Setfocus;

end;

procedure TfPODGRUPPA_RECORDf.FormActivated;
begin

  CLALL;

if pActionPODGRUPPA='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionPODGRUPPA='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from podgruppa where id='+pID);
  ADQ.Open;
  ADQ.First;

  Edit_podgruppa.Text:=ADQ.fieldbyname('podgruppa').AsString;

end;

if pActionPODGRUPPA='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_podgruppa.Visible:=true;

end;

end;

procedure TfPODGRUPPA_RECORDf.BtnOK;
begin

if pActionPODGRUPPA='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'podgruppa '+
  '('+
  'podgruppa,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+trim(Edit_podgruppa.Text)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionPODGRUPPA='UPDATE' then begin

 CheckInput;

 pS:='UPDATE '+
  'podgruppa set ' +
  'podgruppa='''+trim(Edit_podgruppa.Text)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fPODGRUPPAf.ListView1.Selected.SubItems[0]:=trim(Edit_podgruppa.Text);

end;

if pActionPODGRUPPA='SEARCH' then begin

if (Check_podgruppa.Checked=false) then abort;

   pSearch:='';

   if Check_podgruppa.Checked=true then pSearch:=pSearch+' and podgruppa.podgruppa like '''+trim(Edit_podgruppa.Text)+''' ';

end;

Close;

end;

procedure TfPODGRUPPA_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfPODGRUPPA_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfPODGRUPPA_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfPODGRUPPA_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.