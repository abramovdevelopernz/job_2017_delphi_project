unit fGRUPPA_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfGRUPPA_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_spetsialmznostmz_id: TLabel;
    Label_god_nachala_obucheniya: TLabel;
    Label_nazvanie_gruppy: TLabel;
    Label_vecherniy: TLabel;
    Label_yazyk_obucheniya_id: TLabel;
    Label_kolichestvo_studentov: TLabel;

    Combo_spetsialmznostmz_id: TComboBox;
    Button_spetsialmznostmz_id: TButton;
    Edit_god_nachala_obucheniya: TEdit;
    Edit_nazvanie_gruppy: TEdit;
    CheckBox_vecherniy: TCheckBox;
    Combo_yazyk_obucheniya_id: TComboBox;
    Button_yazyk_obucheniya_id: TButton;
    Edit_kolichestvo_studentov: TEdit;

    Check_spetsialmznostmz_id: TCheckBox;
    Check_god_nachala_obucheniya: TCheckBox;
    Check_nazvanie_gruppy: TCheckBox;
    Check_vecherniy: TCheckBox;
    Check_yazyk_obucheniya_id: TCheckBox;
    Check_kolichestvo_studentov: TCheckBox;
    Label1: TLabel;
    Combo_za: TComboBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;
    procedure LoadSPETSIALMZNOSTMZ;
    procedure GetSPETSIALMZNOSTMZ(pCombo:TComboBox;pSID:integer);
    procedure Button_spetsialmznostmz_idClick(Sender: TObject);

    procedure LoadYAZYK_OBUCHENIYA;
    procedure GetYAZYK_OBUCHENIYA(pCombo:TComboBox;pSID:integer);
    procedure Button_yazyk_obucheniya_idClick(Sender: TObject);


    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);

    procedure Edit_god_nachala_obucheniyaKeyPress(Sender: TObject;var Key: Char);
    procedure Edit_kolichestvo_studentovKeyPress(Sender: TObject;var Key: Char);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fGRUPPA_RECORDf: TfGRUPPA_RECORDf;

implementation

uses  
    fGRUPPA,
    fSPETSIALMZNOSTMZ,
    fYAZYK_OBUCHENIYA,
    fMW, DB;

{$R *.dfm}

procedure TfGRUPPA_RECORDf.Edit_god_nachala_obucheniyaKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'.. '9',#8]) then key:= #0;
end;

procedure TfGRUPPA_RECORDf.Edit_kolichestvo_studentovKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'.. '9',#8]) then key:= #0;
end;

procedure TfGRUPPA_RECORDf.GetSPETSIALMZNOSTMZ(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfGRUPPA_RECORDf.LoadSPETSIALMZNOSTMZ;
begin

Combo_spetsialmznostmz_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from spetsialmznostmz where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_spetsialmznostmz_id.AddItem(ADQ.FieldByName('spetsialmznostmz_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_spetsialmznostmz_id.ItemIndex:=0;

end;

procedure TfGRUPPA_RECORDf.Button_spetsialmznostmz_idClick(Sender: TObject);
begin

  fSPETSIALMZNOSTMZf.Caption:='����� �������������';
  if Combo_spetsialmznostmz_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_spetsialmznostmz_id.Items.Objects[Combo_spetsialmznostmz_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fSPETSIALMZNOSTMZf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='GRUPPA';

  if pNoAction=false then begin
    LoadSPETSIALMZNOSTMZ;
    if pSelectedID<>'' then GetSPETSIALMZNOSTMZ(Combo_spetsialmznostmz_id,strtoint(pSelectedID));
  end;

end;

procedure TfGRUPPA_RECORDf.GetYAZYK_OBUCHENIYA(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfGRUPPA_RECORDf.LoadYAZYK_OBUCHENIYA;
begin

Combo_yazyk_obucheniya_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from yazyk_obucheniya where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_yazyk_obucheniya_id.AddItem(ADQ.FieldByName('yazyk_obucheniya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_yazyk_obucheniya_id.ItemIndex:=0;

end;

procedure TfGRUPPA_RECORDf.Button_yazyk_obucheniya_idClick(Sender: TObject);
begin

  fYAZYK_OBUCHENIYAf.Caption:='����� ���� ��������';
  if Combo_yazyk_obucheniya_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_yazyk_obucheniya_id.Items.Objects[Combo_yazyk_obucheniya_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fYAZYK_OBUCHENIYAf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='GRUPPA';

  if pNoAction=false then begin
    LoadYAZYK_OBUCHENIYA;
    if pSelectedID<>'' then GetYAZYK_OBUCHENIYA(Combo_yazyk_obucheniya_id,strtoint(pSelectedID));
  end;

end;

procedure TfGRUPPA_RECORDf.CheckInput;
begin

if trim(Combo_spetsialmznostmz_id.Text)='' then begin
  Combo_spetsialmznostmz_id.SetFocus;
  abort;
end;

if trim(Edit_god_nachala_obucheniya.Text)='' then begin
  Edit_god_nachala_obucheniya.SetFocus;
  abort;
end;

if trim(Edit_nazvanie_gruppy.Text)='' then begin
  Edit_nazvanie_gruppy.SetFocus;
  abort;
end;

if trim(Combo_yazyk_obucheniya_id.Text)='' then begin
  Combo_yazyk_obucheniya_id.SetFocus;
  abort;
end;

if trim(Edit_kolichestvo_studentov.Text)='' then begin
  Edit_kolichestvo_studentov.SetFocus;
  abort;
end;

end;
procedure TfGRUPPA_RECORDf.CLALL;
begin

  LoadSPETSIALMZNOSTMZ;
  LoadYAZYK_OBUCHENIYA;

  Check_spetsialmznostmz_id.Checked:=false;
  Check_spetsialmznostmz_id.Visible:=false;
  Check_god_nachala_obucheniya.Checked:=false;
  Check_god_nachala_obucheniya.Visible:=false;
  Check_nazvanie_gruppy.Checked:=false;
  Check_nazvanie_gruppy.Visible:=false;
  Check_vecherniy.Checked:=false;
  Check_vecherniy.Visible:=false;
  Check_yazyk_obucheniya_id.Checked:=false;
  Check_yazyk_obucheniya_id.Visible:=false;
  Check_kolichestvo_studentov.Checked:=false;
  Check_kolichestvo_studentov.Visible:=false;

  Edit_god_nachala_obucheniya.Clear;
  Edit_nazvanie_gruppy.Clear;
  Check_vecherniy.Checked:=false;
  Edit_kolichestvo_studentov.Clear;

  Combo_za.ItemIndex:=0;

  Combo_spetsialmznostmz_id.Setfocus;

end;

procedure TfGRUPPA_RECORDf.FormActivated;
begin

  CLALL;

if pActionGRUPPA='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionGRUPPA='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from gruppa where id='+pID);
  ADQ.Open;
  ADQ.First;

  GetSPETSIALMZNOSTMZ(Combo_spetsialmznostmz_id,ADQ.fieldbyname('spetsialmznostmz_id').AsInteger);
  Edit_god_nachala_obucheniya.Text:=ADQ.fieldbyname('god_nachala_obucheniya').AsString;
  Edit_nazvanie_gruppy.Text:=ADQ.fieldbyname('nazvanie_gruppy').AsString;
  CheckBox_vecherniy.Checked:=ADQ.fieldbyname('vecherniy').AsBoolean;
  GetYAZYK_OBUCHENIYA(Combo_yazyk_obucheniya_id,ADQ.fieldbyname('yazyk_obucheniya_id').AsInteger);
  Edit_kolichestvo_studentov.Text:=ADQ.fieldbyname('kolichestvo_studentov').AsString;
  Combo_za.ItemIndex:=ADQ.fieldbyname('za_id').AsInteger-1;

end;

if pActionGRUPPA='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_spetsialmznostmz_id.Visible:=true;
  Check_god_nachala_obucheniya.Visible:=true;
  Check_nazvanie_gruppy.Visible:=true;
  Check_vecherniy.Visible:=true;
  Check_yazyk_obucheniya_id.Visible:=true;
  Check_kolichestvo_studentov.Visible:=true;

end;

end;

procedure TfGRUPPA_RECORDf.BtnOK;
begin

if pActionGRUPPA='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'gruppa '+
  '('+
  'za_id,'+
  'spetsialmznostmz_id,'+
  'god_nachala_obucheniya,'+
  'nazvanie_gruppy,'+
  'vecherniy,'+
  'yazyk_obucheniya_id,'+
  'kolichestvo_studentov,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+inttostr(Combo_za.ItemIndex+1)+''', '+
  ' '''+Inttostr(Integer(Combo_spetsialmznostmz_id.Items.Objects[Combo_spetsialmznostmz_id.ItemIndex]))+''', '+
  ' '''+trim(Edit_god_nachala_obucheniya.Text)+''', '+
  ' '''+trim(Edit_nazvanie_gruppy.Text)+''', '+
  ' '''+booltoStr(CheckBox_vecherniy.Checked)+''', '+
  ' '''+Inttostr(Integer(Combo_yazyk_obucheniya_id.Items.Objects[Combo_yazyk_obucheniya_id.ItemIndex]))+''', '+
  ' '''+trim(Edit_kolichestvo_studentov.Text)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionGRUPPA='UPDATE' then begin

 CheckInput;

 pS:='UPDATE '+
  'gruppa set ' +
  'za_id='''+Inttostr(Combo_za.ItemIndex+1)+''','+
  'spetsialmznostmz_id='''+Inttostr(Integer(Combo_spetsialmznostmz_id.Items.Objects[Combo_spetsialmznostmz_id.ItemIndex]))+''','+
  'god_nachala_obucheniya='''+trim(Edit_god_nachala_obucheniya.Text)+''','+
  'nazvanie_gruppy='''+trim(Edit_nazvanie_gruppy.Text)+''','+
  'vecherniy='''+BoolToStr(CheckBox_vecherniy.Checked)+''','+
  'yazyk_obucheniya_id='''+Inttostr(Integer(Combo_yazyk_obucheniya_id.Items.Objects[Combo_yazyk_obucheniya_id.ItemIndex]))+''','+
  'kolichestvo_studentov='''+trim(Edit_kolichestvo_studentov.Text)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fGRUPPAf.ListView1.Selected.SubItems[0]:=trim(Combo_spetsialmznostmz_id.Text);
   fGRUPPAf.ListView1.Selected.SubItems[1]:=trim(Edit_god_nachala_obucheniya.Text);
   fGRUPPAf.ListView1.Selected.SubItems[2]:=trim(Edit_nazvanie_gruppy.Text);
   if CheckBox_vecherniy.Checked=true then fGRUPPAf.ListView1.Selected.SubItems[3]:='��' else fGRUPPAf.ListView1.Selected.SubItems[3]:='���';
   fGRUPPAf.ListView1.Selected.SubItems[4]:=trim(Combo_yazyk_obucheniya_id.Text);
   fGRUPPAf.ListView1.Selected.SubItems[5]:=trim(Edit_kolichestvo_studentov.Text);
   fGRUPPAf.ListView1.Selected.SubItems[6]:=trim(Combo_za.Text);
end;

if pActionGRUPPA='SEARCH' then begin

if (Check_spetsialmznostmz_id.Checked=false) and (Check_god_nachala_obucheniya.Checked=false) and (Check_nazvanie_gruppy.Checked=false) and (Check_vecherniy.Checked=false) and (Check_yazyk_obucheniya_id.Checked=false) and (Check_kolichestvo_studentov.Checked=false) then abort;

   pSearch:='';

   if Check_spetsialmznostmz_id.Checked=true then pSearch:=pSearch+' and gruppa.spetsialmznostmz_id = '+Inttostr(Integer(Combo_spetsialmznostmz_id.Items.Objects[Combo_spetsialmznostmz_id.ItemIndex]))+' ';
   if Check_god_nachala_obucheniya.Checked=true then pSearch:=pSearch+' and gruppa.god_nachala_obucheniya like '''+trim(Edit_god_nachala_obucheniya.Text)+''' ';
   if Check_nazvanie_gruppy.Checked=true then pSearch:=pSearch+' and gruppa.nazvanie_gruppy like '''+trim(Edit_nazvanie_gruppy.Text)+''' ';
   if Check_vecherniy.Checked=true then pSearch:=pSearch+' and gruppa.vecherniy = '+BoolToStr(CheckBox_vecherniy.Checked)+' ';
   if Check_yazyk_obucheniya_id.Checked=true then pSearch:=pSearch+' and gruppa.yazyk_obucheniya_id = '+Inttostr(Integer(Combo_yazyk_obucheniya_id.Items.Objects[Combo_yazyk_obucheniya_id.ItemIndex]))+' ';
   if Check_kolichestvo_studentov.Checked=true then pSearch:=pSearch+' and gruppa.kolichestvo_studentov like '''+trim(Edit_kolichestvo_studentov.Text)+''' ';

end;

Close;

end;

procedure TfGRUPPA_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfGRUPPA_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfGRUPPA_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfGRUPPA_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.