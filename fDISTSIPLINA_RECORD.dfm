object fDISTSIPLINA_RECORDf: TfDISTSIPLINA_RECORDf
  Left = 552
  Top = 215
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  ClientHeight = 245
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 197
    Width = 472
    Height = 48
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = 'OK'
      TabOrder = 0
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 472
    Height = 197
    Align = alClient
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label_distsiplina_na_russkom: TLabel
      Left = 40
      Top = 16
      Width = 124
      Height = 13
      Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1072' '#1085#1072' '#1088#1091#1089#1089#1082#1086#1084
    end
    object Label_distsiplina_na_kazakhskom: TLabel
      Left = 28
      Top = 40
      Width = 136
      Height = 13
      Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1072' '#1085#1072' '#1082#1072#1079#1072#1093#1089#1082#1086#1084
    end
    object Label_indeks_na_russkom: TLabel
      Left = 65
      Top = 64
      Width = 99
      Height = 13
      Caption = #1048#1085#1076#1077#1082#1089' '#1085#1072' '#1088#1091#1089#1089#1082#1086#1084
    end
    object Label_indeks_na_kazakhskom: TLabel
      Left = 53
      Top = 88
      Width = 111
      Height = 13
      Caption = #1048#1085#1076#1077#1082#1089' '#1085#1072' '#1082#1072#1079#1072#1093#1089#1082#1086#1084
    end
    object Label_kol_podgrupp_po_praktike: TLabel
      Left = 31
      Top = 112
      Width = 133
      Height = 13
      Caption = #1050#1086#1083' '#1087#1086#1076#1075#1088#1091#1087#1087' '#1087#1086' '#1087#1088#1072#1082#1090#1080#1082#1077
    end
    object Label_itogo_chasov_za_vse_semestry: TLabel
      Left = 12
      Top = 136
      Width = 152
      Height = 13
      Caption = #1048#1090#1086#1075#1086' '#1095#1072#1089#1086#1074' '#1079#1072' '#1074#1089#1077' '#1089#1077#1084#1077#1089#1090#1088#1099
    end
    object Label_semestry: TLabel
      Left = 112
      Top = 160
      Width = 52
      Height = 13
      Caption = #1057#1077#1084#1077#1089#1090#1088#1099
    end
    object Check_distsiplina_na_russkom: TCheckBox
      Left = 448
      Top = 16
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 0
    end
    object Check_distsiplina_na_kazakhskom: TCheckBox
      Left = 448
      Top = 40
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 1
    end
    object Check_indeks_na_russkom: TCheckBox
      Left = 448
      Top = 64
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 2
    end
    object Check_indeks_na_kazakhskom: TCheckBox
      Left = 448
      Top = 88
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 3
    end
    object Check_kol_podgrupp_po_praktike: TCheckBox
      Left = 448
      Top = 112
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 4
    end
    object Check_itogo_chasov_za_vse_semestry: TCheckBox
      Left = 448
      Top = 136
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 5
    end
    object Check_semestry: TCheckBox
      Left = 448
      Top = 160
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 7
    end
    object Edit_distsiplina_na_russkom: TEdit
      Left = 168
      Top = 16
      Width = 273
      Height = 25
      Ctl3D = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 6
      Text = 'Edit_distsiplina_na_russkom'
    end
    object Edit_distsiplina_na_kazakhskom: TEdit
      Left = 168
      Top = 40
      Width = 273
      Height = 25
      Ctl3D = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 8
      Text = 'Edit_distsiplina_na_kazakhskom'
    end
    object Edit_indeks_na_russkom: TEdit
      Left = 168
      Top = 64
      Width = 273
      Height = 25
      Ctl3D = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 9
      Text = 'Edit_indeks_na_russkom'
    end
    object Edit_indeks_na_kazakhskom: TEdit
      Left = 168
      Top = 88
      Width = 273
      Height = 25
      Ctl3D = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 10
      Text = 'Edit_indeks_na_kazakhskom'
    end
    object Edit_kol_podgrupp_po_praktike: TEdit
      Left = 168
      Top = 112
      Width = 273
      Height = 25
      Ctl3D = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 11
      Text = 'Edit_kol_podgrupp_po_praktike'
      OnKeyPress = Edit_kol_podgrupp_po_praktikeKeyPress
    end
    object Edit_itogo_chasov_za_vse_semestry: TEdit
      Left = 168
      Top = 136
      Width = 273
      Height = 25
      Ctl3D = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 12
      Text = 'Edit_itogo_chasov_za_vse_semestry'
      OnKeyPress = Edit_itogo_chasov_za_vse_semestryKeyPress
    end
    object Edit_semestry: TEdit
      Left = 168
      Top = 160
      Width = 273
      Height = 25
      Ctl3D = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 13
      Text = 'Edit_semestry'
    end
  end
end
