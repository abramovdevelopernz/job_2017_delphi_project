object fSVOBOD_CHASYf: TfSVOBOD_CHASYf
  Left = 430
  Top = 154
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = #1059#1089#1090#1072#1085#1086#1074#1082#1072' '#1089#1074#1086#1073#1086#1076#1085#1099#1093' '#1095#1072#1089#1086#1074' '#1087#1088#1077#1087#1086#1076#1072#1074#1072#1090#1077#1083#1103
  ClientHeight = 264
  ClientWidth = 746
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 218
    Width = 746
    Height = 46
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWindow
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 746
    Height = 218
    Align = alClient
    BevelOuter = bvLowered
    Color = clWindow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object GroupBox1: TGroupBox
      Left = 8
      Top = 8
      Width = 57
      Height = 201
      Caption = #1055#1072#1088#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 32
        Width = 7
        Height = 16
        Caption = '1'
      end
      object Label2: TLabel
        Left = 16
        Top = 56
        Width = 7
        Height = 16
        Caption = '2'
      end
      object Label3: TLabel
        Left = 16
        Top = 80
        Width = 7
        Height = 16
        Caption = '3'
      end
      object Label4: TLabel
        Left = 16
        Top = 104
        Width = 7
        Height = 16
        Caption = '4'
      end
      object Label5: TLabel
        Left = 16
        Top = 128
        Width = 7
        Height = 16
        Caption = '5'
      end
      object Label6: TLabel
        Left = 16
        Top = 152
        Width = 7
        Height = 16
        Caption = '6'
      end
      object Label7: TLabel
        Left = 16
        Top = 176
        Width = 7
        Height = 16
        Caption = '7'
      end
    end
    object GroupBox2: TGroupBox
      Left = 72
      Top = 8
      Width = 105
      Height = 201
      Caption = #1055#1086#1085#1077#1076#1077#1083#1100#1085#1080#1082
      TabOrder = 1
      object CheckBox1: TCheckBox
        Left = 16
        Top = 32
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 0
        OnClick = CheckBox1Click
      end
      object CheckBox2: TCheckBox
        Left = 16
        Top = 56
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 1
        OnClick = CheckBox2Click
      end
      object CheckBox3: TCheckBox
        Left = 16
        Top = 80
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 2
        OnClick = CheckBox3Click
      end
      object CheckBox4: TCheckBox
        Left = 16
        Top = 104
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 3
        OnClick = CheckBox4Click
      end
      object CheckBox5: TCheckBox
        Left = 16
        Top = 128
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 4
        OnClick = CheckBox5Click
      end
      object CheckBox6: TCheckBox
        Left = 16
        Top = 152
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 5
        OnClick = CheckBox6Click
      end
      object CheckBox7: TCheckBox
        Left = 16
        Top = 176
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 6
        OnClick = CheckBox7Click
      end
    end
    object GroupBox3: TGroupBox
      Left = 184
      Top = 8
      Width = 105
      Height = 201
      Caption = #1042#1090#1086#1088#1085#1080#1082
      TabOrder = 2
      object CheckBox8: TCheckBox
        Left = 16
        Top = 32
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 0
        OnClick = CheckBox8Click
      end
      object CheckBox9: TCheckBox
        Left = 16
        Top = 56
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 1
        OnClick = CheckBox9Click
      end
      object CheckBox10: TCheckBox
        Left = 16
        Top = 80
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 2
        OnClick = CheckBox10Click
      end
      object CheckBox11: TCheckBox
        Left = 16
        Top = 104
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 3
        OnClick = CheckBox11Click
      end
      object CheckBox12: TCheckBox
        Left = 16
        Top = 128
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 4
        OnClick = CheckBox12Click
      end
      object CheckBox13: TCheckBox
        Left = 16
        Top = 152
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 5
        OnClick = CheckBox13Click
      end
      object CheckBox14: TCheckBox
        Left = 16
        Top = 176
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 6
        OnClick = CheckBox14Click
      end
    end
    object GroupBox4: TGroupBox
      Left = 296
      Top = 8
      Width = 105
      Height = 201
      Caption = #1057#1088#1077#1076#1072
      TabOrder = 3
      object CheckBox15: TCheckBox
        Left = 16
        Top = 32
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 0
        OnClick = CheckBox15Click
      end
      object CheckBox16: TCheckBox
        Left = 16
        Top = 56
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 1
        OnClick = CheckBox16Click
      end
      object CheckBox17: TCheckBox
        Left = 16
        Top = 80
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 2
        OnClick = CheckBox17Click
      end
      object CheckBox18: TCheckBox
        Left = 16
        Top = 104
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 3
        OnClick = CheckBox18Click
      end
      object CheckBox19: TCheckBox
        Left = 16
        Top = 128
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 4
        OnClick = CheckBox19Click
      end
      object CheckBox20: TCheckBox
        Left = 16
        Top = 152
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 5
        OnClick = CheckBox20Click
      end
      object CheckBox21: TCheckBox
        Left = 16
        Top = 176
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 6
        OnClick = CheckBox21Click
      end
    end
    object GroupBox5: TGroupBox
      Left = 408
      Top = 8
      Width = 105
      Height = 201
      Caption = #1063#1077#1090#1074#1077#1088#1075
      TabOrder = 4
      object CheckBox22: TCheckBox
        Left = 16
        Top = 32
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 0
        OnClick = CheckBox22Click
      end
      object CheckBox23: TCheckBox
        Left = 16
        Top = 56
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 1
        OnClick = CheckBox23Click
      end
      object CheckBox24: TCheckBox
        Left = 16
        Top = 80
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 2
        OnClick = CheckBox24Click
      end
      object CheckBox25: TCheckBox
        Left = 16
        Top = 104
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 3
        OnClick = CheckBox25Click
      end
      object CheckBox26: TCheckBox
        Left = 16
        Top = 128
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 4
        OnClick = CheckBox26Click
      end
      object CheckBox27: TCheckBox
        Left = 16
        Top = 152
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 5
        OnClick = CheckBox27Click
      end
      object CheckBox28: TCheckBox
        Left = 16
        Top = 176
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 6
        OnClick = CheckBox28Click
      end
    end
    object GroupBox6: TGroupBox
      Left = 520
      Top = 8
      Width = 105
      Height = 201
      Caption = #1055#1103#1090#1085#1080#1094#1072
      TabOrder = 5
      object CheckBox29: TCheckBox
        Left = 16
        Top = 32
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 0
        OnClick = CheckBox29Click
      end
      object CheckBox30: TCheckBox
        Left = 16
        Top = 56
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 1
        OnClick = CheckBox30Click
      end
      object CheckBox31: TCheckBox
        Left = 16
        Top = 80
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 2
        OnClick = CheckBox31Click
      end
      object CheckBox32: TCheckBox
        Left = 16
        Top = 104
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 3
        OnClick = CheckBox32Click
      end
      object CheckBox33: TCheckBox
        Left = 16
        Top = 128
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 4
        OnClick = CheckBox33Click
      end
      object CheckBox34: TCheckBox
        Left = 16
        Top = 152
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 5
        OnClick = CheckBox34Click
      end
      object CheckBox35: TCheckBox
        Left = 16
        Top = 176
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 6
        OnClick = CheckBox35Click
      end
    end
    object GroupBox7: TGroupBox
      Left = 632
      Top = 8
      Width = 105
      Height = 201
      Caption = #1057#1091#1073#1073#1086#1090#1072
      TabOrder = 6
      object CheckBox36: TCheckBox
        Left = 16
        Top = 32
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 0
        OnClick = CheckBox36Click
      end
      object CheckBox37: TCheckBox
        Left = 16
        Top = 56
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 1
        OnClick = CheckBox37Click
      end
      object CheckBox38: TCheckBox
        Left = 16
        Top = 80
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 2
        OnClick = CheckBox38Click
      end
      object CheckBox39: TCheckBox
        Left = 16
        Top = 104
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 3
        OnClick = CheckBox39Click
      end
      object CheckBox40: TCheckBox
        Left = 16
        Top = 128
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 4
        OnClick = CheckBox40Click
      end
      object CheckBox41: TCheckBox
        Left = 16
        Top = 152
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 5
        OnClick = CheckBox41Click
      end
      object CheckBox42: TCheckBox
        Left = 16
        Top = 176
        Width = 80
        Height = 17
        Cursor = crHandPoint
        TabOrder = 6
        OnClick = CheckBox42Click
      end
    end
  end
end
