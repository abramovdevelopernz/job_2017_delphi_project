object fNAGRUZKA_NA_PREPODAVATELYA_RECORDf: TfNAGRUZKA_NA_PREPODAVATELYA_RECORDf
  Left = 175
  Top = 280
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  ClientHeight = 202
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 154
    Width = 472
    Height = 48
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = 'OK'
      TabOrder = 0
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 472
    Height = 154
    Align = alClient
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label_DISTSIPLINA: TLabel
      Left = 75
      Top = 8
      Width = 63
      Height = 13
      Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1072
    end
    object Label_gruppa_id: TLabel
      Left = 103
      Top = 32
      Width = 35
      Height = 13
      Caption = #1043#1088#1091#1087#1087#1072
    end
    object Label_podgruppa_id: TLabel
      Left = 84
      Top = 56
      Width = 54
      Height = 13
      Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
    end
    object Label_vid_nagruzki_id: TLabel
      Left = 70
      Top = 80
      Width = 68
      Height = 13
      Caption = #1042#1080#1076' '#1085#1072#1075#1088#1091#1079#1082#1080
    end
    object Label_teoriya_chasov_itogo: TLabel
      Left = 38
      Top = 104
      Width = 100
      Height = 13
      Caption = #1058#1077#1086#1088#1080#1103' '#1095#1072#1089#1086#1074' '#1080#1090#1086#1075#1086
    end
    object Label_praktika_chasov_itogo: TLabel
      Left = 26
      Top = 128
      Width = 112
      Height = 13
      Caption = #1055#1088#1072#1082#1090#1080#1082#1072' '#1095#1072#1089#1086#1074' '#1080#1090#1086#1075#1086
    end
    object Check_DISTSIPLINA: TCheckBox
      Left = 448
      Top = 8
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 0
    end
    object Check_gruppa_id: TCheckBox
      Left = 448
      Top = 32
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 1
    end
    object Check_podgruppa_id: TCheckBox
      Left = 448
      Top = 56
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 2
    end
    object Check_vid_nagruzki_id: TCheckBox
      Left = 448
      Top = 80
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 3
    end
    object Check_teoriya_chasov_itogo: TCheckBox
      Left = 448
      Top = 104
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 4
    end
    object Check_praktika_chasov_itogo: TCheckBox
      Left = 448
      Top = 128
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 5
    end
    object Combo_distsiplina_id: TComboBox
      Left = 144
      Top = 8
      Width = 265
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 6
    end
    object Button_DISTSIPLINA: TButton
      Left = 416
      Top = 8
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 7
      OnClick = Button_DISTSIPLINAClick
    end
    object Combo_gruppa_id: TComboBox
      Left = 144
      Top = 32
      Width = 265
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 8
    end
    object Button_gruppa_id: TButton
      Left = 416
      Top = 32
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 9
      OnClick = Button_gruppa_idClick
    end
    object Combo_podgruppa_id: TComboBox
      Left = 144
      Top = 56
      Width = 265
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 10
    end
    object Button_podgruppa_id: TButton
      Left = 416
      Top = 56
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 11
      OnClick = Button_podgruppa_idClick
    end
    object Combo_vid_nagruzki_id: TComboBox
      Left = 144
      Top = 80
      Width = 265
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 12
    end
    object Button_vid_nagruzki_id: TButton
      Left = 416
      Top = 80
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 13
      Visible = False
      OnClick = Button_vid_nagruzki_idClick
    end
    object Edit_teoriya_chasov_itogo: TEdit
      Left = 144
      Top = 104
      Width = 297
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 14
      Text = 'Edit_teoriya_chasov_itogo'
      OnKeyPress = Edit_teoriya_chasov_itogoKeyPress
    end
    object Edit_praktika_chasov_itogo: TEdit
      Left = 144
      Top = 128
      Width = 297
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 15
      Text = 'Edit_praktika_chasov_itogo'
      OnKeyPress = Edit_praktika_chasov_itogoKeyPress
    end
  end
end
