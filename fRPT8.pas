unit fRPT8;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls,clipbrd;

type
  TfRPT8f = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    ListView1: TListView;
    Label1: TLabel;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    Button2: TButton;
    Button3: TButton;
    procedure FormActivate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button2Click(Sender: TObject);
    procedure Search1;
    procedure ListView1ColumnClick(Sender: TObject; Column: TListColumn);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fRPT8f: TfRPT8f;

implementation

uses fMW;

{$R *.dfm}

function CustomSortProc(Item1, Item2: TListItem; ParamSort: integer): integer; stdcall;
begin
  if ParamSort=0 then
    Result := CompareText(Item1.Caption,Item2.Caption)
  else
    if Item1.SubItems.Count>ParamSort-1 then
    begin
      if Item2.SubItems.Count>ParamSort-1 then
        Result := CompareText(Item1.SubItems[ParamSort-1],Item2.SubItems[ParamSort-1])
      else
        Result := 1;
    end
    else
      Result:=-1;
end;

procedure TfRPT8f.Search1;
var
pOrder,pPravilo:string;
begin

  ListView1.Clear;
  Screen.Cursor := crHourglass;
  Application.ProcessMessages;

pS:='Select '+
' ZAYAVKA.id as ZAYAVKA_ID, '+
' ZAKAZCHIK.ID as ZAKAZCHIK_ID,'+
' ZAKAZCHIK.iin_bin as ZAKAZCHIK_iin_bin,'+
' ZAKAZCHIK.nazvanie_predpriyatiya_rus as ZAKAZCHIK_name,'+
' STATUS_ZAYAVKI.STATUS_ZAYAVKI as STATUS_ZAYAVKI_name,'+
' RAZRABOTCHIK.RAZRABOTCHIK as RAZRABOTCHIK_name,'+
' KLASS_OPASNOSTI.KLASS_OPASNOSTI as KLASS_OPASNOSTI_name,'+
' SERIYA.SERIYA as SERIYA_name,'+
' ZAYAVKA.nomer_razresheniya,'+
' ZAYAVKA.zakazchik_id,'+
' ZAYAVKA.srok_ot,'+
' ZAYAVKA.srok_do,'+ 
' ZAYAVKA.data_vydachi,'+
' ZAYAVKA.data_priostanovleniya_ot,'+
' ZAYAVKA.data_priostanovleniya_do,'+
' ZAYAVKA.data_annulirovaniya,'+
' PRICHINY_OTKAZA.PRICHINY_OTKAZA as PRICHINY_OTKAZA_name,'+
' ZAYAVKA.nomer_vkhodyashchego,'+
' ZAYAVKA.data_vkhodyashchego,'+
' ZAYAVKA.nomer_vozvrata,'+
' ZAYAVKA.data_vozvrata,'+
' ZAYAVKA.povtor,'+
' ZAYAVKA.prichina_annulirovaniya,'+
' ZAYAVKA.prichina_priostanovleniya,'+
' ZAYAVKA.nomer_uvedomleniya,'+
' ZAYAVKA.data_uvedomleniya,'+
' ZAYAVKA.currentuser,'+
' ZAYAVKA.currentdate '+
' from '+
' STATUS_ZAYAVKI,'+
' RAZRABOTCHIK,'+ 
' SERIYA,'+
' PRICHINY_OTKAZA,'+
' ZAKAZCHIK,'+
' KLASS_OPASNOSTI,'+
' RAYON,'+
' ZAYAVKA '+
' where '+
' KLASS_OPASNOSTI.id=ZAYAVKA.KLASS_OPASNOSTI_id and ' +
' ZAKAZCHIK.id=ZAYAVKA.zakazchik_id and ' +
' ZAKAZCHIK.rayon_id=RAYON.id and ' +
' STATUS_ZAYAVKI.id=ZAYAVKA.status_zayavki_id and ' +
' RAZRABOTCHIK.id=ZAYAVKA.razrabotchik_id and ' +
' SERIYA.id=ZAYAVKA.seriya_id and ' +
' PRICHINY_OTKAZA.id=ZAYAVKA.prichiny_otkaza_id and ' +
' ZAKAZCHIK.deleted=0 and ' +
' ZAYAVKA.status_zayavki_id=3 and ' +
' ZAYAVKA.deleted=0 and '+
' ZAYAVKA.srok_do between ''2000-01-01''  and ''2017-01-01''' ;

{
if ComboBox1.ItemIndex=0 then pOrder:='ID';
if ComboBox1.ItemIndex=1 then pOrder:='ZAKAZCHIK_name';
if ComboBox1.ItemIndex=2 then pOrder:='STATUS_ZAYAVKI_name';
if ComboBox1.ItemIndex=3 then pOrder:='RAZRABOTCHIK_name';
if ComboBox1.ItemIndex=4 then pOrder:='SERIYA_name';
if ComboBox1.ItemIndex=5 then pOrder:='nomer_razresheniya';
if ComboBox1.ItemIndex=6 then pOrder:='srok_ot';
if ComboBox1.ItemIndex=7 then pOrder:='srok_do';
if ComboBox1.ItemIndex=8 then pOrder:='data_vydachi';
if ComboBox1.ItemIndex=9 then pOrder:='data_priostanovleniya_ot';
if ComboBox1.ItemIndex=10 then pOrder:='data_priostanovleniya_do';
if ComboBox1.ItemIndex=11 then pOrder:='data_annulirovaniya';
if ComboBox1.ItemIndex=12 then pOrder:='PRICHINY_OTKAZA_name';
if ComboBox1.ItemIndex=13 then pOrder:='nomer_vkhodyashchego';
if ComboBox1.ItemIndex=14 then pOrder:='data_vkhodyashchego';
if ComboBox1.ItemIndex=15 then pOrder:='nomer_vozvrata';
if ComboBox1.ItemIndex=16 then pOrder:='data_vozvrata';
if ComboBox1.ItemIndex=17 then pOrder:='povtor';
if ComboBox1.ItemIndex=18 then pOrder:='prichina_annulirovaniya';
if ComboBox1.ItemIndex=19 then pOrder:='prichina_priostanovleniya';
if ComboBox1.ItemIndex=20 then pOrder:='nomer_uvedomleniya';
if ComboBox1.ItemIndex=21 then pOrder:='data_uvedomleniya';
if ComboBox1.ItemIndex=22 then pOrder:='KLASS_OPASNOSTI_name';

  if RadioButton1.Checked=true then pPravilo:='asc';
  if RadioButton2.Checked=true then pPravilo:='desc';

  pS:=pS+' order by '+pOrder+' '+pPravilo;

  fMWf.LogAction('����� �� �������',pS);


  Clipboard.AsText:=pS;
}


//  Clipboard.AsText:=pS;
//  ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS+' order by ZAKAZCHIK_iin_bin');
  ADQ.Open;

  if ADQ.RecordCount = 0 then
  begin
    ListView1.Clear;
    StatusBar1.panels[0].text:='����� ������� : 0';
    Screen.Cursor := crDefault;
    exit;
  end;

  ListView1.Visible := false;
  ListView1.Items.Clear;

  ADQ.Last;  
  ADQ.First;
  while not ADQ.Eof do
  begin
    L := ListView1.Items.Add;
    L.Caption := trim(ADQ.fieldbyname('ZAYAVKA_ID').AsString);
    L.SubItems.Append(ADQ.fieldbyname('ZAKAZCHIK_iin_bin').AsString);
    L.SubItems.Append(ADQ.fieldbyname('ZAKAZCHIK_name').AsString);
    L.SubItems.Append(ADQ.fieldbyname('SERIYA_name').AsString);
    L.SubItems.Append(ADQ.fieldbyname('nomer_razresheniya').AsString);
    L.SubItems.Append(ADQ.fieldbyname('data_vydachi').AsString);
    L.SubItems.Append(ADQ.fieldbyname('srok_ot').AsString);
    L.SubItems.Append(ADQ.fieldbyname('srok_do').AsString);
    L.SubItems.Append(ADQ.fieldbyname('KLASS_OPASNOSTI_name').AsString);
    L.SubItems.Append('-');
    L.SubItems.Append('-');
    L.SubItems.Append('-');
    L.SubItems.Append('-');
    L.SubItems.Append('-');
    ADQ.Next;
  end;

  ListView1.Visible := true;

  if ListView1.Items.Count>0 then ListView1.Items[ListView1.Items.Count-1].Selected:=true;

  StatusBar1.panels[0].text:='����� ������� : '+inttostr(ADQ.recordcount);

  Screen.Cursor := crDefault;

 end;

procedure TfRPT8f.FormActivate(Sender: TObject);
begin
StatusBar1.panels[0].text:='����� ������� : 0';
DateTimePicker1.Date:=strtodate('01.01.2010');
DateTimePicker2.Date:=strtodate('01.01.2017');
Self.WindowState:=wsMaximized;
fMWf.SetLCW(ListView1);
end;

procedure TfRPT8f.Button1Click(Sender: TObject);
begin
Close;
end;

procedure TfRPT8f.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=vk_escape then close;
end;

procedure TfRPT8f.Button2Click(Sender: TObject);
var
i:integer;
begin

Search1;

for i:=0 to ListView1.Items.Count-1 do begin

ListView1.Items[i].Selected:=true;


pS:='Select '+
' ZAYAVKA.id as ZAYAVKA_ID, '+
' ZAKAZCHIK.ID as ZAKAZCHIK_ID,'+
' ZAKAZCHIK.iin_bin as ZAKAZCHIK_iin_bin,'+
' ZAKAZCHIK.nazvanie_predpriyatiya_rus as ZAKAZCHIK_name,'+
' STATUS_ZAYAVKI.STATUS_ZAYAVKI as STATUS_ZAYAVKI_name,'+
' RAZRABOTCHIK.RAZRABOTCHIK as RAZRABOTCHIK_name,'+
' KLASS_OPASNOSTI.KLASS_OPASNOSTI as KLASS_OPASNOSTI_name,'+
' SERIYA.SERIYA as SERIYA_name,'+
' ZAYAVKA.nomer_razresheniya,'+
' ZAYAVKA.zakazchik_id,'+
' ZAYAVKA.srok_ot,'+
' ZAYAVKA.srok_do,'+ 
' ZAYAVKA.data_vydachi,'+
' ZAYAVKA.data_priostanovleniya_ot,'+
' ZAYAVKA.data_priostanovleniya_do,'+
' ZAYAVKA.data_annulirovaniya,'+
' PRICHINY_OTKAZA.PRICHINY_OTKAZA as PRICHINY_OTKAZA_name,'+
' ZAYAVKA.nomer_vkhodyashchego,'+
' ZAYAVKA.data_vkhodyashchego,'+
' ZAYAVKA.nomer_vozvrata,'+
' ZAYAVKA.data_vozvrata,'+
' ZAYAVKA.povtor,'+
' ZAYAVKA.prichina_annulirovaniya,'+
' ZAYAVKA.prichina_priostanovleniya,'+
' ZAYAVKA.nomer_uvedomleniya,'+
' ZAYAVKA.data_uvedomleniya,'+
' ZAYAVKA.currentuser,'+
' ZAYAVKA.currentdate '+
' from '+
' STATUS_ZAYAVKI,'+
' RAZRABOTCHIK,'+ 
' SERIYA,'+
' PRICHINY_OTKAZA,'+
' ZAKAZCHIK,'+
' KLASS_OPASNOSTI,'+
' RAYON,'+
' ZAYAVKA '+
' where '+
' KLASS_OPASNOSTI.id=ZAYAVKA.KLASS_OPASNOSTI_id and ' +
' ZAKAZCHIK.id=ZAYAVKA.zakazchik_id and ' +
' ZAKAZCHIK.rayon_id=RAYON.id and ' +
' STATUS_ZAYAVKI.id=ZAYAVKA.status_zayavki_id and ' +
' RAZRABOTCHIK.id=ZAYAVKA.razrabotchik_id and ' +
' SERIYA.id=ZAYAVKA.seriya_id and ' +
' PRICHINY_OTKAZA.id=ZAYAVKA.prichiny_otkaza_id and ' +
' ZAKAZCHIK.deleted=0 and ' +
' ZAYAVKA.status_zayavki_id=3 and ' +
' ZAYAVKA.deleted=0 and '+
' ZAKAZCHIK.iin_bin='''+ListView1.Selected.SubItems[0]+''' and '+
' ZAYAVKA.srok_ot between ''2017-01-01''  and ''2020-01-01''' ;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add(pS);
ADQ.Open;

if ADQ.RecordCount>0 then begin
ListView1.Selected.SubItems[9]:=ADQ.fieldbyname('nomer_razresheniya').AsString;
ListView1.Selected.SubItems[10]:=ADQ.fieldbyname('data_vydachi').AsString;
ListView1.Selected.SubItems[11]:=ADQ.fieldbyname('srok_ot').AsString;
ListView1.Selected.SubItems[12]:=ADQ.fieldbyname('srok_do').AsString;
end else begin

end;





Self.Caption:='���������� '+inttostr(i)+' �� '+inttostr(ListView1.Items.Count);

end;


end;

procedure TfRPT8f.ListView1ColumnClick(Sender: TObject;
  Column: TListColumn);
begin
ListView1.CustomSort(@CustomSortProc, Column.Index);
end;

procedure TfRPT8f.Button3Click(Sender: TObject);
begin
fMWf.LVToExcel(ListView1);
end;

end.
