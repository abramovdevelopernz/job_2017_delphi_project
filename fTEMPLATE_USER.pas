unit fTEMPLATE_USER;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, Menus, ComCtrls, StdCtrls, ExtCtrls;

type
  TfTEMPLATE_USERf = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    Panel2: TPanel;
    ListView1: TListView;

    procedure CreateCol;                 //�������� ������� ��� ������
    procedure LV1(pSearchString:string); //�������� ������ ��������
    procedure SearchRecord;              //����� �������
    procedure UpdateRecord;              //���������� �������
    procedure InsertRecord;              //���������� �������
    procedure DeleteRecord;              //�������� �������

    procedure Button8Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure ListView1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fTEMPLATE_USERf: TfTEMPLATE_USERf;

implementation
uses fMW,fSHOWRECORD,fTEMPLATE_USER_RECORD;
{$R *.dfm}
//���������� �������
procedure TfTEMPLATE_USERf.InsertRecord;
begin
  pNoAction:=false;
  pAction:='INSERT';
  fTEMPLATE_USER_RECORDf.showmodal;
  if pNoAction=false then LV1('');
end;

//���������� �������
procedure TfTEMPLATE_USERf.UpdateRecord;
begin
  if ListView1.ItemIndex=-1 then abort;
  pNoAction:=false;
  pID:=ListView1.Selected.Caption;
  pAction:='UPDATE';
  fTEMPLATE_USER_RECORDf.showmodal;

end;

//����� �������
procedure TfTEMPLATE_USERf.SearchRecord;
begin
  pNoAction:=false;
  pAction:='SEARCH';
  fTEMPLATE_USER_RECORDf.showmodal;

  if pNoAction=false then begin
    LV1(pSearch);
    Button7.Visible:=true;
  end;

end;
//�������� �������
procedure TfTEMPLATE_USERf.DeleteRecord;
begin

if ListView1.ItemIndex=-1 then abort;

if MessageBox(Self.Handle, '������������� �������� ������?', '�������� ������', MB_OKCANCEL) =  IDOK then begin

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Update template_user set deleted=1,deleteuser='''+pMyUser+''',deletedate='''+datetostr(now)+' '+timetostr(now)+''' where id='+ListView1.Selected.Caption);
  ADQ.ExecSQL;

LV1('');

end;

end;

//�������� ������ ��������
procedure TfTEMPLATE_USERf.LV1(pSearchString:string);
begin
  ListView1.Clear;
  Screen.Cursor := crHourglass;
  Application.ProcessMessages;

pS:='Select '+
' template_user.id,'+
' template_user.name, '+
' template_user.lgn, '+
' template_user.ps '+
' from '+
' template_user '+
' where '+
' template_user.deleted=0';

  if trim(pSearchString)<>'' then pS:=pS+pSearchString;
  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.Open;

  if ADQ.RecordCount = 0 then
  begin
    ListView1.Clear;
    StatusBar1.panels[0].text:='����� ������� : 0';
    Screen.Cursor := crDefault;
  end;

  ListView1.Visible := false;
  ListView1.Items.Clear;

  ADQ.First;

  while not ADQ.Eof do
  begin
    L := ListView1.Items.Add;
    L.Caption := trim(ADQ.fieldbyname('id').AsString);
    L.SubItems.Append(ADQ.fieldbyname('name').AsString);
    L.SubItems.Append(ADQ.fieldbyname('lgn').AsString);
    L.SubItems.Append(ADQ.fieldbyname('ps').AsString);

    ADQ.Next;
  end;

  ListView1.Visible := true;

  if ListView1.Items.Count>0 then begin
    ListView1.Items[ListView1.Items.Count-1].Selected:=true;
    Button3.Enabled:=true;
    Button4.Enabled:=true;
    Button8.Enabled:=true;
  end else begin
    Button3.Enabled:=false;
    Button4.Enabled:=false;
    Button8.Enabled:=false;
  end;

  StatusBar1.panels[0].text:='����� ������� : '+inttostr(ADQ.recordcount);

  Screen.Cursor := crDefault;

end;

//�������� ������� ��� ������
procedure TfTEMPLATE_USERf.CreateCol;
var
  i:integer;
begin

  for i:=0 to ListView1.Columns.Count-1 do begin
    ListView1.Columns.Delete(0);
  end;

  ListView1.Columns.Add;
  ListView1.Column[0].Caption:='ID';
  ListView1.Column[0].Width:=0;
  ListView1.Columns.Add;
  ListView1.Column[1].Caption:='���';
  ListView1.Column[1].Width:=150;

  ListView1.Columns.Add;
  ListView1.Column[2].Caption:='�����';
  ListView1.Column[2].Width:=150;

  ListView1.Columns.Add;
  ListView1.Column[3].Caption:='������';
  ListView1.Column[3].Width:=150;

end;

procedure TfTEMPLATE_USERf.Button8Click(Sender: TObject);
begin
if ListView1.ItemIndex>-1 then begin
  fMWf.ShowRecord(ListView1);
  fSHOWRECORDf.ShowModal;
end;
end;

procedure TfTEMPLATE_USERf.Button7Click(Sender: TObject);
begin
  Button7.Visible:=false;
  LV1('');
end;

procedure TfTEMPLATE_USERf.ListView1Click(Sender: TObject);
begin
if (ListView1.ItemIndex<0) then begin
  Button3.Enabled:=false;
  Button4.Enabled:=false;
  Button8.Enabled:=false;
end else begin
  Button3.Enabled:=true;
  Button4.Enabled:=true;
  Button8.Enabled:=true;
end;
end;

procedure TfTEMPLATE_USERf.FormActivate(Sender: TObject);
begin
  Button7.Visible:=false;
  CreateCol;
  LV1('');
  fMWf.SetLCW(ListView1);  
end;

procedure TfTEMPLATE_USERf.Button6Click(Sender: TObject);
begin
  fMWf.LVToExcel(ListView1);
end;

procedure TfTEMPLATE_USERf.Button5Click(Sender: TObject);
begin
  SearchRecord;
end;

procedure TfTEMPLATE_USERf.Button4Click(Sender: TObject);
begin
  DeleteRecord;
end;

procedure TfTEMPLATE_USERf.Button3Click(Sender: TObject);
begin
  UpdateRecord;
end;

procedure TfTEMPLATE_USERf.Button2Click(Sender: TObject);
begin
  InsertRecord;
end;

procedure TfTEMPLATE_USERf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_insert then Button2Click(Self);
  if key=vk_f2 then Button3Click(Self);
  if key=vk_delete then Button4Click(Self);
  if key=vk_f3 then Button5Click(Self);
  if key=vk_escape then close;
end;

procedure TfTEMPLATE_USERf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close;
end;

procedure TfTEMPLATE_USERf.FormResize(Sender: TObject);
begin
if ListView1.Columns.Count>0 then fMWf.SetLCW(ListView1);
end;

end.