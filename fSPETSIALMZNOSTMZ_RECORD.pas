unit fSPETSIALMZNOSTMZ_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfSPETSIALMZNOSTMZ_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_spetsialmznostmz_na_russkom: TLabel;
    Label_abbreviatura_na_russkom: TLabel;
    Label_spetsialmznostmz_na_kazakhskom: TLabel;
    Label_abbreviatura_na_kazakhskom: TLabel;

    Edit_spetsialmznostmz_na_russkom: TEdit;
    Edit_abbreviatura_na_russkom: TEdit;
    Edit_spetsialmznostmz_na_kazakhskom: TEdit;
    Edit_abbreviatura_na_kazakhskom: TEdit;

    Check_spetsialmznostmz_na_russkom: TCheckBox;
    Check_abbreviatura_na_russkom: TCheckBox;
    Check_spetsialmznostmz_na_kazakhskom: TCheckBox;
    Check_abbreviatura_na_kazakhskom: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fSPETSIALMZNOSTMZ_RECORDf: TfSPETSIALMZNOSTMZ_RECORDf;

implementation

uses  
    fSPETSIALMZNOSTMZ,
    fMW;

{$R *.dfm}

procedure TfSPETSIALMZNOSTMZ_RECORDf.CheckInput;
begin

if trim(Edit_spetsialmznostmz_na_russkom.Text)='' then begin
  Edit_spetsialmznostmz_na_russkom.SetFocus;
  abort;
end;

if trim(Edit_abbreviatura_na_russkom.Text)='' then begin
  Edit_abbreviatura_na_russkom.SetFocus;
  abort;
end;

if trim(Edit_spetsialmznostmz_na_kazakhskom.Text)='' then begin
  Edit_spetsialmznostmz_na_kazakhskom.SetFocus;
  abort;
end;

if trim(Edit_abbreviatura_na_kazakhskom.Text)='' then begin
  Edit_abbreviatura_na_kazakhskom.SetFocus;
  abort;
end;

end;
procedure TfSPETSIALMZNOSTMZ_RECORDf.CLALL;
begin


  Check_spetsialmznostmz_na_russkom.Checked:=false;
  Check_spetsialmznostmz_na_russkom.Visible:=false;
  Check_abbreviatura_na_russkom.Checked:=false;
  Check_abbreviatura_na_russkom.Visible:=false;
  Check_spetsialmznostmz_na_kazakhskom.Checked:=false;
  Check_spetsialmznostmz_na_kazakhskom.Visible:=false;
  Check_abbreviatura_na_kazakhskom.Checked:=false;
  Check_abbreviatura_na_kazakhskom.Visible:=false;

  Edit_spetsialmznostmz_na_russkom.Clear;
  Edit_abbreviatura_na_russkom.Clear;
  Edit_spetsialmznostmz_na_kazakhskom.Clear;
  Edit_abbreviatura_na_kazakhskom.Clear;

  Edit_spetsialmznostmz_na_russkom.Setfocus;

end;

procedure TfSPETSIALMZNOSTMZ_RECORDf.FormActivated;
begin

  CLALL;

if pActionSPETSIALMZNOSTMZ='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionSPETSIALMZNOSTMZ='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from spetsialmznostmz where id='+pID);
  ADQ.Open;
  ADQ.First;

  Edit_spetsialmznostmz_na_russkom.Text:=ADQ.fieldbyname('spetsialmznostmz_na_russkom').AsString;
  Edit_abbreviatura_na_russkom.Text:=ADQ.fieldbyname('abbreviatura_na_russkom').AsString;
  Edit_spetsialmznostmz_na_kazakhskom.Text:=ADQ.fieldbyname('spetsialmznostmz_na_kazakhskom').AsString;
  Edit_abbreviatura_na_kazakhskom.Text:=ADQ.fieldbyname('abbreviatura_na_kazakhskom').AsString;

end;

if pActionSPETSIALMZNOSTMZ='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_spetsialmznostmz_na_russkom.Visible:=true;
  Check_abbreviatura_na_russkom.Visible:=true;
  Check_spetsialmznostmz_na_kazakhskom.Visible:=true;
  Check_abbreviatura_na_kazakhskom.Visible:=true;

end;

end;

procedure TfSPETSIALMZNOSTMZ_RECORDf.BtnOK;
begin

if pActionSPETSIALMZNOSTMZ='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'spetsialmznostmz '+
  '('+
  'spetsialmznostmz_na_russkom,'+
  'abbreviatura_na_russkom,'+
  'spetsialmznostmz_na_kazakhskom,'+
  'abbreviatura_na_kazakhskom,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+trim(Edit_spetsialmznostmz_na_russkom.Text)+''', '+
  ' '''+trim(Edit_abbreviatura_na_russkom.Text)+''', '+
  ' '''+trim(Edit_spetsialmznostmz_na_kazakhskom.Text)+''', '+
  ' '''+trim(Edit_abbreviatura_na_kazakhskom.Text)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionSPETSIALMZNOSTMZ='UPDATE' then begin

 CheckInput;

 pS:='UPDATE '+
  'spetsialmznostmz set ' +
  'spetsialmznostmz_na_russkom='''+trim(Edit_spetsialmznostmz_na_russkom.Text)+''','+
  'abbreviatura_na_russkom='''+trim(Edit_abbreviatura_na_russkom.Text)+''','+
  'spetsialmznostmz_na_kazakhskom='''+trim(Edit_spetsialmznostmz_na_kazakhskom.Text)+''','+
  'abbreviatura_na_kazakhskom='''+trim(Edit_abbreviatura_na_kazakhskom.Text)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fSPETSIALMZNOSTMZf.ListView1.Selected.SubItems[0]:=trim(Edit_spetsialmznostmz_na_russkom.Text);
   fSPETSIALMZNOSTMZf.ListView1.Selected.SubItems[1]:=trim(Edit_abbreviatura_na_russkom.Text);
   fSPETSIALMZNOSTMZf.ListView1.Selected.SubItems[2]:=trim(Edit_spetsialmznostmz_na_kazakhskom.Text);
   fSPETSIALMZNOSTMZf.ListView1.Selected.SubItems[3]:=trim(Edit_abbreviatura_na_kazakhskom.Text);

end;

if pActionSPETSIALMZNOSTMZ='SEARCH' then begin

if (Check_spetsialmznostmz_na_russkom.Checked=false) and (Check_abbreviatura_na_russkom.Checked=false) and (Check_spetsialmznostmz_na_kazakhskom.Checked=false) and (Check_abbreviatura_na_kazakhskom.Checked=false) then abort;

   pSearch:='';

   if Check_spetsialmznostmz_na_russkom.Checked=true then pSearch:=pSearch+' and spetsialmznostmz.spetsialmznostmz_na_russkom like '''+trim(Edit_spetsialmznostmz_na_russkom.Text)+''' ';
   if Check_abbreviatura_na_russkom.Checked=true then pSearch:=pSearch+' and spetsialmznostmz.abbreviatura_na_russkom like '''+trim(Edit_abbreviatura_na_russkom.Text)+''' ';
   if Check_spetsialmznostmz_na_kazakhskom.Checked=true then pSearch:=pSearch+' and spetsialmznostmz.spetsialmznostmz_na_kazakhskom like '''+trim(Edit_spetsialmznostmz_na_kazakhskom.Text)+''' ';
   if Check_abbreviatura_na_kazakhskom.Checked=true then pSearch:=pSearch+' and spetsialmznostmz.abbreviatura_na_kazakhskom like '''+trim(Edit_abbreviatura_na_kazakhskom.Text)+''' ';

end;

Close;

end;

procedure TfSPETSIALMZNOSTMZ_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfSPETSIALMZNOSTMZ_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfSPETSIALMZNOSTMZ_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfSPETSIALMZNOSTMZ_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.