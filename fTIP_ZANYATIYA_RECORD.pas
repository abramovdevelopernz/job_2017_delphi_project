unit fTIP_ZANYATIYA_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfTIP_ZANYATIYA_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_tip_zanyatiya: TLabel;

    Edit_tip_zanyatiya: TEdit;

    Check_tip_zanyatiya: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fTIP_ZANYATIYA_RECORDf: TfTIP_ZANYATIYA_RECORDf;

implementation

uses  
    fTIP_ZANYATIYA,
    fMW;

{$R *.dfm}

procedure TfTIP_ZANYATIYA_RECORDf.CheckInput;
begin

if trim(Edit_tip_zanyatiya.Text)='' then begin
  Edit_tip_zanyatiya.SetFocus;
  abort;
end;

end;
procedure TfTIP_ZANYATIYA_RECORDf.CLALL;
begin


  Check_tip_zanyatiya.Checked:=false;
  Check_tip_zanyatiya.Visible:=false;

  Edit_tip_zanyatiya.Clear;

  Edit_tip_zanyatiya.Setfocus;

end;

procedure TfTIP_ZANYATIYA_RECORDf.FormActivated;
begin

  CLALL;

if pActionTIP_ZANYATIYA='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionTIP_ZANYATIYA='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from tip_zanyatiya where id='+pID);
  ADQ.Open;
  ADQ.First;

  Edit_tip_zanyatiya.Text:=ADQ.fieldbyname('tip_zanyatiya').AsString;

end;

if pActionTIP_ZANYATIYA='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_tip_zanyatiya.Visible:=true;

end;

end;

procedure TfTIP_ZANYATIYA_RECORDf.BtnOK;
begin

if pActionTIP_ZANYATIYA='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'tip_zanyatiya '+
  '('+
  'tip_zanyatiya,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+trim(Edit_tip_zanyatiya.Text)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionTIP_ZANYATIYA='UPDATE' then begin

 CheckInput;

 pS:='UPDATE '+
  'tip_zanyatiya set ' +
  'tip_zanyatiya='''+trim(Edit_tip_zanyatiya.Text)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fTIP_ZANYATIYAf.ListView1.Selected.SubItems[0]:=trim(Edit_tip_zanyatiya.Text);

end;

if pActionTIP_ZANYATIYA='SEARCH' then begin

if (Check_tip_zanyatiya.Checked=false) then abort;

   pSearch:='';

   if Check_tip_zanyatiya.Checked=true then pSearch:=pSearch+' and tip_zanyatiya.tip_zanyatiya like '''+trim(Edit_tip_zanyatiya.Text)+''' ';

end;

Close;

end;

procedure TfTIP_ZANYATIYA_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfTIP_ZANYATIYA_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfTIP_ZANYATIYA_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfTIP_ZANYATIYA_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.