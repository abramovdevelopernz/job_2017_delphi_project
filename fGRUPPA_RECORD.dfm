object fGRUPPA_RECORDf: TfGRUPPA_RECORDf
  Left = 544
  Top = 203
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  ClientHeight = 243
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 195
    Width = 472
    Height = 48
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = 'OK'
      TabOrder = 0
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 472
    Height = 195
    Align = alClient
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label_spetsialmznostmz_id: TLabel
      Left = 56
      Top = 16
      Width = 78
      Height = 13
      Caption = #1057#1087#1077#1094#1080#1072#1083#1100#1085#1086#1089#1090#1100
    end
    object Label_god_nachala_obucheniya: TLabel
      Left = 29
      Top = 40
      Width = 105
      Height = 13
      Caption = #1043#1086#1076' '#1085#1072#1095#1072#1083#1072' '#1086#1073#1091#1095#1077#1085#1080#1103
    end
    object Label_nazvanie_gruppy: TLabel
      Left = 45
      Top = 64
      Width = 89
      Height = 13
      Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1075#1088#1091#1087#1087#1099
    end
    object Label_vecherniy: TLabel
      Left = 86
      Top = 88
      Width = 48
      Height = 13
      Caption = #1042#1077#1095#1077#1088#1085#1080#1081
    end
    object Label_yazyk_obucheniya_id: TLabel
      Left = 57
      Top = 112
      Width = 77
      Height = 13
      Caption = #1071#1079#1099#1082' '#1086#1073#1091#1095#1077#1085#1080#1103
    end
    object Label_kolichestvo_studentov: TLabel
      Left = 21
      Top = 136
      Width = 113
      Height = 13
      Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1089#1090#1091#1076#1077#1085#1090#1086#1074
    end
    object Label1: TLabel
      Left = 9
      Top = 160
      Width = 129
      Height = 13
      Caption = #1060#1080#1085#1072#1085#1089#1080#1088#1086#1074#1072#1085#1080#1077' '#1079#1072' '#1089#1095#1077#1090
    end
    object Check_spetsialmznostmz_id: TCheckBox
      Left = 448
      Top = 16
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 0
    end
    object Check_god_nachala_obucheniya: TCheckBox
      Left = 448
      Top = 40
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 1
    end
    object Check_nazvanie_gruppy: TCheckBox
      Left = 448
      Top = 64
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 2
    end
    object Check_vecherniy: TCheckBox
      Left = 448
      Top = 88
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 3
    end
    object Check_yazyk_obucheniya_id: TCheckBox
      Left = 448
      Top = 112
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 4
    end
    object Check_kolichestvo_studentov: TCheckBox
      Left = 448
      Top = 136
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 6
    end
    object Combo_spetsialmznostmz_id: TComboBox
      Left = 144
      Top = 16
      Width = 265
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 5
    end
    object Button_spetsialmznostmz_id: TButton
      Left = 416
      Top = 16
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 7
      OnClick = Button_spetsialmznostmz_idClick
    end
    object Edit_god_nachala_obucheniya: TEdit
      Left = 144
      Top = 40
      Width = 297
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 8
      Text = 'Edit_god_nachala_obucheniya'
      OnKeyPress = Edit_god_nachala_obucheniyaKeyPress
    end
    object Edit_nazvanie_gruppy: TEdit
      Left = 144
      Top = 64
      Width = 297
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 9
      Text = 'Edit_nazvanie_gruppy'
    end
    object CheckBox_vecherniy: TCheckBox
      Left = 144
      Top = 88
      Width = 17
      Height = 17
      Cursor = crHandPoint
      TabOrder = 10
    end
    object Combo_yazyk_obucheniya_id: TComboBox
      Left = 144
      Top = 112
      Width = 265
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 11
    end
    object Button_yazyk_obucheniya_id: TButton
      Left = 416
      Top = 112
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 12
      OnClick = Button_yazyk_obucheniya_idClick
    end
    object Edit_kolichestvo_studentov: TEdit
      Left = 144
      Top = 136
      Width = 297
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 13
      Text = 'Edit_kolichestvo_studentov'
      OnKeyPress = Edit_kolichestvo_studentovKeyPress
    end
    object Combo_za: TComboBox
      Left = 144
      Top = 160
      Width = 297
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 14
      Items.Strings = (
        #1041#1070#1044#1046#1045#1058
        #1044#1054#1043#1054#1042#1054#1056)
    end
  end
end
