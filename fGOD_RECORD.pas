unit fGOD_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfGOD_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_god: TLabel;

    Edit_god: TEdit;

    Check_god: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fGOD_RECORDf: TfGOD_RECORDf;

implementation

uses  
    fGOD,
    fMW;

{$R *.dfm}

procedure TfGOD_RECORDf.CheckInput;
begin

if trim(Edit_god.Text)='' then begin
  Edit_god.SetFocus;
  abort;
end;

end;
procedure TfGOD_RECORDf.CLALL;
begin


  Check_god.Checked:=false;
  Check_god.Visible:=false;

  Edit_god.Clear;

  Edit_god.Setfocus;

end;

procedure TfGOD_RECORDf.FormActivated;
begin

  CLALL;

if pActionGOD='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionGOD='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from god where id='+pID);
  ADQ.Open;
  ADQ.First;

  Edit_god.Text:=ADQ.fieldbyname('god').AsString;

end;

if pActionGOD='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_god.Visible:=true;

end;

end;

procedure TfGOD_RECORDf.BtnOK;
begin

if pActionGOD='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'god '+
  '('+
  'god,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+trim(Edit_god.Text)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionGOD='UPDATE' then begin

 CheckInput;

 pS:='UPDATE '+
  'god set ' +
  'god='''+trim(Edit_god.Text)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fGODf.ListView1.Selected.SubItems[0]:=trim(Edit_god.Text);

end;

if pActionGOD='SEARCH' then begin

if (Check_god.Checked=false) then abort;

   pSearch:='';

   if Check_god.Checked=true then pSearch:=pSearch+' and god.god like '''+trim(Edit_god.Text)+''' ';

end;

Close;

end;

procedure TfGOD_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfGOD_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfGOD_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfGOD_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.