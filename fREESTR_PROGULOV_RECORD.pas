unit fREESTR_PROGULOV_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfREESTR_PROGULOV_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_god_id: TLabel;
    Label_polugodie_id: TLabel;
    Label_data_progula: TLabel;
    Label_para_id: TLabel;
    Label_nomer_stroki: TLabel;
    Label_prepodavatelmz_id: TLabel;
    Label_distsiplina_id: TLabel;
    Label_podgruppa_id: TLabel;
    Label_tip_zanyatiya_id: TLabel;
    Label_kabinet_id: TLabel;
    Label_prichina_progula: TLabel;

    Combo_god_id: TComboBox;
    Button_god_id: TButton;
    Combo_polugodie_id: TComboBox;
    Button_polugodie_id: TButton;
    DateTime_data_progula: TDateTimePicker;
    Combo_para_id: TComboBox;
    Button_para_id: TButton;
    Edit_nomer_stroki: TEdit;
    Combo_prepodavatelmz_id: TComboBox;
    Button_prepodavatelmz_id: TButton;
    Combo_distsiplina_id: TComboBox;
    Button_distsiplina_id: TButton;
    Combo_podgruppa_id: TComboBox;
    Button_podgruppa_id: TButton;
    Combo_tip_zanyatiya_id: TComboBox;
    Button_tip_zanyatiya_id: TButton;
    Combo_kabinet_id: TComboBox;
    Button_kabinet_id: TButton;
    Edit_prichina_progula: TEdit;

    Check_god_id: TCheckBox;
    Check_polugodie_id: TCheckBox;
    Check_data_progula: TCheckBox;
    Check_para_id: TCheckBox;
    Check_nomer_stroki: TCheckBox;
    Check_prepodavatelmz_id: TCheckBox;
    Check_distsiplina_id: TCheckBox;
    Check_podgruppa_id: TCheckBox;
    Check_tip_zanyatiya_id: TCheckBox;
    Check_kabinet_id: TCheckBox;
    Check_prichina_progula: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;
    procedure Loadgod;
    procedure Getgod(pCombo:TComboBox;pSID:integer);
    procedure Button_god_idClick(Sender: TObject);

    procedure Loadpolugodie;
    procedure Getpolugodie(pCombo:TComboBox;pSID:integer);
    procedure Button_polugodie_idClick(Sender: TObject);

    procedure Loadpara;
    procedure Getpara(pCombo:TComboBox;pSID:integer);
    procedure Button_para_idClick(Sender: TObject);

    procedure Loadprepodavatelmz;
    procedure Getprepodavatelmz(pCombo:TComboBox;pSID:integer);
    procedure Button_prepodavatelmz_idClick(Sender: TObject);

    procedure Loaddistsiplina;
    procedure Getdistsiplina(pCombo:TComboBox;pSID:integer);
    procedure Button_distsiplina_idClick(Sender: TObject);

    procedure Loadpodgruppa;
    procedure Getpodgruppa(pCombo:TComboBox;pSID:integer);
    procedure Button_podgruppa_idClick(Sender: TObject);

    procedure Loadtip_zanyatiya;
    procedure Gettip_zanyatiya(pCombo:TComboBox;pSID:integer);
    procedure Button_tip_zanyatiya_idClick(Sender: TObject);

    procedure Loadkabinet;
    procedure Getkabinet(pCombo:TComboBox;pSID:integer);
    procedure Button_kabinet_idClick(Sender: TObject);


    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fREESTR_PROGULOV_RECORDf: TfREESTR_PROGULOV_RECORDf;

implementation

uses  
    fREESTR_PROGULOV,
    fgod,
    fpolugodie,
    fpara,
    fprepodavatelmz,
    fdistsiplina,
    fpodgruppa,
    ftip_zanyatiya,
    fkabinet,
    fMW;

{$R *.dfm}

procedure TfREESTR_PROGULOV_RECORDf.Getgod(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfREESTR_PROGULOV_RECORDf.Loadgod;
begin

Combo_god_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from god where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_god_id.AddItem(ADQ.FieldByName('god').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_god_id.ItemIndex:=0;

end;

procedure TfREESTR_PROGULOV_RECORDf.Button_god_idClick(Sender: TObject);
begin

  fgodf.Caption:='����� ���';
  if Combo_god_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_god_id.Items.Objects[Combo_god_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fgodf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='REESTR_PROGULOV';

  if pNoAction=false then begin
    Loadgod;
    if pSelectedID<>'' then Getgod(Combo_god_id,strtoint(pSelectedID));
  end;

end;

procedure TfREESTR_PROGULOV_RECORDf.Getpolugodie(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfREESTR_PROGULOV_RECORDf.Loadpolugodie;
begin

Combo_polugodie_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from polugodie where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_polugodie_id.AddItem(ADQ.FieldByName('polugodie').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_polugodie_id.ItemIndex:=0;

end;

procedure TfREESTR_PROGULOV_RECORDf.Button_polugodie_idClick(Sender: TObject);
begin

  fpolugodief.Caption:='����� ���������';
  if Combo_polugodie_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_polugodie_id.Items.Objects[Combo_polugodie_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fpolugodief.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='REESTR_PROGULOV';

  if pNoAction=false then begin
    Loadpolugodie;
    if pSelectedID<>'' then Getpolugodie(Combo_polugodie_id,strtoint(pSelectedID));
  end;

end;

procedure TfREESTR_PROGULOV_RECORDf.Getpara(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfREESTR_PROGULOV_RECORDf.Loadpara;
begin

Combo_para_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from para where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_para_id.AddItem(ADQ.FieldByName('para').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_para_id.ItemIndex:=0;

end;

procedure TfREESTR_PROGULOV_RECORDf.Button_para_idClick(Sender: TObject);
begin

  fparaf.Caption:='����� ����';
  if Combo_para_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_para_id.Items.Objects[Combo_para_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fparaf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='REESTR_PROGULOV';

  if pNoAction=false then begin
    Loadpara;
    if pSelectedID<>'' then Getpara(Combo_para_id,strtoint(pSelectedID));
  end;

end;

procedure TfREESTR_PROGULOV_RECORDf.Getprepodavatelmz(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfREESTR_PROGULOV_RECORDf.Loadprepodavatelmz;
begin

Combo_prepodavatelmz_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from prepodavatelmz where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_prepodavatelmz_id.AddItem(ADQ.FieldByName('fio').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_prepodavatelmz_id.ItemIndex:=0;

end;

procedure TfREESTR_PROGULOV_RECORDf.Button_prepodavatelmz_idClick(Sender: TObject);
begin

  fprepodavatelmzf.Caption:='����� �������������';
  if Combo_prepodavatelmz_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fprepodavatelmzf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='REESTR_PROGULOV';

  if pNoAction=false then begin
    Loadprepodavatelmz;
    if pSelectedID<>'' then Getprepodavatelmz(Combo_prepodavatelmz_id,strtoint(pSelectedID));
  end;

end;

procedure TfREESTR_PROGULOV_RECORDf.Getdistsiplina(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfREESTR_PROGULOV_RECORDf.Loaddistsiplina;
begin

Combo_distsiplina_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from distsiplina where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_distsiplina_id.AddItem(ADQ.FieldByName('distsiplina_na_russkom').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_distsiplina_id.ItemIndex:=0;

end;

procedure TfREESTR_PROGULOV_RECORDf.Button_distsiplina_idClick(Sender: TObject);
begin

  fdistsiplinaf.Caption:='����� ����������';
  if Combo_distsiplina_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_distsiplina_id.Items.Objects[Combo_distsiplina_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fdistsiplinaf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='REESTR_PROGULOV';

  if pNoAction=false then begin
    Loaddistsiplina;
    if pSelectedID<>'' then Getdistsiplina(Combo_distsiplina_id,strtoint(pSelectedID));
  end;

end;

procedure TfREESTR_PROGULOV_RECORDf.Getpodgruppa(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfREESTR_PROGULOV_RECORDf.Loadpodgruppa;
begin

Combo_podgruppa_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from podgruppa where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_podgruppa_id.AddItem(ADQ.FieldByName('podgruppa').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_podgruppa_id.ItemIndex:=0;

end;

procedure TfREESTR_PROGULOV_RECORDf.Button_podgruppa_idClick(Sender: TObject);
begin

  fpodgruppaf.Caption:='����� ���������';
  if Combo_podgruppa_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_podgruppa_id.Items.Objects[Combo_podgruppa_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fpodgruppaf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='REESTR_PROGULOV';

  if pNoAction=false then begin
    Loadpodgruppa;
    if pSelectedID<>'' then Getpodgruppa(Combo_podgruppa_id,strtoint(pSelectedID));
  end;

end;

procedure TfREESTR_PROGULOV_RECORDf.Gettip_zanyatiya(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfREESTR_PROGULOV_RECORDf.Loadtip_zanyatiya;
begin

Combo_tip_zanyatiya_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from tip_zanyatiya where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_tip_zanyatiya_id.AddItem(ADQ.FieldByName('tip_zanyatiya').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_tip_zanyatiya_id.ItemIndex:=0;

end;

procedure TfREESTR_PROGULOV_RECORDf.Button_tip_zanyatiya_idClick(Sender: TObject);
begin

  ftip_zanyatiyaf.Caption:='����� ��� �������';
  if Combo_tip_zanyatiya_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_tip_zanyatiya_id.Items.Objects[Combo_tip_zanyatiya_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  ftip_zanyatiyaf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='REESTR_PROGULOV';

  if pNoAction=false then begin
    Loadtip_zanyatiya;
    if pSelectedID<>'' then Gettip_zanyatiya(Combo_tip_zanyatiya_id,strtoint(pSelectedID));
  end;

end;

procedure TfREESTR_PROGULOV_RECORDf.Getkabinet(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfREESTR_PROGULOV_RECORDf.Loadkabinet;
begin

Combo_kabinet_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from kabinet where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_kabinet_id.AddItem(ADQ.FieldByName('nomer_kabineta').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_kabinet_id.ItemIndex:=0;

end;

procedure TfREESTR_PROGULOV_RECORDf.Button_kabinet_idClick(Sender: TObject);
begin

  fkabinetf.Caption:='����� �������';
  if Combo_kabinet_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_kabinet_id.Items.Objects[Combo_kabinet_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fkabinetf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='REESTR_PROGULOV';

  if pNoAction=false then begin
    Loadkabinet;
    if pSelectedID<>'' then Getkabinet(Combo_kabinet_id,strtoint(pSelectedID));
  end;

end;

procedure TfREESTR_PROGULOV_RECORDf.CheckInput;
begin

if trim(Combo_god_id.Text)='' then begin
  Combo_god_id.SetFocus;
  abort;
end;

if trim(Combo_polugodie_id.Text)='' then begin
  Combo_polugodie_id.SetFocus;
  abort;
end;

if trim(Combo_para_id.Text)='' then begin
  Combo_para_id.SetFocus;
  abort;
end;

if trim(Combo_prepodavatelmz_id.Text)='' then begin
  Combo_prepodavatelmz_id.SetFocus;
  abort;
end;

if trim(Combo_distsiplina_id.Text)='' then begin
  Combo_distsiplina_id.SetFocus;
  abort;
end;

if trim(Combo_podgruppa_id.Text)='' then begin
  Combo_podgruppa_id.SetFocus;
  abort;
end;

if trim(Combo_tip_zanyatiya_id.Text)='' then begin
  Combo_tip_zanyatiya_id.SetFocus;
  abort;
end;

if trim(Combo_kabinet_id.Text)='' then begin
  Combo_kabinet_id.SetFocus;
  abort;
end;

end;
procedure TfREESTR_PROGULOV_RECORDf.CLALL;
begin

  Loadgod;
  Loadpolugodie;
  Loadpara;
  Loadprepodavatelmz;
  Loaddistsiplina;
  Loadpodgruppa;
  Loadtip_zanyatiya;
  Loadkabinet;

  Check_god_id.Checked:=false;
  Check_god_id.Visible:=false;
  Check_polugodie_id.Checked:=false;
  Check_polugodie_id.Visible:=false;
  Check_data_progula.Checked:=false;
  Check_data_progula.Visible:=false;
  Check_para_id.Checked:=false;
  Check_para_id.Visible:=false;
  Check_nomer_stroki.Checked:=false;
  Check_nomer_stroki.Visible:=false;
  Check_prepodavatelmz_id.Checked:=false;
  Check_prepodavatelmz_id.Visible:=false;
  Check_distsiplina_id.Checked:=false;
  Check_distsiplina_id.Visible:=false;
  Check_podgruppa_id.Checked:=false;
  Check_podgruppa_id.Visible:=false;
  Check_tip_zanyatiya_id.Checked:=false;
  Check_tip_zanyatiya_id.Visible:=false;
  Check_kabinet_id.Checked:=false;
  Check_kabinet_id.Visible:=false;
  Check_prichina_progula.Checked:=false;
  Check_prichina_progula.Visible:=false;

  DateTime_data_progula.Date:=now;
  Edit_nomer_stroki.Clear;
  Edit_prichina_progula.Clear;

  Combo_god_id.Setfocus;

end;

procedure TfREESTR_PROGULOV_RECORDf.FormActivated;
begin

  CLALL;

if pActionREESTR_PROGULOV='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionREESTR_PROGULOV='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from REESTR_PROGULOV where id='+pID);
  ADQ.Open;
  ADQ.First;

  Getgod(Combo_god_id,ADQ.fieldbyname('god_id').AsInteger);
  Getpolugodie(Combo_polugodie_id,ADQ.fieldbyname('polugodie_id').AsInteger);
  DateTime_data_progula.Date:=ADQ.fieldbyname('data_progula').AsDateTime;
  Getpara(Combo_para_id,ADQ.fieldbyname('para_id').AsInteger);
  Edit_nomer_stroki.Text:=ADQ.fieldbyname('nomer_stroki').AsString;
  Getprepodavatelmz(Combo_prepodavatelmz_id,ADQ.fieldbyname('prepodavatelmz_id').AsInteger);
  Getdistsiplina(Combo_distsiplina_id,ADQ.fieldbyname('distsiplina_id').AsInteger);
  Getpodgruppa(Combo_podgruppa_id,ADQ.fieldbyname('podgruppa_id').AsInteger);
  Gettip_zanyatiya(Combo_tip_zanyatiya_id,ADQ.fieldbyname('tip_zanyatiya_id').AsInteger);
  Getkabinet(Combo_kabinet_id,ADQ.fieldbyname('kabinet_id').AsInteger);
  Edit_prichina_progula.Text:=ADQ.fieldbyname('prichina_progula').AsString;

end;

if pActionREESTR_PROGULOV='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_god_id.Visible:=true;
  Check_polugodie_id.Visible:=true;
  Check_data_progula.Visible:=true;
  Check_para_id.Visible:=true;
  Check_nomer_stroki.Visible:=true;
  Check_prepodavatelmz_id.Visible:=true;
  Check_distsiplina_id.Visible:=true;
  Check_podgruppa_id.Visible:=true;
  Check_tip_zanyatiya_id.Visible:=true;
  Check_kabinet_id.Visible:=true;
  Check_prichina_progula.Visible:=true;

end;

end;

procedure TfREESTR_PROGULOV_RECORDf.BtnOK;
begin

if pActionREESTR_PROGULOV='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'REESTR_PROGULOV '+
  '('+
  'god_id,'+
  'polugodie_id,'+
  'data_progula,'+
  'para_id,'+
  'nomer_stroki,'+
  'prepodavatelmz_id,'+
  'distsiplina_id,'+
  'podgruppa_id,'+
  'tip_zanyatiya_id,'+
  'kabinet_id,'+
  'prichina_progula,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+Inttostr(Integer(Combo_god_id.Items.Objects[Combo_god_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_polugodie_id.Items.Objects[Combo_polugodie_id.ItemIndex]))+''', '+
  ' '''+datetostr(DateTime_data_progula.Date)+''', '+
  ' '''+Inttostr(Integer(Combo_para_id.Items.Objects[Combo_para_id.ItemIndex]))+''', '+
  ' '''+trim(Edit_nomer_stroki.Text)+''', '+
  ' '''+Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_distsiplina_id.Items.Objects[Combo_distsiplina_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_podgruppa_id.Items.Objects[Combo_podgruppa_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_tip_zanyatiya_id.Items.Objects[Combo_tip_zanyatiya_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_kabinet_id.Items.Objects[Combo_kabinet_id.ItemIndex]))+''', '+
  ' '''+trim(Edit_prichina_progula.Text)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionREESTR_PROGULOV='UPDATE' then begin


 pS:='UPDATE '+
  'REESTR_PROGULOV set ' +
  'god_id='''+Inttostr(Integer(Combo_god_id.Items.Objects[Combo_god_id.ItemIndex]))+''','+
  'polugodie_id='''+Inttostr(Integer(Combo_polugodie_id.Items.Objects[Combo_polugodie_id.ItemIndex]))+''','+
  'data_progula='''+datetostr(DateTime_data_progula.date)+''','+
  'para_id='''+Inttostr(Integer(Combo_para_id.Items.Objects[Combo_para_id.ItemIndex]))+''','+
  'nomer_stroki='''+trim(Edit_nomer_stroki.Text)+''','+
  'prepodavatelmz_id='''+Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+''','+
  'distsiplina_id='''+Inttostr(Integer(Combo_distsiplina_id.Items.Objects[Combo_distsiplina_id.ItemIndex]))+''','+
  'podgruppa_id='''+Inttostr(Integer(Combo_podgruppa_id.Items.Objects[Combo_podgruppa_id.ItemIndex]))+''','+
  'tip_zanyatiya_id='''+Inttostr(Integer(Combo_tip_zanyatiya_id.Items.Objects[Combo_tip_zanyatiya_id.ItemIndex]))+''','+
  'kabinet_id='''+Inttostr(Integer(Combo_kabinet_id.Items.Objects[Combo_kabinet_id.ItemIndex]))+''','+
  'prichina_progula='''+trim(Edit_prichina_progula.Text)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fREESTR_PROGULOVf.ListView1.Selected.SubItems[0]:=trim(Combo_god_id.Text);
   fREESTR_PROGULOVf.ListView1.Selected.SubItems[1]:=trim(Combo_polugodie_id.Text);
   fREESTR_PROGULOVf.ListView1.Selected.SubItems[2]:=datetostr(DateTime_data_progula.date);
   fREESTR_PROGULOVf.ListView1.Selected.SubItems[3]:=trim(Combo_para_id.Text);
   fREESTR_PROGULOVf.ListView1.Selected.SubItems[4]:=trim(Edit_nomer_stroki.Text);
   fREESTR_PROGULOVf.ListView1.Selected.SubItems[5]:=trim(Combo_prepodavatelmz_id.Text);
   fREESTR_PROGULOVf.ListView1.Selected.SubItems[6]:=trim(Combo_distsiplina_id.Text);
   fREESTR_PROGULOVf.ListView1.Selected.SubItems[7]:=trim(Combo_podgruppa_id.Text);
   fREESTR_PROGULOVf.ListView1.Selected.SubItems[8]:=trim(Combo_tip_zanyatiya_id.Text);
   fREESTR_PROGULOVf.ListView1.Selected.SubItems[9]:=trim(Combo_kabinet_id.Text);
   fREESTR_PROGULOVf.ListView1.Selected.SubItems[10]:=trim(Edit_prichina_progula.Text);

end;

if pActionREESTR_PROGULOV='SEARCH' then begin

if (Check_god_id.Checked=false) and (Check_polugodie_id.Checked=false) and (Check_data_progula.Checked=false) and (Check_para_id.Checked=false) and (Check_nomer_stroki.Checked=false) and (Check_prepodavatelmz_id.Checked=false) and (Check_distsiplina_id.Checked=false) and (Check_podgruppa_id.Checked=false) and (Check_tip_zanyatiya_id.Checked=false) and (Check_kabinet_id.Checked=false) and (Check_prichina_progula.Checked=false) then abort;

   pSearch:='';

   if Check_god_id.Checked=true then pSearch:=pSearch+' and REESTR_PROGULOV.god_id = '+Inttostr(Integer(Combo_god_id.Items.Objects[Combo_god_id.ItemIndex]))+' ';
   if Check_polugodie_id.Checked=true then pSearch:=pSearch+' and REESTR_PROGULOV.polugodie_id = '+Inttostr(Integer(Combo_polugodie_id.Items.Objects[Combo_polugodie_id.ItemIndex]))+' ';
   if Check_data_progula.Checked=true then pSearch:=pSearch+' and REESTR_PROGULOV.data_progula = #'+FormatDateTime('YYYY-MM-DD',DateTime_data_progula.Date)+'#';
   if Check_para_id.Checked=true then pSearch:=pSearch+' and REESTR_PROGULOV.para_id = '+Inttostr(Integer(Combo_para_id.Items.Objects[Combo_para_id.ItemIndex]))+' ';
   if Check_nomer_stroki.Checked=true then pSearch:=pSearch+' and REESTR_PROGULOV.nomer_stroki like '''+trim(Edit_nomer_stroki.Text)+''' ';
   if Check_prepodavatelmz_id.Checked=true then pSearch:=pSearch+' and REESTR_PROGULOV.prepodavatelmz_id = '+Inttostr(Integer(Combo_prepodavatelmz_id.Items.Objects[Combo_prepodavatelmz_id.ItemIndex]))+' ';
   if Check_distsiplina_id.Checked=true then pSearch:=pSearch+' and REESTR_PROGULOV.distsiplina_id = '+Inttostr(Integer(Combo_distsiplina_id.Items.Objects[Combo_distsiplina_id.ItemIndex]))+' ';
   if Check_podgruppa_id.Checked=true then pSearch:=pSearch+' and REESTR_PROGULOV.podgruppa_id = '+Inttostr(Integer(Combo_podgruppa_id.Items.Objects[Combo_podgruppa_id.ItemIndex]))+' ';
   if Check_tip_zanyatiya_id.Checked=true then pSearch:=pSearch+' and REESTR_PROGULOV.tip_zanyatiya_id = '+Inttostr(Integer(Combo_tip_zanyatiya_id.Items.Objects[Combo_tip_zanyatiya_id.ItemIndex]))+' ';
   if Check_kabinet_id.Checked=true then pSearch:=pSearch+' and REESTR_PROGULOV.kabinet_id = '+Inttostr(Integer(Combo_kabinet_id.Items.Objects[Combo_kabinet_id.ItemIndex]))+' ';
   if Check_prichina_progula.Checked=true then pSearch:=pSearch+' and REESTR_PROGULOV.prichina_progula like '''+trim(Edit_prichina_progula.Text)+''' ';

end;

Close;

end;

procedure TfREESTR_PROGULOV_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfREESTR_PROGULOV_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfREESTR_PROGULOV_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfREESTR_PROGULOV_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.