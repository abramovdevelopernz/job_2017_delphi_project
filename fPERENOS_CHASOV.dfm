object fPERENOS_CHASOVf: TfPERENOS_CHASOVf
  Left = 554
  Top = 89
  Width = 315
  Height = 412
  BorderIcons = []
  Caption = #1055#1077#1088#1077#1085#1086#1089' '#1095#1072#1089#1086#1074' '#1085#1072' '#1076#1088#1091#1075#1086#1075#1086' '#1087#1088#1077#1087#1086#1076#1072#1074#1072#1090#1077#1083#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 340
    Width = 307
    Height = 45
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWindow
    TabOrder = 0
    object Button1: TButton
      Left = 16
      Top = 8
      Width = 121
      Height = 33
      Cursor = crHandPoint
      Caption = #1055#1077#1088#1077#1085#1077#1089#1090#1080' '#1095#1072#1089#1099
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 224
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 307
    Height = 340
    Align = alClient
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 72
      Top = 8
      Width = 130
      Height = 13
      Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1087#1088#1077#1087#1086#1076#1072#1074#1072#1090#1077#1083#1103
    end
    object Label2: TLabel
      Left = 14
      Top = 152
      Width = 100
      Height = 13
      Caption = #1058#1077#1086#1088#1080#1080' '#1095#1072#1089#1086#1074' '#1080#1090#1086#1075#1086
    end
    object Label3: TLabel
      Left = 14
      Top = 176
      Width = 112
      Height = 13
      Caption = #1055#1088#1072#1082#1090#1080#1082#1080' '#1095#1072#1089#1086#1074' '#1080#1090#1086#1075#1086
    end
    object Label4: TLabel
      Left = 14
      Top = 104
      Width = 60
      Height = 13
      Caption = #1044#1083#1103' '#1075#1088#1091#1087#1087#1099
    end
    object Label5: TLabel
      Left = 14
      Top = 128
      Width = 78
      Height = 13
      Caption = #1044#1083#1103' '#1087#1086#1076#1075#1088#1091#1087#1087#1099
    end
    object Label6: TLabel
      Left = 14
      Top = 80
      Width = 63
      Height = 13
      Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1072
    end
    object Label7: TLabel
      Left = 64
      Top = 24
      Width = 151
      Height = 13
      Caption = #1085#1072' '#1082#1086#1090#1086#1088#1086#1075#1086' '#1087#1077#1088#1077#1085#1086#1089#1080#1090#1077' '#1095#1072#1089#1099
    end
    object Label8: TLabel
      Left = 14
      Top = 200
      Width = 150
      Height = 13
      Caption = #1042#1099#1095#1080#1090#1072#1085#1086' '#1090#1077#1086#1088#1080#1080' '#1095#1072#1089#1086#1074' '#1080#1090#1086#1075#1086
    end
    object Label9: TLabel
      Left = 14
      Top = 224
      Width = 162
      Height = 13
      Caption = #1042#1099#1095#1080#1090#1072#1085#1086' '#1087#1088#1072#1082#1090#1080#1082#1080' '#1095#1072#1089#1086#1074' '#1080#1090#1086#1075#1086
    end
    object Combo_prepodavatelmz: TComboBox
      Left = 16
      Top = 48
      Width = 281
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
    end
    object GroupBox1: TGroupBox
      Left = 16
      Top = 248
      Width = 281
      Height = 81
      Caption = #1050' '#1087#1077#1088#1077#1085#1086#1089#1091
      TabOrder = 1
      object Label10: TLabel
        Left = 16
        Top = 24
        Width = 69
        Height = 13
        Caption = #1058#1077#1086#1088#1080#1080' '#1095#1072#1089#1086#1074
      end
      object Label11: TLabel
        Left = 16
        Top = 48
        Width = 81
        Height = 13
        Caption = #1055#1088#1072#1082#1090#1080#1082#1080' '#1095#1072#1089#1086#1074
      end
    end
  end
end
