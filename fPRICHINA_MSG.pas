unit fPRICHINA_MSG;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfPRICHINA_MSGf = class(TForm)
    Panel1: TPanel;
    Memo1: TMemo;
    Label1: TLabel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fPRICHINA_MSGf: TfPRICHINA_MSGf;

implementation
uses fMW;
{$R *.dfm}

procedure TfPRICHINA_MSGf.Button1Click(Sender: TObject);
begin
  pOtmena:=true;
  Close;
end;

procedure TfPRICHINA_MSGf.Button2Click(Sender: TObject);
begin
  pPRICHINA_MSG:=trim(Memo1.Text);
  pOtmena:=false;
  Close;
end;

procedure TfPRICHINA_MSGf.FormActivate(Sender: TObject);
begin
pOtmena:=true;
Memo1.Clear;
Memo1.SetFocus;
end;

end.
