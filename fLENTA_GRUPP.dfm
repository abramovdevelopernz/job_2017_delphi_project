object fLENTA_GRUPPf: TfLENTA_GRUPPf
  Left = 470
  Top = 275
  Width = 928
  Height = 480
  Caption = #1051#1077#1085#1090#1072' '#1075#1088#1091#1087#1087#1099
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 434
    Width = 920
    Height = 19
    Color = clWindow
    Panels = <
      item
        Width = 200
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 305
    Height = 434
    Align = alLeft
    BevelOuter = bvLowered
    Color = clWindow
    TabOrder = 1
    object Label1: TLabel
      Left = 15
      Top = 256
      Width = 35
      Height = 13
      Caption = #1043#1088#1091#1087#1087#1072
    end
    object Label2: TLabel
      Left = 15
      Top = 64
      Width = 26
      Height = 13
      Caption = #1044#1072#1090#1072
    end
    object Label3: TLabel
      Left = 15
      Top = 112
      Width = 26
      Height = 13
      Caption = #1055#1072#1088#1072
    end
    object Label4: TLabel
      Left = 15
      Top = 160
      Width = 63
      Height = 13
      Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1072
    end
    object Label5: TLabel
      Left = 15
      Top = 208
      Width = 79
      Height = 13
      Caption = #1055#1088#1077#1087#1086#1076#1072#1074#1072#1090#1077#1083#1100
    end
    object Label6: TLabel
      Left = 15
      Top = 304
      Width = 54
      Height = 13
      Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
    end
    object Label7: TLabel
      Left = 15
      Top = 344
      Width = 19
      Height = 13
      Caption = #1058#1080#1087
    end
    object Label8: TLabel
      Left = 15
      Top = 384
      Width = 42
      Height = 13
      Caption = #1050#1072#1073#1080#1085#1077#1090
    end
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button3: TButton
      Left = 88
      Top = 8
      Width = 73
      Height = 33
      Cursor = crHandPoint
      Caption = #1055#1077#1095#1072#1090#1100
      TabOrder = 1
      OnClick = Button3Click
    end
    object DateTimePicker1: TDateTimePicker
      Left = 15
      Top = 80
      Width = 260
      Height = 21
      Date = 42839.529448182870000000
      Time = 42839.529448182870000000
      TabOrder = 2
    end
    object Combo_distsiplina: TComboBox
      Left = 15
      Top = 176
      Width = 260
      Height = 22
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ItemHeight = 14
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
    end
    object Combo_prepodavatelmz: TComboBox
      Left = 15
      Top = 224
      Width = 260
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 4
    end
    object Combo_podgruppa: TComboBox
      Left = 15
      Top = 320
      Width = 260
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 5
    end
    object Combo_tip_zanyatiya: TComboBox
      Left = 15
      Top = 360
      Width = 260
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 6
    end
    object Combo_kabinet: TComboBox
      Left = 15
      Top = 400
      Width = 260
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 7
    end
    object Combo_para: TComboBox
      Left = 15
      Top = 128
      Width = 260
      Height = 22
      Style = csDropDownList
      CharCase = ecUpperCase
      Ctl3D = True
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ItemHeight = 14
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 8
    end
    object CheckBox1: TCheckBox
      Left = 280
      Top = 80
      Width = 17
      Height = 17
      Cursor = crHandPoint
      TabOrder = 9
    end
    object CheckBox2: TCheckBox
      Left = 280
      Top = 128
      Width = 17
      Height = 17
      Cursor = crHandPoint
      TabOrder = 10
    end
    object CheckBox3: TCheckBox
      Left = 280
      Top = 176
      Width = 17
      Height = 17
      Cursor = crHandPoint
      TabOrder = 11
    end
    object CheckBox4: TCheckBox
      Left = 280
      Top = 224
      Width = 17
      Height = 17
      Cursor = crHandPoint
      TabOrder = 12
    end
    object CheckBox5: TCheckBox
      Left = 280
      Top = 272
      Width = 17
      Height = 17
      Cursor = crHandPoint
      TabOrder = 13
    end
    object CheckBox6: TCheckBox
      Left = 280
      Top = 320
      Width = 17
      Height = 17
      Cursor = crHandPoint
      TabOrder = 14
    end
    object CheckBox7: TCheckBox
      Left = 280
      Top = 360
      Width = 17
      Height = 17
      Cursor = crHandPoint
      TabOrder = 15
    end
    object CheckBox8: TCheckBox
      Left = 280
      Top = 400
      Width = 17
      Height = 17
      Cursor = crHandPoint
      TabOrder = 16
    end
    object Button2: TButton
      Left = 168
      Top = 8
      Width = 129
      Height = 33
      Cursor = crHandPoint
      Caption = #1055#1086#1080#1089#1082
      TabOrder = 17
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel
    Left = 305
    Top = 0
    Width = 615
    Height = 434
    Align = alClient
    BevelOuter = bvLowered
    Color = clWindow
    TabOrder = 2
    object ListView1: TListView
      Left = 1
      Top = 1
      Width = 613
      Height = 432
      Align = alClient
      Columns = <
        item
          Caption = 'ID'
          Width = 100
        end
        item
          Caption = #1044#1072#1090#1072
        end
        item
          Caption = #1055#1072#1088#1072
          Width = 100
        end
        item
          Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1072
          Width = 100
        end
        item
          Caption = #1055#1088#1077#1087#1086#1076#1072#1074#1072#1090#1077#1083#1100
          Width = 100
        end
        item
          Caption = #1043#1088#1091#1087#1087#1072
        end
        item
          Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
          Width = 100
        end
        item
          Caption = #1058#1080#1087
          Width = 100
        end
        item
          Caption = #1050#1072#1073#1080#1085#1077#1090
          Width = 100
        end>
      Ctl3D = False
      FlatScrollBars = True
      GridLines = True
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      ViewStyle = vsReport
    end
  end
  object Combo_gruppa: TComboBox
    Left = 15
    Top = 270
    Width = 260
    Height = 21
    Style = csDropDownList
    CharCase = ecUpperCase
    Ctl3D = True
    DropDownCount = 50
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 3
  end
end
