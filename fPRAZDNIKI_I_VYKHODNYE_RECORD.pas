unit fPRAZDNIKI_I_VYKHODNYE_RECORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Dialogs, ExtCtrls, StdCtrls, ComCtrls, Clipbrd;
type
  TfPRAZDNIKI_I_VYKHODNYE_RECORDf = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;

    Label_god_id: TLabel;
    Label_polugodie_id: TLabel;
    Label_data: TLabel;

    Combo_god_id: TComboBox;
    Button_god_id: TButton;
    Combo_polugodie_id: TComboBox;
    Button_polugodie_id: TButton;
    DateTime_data: TDateTimePicker;

    Check_god_id: TCheckBox;
    Check_polugodie_id: TCheckBox;
    Check_data: TCheckBox;

    procedure FormActivated;
    procedure CLALL;
    procedure BtnOK;

    procedure CheckInput;
    procedure LoadGOD;
    procedure GetGOD(pCombo:TComboBox;pSID:integer);
    procedure Button_god_idClick(Sender: TObject);

    procedure LoadPOLUGODIE;
    procedure GetPOLUGODIE(pCombo:TComboBox;pSID:integer);
    procedure Button_polugodie_idClick(Sender: TObject);


    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormActivate(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  fPRAZDNIKI_I_VYKHODNYE_RECORDf: TfPRAZDNIKI_I_VYKHODNYE_RECORDf;

implementation

uses  
    fPRAZDNIKI_I_VYKHODNYE,
    fGOD,
    fPOLUGODIE,
    fMW;

{$R *.dfm}

procedure TfPRAZDNIKI_I_VYKHODNYE_RECORDf.GetGOD(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfPRAZDNIKI_I_VYKHODNYE_RECORDf.LoadGOD;
begin

Combo_god_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from god where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_god_id.AddItem(ADQ.FieldByName('god').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_god_id.ItemIndex:=0;

end;

procedure TfPRAZDNIKI_I_VYKHODNYE_RECORDf.Button_god_idClick(Sender: TObject);
begin

  fGODf.Caption:='����� ���';
  if Combo_god_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_god_id.Items.Objects[Combo_god_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fGODf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='PRAZDNIKI_I_VYKHODNYE';

  if pNoAction=false then begin
    LoadGOD;
    if pSelectedID<>'' then GetGOD(Combo_god_id,strtoint(pSelectedID));
  end;

end;

procedure TfPRAZDNIKI_I_VYKHODNYE_RECORDf.GetPOLUGODIE(pCombo:TComboBox;pSID:integer);
var
  j:integer;
begin

  for j:=0 to pCombo.Items.Count-1
  do if Integer(pCombo.Items.Objects[j]) = pSID
  then pCombo.ItemIndex:=j;

end;

procedure TfPRAZDNIKI_I_VYKHODNYE_RECORDf.LoadPOLUGODIE;
begin

Combo_polugodie_id.Clear;

ADQ.Close;
ADQ.SQL.Clear;
ADQ.SQL.Add('Select * from polugodie where deleted=0 order by id');
ADQ.Open;

ADQ.First;

while not ADQ.Eof do begin
  Combo_polugodie_id.AddItem(ADQ.FieldByName('polugodie').AsString, Pointer(ADQ.FieldByName('id').AsInteger));
  ADQ.Next;
end;

if ADQ.RecordCount>0 then Combo_polugodie_id.ItemIndex:=0;

end;

procedure TfPRAZDNIKI_I_VYKHODNYE_RECORDf.Button_polugodie_idClick(Sender: TObject);
begin

  fPOLUGODIEf.Caption:='����� ���������';
  if Combo_polugodie_id.Text='' then pSelectedSID:='0' else pSelectedSID:=Inttostr(Integer(Combo_polugodie_id.Items.Objects[Combo_polugodie_id.ItemIndex]));
  if pLink<>'' then begin
    pLinkVrem:=pLink;
    pLink:='';
  end;

  fPOLUGODIEf.ShowModal;


  pLink:=pLinkVrem;

  pPhoto:=false;

  pTableName:='PRAZDNIKI_I_VYKHODNYE';

  if pNoAction=false then begin
    LoadPOLUGODIE;
    if pSelectedID<>'' then GetPOLUGODIE(Combo_polugodie_id,strtoint(pSelectedID));
  end;

end;

procedure TfPRAZDNIKI_I_VYKHODNYE_RECORDf.CheckInput;
begin

if trim(Combo_god_id.Text)='' then begin
  Combo_god_id.SetFocus;
  abort;
end;

if trim(Combo_polugodie_id.Text)='' then begin
  Combo_polugodie_id.SetFocus;
  abort;
end;

end;
procedure TfPRAZDNIKI_I_VYKHODNYE_RECORDf.CLALL;
begin

  LoadGOD;
  LoadPOLUGODIE;

  Check_god_id.Checked:=false;
  Check_god_id.Visible:=false;
  Check_polugodie_id.Checked:=false;
  Check_polugodie_id.Visible:=false;
  Check_data.Checked:=false;
  Check_data.Visible:=false;

  DateTime_data.Date:=now;

  Combo_god_id.Setfocus;

end;

procedure TfPRAZDNIKI_I_VYKHODNYE_RECORDf.FormActivated;
begin

  CLALL;

if pActionPRAZDNIKI_I_VYKHODNYE='INSERT' then begin
  Self.Caption:='���������� ������';
end;

if pActionPRAZDNIKI_I_VYKHODNYE='UPDATE' then begin

  Self.Caption:='��������� ������';

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add('Select * from prazdniki_i_vykhodnye where id='+pID);
  ADQ.Open;
  ADQ.First;

  GetGOD(Combo_god_id,ADQ.fieldbyname('god_id').AsInteger);
  GetPOLUGODIE(Combo_polugodie_id,ADQ.fieldbyname('polugodie_id').AsInteger);
  DateTime_data.Date:=ADQ.fieldbyname('data').AsDateTime;

end;

if pActionPRAZDNIKI_I_VYKHODNYE='SEARCH' then begin

  Self.Caption:='����� ������';

  Check_god_id.Visible:=true;
  Check_polugodie_id.Visible:=true;
  Check_data.Visible:=true;

end;

end;

procedure TfPRAZDNIKI_I_VYKHODNYE_RECORDf.BtnOK;
begin

if pActionPRAZDNIKI_I_VYKHODNYE='INSERT' then begin

  CheckInput;

  pS:='INSERT into '+
  'prazdniki_i_vykhodnye '+
  '('+
  'god_id,'+
  'polugodie_id,'+
  'data,'+
  'deleted,'+
  'currentuser,'+
  'currentdate '+
  ')'+
  'values '+
  '('+
  ' '''+Inttostr(Integer(Combo_god_id.Items.Objects[Combo_god_id.ItemIndex]))+''', '+
  ' '''+Inttostr(Integer(Combo_polugodie_id.Items.Objects[Combo_polugodie_id.ItemIndex]))+''', '+
  ' '''+datetostr(DateTime_data.Date)+''', '+
  ' ''0'', '+
  ' '''+pMyUser+''', '+
  ' '''+datetostr(now)+' '+timetostr(now)+''' '+
  ')';

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

end;

if pActionPRAZDNIKI_I_VYKHODNYE='UPDATE' then begin


 pS:='UPDATE '+
  'prazdniki_i_vykhodnye set ' +
  'god_id='''+Inttostr(Integer(Combo_god_id.Items.Objects[Combo_god_id.ItemIndex]))+''','+
  'polugodie_id='''+Inttostr(Integer(Combo_polugodie_id.Items.Objects[Combo_polugodie_id.ItemIndex]))+''','+
  'data='''+datetostr(DateTime_data.date)+''','+
  'updateuser='''+pMyUser+''','+
  'updatedate='''+datetostr(now)+' '+timetostr(now)+''' '+
  'where id='+pID;

  //Clipboard.AsText:=pS;
  //ShowMessage(pS);

  ADQ.Close;
  ADQ.SQL.Clear;
  ADQ.SQL.Add(pS);
  ADQ.ExecSQL;

   fPRAZDNIKI_I_VYKHODNYEf.ListView1.Selected.SubItems[0]:=trim(Combo_god_id.Text);
   fPRAZDNIKI_I_VYKHODNYEf.ListView1.Selected.SubItems[1]:=trim(Combo_polugodie_id.Text);
   fPRAZDNIKI_I_VYKHODNYEf.ListView1.Selected.SubItems[2]:=datetostr(DateTime_data.date);

end;

if pActionPRAZDNIKI_I_VYKHODNYE='SEARCH' then begin

if (Check_god_id.Checked=false) and (Check_polugodie_id.Checked=false) and (Check_data.Checked=false) then abort;

   pSearch:='';

   if Check_god_id.Checked=true then pSearch:=pSearch+' and prazdniki_i_vykhodnye.god_id = '+Inttostr(Integer(Combo_god_id.Items.Objects[Combo_god_id.ItemIndex]))+' ';
   if Check_polugodie_id.Checked=true then pSearch:=pSearch+' and prazdniki_i_vykhodnye.polugodie_id = '+Inttostr(Integer(Combo_polugodie_id.Items.Objects[Combo_polugodie_id.ItemIndex]))+' ';
   if Check_data.Checked=true then pSearch:=pSearch+' and prazdniki_i_vykhodnye.data = #'+FormatDateTime('YYYY-MM-DD',DateTime_data.Date)+'#';

end;

Close;

end;

procedure TfPRAZDNIKI_I_VYKHODNYE_RECORDf.Button1Click(Sender: TObject);
begin
  pNoAction:=true;
  Close
end;

procedure TfPRAZDNIKI_I_VYKHODNYE_RECORDf.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if key=vk_escape then Button1Click(Self);
end;

procedure TfPRAZDNIKI_I_VYKHODNYE_RECORDf.FormActivate(Sender: TObject);
begin
  FormActivated;
end;

procedure TfPRAZDNIKI_I_VYKHODNYE_RECORDf.Button2Click(Sender: TObject);
begin
  BtnOK;
end;

end.