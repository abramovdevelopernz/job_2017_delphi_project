object fRASPISANIE_RECORDf: TfRASPISANIE_RECORDf
  Left = 335
  Top = 261
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  ClientHeight = 220
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 172
    Width = 472
    Height = 48
    Align = alBottom
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 33
      Cursor = crHandPoint
      Caption = 'OK'
      TabOrder = 0
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 472
    Height = 172
    Align = alClient
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label_para_id: TLabel
      Left = 112
      Top = 16
      Width = 26
      Height = 13
      Caption = #1055#1072#1088#1072
    end
    object Label_DISTSIPLINA: TLabel
      Left = 75
      Top = 40
      Width = 63
      Height = 13
      Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1072
    end
    object Label_prepodavatelmz_id: TLabel
      Left = 59
      Top = 64
      Width = 79
      Height = 13
      Caption = #1055#1088#1077#1087#1086#1076#1072#1074#1072#1090#1077#1083#1100
    end
    object Label_kabinet_id: TLabel
      Left = 96
      Top = 88
      Width = 42
      Height = 13
      Caption = #1050#1072#1073#1080#1085#1077#1090
    end
    object Label_tip_zanyatiya_id: TLabel
      Left = 75
      Top = 112
      Width = 63
      Height = 13
      Caption = #1058#1080#1087' '#1079#1072#1085#1103#1090#1080#1103
    end
    object Label_data: TLabel
      Left = 112
      Top = 136
      Width = 26
      Height = 13
      Caption = #1044#1072#1090#1072
    end
    object Check_para_id: TCheckBox
      Left = 448
      Top = 16
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 0
    end
    object Check_DISTSIPLINA: TCheckBox
      Left = 448
      Top = 40
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 1
    end
    object Check_prepodavatelmz_id: TCheckBox
      Left = 448
      Top = 64
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 2
    end
    object Check_kabinet_id: TCheckBox
      Left = 448
      Top = 88
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 3
    end
    object Check_tip_zanyatiya_id: TCheckBox
      Left = 448
      Top = 112
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 4
    end
    object Check_data: TCheckBox
      Left = 448
      Top = 136
      Width = 17
      Height = 17
      Cursor = crHandPoint
      Ctl3D = True
      DragCursor = crDefault
      ParentCtl3D = False
      TabOrder = 6
    end
    object Combo_para_id: TComboBox
      Left = 144
      Top = 16
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 5
    end
    object Button_para_id: TButton
      Left = 416
      Top = 16
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 7
      OnClick = Button_para_idClick
    end
    object Combo_DISTSIPLINA: TComboBox
      Left = 144
      Top = 40
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 8
    end
    object Button_DISTSIPLINA: TButton
      Left = 416
      Top = 40
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 9
      OnClick = Button_DISTSIPLINAClick
    end
    object Combo_prepodavatelmz_id: TComboBox
      Left = 144
      Top = 64
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 10
    end
    object Button_prepodavatelmz_id: TButton
      Left = 416
      Top = 64
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 11
      OnClick = Button_prepodavatelmz_idClick
    end
    object Combo_kabinet_id: TComboBox
      Left = 144
      Top = 88
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 12
    end
    object Button_kabinet_id: TButton
      Left = 416
      Top = 88
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 13
      OnClick = Button_kabinet_idClick
    end
    object Combo_tip_zanyatiya_id: TComboBox
      Left = 144
      Top = 112
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 14
    end
    object Button_tip_zanyatiya_id: TButton
      Left = 416
      Top = 112
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Caption = '...'
      TabOrder = 15
      OnClick = Button_tip_zanyatiya_idClick
    end
    object DateTime_data: TDateTimePicker
      Left = 144
      Top = 136
      Width = 297
      Height = 21
      Date = 40887.604251655100000000
      Time = 40887.604251655100000000
      TabOrder = 16
    end
  end
end
